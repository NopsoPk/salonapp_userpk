import { NgModule } from '@angular/core';
import { CalendarComponent } from './calendar/calendar';
import { ExpandableComponent } from './expandable/expandable';
import { ExpandableHeader } from './expandable-header/expandable-header';
import { SalonCardComponent } from './salon-card/salon-card';
@NgModule({
    declarations: [
        // CalendarComponent,
        // ExpandableComponent,
        // ExpandableHeader,
        // SalonCardComponent
    ],
    imports: [],
    exports: [
        // CalendarComponent,
        // ExpandableComponent,
        // ExpandableHeader,
        // SalonCardComponent
    ]
})
export class ComponentsModule { }
