import { SHIPPING_ADDRESS_DATA } from '../SalonAppUser-Interface';
import { Platform } from 'ionic-angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { GlobalProvider } from '../global/global';
import { BehaviorSubject } from "rxjs";
import { Storage } from "@ionic/storage";


@Injectable()
export class ShippingAddressModal {

  /* Global Vairables */
  database: SQLiteObject
  private databaseReady: BehaviorSubject<boolean>;


  constructor(
    public http: HttpClient,
    private sqlite: SQLite,
    public sQLitePorter: SQLitePorter,
    public storage: Storage,
    public platform: Platform,
    public serviceManager: GlobalProvider, ) {
    console.log('Hello ShippingAddressModal');
    this.databaseReady = new BehaviorSubject(false)
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;
          console.log('database is setup');
          this.databaseReady.next(true)

        }, err => {
          console.log('database is nullll');

        })
    });
  }

  getDatabaseState() {
    return this.databaseReady.asObservable()
  }

  createShippingAddressTable() {

    let createTableQuery = "CREATE TABLE IF NOT EXISTS  `ShippingAddress` (`o_id` int(11),`o_address` text ,`o_city` varchar(250) ,`o_province` varchar(250) ,`o_completeAddress` text, `isdefaultaddress` tinyint(1) ,`o_contact_person` text ,`o_phone` text ) "
    let isTableCreated = false
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        isTableCreated = true
        console.log('ShippingAddresss table created');

        return isTableCreated
      })
      .catch(e => {
        let msg: string = e.message

        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }


        console.log('error: InsertInToShippingAddressTable ' + e.message);
        let errorMessage: string = e.message
        if (errorMessage.trim().toLowerCase() === 'table ShippingAddresss already exists'.trim().toLowerCase()) {
          isTableCreated = true
          return isTableCreated
        } else {
          isTableCreated = false
          return isTableCreated
        }
      });
  }

  InsertInToShippingAddressTable(ShippingAddressData: SHIPPING_ADDRESS_DATA[]) {
    if (!ShippingAddressData || ShippingAddressData.length === 0) {
      return
    }
    console.log('insertionTest: ' + ShippingAddressData.length + ' ShippingAddress were sent for dumping');

    let firstPart = 'INSERT INTO `ShippingAddress` (`o_id`,`o_address`, `o_city`, `o_province`, `o_completeAddress`, `isdefaultaddress`, `o_contact_person`, `o_phone` ) VALUES';
    let secondPart = ""

    let completShippingAddress = ""


    return this.database.executeSql('select max(`o_id`) as maxID from `ShippingAddress` ', []).then(res => {
      let maxID = 0

      if (res.rows.item(0).maxID) maxID = res.rows.item(0).maxID

      ShippingAddressData.forEach((sa, index) => {
        maxID += 1
        if (maxID === 1) sa.isdefaultaddress = true
        secondPart += '("' + maxID + '","' + sa.o_address + '", "' + sa.o_city + '", "' + sa.o_province + '", "' + sa.o_completeAddress + '" , "' + Number(sa.isdefaultaddress) + '", "' + sa.o_contact_person + '", "' + sa.o_phone + '"  ),';
        completShippingAddress += sa.o_completeAddress + ','
      });

      let finalQuery = firstPart + secondPart
      let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)


      completShippingAddress = completShippingAddress.slice(0, completShippingAddress.length - 1)
      // console.log(slicedQuery);

      
      let isDataInserted = false
      return this.database.executeSql(slicedQuery, [])
        .then(res => {
          console.log('insertionTest: ' + res.rowsAffected + ' ShippingAddresss were dumped');
          res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false
          return isDataInserted
        })
        .catch(e => {

          this.serviceManager.sendErrorToServer(e.message, slicedQuery)
          isDataInserted = false
          return e.message
        });
    }).catch(e => {

    })


  }
  getMaxID() {


    let Inserquery = 'select max(o_id) as maxID from `ShippingAddress` '// + completeAddress//(' + btc_id + ');'
    let beautyTips = []


    return this.database.executeSql(Inserquery, [])
      .then(tipsDetailDataSource => {
        return (tipsDetailDataSource.rows.item(0))

      })
      .catch(e => {

        this.serviceManager.sendErrorToServer(e.message, Inserquery)
        console.log('error:  tipsDetailDataSource ' + e.message);

        return beautyTips

      });
  }
  async getShippingAddress() {

    let Inserquery = 'select o_completeAddress, o_id, o_address,o_city, o_province, isdefaultaddress, o_contact_person, o_phone  from `ShippingAddress` group by o_completeAddress'
    let beautyTips = []


    try {
      const sa = await this.database.executeSql(Inserquery, []);
      for (var i = 0; i < sa.rows.length; i++) {
        beautyTips.push(sa.rows.item(i));
        // console.log(JSON.stringify(sa.rows.item(i)));

      }
      return beautyTips;
    }
    catch (e) {
      ;
      this.serviceManager.sendErrorToServer(e.message, Inserquery);
      console.log('error:  tipsDetailDataSource ' + e.message);
      return beautyTips;
    }
  }

  async getDefaultShippingAddress() {


    let Inserquery = 'select * from `ShippingAddress` where  `isdefaultaddress` = 1  limit 1'
    let shippingAddress: SHIPPING_ADDRESS_DATA

    return this.database.executeSql(Inserquery, []).then(res => {
      if (res.rows.length > 0) {
        shippingAddress = res.rows.item(0)
      }
      return shippingAddress


    }).catch(e => {

      this.serviceManager.sendErrorToServer(e.message, Inserquery);
      console.log('error:  tipsDetailDataSource ' + e.message);
    })

  }


  TruncateShippingAddressTable() {
    let deleteQuery = "DELETE FROM `ShippingAddress` ;"
    let isDeleted = false

    return this.database.executeSql(deleteQuery, [])
      .then(res => {


        isDeleted = true
        return isDeleted
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteQuery)
        console.log('error: ShippingAddresss');

        isDeleted = false
        return e.message

      });
  }

  delteDuplicateShippingAddress(completeAddress) {

    if (!completeAddress) {
      return
    }
    let deleteSelectedQuery = 'DELETE FROM `ShippingAddress`  where o_completeAddress IN (' + completeAddress + ');'
    return this.database.executeSql(deleteSelectedQuery, [])
      .then(res => {
        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery)
        return false;
      });
  }
  deleteAddress(o_completeAddress) {

    if (!o_completeAddress) {
      return
    }
    let deleteSelectedQuery = 'DELETE FROM `ShippingAddress`  where `o_completeAddress` =  "' + o_completeAddress + '" '
    console.log(deleteSelectedQuery);
    
    return this.database.executeSql(deleteSelectedQuery, [])
      .then(res => {

        return true;
      })
      .catch(e => {

        this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery)
        return false;
      });
  }


  removeCartItemsAfterOrderPlaced() {
    let updateQuery = "UPDATE `ShippingAddress` SET `isdefaultaddress` =  0"
    console.log(updateQuery);

    return this.database.executeSql(updateQuery, [])
      .then(res => {
        console.log('success: customer updated');
        return true
      })
      .catch(e => {

        this.serviceManager.sendErrorToServer(e.message, updateQuery)
        console.log('error: ' + e.message);
        return false
      });
  }
  async updateShippingAddress(sa: SHIPPING_ADDRESS_DATA) {

    let updateQuery = 'UPDATE `ShippingAddress` SET `isdefaultaddress` =  ' + Number(sa.isdefaultaddress) + ', o_address =  "' + sa.o_address + '", o_city = "' + sa.o_city + '", `o_province` =  "' + sa.o_province + '", `o_completeAddress` = "' + sa.o_completeAddress + '", `o_contact_person` = "' + sa.o_contact_person + '", `o_phone` = "' + sa.o_phone + '" where `o_id` = ' + sa.o_id
    console.log(updateQuery);
    
    try {
      const res = await this.database.executeSql(updateQuery, []);
      if (sa.isdefaultaddress)
        return this.database.executeSql('UPDATE `ShippingAddress` SET `isdefaultaddress` = 0 where `o_id` != ' + sa.o_id, []);
      console.log('success: customer updated');
      return true;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, updateQuery);
      console.log('error: ' + e.message);
      return false;
    }
  }
  makeAddressAsDefault(objAddress: SHIPPING_ADDRESS_DATA) {
    let updateQuery = "UPDATE `ShippingAddress` SET `isdefaultaddress` =  1 where o_id =  " + objAddress.o_id
    console.log(updateQuery);

    return this.database.executeSql("UPDATE `ShippingAddress` SET `isdefaultaddress` =  0 ", []).then(() => {
      return this.database.executeSql(updateQuery, [])
        .then(res => {
          console.log('success: customer updated');
          return true
        })
        .catch(e => {

          this.serviceManager.sendErrorToServer(e.message, updateQuery)
          console.log('error: ' + e.message);
          return false
        });
    })

  }

}
