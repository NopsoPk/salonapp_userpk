import { PRODUCT_CATEGORY } from '../SalonAppUser-Interface';
import { Platform } from 'ionic-angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { GlobalProvider } from '../global/global';
import { BehaviorSubject } from "rxjs";
import { Storage } from "@ionic/storage";

@Injectable()
export class ProductCategoryModal {

  
  /* Global Vairables */
  database: SQLiteObject
  private databaseReady: BehaviorSubject<boolean>;


  constructor(
    public http: HttpClient,
    private sqlite: SQLite,
    public sQLitePorter: SQLitePorter,
    public storage: Storage,
    public platform: Platform,
    public serviceManager: GlobalProvider, ) {
    this.databaseReady = new BehaviorSubject(false)
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;
          console.log('database is setup');
          this.databaseReady.next(true)

        }, err => {
          console.log('database is nullll');

        })
    });
  }

  getDatabaseState() {
    return this.databaseReady.asObservable()
  }

  createProductCategoryTable() {
    console.log('came in ProductCategory Modal');


    let createTableQuery = 'CREATE TABLE IF NOT EXISTS  `product_categories` (`pc_id` int(11) ,`pc_name` varchar(250), `pc_description` text, `pc_image` varchar(250), `pc_status` int(11), `pc_created_at` text ,`pc_modified_date` text)  '
    let isTableCreated = false
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        isTableCreated = true
        
        
        console.log('ProductCategory table created');
        
        return res
      })
      .catch(e => {
        
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }


        console.log('error: InsertInToProductCategoryTable ' + e.message);
        let errorMessage: string = e.message
        if (errorMessage.trim().toLowerCase() === 'table ProductCategory already exists'.trim().toLowerCase()) {
          isTableCreated = true
          return isTableCreated
        } else {
          isTableCreated = false
          return isTableCreated
        }
      });
  }

  async delteDuplicateCategories(ProductCatIds) {
    if (!ProductCatIds) {
      return
    }
    let deleteSelectedQuery = 'DELETE FROM `product_categories`  where pc_id IN (' + ProductCatIds + ');'
    try {
      const res = await this.database.executeSql(deleteSelectedQuery, []);

      return true;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery);
      return false;
    }
  }

  InsertInToProductCategoryTable(ProductCategoryData: PRODUCT_CATEGORY[]) {
    if (!ProductCategoryData || ProductCategoryData.length === 0) {
      return
    }


    let firstPart = 'INSERT INTO `product_categories` (`pc_id`, `pc_name`, `pc_description`, `pc_image`, `pc_status`, `pc_created_at`, `pc_modified_date`) VALUES';
    let secondPart = ""
    
    let ProductCategoryIds = ""

    console.log('insertionTest: ' + ProductCategoryData.length + ' product_categories were sent for dumping');

    ProductCategoryData.forEach(PC => {
      secondPart += '("' + PC.pc_id + '", "' + PC.pc_name + '", "' + PC.pc_description + '", "' + PC.pc_image + '" , "' + PC.pc_status + '", "' + PC.pc_created_at + '" , "' + PC.pc_modified_date + '" ),';
      ProductCategoryIds += PC.pc_id +','
    });

    let finalQuery = firstPart + secondPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    ProductCategoryIds = ProductCategoryIds.slice(0, ProductCategoryIds.length - 1)
    


    return this.delteDuplicateProductCategory(ProductCategoryIds).then(() => {
      let isDataInserted = false
      return this.database.executeSql(slicedQuery, [])
        .then(res => {
          console.log('insertionTest: ' + res.rowsAffected + ' product_categories were dumped');
          res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false
          return isDataInserted
        })
        .catch(e => {
          this.serviceManager.sendErrorToServer(e.message, slicedQuery)
          isDataInserted = false
          return e.message
        });

    }).catch(e => {
      this.serviceManager.sendErrorToServer(e.message, slicedQuery)
      return e.message
    });
  }

  getProductCategory() {
    let Inserquery = "select * from `product_categories` "
    let ProductCategory = []
    return this.database.executeSql(Inserquery, [])
      .then(ProductCategoryDataSource => {
        for (var i = 0; i < ProductCategoryDataSource.rows.length; i++) {
          ProductCategory.push(ProductCategoryDataSource.rows.item(i))
        }
        return ProductCategory
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Inserquery)
        console.log('error:  ProductCategoryDataSource ' + e.message);

        return ProductCategory

      });
  }

  TruncateProductCategoryTable() {
    let deleteQuery = "DELETE FROM `product_categories` ;"
    let isDeleted = false

    return this.database.executeSql(deleteQuery, [])
      .then(res => {
        

        isDeleted = true
        return isDeleted
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteQuery)
        console.log('error: product_categories');

        isDeleted = false
        return e.message

      });
  }

  delteDuplicateProductCategory(ProductCategoryIds) {

    if (!ProductCategoryIds) {
      return
    }
    let deleteSelectedQuery = 'DELETE FROM `product_categories`  where pc_id IN (' + ProductCategoryIds + ');'
    return this.database.executeSql(deleteSelectedQuery, [])
      .then(res => {
        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery)
        return false;
      });
  }

}
