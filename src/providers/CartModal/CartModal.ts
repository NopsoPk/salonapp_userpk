import { PRODUCT } from '../SalonAppUser-Interface';
import { Platform } from 'ionic-angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { GlobalProvider } from '../global/global';
import { BehaviorSubject } from "rxjs";
import { Storage } from "@ionic/storage";


@Injectable()
export class CartModal {

  /* Global Vairables */
  database: SQLiteObject
  private databaseReady: BehaviorSubject<boolean>;


  constructor(
    public http: HttpClient,
    private sqlite: SQLite,
    public sQLitePorter: SQLitePorter,
    public storage: Storage,
    public platform: Platform,
    public serviceManager: GlobalProvider, ) {
    this.databaseReady = new BehaviorSubject(false)
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;
          console.log('database is setup');
          this.databaseReady.next(true)

        }, err => {
          console.log('database is nullll');

        })
    });
  }

  getDatabaseState() {
    return this.databaseReady.asObservable()
  }


  async createCartTable() {

    let createTableQuery = "CREATE TABLE IF NOT EXISTS  `carts` (`p_id` int(11) ,`p_name` varchar(250) ,`p_description` text ,`p_image` varchar(250) ,`p_status` int(11) ,`p_cost` float ,`p_rrp` float ,`p_price` float ,`p_weight` varchar(100) ,`p_dimention` varchar(250) ,`pc_id` int(11) ,`pb_id` int(11) ,`p_created_at` text ,`p_modified_date` text, `isAddededtoCart` tinyint(1), `p_quantity` tinyint(2), `isChecked` tinyint(1), `pb_name` text) "
    let isTableCreated = false
    try {
      const res = await this.database.executeSql(createTableQuery, []);
      isTableCreated = true;
      console.log('carts table created');
      return res;
    }
    catch (e) {
      let msg: string = e.message;
      if (!msg.includes('already exists')) {
        this.serviceManager.sendErrorToServer(e.message, createTableQuery);
      }
      console.log('error: InsertInToCartTable ' + e.message);
      let errorMessage: string = e.message;
      if (errorMessage.trim().toLowerCase() === 'table carts already exists'.trim().toLowerCase()) {
        isTableCreated = true;
        return isTableCreated;
      }
      else {
        isTableCreated = false;
        return isTableCreated;
      }
    }
  }

  async InsertInToCartTable(product: PRODUCT) {
    if (!product) {
      return
    }

    let firstPart = 'INSERT INTO `carts` (`p_id`, `p_name`, `p_description`, `p_image`, `p_status`, `p_cost`, `p_rrp`, `p_price`, `p_weight`, `p_dimention`, `pc_id`, `pb_id`, `p_created_at`, `p_modified_date`,`isAddededtoCart`,`p_quantity`, `isChecked`, `pb_name` ) VALUES';
    let secondPart = ""

    let ProductIds = ""




    secondPart += '("' + product.p_id + '", "' + product.p_name + '", "' + product.p_description + '", "' + product.p_image + '" , "' + product.p_status + '", "' + Math.floor(product.p_cost) + '" , "' + Math.floor(product.p_rrp) + '", "' + Math.floor(product.p_price) + '", "' + product.p_weight + '", "' + product.p_dimention + '", "' + product.pc_id + '", "' + product.pb_id + '", "' + product.p_created_at + '", "' + product.p_modified_date + '", "' + 1 + '", "' + 1 + '", "' + 1 + '", "' + product.pb_name + '")';
    ProductIds += product.p_id


    let finalQuery = firstPart + secondPart

    // let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    // ProductIds = ProductIds.slice(0, ProductIds.length - 1)

    try {
      await this.deleteCartItem(ProductIds);
      let isDataInserted = false;
      try {
        const res = await this.database.executeSql(finalQuery, []);
        console.log('insertionTest: ' + res.rowsAffected + ' carts were dumped');
        res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false;
        return isDataInserted;
      }
      catch (e) {
        this.serviceManager.sendErrorToServer(e.message, finalQuery);
        isDataInserted = false;
        return e.message;
      }
    }
    catch (e_1) {
      this.serviceManager.sendErrorToServer(e_1.message, finalQuery);
      return e_1.message;
    }
  }
  getProducts(btc_id) {

    let Inserquery = 'select * from `carts` where  pc_id = ' + btc_id//(' + btc_id + ');'
    let beautyTips = []


    return this.database.executeSql(Inserquery, [])
      .then(tipsDetailDataSource => {
        for (var i = 0; i < tipsDetailDataSource.rows.length; i++) {
          beautyTips.push(tipsDetailDataSource.rows.item(i))
        }
        return beautyTips
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Inserquery)
        console.log('error:  tipsDetailDataSource ' + e.message);

        return beautyTips

      });
  }

  async getCartItems() {

    let Inserquery = 'select * from `carts`'
    let beautyTips = []



    return await this.database.executeSql(Inserquery, []).then(tipsDetailDataSource => {
      for (var i = 0; i < tipsDetailDataSource.rows.length; i++) {
        beautyTips.push(tipsDetailDataSource.rows.item(i));
      }
      return beautyTips;

    }).catch(e => {

      this.serviceManager.sendErrorToServer(e.message, Inserquery);
      console.log('error:  tipsDetailDataSource ' + e.message);

      return beautyTips;
    })




  }


  async isThisProductTobeAddedInOrder(product: PRODUCT) {
    let updateQuery = ''
    let val = Number(product.isChecked)
    updateQuery = "UPDATE `carts` SET `isChecked` =  " + val + " where p_id = " + product.p_id

    try {
      await this.database.executeSql(updateQuery, []).then(res => {

      }).catch(e => {

        this.serviceManager.sendErrorToServer(e.message, updateQuery);
        console.log('error: ' + e.message);
      });
      console.log('success: Cart updated');
      return true;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, updateQuery);
      console.log('error: ' + e.message);
      return false;
    }
  }
  async updateProductQuantity(product: PRODUCT) {
    let updateQuery = ''

    updateQuery = "UPDATE `carts` SET `p_quantity` = " + Number(product.p_quantity) + "  where p_id = " + product.p_id

    console.log(updateQuery);

    try {
      await this.database.executeSql(updateQuery, []).then(res => {

      }).catch(e => {

        this.serviceManager.sendErrorToServer(e.message, updateQuery);
        console.log('error: ' + e.message);
      });
      console.log('success: Cart updated');
      return true;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, updateQuery);
      console.log('error: ' + e.message);
      return false;
    }
  }
  async updateCartTable(product: PRODUCT) {
    let updateQuery = ''

    updateQuery = "UPDATE `carts` SET `isAddededtoCart` = " + Number(product.isAddedtoCart) + " , `isChecked` = " + Number(product.isAddedtoCart) + " ,`p_quantity` = " + Number(product.p_quantity) + "  where p_id = " + product.p_id

    console.log(updateQuery);

    try {
      await this.database.executeSql(updateQuery, []).then(res => {

      }).catch(e => {

        this.serviceManager.sendErrorToServer(e.message, updateQuery);
        console.log('error: ' + e.message);
      });
      console.log('success: Cart updated');
      return true;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, updateQuery);
      console.log('error: ' + e.message);
      return false;
    }
  }

  async TruncateCartTable() {
    let deleteQuery = "DELETE FROM `carts` ;"
    let isDeleted = false

    try {
      const res = await this.database.executeSql(deleteQuery, []);
      isDeleted = true;
      return isDeleted;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, deleteQuery);
      console.log('error: products');
      isDeleted = false;
      return e.message;
    }
  }
  async removeProductFromCart(product: PRODUCT) {
    let deleteQuery = "DELETE FROM `carts` where p_id = " + product.p_id
    let isDeleted = false

    try {
      const res = await this.database.executeSql(deleteQuery, []);

      isDeleted = true;
      return isDeleted;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, deleteQuery);
      console.log('error: products');
      isDeleted = false;
      return e.message;
    }
  }

  async deleteCartItem(ProductIds) {

    if (!ProductIds) {
      return
    }
    let deleteSelectedQuery = 'DELETE FROM `carts`  where p_id = ' + ProductIds
    try {
      const res = await this.database.executeSql(deleteSelectedQuery, []);
      return true;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery);
      return false;
    }
  }
  async deleteDuplicateCartItems(ProductIds: string) {

    if (!ProductIds || ProductIds.trim().length === 0) {
      return
    }
    let deleteSelectedQuery = 'DELETE FROM `carts`   p_id IN (' + ProductIds + ');'
    try {
      const res = await this.database.executeSql(deleteSelectedQuery, []);
      return res.rowsAffected;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery);
      return false;
    }
  }
  async delteItemWhoseOrderIsPlaced(ProductIds) {

    if (!ProductIds) {
      return
    }
    let deleteSelectedQuery = 'DELETE FROM `carts`  where p_id IN (' + ProductIds + ')'
    try {
      const res = await this.database.executeSql(deleteSelectedQuery, []);
      return true;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery);
      return false;
    }
  }

  async removeCartItemsAfterOrderPlaced() {
    let updateQuery = "UPDATE `carts` SET `isAddededtoCart` =  0"


    try {
      const res = await this.database.executeSql(updateQuery, []);
      console.log('success: customer updated');
      return true;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, updateQuery);
      console.log('error: ' + e.message);
      return false;
    }
  }
}
