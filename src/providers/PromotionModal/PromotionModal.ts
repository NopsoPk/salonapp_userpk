import { PRODUCT, PROMOTIONS } from '../SalonAppUser-Interface';
import { Platform } from 'ionic-angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { GlobalProvider } from '../global/global';
import { BehaviorSubject } from "rxjs";
import { Storage } from "@ionic/storage";



@Injectable()
export class PromotionModal {
  /* Global Vairables */
  database: SQLiteObject
  private databaseReady: BehaviorSubject<boolean>;


  constructor(
    public http: HttpClient,
    private sqlite: SQLite,
    public sQLitePorter: SQLitePorter,
    public storage: Storage,
    public platform: Platform,
    public serviceManager: GlobalProvider, ) {
    this.databaseReady = new BehaviorSubject(false)
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;
          console.log('database is setup');
          this.databaseReady.next(true)

        }, err => {
          console.log('database is nullll');

        })
    });
  }

  getDatabaseState() {
    return this.databaseReady.asObservable()
  }

  async createPromotionTable() {

    let createTableQuery = "CREATE TABLE `promo_codes` ( `pc_id` int(11) NOT NULL, `pc_title` varchar(32) NOT NULL, `pc_code` varchar(16) NOT NULL, `pc_max_value` int(5) NOT NULL, `pc_percent` smallint(3) NOT NULL, `pc_expiry` datetime NOT NULL, `pc_total` int(6) NOT NULL,`pc_used` int(6) NOT NULL, `pc_balance` int(6) NOT NULL, `cust_id` int(11) DEFAULT NULL)"
    let isTableCreated = false
    try {
      const res = await this.database.executeSql(createTableQuery, []);
      isTableCreated = true;
      console.log('promotions table created');
      return res;
    }
    catch (e) {
      let msg: string = e.message;
      
      if (!msg.includes('already exists')) {
        this.serviceManager.sendErrorToServer(e.message, createTableQuery);
      }
      console.log('error: InsertInToPromotionTable ' + e.message);
      let errorMessage: string = e.message;
      if (errorMessage.trim().toLowerCase() === 'table promotions already exists'.trim().toLowerCase()) {
        isTableCreated = true;
        return isTableCreated;
      }
      else {
        isTableCreated = false;
        return isTableCreated;
      }
    }
  }

  async InsertInToPromotionTable(PromotionData: PROMOTIONS[]) {
    if (!PromotionData || PromotionData.length === 0) {
      return
    }

    let firstPart = 'INSERT INTO `promo_codes` (`pc_id`, `pc_title`, `pc_code`, `pc_max_value`, `pc_percent`, `pc_expiry`, `pc_total`, `pc_used`, `pc_balance`, `cust_id`) VALUES';
    let secondPart = ""

    let PromoCodeIDs = ""
    let zeroValue = 0
    console.log('insertionTest: ' + PromotionData.length + ' Promotions were sent for dumping');

    PromotionData.forEach(promotion => {
      secondPart += '("' + promotion.pc_id + '", "' + promotion.pc_title + '", "' + promotion.pc_code + '", "' + promotion.pc_max_value + '" , "' + promotion.pc_percent + '", "' + promotion.pc_expiry + '" , "' + promotion.pc_total + '", "' + promotion.pc_used + '", "' + promotion.pc_balance + '", "' + promotion.cust_id + '"),';
      PromoCodeIDs += promotion.pc_id + ','
    });

    let finalQuery = firstPart + secondPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    // console.log(slicedQuery);

    PromoCodeIDs = PromoCodeIDs.slice(0, PromoCodeIDs.length - 1)



    try {
      await this.delteDuplicatePromotion(PromoCodeIDs);
      let isDataInserted = false;
      try {
        const res = await this.database.executeSql(slicedQuery, []);
        console.log('insertionTest: ' + res.rowsAffected + ' promotions were dumped');
        res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false;
        return isDataInserted;
      }
      catch (e) {
        
        this.serviceManager.sendErrorToServer(e.message, slicedQuery);
        isDataInserted = false;
        return e.message;
      }
    }
    catch (e_1) {

      this.serviceManager.sendErrorToServer(e_1.message, slicedQuery);
      return e_1.message;
    }
  }


  async delteDuplicatePromotion(PromoCodeIDs: string) {

    if (!PromoCodeIDs) {
      return
    }
    let deleteSelectedQuery = 'DELETE FROM `promo_codes`  where pc_id IN (' + PromoCodeIDs + ');'
    try {
      const res = await this.database.executeSql(deleteSelectedQuery, []);

      return true;
    }
    catch (e) {
      
      this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery);
      return false;
    }
  }

  async getPromotions() {
    let Inserquery = "select * from `promo_codes`"
    let promotionArray = []
    try {
      const promotions = await this.database.executeSql(Inserquery, []);
      for (var i = 0; i < promotions.rows.length; i++) {
        promotionArray.push(promotions.rows.item(i));
      }
      return promotionArray;
    }
    catch (e) {
      
      this.serviceManager.sendErrorToServer(e.message, Inserquery);
      console.log('error:  Promotions ' + e.message);
      return promotionArray;
    }
  }
}
