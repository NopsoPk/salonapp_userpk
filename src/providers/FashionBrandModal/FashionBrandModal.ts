import { Platform } from 'ionic-angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Allfavorite } from './../SalonAppUser-Interface';
import {  FASHION_BRANDS, FASHION_BRANDS_COLLECTION, FASHION_COLLECTION, FBC_IMAGES, FBC_DATES, FB_IMAGES } from './../../providers/SalonAppUser-Interface';
import { GlobalProvider } from './../../providers/global/global';
import { BehaviorSubject } from "rxjs/Rx";
import { Storage } from "@ionic/storage";

@Injectable()
export class FashionBrandModal {
  database: SQLiteObject
  private databaseReady: BehaviorSubject<boolean>;
  constructor(
    public http: HttpClient,
    private sqlite: SQLite,
    public sQLitePorter: SQLitePorter,
    public storage: Storage,
    public platform: Platform,
    public serviceManager: GlobalProvider,
  ) {
    this.databaseReady = new BehaviorSubject(false)
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;

          this.databaseReady.next(true)

        }, err => {
          console.log('database is nullll');

        })
    });
  }
  getDatabaseState() {
    return this.databaseReady.asObservable()
  }
  createFash_BrandsTable() {
    let createTableQuery = "CREATE TABLE IF NOT EXISTS `fash_brands` (`fb_featured` int(4),  `fb_id` int(4), `fb_name` varchar(64) ,`fb_status` tinyint(1)  DEFAULT '1',`fb_image` varchar(255) , `fb_gender` tinyint(1) )"
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
       // alert('FashionBrandTable created')
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }
        console.log('Error while creating table fash_brands', e.message)
      });
  }
  
  
  
 
  
  //for dates collection
  createFashion_Dates_Table() {
    let createTableQuery = "CREATE TABLE IF NOT EXISTS `fash_dates` (`fash_dates_id` int(4),   `fbc_genders` tinyint(1)  DEFAULT '1' , `fbc_date` varchar(255) , `fbc_type` varchar(255)  )"
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        console.log('created table fash_date')
       // alert('FashionBrandTable created')
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }
        console.log('Error while creating table fash_date', e.message)
      });
  }
  getFBC_ImagMaxDate(Fc_idFb_id, fbc_genders, type) {
    let dateMax = "";
    //let Query = 'SELECT  fbci_datetime   FROM fash_dates '
    let Query=  'SELECT  fbc_date FROM fash_dates where fash_dates_id= ' + Fc_idFb_id + ' and fbc_genders= "' + fbc_genders + '" and fbc_type = "' + type + '"  limit 1 '
    
    return this.database.executeSql(Query, [])
      .then(style => {

        if (style.rows.length > 0) {
        console.log('DataFound')
          for (var i = 0; i < style.rows.length; i++) {
            dateMax = style.rows.item(i).fbc_date;

          }
          return dateMax;
        } else {
          return dateMax;
        }
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log('error:  ' + e.message);
        return dateMax

      });
  }
  saveIntoFashDatesTable(dateObj, gender) {
    if (!dateObj ) {
      return 
    }
    console.log('TryingToInsertDates')
  //  let createTableQuery = "CREATE TABLE IF NOT EXISTS `fash_brands` (`fb_featured` int(4),  `fb_id` int(4), `fb_name` varchar(64) ,`fb_status` tinyint(1)  DEFAULT '1',`fb_image` varchar(255) , `fb_gender` tinyint(1) )"
    let fieledsPart = "INSERT INTO `fash_dates` (`fash_dates_id`,  `fbc_genders`, `fbc_date` , `fbc_type` ) VALUES ";
    let ValuesPart = ""
   // let scIDs = ""
   let fc_IDS=""
   ValuesPart += '("' + dateObj.fash_dates_id + '", "'  + dateObj.fbc_genders + '", "'  +dateObj.fbc_date + '", "'   + dateObj.fbc_type + '"),';
   fc_IDS += dateObj.fash_dates_id + ','

    let finalQuery = fieledsPart + ValuesPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    fc_IDS = fc_IDS.slice(0, fc_IDS.length - 1)
    return this.deleteMultipleFashDates(fc_IDS, gender).then(isDeleted => {
      if (isDeleted) {
        return this.database.executeSql(slicedQuery, []).then(res => {
          // alert('insertionTest: ' + res.rowsAffected + ' fash_collections were dumped')
          console.log('insertionTest: ' + res.rowsAffected + ' fash_dates were dumped');
        })
          .catch(e => {
            this.serviceManager.sendErrorToServer(e.message, slicedQuery)
            // alert('eror: while inserting into fash_collections' + e.message)
            console.log('eror: while inserting into fash_dates' + e.message)
          });
      }
    })
  }
  deleteMultipleFashDates(fash_dates_id, gender) {

    let deleteCategorires = 'DELETE FROM fash_dates where fash_dates_id IN (' + fash_dates_id + ') and fbc_genders= "' + gender + '"';
    return this.database.executeSql(deleteCategorires, [])
      .then(res => {

        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteCategorires)
        return false;
      });
  }
  //for brands dates table
  createFashion_Dates_Bran_Table() {
    let createTableQuery = "CREATE TABLE IF NOT EXISTS `fash_dates_b` (`fash_dates_id` int(4),   `fbc_genders` tinyint(1)  DEFAULT '1' , `fbc_date` varchar(255) , `fbc_type` varchar(255)  )"
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        console.log('created table fash_date')
       // alert('FashionBrandTable created')
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }
        console.log('Error while creating table fash_date', e.message)
      });
  }
  getFB_ImagMaxDate(Fc_idFb_id, fbc_genders, type) {
    let dateMax = "";
    //let Query = 'SELECT  fbci_datetime   FROM fash_dates '
    let Query=  'SELECT  fbc_date FROM fash_dates_b where fash_dates_id= ' + Fc_idFb_id + ' and fbc_genders= "' + fbc_genders + '" and fbc_type = "' + type + '"  limit 1 '
    
    return this.database.executeSql(Query, [])
      .then(style => {

        if (style.rows.length > 0) {

          for (var i = 0; i < style.rows.length; i++) {
            dateMax = style.rows.item(i).fbc_date;

          }
          return dateMax;
        } else {
          return dateMax;
        }
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log('error:  ' + e.message);
        return dateMax

      });
  }
  saveIntoFashDatesBrandTable(dateObj, gender) {
    if (!dateObj ) {
      return 
    }
    console.log('TryingToInsertDates')
  //  let createTableQuery = "CREATE TABLE IF NOT EXISTS `fash_brands` (`fb_featured` int(4),  `fb_id` int(4), `fb_name` varchar(64) ,`fb_status` tinyint(1)  DEFAULT '1',`fb_image` varchar(255) , `fb_gender` tinyint(1) )"
    let fieledsPart = "INSERT INTO `fash_dates_b` (`fash_dates_id`,  `fbc_genders`, `fbc_date` , `fbc_type` ) VALUES ";
    let ValuesPart = ""
   // let scIDs = ""
   let fc_IDS=""
   ValuesPart += '("' + dateObj.fash_dates_id + '", "'  + dateObj.fbc_genders + '", "'  +dateObj.fbc_date + '", "'   + dateObj.fbc_type + '"),';
   fc_IDS += dateObj.fash_dates_id + ','

    let finalQuery = fieledsPart + ValuesPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    fc_IDS = fc_IDS.slice(0, fc_IDS.length - 1)
    return this.deleteMultipleFashDatesBrand(fc_IDS, gender).then(isDeleted => {
      if (isDeleted) {
        return this.database.executeSql(slicedQuery, []).then(res => {
          // alert('insertionTest: ' + res.rowsAffected + ' fash_collections were dumped')
          console.log('insertionTest: ' + res.rowsAffected + ' fash_dates were dumped');
        })
          .catch(e => {
            this.serviceManager.sendErrorToServer(e.message, slicedQuery)
            // alert('eror: while inserting into fash_collections' + e.message)
            console.log('eror: while inserting into fash_dates' + e.message)
          });
      }
    })
  }
  deleteMultipleFashDatesBrand(fash_dates_id, gender) {

    let deleteCategorires = 'DELETE FROM fash_dates_b where fash_dates_id IN (' + fash_dates_id + ') and fbc_genders= "' + gender + '"';
    return this.database.executeSql(deleteCategorires, [])
      .then(res => {

        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteCategorires)
        return false;
      });
  }
//end dates
  create_fashion_collectionsTable() {
    let createTableQuery = "CREATE TABLE IF NOT EXISTS `fash_collections` (`fc_id` int(4),  `fc_title` varchar(64), `fc_status` int(4), `fc_image` varchar(100), `fc_gender` int(4) )"
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
       // alert('FashionBrandTable created')
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }
        console.log('Error while creating table fash_brands', e.message)
      });
  }
  saveIntoFashCollectionTable(serviceCategories: FASHION_COLLECTION[] ) {
    console.log('insertionTest: ' + serviceCategories.length + ' fash_brands were sent for dumping');
    if (!serviceCategories || serviceCategories.length === 0) {
      return 
    }
  //  let createTableQuery = "CREATE TABLE IF NOT EXISTS `fash_brands` (`fb_featured` int(4),  `fb_id` int(4), `fb_name` varchar(64) ,`fb_status` tinyint(1)  DEFAULT '1',`fb_image` varchar(255) , `fb_gender` tinyint(1) )"
    let fieledsPart = "INSERT INTO `fash_collections` (`fc_id`,  `fc_title`, `fc_status`, `fc_image`, `fc_gender`) VALUES ";
    let ValuesPart = ""
   // let scIDs = ""
   let fc_IDS=""
    serviceCategories.forEach(fash_brand => {
      console.log('sc_featured',fash_brand.fc_id)
      ValuesPart += '("' + fash_brand.fc_id + '", "'  + fash_brand.fc_title + '", "'  +fash_brand.fc_status + '", "' + fash_brand.fc_image + '", "'  + fash_brand.fc_gender + '"),';
      fc_IDS += fash_brand.fc_id + ','
    });

    let finalQuery = fieledsPart + ValuesPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    fc_IDS = fc_IDS.slice(0, fc_IDS.length - 1)

    return this.deleteMultipleFashionCollection(fc_IDS).then(isDeleted => {
      if (isDeleted) {
        return this.database.executeSql(slicedQuery, []).then(res => {
          // alert('insertionTest: ' + res.rowsAffected + ' fash_collections were dumped')
          console.log('insertionTest: ' + res.rowsAffected + ' fash_collections were dumped');
        })
          .catch(e => {
            this.serviceManager.sendErrorToServer(e.message, slicedQuery)
            // alert('eror: while inserting into fash_collections' + e.message)
            console.log('eror: while inserting into fash_collections' + e.message)
          });
      }
    })
  }
  deleteMultipleFashionCollection(fc_ids) {

    let deleteCategorires = 'DELETE FROM fash_collections where fc_id IN (' + fc_ids + ') ';
    return this.database.executeSql(deleteCategorires, [])
      .then(res => {
         console.log('COllection-Deleted')
        return true;
      })
      .catch(e => {
        console.log('COllection-Deleletion error' +e.message)
        this.serviceManager.sendErrorToServer(e.message, deleteCategorires)
        return false;
      });
  }
  saveIntoFashBrandsTable(serviceCategories: FASHION_BRANDS[]) {
    console.log('insertionTest: ' + serviceCategories.length + ' fash_brands were sent for dumping');
    if (!serviceCategories || serviceCategories.length === 0) {
      return 
    }
  //  let createTableQuery = "CREATE TABLE IF NOT EXISTS `fash_brands` (`fb_featured` int(4),  `fb_id` int(4), `fb_name` varchar(64) ,`fb_status` tinyint(1)  DEFAULT '1',`fb_image` varchar(255) , `fb_gender` tinyint(1) )"
    let fieledsPart = "INSERT INTO `fash_brands` (`fb_featured`,  `fb_id`, `fb_name`, `fb_status`, `fb_image`, `fb_gender`) VALUES ";
    let ValuesPart = ""
   // let scIDs = ""
   let fb_IDS=""
    serviceCategories.forEach(fash_brand => {
      console.log('sc_featured',fash_brand.fb_featured)
      ValuesPart += '("' + fash_brand.fb_featured + '", "'  + fash_brand.fb_id + '", "' + fash_brand.fb_name + '", "' + fash_brand.fb_status + '", "' + fash_brand.fb_image + '", "' + fash_brand.fb_gender + '"),';
      fb_IDS += fash_brand.fb_id + ','
    });

    let finalQuery = fieledsPart + ValuesPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    fb_IDS = fb_IDS.slice(0, fb_IDS.length - 1)

    return this.deleteMultipleFashionBrands(fb_IDS).then(isDeleted => {
      if (isDeleted) {
        return this.database.executeSql(slicedQuery, []).then(res => {
          
          console.log('insertionTest: ' + res.rowsAffected + ' fash_brands were dumped');
        })
          .catch(e => {
            this.serviceManager.sendErrorToServer(e.message, slicedQuery)
           
            console.log('eror: while inserting into fash_brands' + e.message)
          });
      }
    })
  }
  deleteMultipleFashionBrands(fb_ids) {

    let deleteCategorires = 'DELETE FROM fash_brands where fb_id IN (' + fb_ids + ')';
    return this.database.executeSql(deleteCategorires, [])
      .then(res => {

        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteCategorires)
        return false;
      });
  }
  create_table_fash_brand_collections() {				
                                                                                                                											
    let createTableQuery = "CREATE TABLE IF NOT EXISTS `fash_brand_collections` (`fbc_modify_datetime` TEXT, `fbc_featured` tinyint(1), `fbc_gender` TEXT , `fbc_id` int(11), `fbc_image` TEXT, `fbc_name` TEXT, `fbc_status`  TEXT ,  `fb_id` int(11) , `fc_id` int(11), `fbc_keywords`  TEXT , `fbc_link`  TEXT )"
    this.database.executeSql(createTableQuery, [])
      .then(res => {
      //  alert('FashionBrandCollectionTable created')
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }
        console.log('Error while creating table fash_brand_collections', e.message)
      });
  }
  saveIntoFashBranCollectionsTable(ServiceSubCATEGORIE: FASHION_BRANDS_COLLECTION[]) {
       
    if (!ServiceSubCATEGORIE || ServiceSubCATEGORIE.length === 0) {
      return 
    }
    let fieledsPart = "INSERT INTO `fash_brand_collections` (`fbc_modify_datetime`, `fbc_featured`, `fbc_gender`, `fbc_id`, `fbc_image`, `fbc_name`, `fbc_status` , `fb_id` , `fc_id` , `fbc_keywords`, `fbc_link`) VALUES ";
    let ValuesPart = ""
    let fbc_IDS = ""
    ServiceSubCATEGORIE.forEach(subCat => {
      ValuesPart += '("' + subCat.fbc_modify_datetime + '","' + subCat.fbc_featured + '","' + subCat.fbc_gender + '", "' + subCat.fbc_id + '", "' + subCat.fbc_image + '", "' + subCat.fbc_name + '", "' + subCat.fbc_status + '","'+ subCat.fb_id + '","'+ subCat.fc_id + '","'+ subCat.fbc_keywords + '","' + subCat.fbc_link + '"),';
      fbc_IDS += subCat.fbc_id + ','
    });
    let finalQuery = fieledsPart + ValuesPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    fbc_IDS = fbc_IDS.slice(0, fbc_IDS.length - 1)
    return this.deleteMultipleFashionBrandsCollection(fbc_IDS).then(isDeleted => {
      return this.database.executeSql(slicedQuery, []).then(res => {
        console.log('insertionTest: ' + res.rowsAffected + ' fash_brand_collections were dumped');
        // alert('insertionTest: ' + res.rowsAffected + ' fash_brand_collections were dumped')
      })
        .catch(e => {
          // alert('eror: while inserting into fash_brand_collections ' + e.message)
          console.log('eror: while inserting into fash_brand_collections ' + e.message)
          this.serviceManager.sendErrorToServer(e.message, slicedQuery)
        });
    })
  }
  deleteMultipleFashionBrandsCollection(fbc_IDS) {
    let Query = 'DELETE FROM fash_brand_collections where fbc_id IN (' + fbc_IDS + ')';
    return this.database.executeSql(Query, [])
      .then(res => {
        return true
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log('error: not deleted ' + e.message)
        return false
      });
  }
  deleteFashinBrandsCollection() {
    let Query = 'DELETE FROM fash_brand_collections'
    this.database.executeSql(Query, [])
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
      });
  }
  deleteFashionBrands() {
    let Query = 'DELETE FROM fash_brands'
    this.database.executeSql(Query, [])
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
      });
  }
  getFashionBrands() {
    let fashionBrands: FASHION_BRANDS[] = []
    let fashions='select * from fash_brands  order by fb_featured desc'
    //let  Query ='select sc.*, max(ssc_modify_datetime) as modify_datetime, (select ssc.ssc_name from service_sub_categories as ssc where ssc.sc_id = sc.sc_id order by ssc.ssc_featured limit 0, 1) as ssc_name from `service_categories` as sc,`service_sub_categories` as ssc where ssc.sc_id = sc.sc_id group by sc.sc_id order by sc.sc_type desc, sc.sc_featured desc, modify_datetime desc'

    return this.database.executeSql(fashions, [])
      .then(style => {
        if (style.rows.length > 0) {
           console.log('TotoalBrands',style.rows.length)
          //  alert('Here'+JSON.stringify(style.rows.item(i)))
          // console.log("ALLREconrds",style.rows.item(i).fb_name);
          for (var i = 0; i < style.rows.length; i++) {
            console.log('InLoop')
            console.log('fb_name:'+style.rows.item(i).fb_name)
            let subCategory: FASHION_BRANDS = style.rows.item(i)
            fashionBrands.push(subCategory)
          }
          return fashionBrands
        } else {
          return fashionBrands
        }

      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, fashions)
        return fashionBrands

      });
  }
  getFashionCollection() {
    let fashionBrands: FASHION_COLLECTION[] = []
    let fashions='select * from fash_collections '
    return this.database.executeSql(fashions, [])
      .then(style => {
        console.log('TotalCollection',style.rows.length)
        if (style.rows.length > 0) {
          for (var i = 0; i < style.rows.length; i++) {
            let subCategory: FASHION_COLLECTION = style.rows.item(i)
            fashionBrands.push(subCategory)
          }
          return fashionBrands
        } else {
          return fashionBrands
        }
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, fashions)
        return fashionBrands
      });
  }
  createFBC_Images() {
    let createTableQuery = "CREATE TABLE IF NOT EXISTS `fbc_images` (`fbci_id` int(4) , `fbci_name` varchar(40) ,`fbc_id` int(4), `fbci_datetime` varchar(255) , `fbc_name` varchar(255) , `fb_name` varchar(255),  `fb_id` int(4) , `fc_id` int(4) , `fbc_gender` varchar(20)  )"
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        console.log('success: category_sub_style table created.');
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }

        console.log('Error while creating table category_sub_style', e.message)
      });
  }

  createFB_Images() {
    let createTableQuery = "CREATE TABLE IF NOT EXISTS `fb_images` (`fbci_id` int(4) , `fbci_name` varchar(40) ,`fbc_id` int(4), `fbci_datetime` varchar(255) , `fbc_name` varchar(255) , `fb_name` varchar(255),  `fb_id` int(4) , `fc_id` int(4) , `fbc_gender` varchar(20)  )"
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        console.log('success: category_sub_style table created.');
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }

        console.log('Error while creating table category_sub_style', e.message)
      });
  }

  saveIntoFB_IMAGES(serviceCategories: FB_IMAGES[]) {
    // here from tips detail page
     console.log('insertionTest: ' + serviceCategories.length + ' category_sub_style were sent for dumping');
     if (!serviceCategories || serviceCategories.length === 0) {
       return 
     }
     let fieledsPart = "INSERT INTO `fb_images` (`fbci_id`, `fbci_name`, `fbc_id`,   `fbci_datetime` ,   `fbc_name` ,   `fb_name` ,   `fb_id` ,   `fc_id`,  `fbc_gender` ) VALUES ";
     let ValuesPart = ""
     let fbci_ids = ""
     serviceCategories.forEach(serviceCategory => {
       ValuesPart += '("' + serviceCategory.fbci_id + '", "' + serviceCategory.fbci_name + '", "' + serviceCategory.fbc_id +  '", "' + serviceCategory.fbci_datetime+  '", "' + serviceCategory.fbc_name+  '", "' + serviceCategory.fb_name+  '", "' + serviceCategory.fb_id+  '", "'+ serviceCategory.fc_id+  '", "' + serviceCategory.fbc_gender + '"),';
       fbci_ids += serviceCategory.fbci_id + ','
     });
     let finalQuery = fieledsPart + ValuesPart
     let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
     fbci_ids = fbci_ids.slice(0, fbci_ids.length - 1)
     return this.deleteFB_ImageById(fbci_ids).then(isDeleted => {
       if (isDeleted) {
         return this.database.executeSql(slicedQuery, [])
           .then(res => {
             console.log('insertionTest: ' + res.rowsAffected + ' category_sub_style were dumped');
             // console.log('InsertQuesry',slicedQuery)
           })
           .catch(e => {
             this.serviceManager.sendErrorToServer(e.message, slicedQuery)
             console.log('eror: while inserting into service_categories' + e.message)
           });
       }
     })
  }

  saveIntoFBC_IMAGES(serviceCategories: FBC_IMAGES[]) {
    // here from tips detail page
     console.log('insertionTest: ' + serviceCategories.length + ' category_sub_style were sent for dumping');
     if (!serviceCategories || serviceCategories.length === 0) {
       return 
     }
     let fieledsPart = "INSERT INTO `fbc_images` (`fbci_id`, `fbci_name`, `fbc_id`,   `fbci_datetime` ,   `fbc_name` ,   `fb_name` ,   `fb_id` ,   `fc_id`,  `fbc_gender` ) VALUES ";
     let ValuesPart = ""
     let fbci_ids = ""
     serviceCategories.forEach(serviceCategory => {
       ValuesPart += '("' + serviceCategory.fbci_id + '", "' + serviceCategory.fbci_name + '", "' + serviceCategory.fbc_id +  '", "' + serviceCategory.fbci_datetime+  '", "' + serviceCategory.fbc_name+  '", "' + serviceCategory.fb_name+  '", "' + serviceCategory.fb_id+  '", "'+ serviceCategory.fc_id+  '", "' + serviceCategory.fbc_gender + '"),';
       fbci_ids += serviceCategory.fbci_id + ','
     });
     let finalQuery = fieledsPart + ValuesPart
     let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
     fbci_ids = fbci_ids.slice(0, fbci_ids.length - 1)
     return this.deleteFBC_ImageById(fbci_ids).then(isDeleted => {
       if (isDeleted) {
         return this.database.executeSql(slicedQuery, [])
           .then(res => {
             console.log('insertionTest: ' + res.rowsAffected + ' category_sub_style were dumped');
             // console.log('InsertQuesry',slicedQuery)
           })
           .catch(e => {
             this.serviceManager.sendErrorToServer(e.message, slicedQuery)
             console.log('eror: while inserting into service_categories' + e.message)
           });
       }
     })
  }
  deleteFBC_ImageById(fbci_ids) {

    let deleteCategorires = 'DELETE FROM fbc_images where fbci_id IN (' + fbci_ids + ')';
    return this.database.executeSql(deleteCategorires, [])
      .then(res => {
        console.log('success: duplicate service_categories found ' + res.rowsAffected);
        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteCategorires)
        console.log('error: ' + e.message);
        return false;
      });
  }
  deleteFB_ImageById(fbci_ids) {
    let deleteCategorires = 'DELETE FROM fbc_images where fbci_id IN (' + fbci_ids + ')';
    return this.database.executeSql(deleteCategorires, [])
      .then(res => {
        console.log('success: duplicate service_categories found ' + res.rowsAffected);
        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteCategorires)
        console.log('error: ' + e.message);
        return false;
      });
  }
  getFC_Imag(fc_id, gender,  last_fbci_datetime, last_fbci_id, type) {
    let selectQuery=''
   
   
    // let selectQuery=''
    // if (!last_fbci_datetime || last_fbci_datetime === 0)
    // {
    // selectQuery = 'SELECT fbi.*, fbc.fbc_name, fbc.fbc_modify_datetime, af.fav_id  FROM   `fash_brand_collections` as fbc, `fb_images` as fbi  LEFT JOIN all_favorites af ON fbi.fbci_id = af.fav_id and af.cft_id=1   where   fbi.fbc_id=fbc.fbc_id  and fbi.fbc_id in ( select fbc.fbc_id FROM fash_brand_collections as fbc where fbc.fb_id='+fb_id+')  and fbc.fbc_gender= '+gender+' ORDER BY fbc.fbc_modify_datetime  DESC, fbi.fbci_id DESC  limit 4'   
    // }  
    // else
    // {
    //   selectQuery='SELECT fbi.*, fbc.fbc_name, fbc.fbc_modify_datetime, af.fav_id   FROM `fash_brand_collections` as fbc, `fb_images` as fbi  LEFT JOIN all_favorites af ON fbi.fbci_id = af.fav_id and af.cft_id=1   where  fbi.fbc_id=fbc.fbc_id and fbi.fbc_id in ( select fbc.fbc_id FROM fash_brand_collections as fbc where fbc.fb_id='+fb_id+') and fbc.fbc_gender= '+gender+' and ((fbc.fbc_modify_datetime = "'+ last_fbci_datetime +'" and fbi.fbci_id < '+ last_fbci_id +') or fbc.fbc_modify_datetime < "'+ last_fbci_datetime +'") ORDER BY fbc.fbc_modify_datetime DESC, fbi.fbci_id DESC limit 8'
    // }

    
    if (!last_fbci_datetime || last_fbci_datetime === 0)
    {
    selectQuery=  'SELECT fbci.*, fbc.fbc_name, fbc.fbc_modify_datetime, fb.fb_name , af.fav_id   FROM  `fash_brand_collections` as fbc, `fash_brands` as fb, `fbc_images` as fbci  LEFT JOIN all_favorites af ON fbci.fbci_id = af.fav_id  and af.cft_id='+type+'  where  fbci.fbc_id=fbc.fbc_id  and fbc.fb_id = fb.fb_id   and fbci.fbc_id in ( select fbc.fbc_id FROM fash_brand_collections as fbc where fbc.fc_id='+fc_id+')  and fbc.fbc_gender= '+gender+' ORDER BY af.fav_id DESC, fbc.fbc_modify_datetime  DESC, fbci.fbci_id DESC  limit 4'  
    } 
     else
    {
      selectQuery='SELECT fbci.*, fbc.fbc_name, fbc.fbc_modify_datetime,  fb.fb_name , af.fav_id  FROM  `fash_brand_collections` as fbc, `fash_brands` as fb, `fbc_images` as fbci  LEFT JOIN all_favorites af ON fbci.fbci_id = af.fav_id  and af.cft_id='+type+'  where fbci.fbc_id=fbc.fbc_id and fbc.fb_id = fb.fb_id  and fbci.fbc_id in ( select fbc.fbc_id FROM fash_brand_collections as fbc where fbc.fc_id='+fc_id+') and fbc.fbc_gender= '+gender+' and ((fbc.fbc_modify_datetime = "'+ last_fbci_datetime +'" and fbci.fbci_id < '+ last_fbci_id +') or fbc.fbc_modify_datetime < "'+ last_fbci_datetime +'") ORDER BY  af.fav_id DESC, fbc.fbc_modify_datetime DESC, fbci.fbci_id DESC limit 8'
    }

    console.log(selectQuery);
    let categorySubStyle: FBC_IMAGES[] = []
    return this.database.executeSql(selectQuery, [])
      .then(style => {
       
        if (style.rows.length > 0) {
          for (var i = 0; i < style.rows.length; i++) {
            
            let subCategory: FBC_IMAGES = style.rows.item(i)
            if(subCategory.fav_id===undefined || subCategory.fav_id===null){
              subCategory.fav_id=0
            }
            console.log('Items',JSON.stringify(subCategory))
            categorySubStyle.push(subCategory)
          }
        

          return categorySubStyle
        } else {
          return categorySubStyle
        }

      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, selectQuery)
        console.log('error:  ' + e.message);
        return categorySubStyle

      });
  }  
   getFB_Imag(fb_id, gender, last_fbci_datetime, last_fbci_id, type) {
       let selectQuery=''
    if (!last_fbci_datetime || last_fbci_datetime === 0)
    {
    selectQuery = 'SELECT fbi.*, fbc.fbc_name, fbc.fbc_modify_datetime, af.fav_id  FROM   `fash_brand_collections` as fbc, `fb_images` as fbi  LEFT JOIN all_favorites af ON fbi.fbci_id = af.fav_id   and af.cft_id='+type+' where   fbi.fbc_id=fbc.fbc_id  and fbi.fbc_id in ( select fbc.fbc_id FROM fash_brand_collections as fbc where fbc.fb_id='+fb_id+')  and fbc.fbc_gender= '+gender+' ORDER  BY af.fav_id DESC, fbc.fbc_modify_datetime  DESC, fbi.fbci_id DESC  limit 4'   
    }  
    else
    {
      selectQuery='SELECT fbi.*, fbc.fbc_name, fbc.fbc_modify_datetime, af.fav_id   FROM `fash_brand_collections` as fbc, `fb_images` as fbi  LEFT JOIN all_favorites af ON fbi.fbci_id = af.fav_id  and af.cft_id='+type+'   where  fbi.fbc_id=fbc.fbc_id and fbi.fbc_id in ( select fbc.fbc_id FROM fash_brand_collections as fbc where fbc.fb_id='+fb_id+') and fbc.fbc_gender= '+gender+' and ((fbc.fbc_modify_datetime = "'+ last_fbci_datetime +'" and fbi.fbci_id < '+ last_fbci_id +') or fbc.fbc_modify_datetime < "'+ last_fbci_datetime +'") ORDER BY af.fav_id DESC, fbc.fbc_modify_datetime DESC, fbi.fbci_id DESC limit 8'
    }
        console.log(selectQuery)
        let categorySubStyle: FB_IMAGES[] = []
        return this.database.executeSql(selectQuery, [])
          .then(style => {
            // alert('TotalItem'+style.rows.length)
            if (style.rows.length > 0) {
              for (var i = 0; i < style.rows.length; i++) {

                
                let subCategory: FB_IMAGES = style.rows.item(i)
                if(subCategory.fav_id===undefined || subCategory.fav_id===null){
                  subCategory.fav_id=0
                }else{
                  
                }
                console.log(''+JSON.stringify( subCategory))
                categorySubStyle.push(subCategory)
              }
              // categorySubStyle[0].fav_id=301
              return categorySubStyle
            } else {
              return categorySubStyle
            }
          })
          .catch(e => {
            this.serviceManager.sendErrorToServer(e.message, selectQuery)
            console.log('error while getting brands:  ' + e.message);
            return categorySubStyle
          });
  }
  //favorite section 
  createFavoriteFashionTable() {                                                      
    let createTableQuery = "CREATE TABLE IF NOT EXISTS  `fav_fashion` (`fbci_id` int(11) DEFAULT NULL, `fbci_name` varchar(64) DEFAULT NULL , `fbc_id` int(11) DEFAULT NULL, `fbci_datetime`  varchar(64)  , `fbc_name`  varchar(64)  , `fb_name`  varchar(64) );"
    let isTableCreated = false
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        isTableCreated = true
        console.log('success: favorite table created');
        return isTableCreated
      })
      .catch(e => {
        let msg:string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)  
        }
        console.log('error: in favroites'+e.message);
        let errorMessage: string = e.message
        if (errorMessage.trim().toLowerCase() === 'table `posts` already exists'.trim().toLowerCase()) {
          isTableCreated = true
          return isTableCreated
        } else {
          console.log('error: pinterest_pins' + e.message);
          isTableCreated = false
          return isTableCreated
        }
      });
  }
  InsertInToFavFashionTable(favItem) {
    //let createTableQuery = "CREATE TABLE IF NOT EXISTS  `fav_fashion` (`fbci_id` int(11) DEFAULT NULL, `fbci_name` varchar(64) DEFAULT NULL , `fbc_id` int(11) DEFAULT NULL, `fbci_datetime`  varchar(64)  );"
    let basePart = "INSERT INTO `fav_fashion` (`fbci_id`, `fbci_name`,`fbc_id`, `fbci_datetime` , `fbc_name` , `fb_name` ) VALUES"
    let valuesPart = '("'
      + favItem.fbci_id + '", "'
      + favItem.fbci_name + '", "'
      + favItem.fbc_id + '", "'
      + favItem.fbci_datetime + '", "'
      + favItem.fbc_name + '", "'
      + favItem.fb_name + '")';
    let finalQuery = basePart + valuesPart
    console.log('insert_query');
    console.log(finalQuery);
    let isDataInserted = false
    return this.database.executeSql(finalQuery, [])
      .then(res => {
        console.log('success: insert in `favorite` '+res.rowsAffected);
        res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false
        return isDataInserted
      })
      .catch(e => {
        console.log('Errr',e.message)
        this.serviceManager.sendErrorToServer(e.message, finalQuery)  
        isDataInserted = false
        return e.message
      });
  }

  InsertInMultToFavFashionTable(favItems) {
    let basePart = "INSERT INTO `fav_fashion` (`fbci_id`, `fbci_name`,`fbc_id`, `fbci_datetime` , `fbc_name` , `fb_name` ) VALUES"
    let valuesPart = '';
    // let valuesPart = '("'
    //   + favItem.fbci_id + '", "'
    //   + favItem.fbci_name + '", "'
    //   + favItem.fbc_id + '", "'
    //   + favItem.fbci_datetime + '", "'
    //   + favItem.fbc_name + '", "'
    //   + favItem.fb_name + '")';
    favItems.forEach(favItem => {
      valuesPart += '("' + favItem.fbci_id + '", "'  + favItem.fbci_name + '", "'  +favItem.fbc_id + '", "' + favItem.fbci_datetime + '", "'+ favItem.fbc_name + '", "'  + favItem.fb_name + '"),';
    });
    
    let finalQuery = basePart + valuesPart
     let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    console.log('insert_query');
    console.log(slicedQuery);
    let isDataInserted = false
    return this.database.executeSql(slicedQuery, [])
      .then(res => {
        console.log('success: insert in `fav_fashion` '+res.rowsAffected);
        res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false
        return isDataInserted
      })
      .catch(e => {
        console.log('Errr',e.message)
        this.serviceManager.sendErrorToServer(e.message, finalQuery)  
        isDataInserted = false
        return e.message
      });
  }

  checkIfFavFashImg() {
    let selectQuery=''
    selectQuery=  'SELECT *  FROM  `fav_fashion` '
    console.log(selectQuery);
    console.log(selectQuery);
    let isFashionTrends = false
    return this.database.executeSql(selectQuery, [])
      .then(res => {
        console.log('success: insert in `favorite` '+res.rows.length);
        if (res.rows.length > 0) {
          isFashionTrends=true
        }
        return isFashionTrends
      })
      .catch(e => {
        console.log('Errr',e.message)
        this.serviceManager.sendErrorToServer(e.message, selectQuery)  
        isFashionTrends = false
        return e.message
      });
  }

  deleteFavFashion(fbci_id) {
    let deleteQuery = 'DELETE FROM `fav_fashion` where `fbci_id` ='+fbci_id
    let isDeleted = false
    return this.database.executeSql(deleteQuery, [])
      .then(res => {
        console.log('success: post deleted '+isDeleted);
        isDeleted = true
        return isDeleted
      })
      .catch(e => {
        console.log('ErrorIn deleting'+e.message)
        this.serviceManager.sendErrorToServer(e.message, deleteQuery)  
        isDeleted = false
        return e.message
      });
  }

  
  getFBC_Imag_Favourite(type) {
    let selectQuery=''
    selectQuery=  'SELECT *  FROM  `fav_fashion` ' 
    console.log(selectQuery);
    let categorySubStyle: FBC_IMAGES[] = []
    return this.database.executeSql(selectQuery, [])
      .then(style => {
        //  alert('here'+style.rows.length)
        if (style.rows.length > 0) {
          for (var i = 0; i < style.rows.length; i++) {
            
            let subCategory: FBC_IMAGES = style.rows.item(i)
            if(subCategory.fav_id===undefined || subCategory.fav_id===null){
              subCategory.fav_id=subCategory.fbci_id
            }
            console.log('Items',JSON.stringify(subCategory))
            categorySubStyle.push(subCategory)
          }
        

          return categorySubStyle
        } else {
          return categorySubStyle
        }

      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, selectQuery)
        console.log('error:  ' + e.message);
        return categorySubStyle

      });

}

  
  //end favorite section

}