import { PRODUCT_BRANDS } from '../SalonAppUser-Interface';
import { Platform } from 'ionic-angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { GlobalProvider } from '../global/global';
import { BehaviorSubject } from "rxjs";
import { Storage } from "@ionic/storage";


@Injectable()
export class ProductBrandModal {

  /* Global Vairables */
  database: SQLiteObject
  private databaseReady: BehaviorSubject<boolean>;


  constructor(
    public http: HttpClient,
    private sqlite: SQLite,
    public sQLitePorter: SQLitePorter,
    public storage: Storage,
    public platform: Platform,
    public serviceManager: GlobalProvider, ) {
    this.databaseReady = new BehaviorSubject(false)
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;
          console.log('database is setup');
          this.databaseReady.next(true)

        }, err => {
          console.log('database is nullll');

        })
    });
  }

  getDatabaseState() {
    return this.databaseReady.asObservable()
  }

  async createProductBrandTable() {
    console.log('came in createProductBrandTable Modal');
    let createTableQuery = "CREATE TABLE IF NOT EXISTS `product_brands` ( `pb_id` int(11) , `pb_name` varchar(250) ,`pb_image` varchar(250) , `pb_manufacturer` varchar(250) , `pb_status` int(11) , `pv_id` int(11) , `pb_created_at` timestamp  DEFAULT CURRENT_TIMESTAMP , `pb_modified_date` timestamp  DEFAULT '0000-00-00 00:00:00')"
    let isTableCreated = false
    try {
      const res = await this.database.executeSql(createTableQuery, []);
      isTableCreated = true;
      console.log('product_brands table created');
      return res;
    }
    catch (e) {
      let msg: string = e.message;
      if (!msg.includes('already exists')) {
        this.serviceManager.sendErrorToServer(e.message, createTableQuery);
      }
      console.log('error: InsertInToProductBrandsTable ' + e.message);
      let errorMessage: string = e.message;
      if (errorMessage.trim().toLowerCase() === 'table `product_brands` already exists'.trim().toLowerCase()) {
        isTableCreated = true;
        return isTableCreated;
      }
      else {
        isTableCreated = false;
        return isTableCreated;
      }
    }
  }


  async delteDuplicateProductBrands(ProductIds) {
    if (!ProductIds) {
      return
    }
    let deleteSelectedQuery = 'DELETE FROM `product_brands`  where pb_id IN (' + ProductIds + ');'
    try {
      const res = await this.database.executeSql(deleteSelectedQuery, []);

      return true;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery);
      return false;
    }
  }


  async InsertInToProductBrandsTable(productBrands:PRODUCT_BRANDS[]) {
    if (!productBrands || productBrands.length === 0) {
      return
    }
    console.log('insertionTest: ' + productBrands.length + ' product_brand were sent for dumping');

    let firstPart = "INSERT INTO `product_brands` (`pb_id`, `pb_name`,`pb_image`, `pb_manufacturer`, `pb_status`, `pv_id`, `pb_created_at`, `pb_modified_date`) VALUES";
    let secondPart = ""
    console.log('productBrands', productBrands);
    let pb_ids = ""

    productBrands.forEach(element => {
      let section: PRODUCT_BRANDS = element

      secondPart += '("' + section.pb_id + '", "' + section.pb_name + '", "' + section.pb_image + '", "' + section.pb_manufacturer + '", "' + section.pb_status + '" , "' + section.pv_id + '", "' + section.pb_created_at + '", "' + section.pb_modified_date + '"),';

      pb_ids += section.pb_id + ','
    });

    let finalQuery = firstPart + secondPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    pb_ids = pb_ids.slice(0, pb_ids.length - 1)

    console.log("sectionIDs", pb_ids)

    const tblProductBrandDeleted = await this.deleteMultipleSections(pb_ids);
    let isDataInserted = false;
    try {
      const res = await this.database.executeSql(slicedQuery, []);
      console.log('insertionTest: ' + res.rowsAffected + ' product_brand were dumped');
      res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false;
      return isDataInserted;
    }
    catch (e) {
      console.log(e.message);
      
      this.serviceManager.sendErrorToServer(e.message, slicedQuery);
      isDataInserted = false;
      return e.message;
    }
  }

  async getProductAllBrands() {
    let Inserquery = "select * from `product_brands`"
    let ProductBrands = []
    try {
      const ProductBrandsArray = await this.database.executeSql(Inserquery, []);
      for (var i = 0; i < ProductBrandsArray.rows.length; i++) {
        let ProductBrand = ProductBrandsArray.rows.item(i);
        ProductBrands.push(ProductBrand);
      }
      
      return ProductBrands;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, Inserquery);
      console.log('error:  ProductBrands ' + e.message);
      return ProductBrands;
    }
  }
  async getProductBrand(pb_id) {
    let Inserquery = "select * from `product_brands` where pb_id =" + pb_id
    let ProductBrands = []
    try {
      const ProductBrandsArray = await this.database.executeSql(Inserquery, []);
      for (var i = 0; i < ProductBrandsArray.rows.length; i++) {
        let ProductBrand = ProductBrandsArray.rows.item(i);
        ProductBrands.push(ProductBrand);
      }
      return ProductBrands;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, Inserquery);
      console.log('error:  ProductBrands ' + e.message);
      return ProductBrands;
    }
  }

  async TruncCateProductBrandsTable() {
    let deleteQuery = "DELETE FROM `product_brands` ;"

    let isDeleted = false

    try {
      const res = await this.database.executeSql(deleteQuery, []);
      isDeleted = true;
      return isDeleted;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, deleteQuery);
      console.log('error: `product_brands`');
      isDeleted = false;
      return e.message;
    }
  }

  async deleteMultipleSections(pb_ids) {

    let deleteSelectedQuery = 'DELETE FROM `product_brands`  where pb_id IN (' + pb_ids + ');'
    try {
      const res = await this.database.executeSql(deleteSelectedQuery, []);
      return true;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery);
      return false;
    }
  }


}
