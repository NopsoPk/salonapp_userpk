import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import {Salon} from '../../providers/SalonAppUser-Interface'
import { ToastController,
         LoadingController   
 } from 'ionic-angular';
import { retry } from 'rxjs/operators/retry';

/*
  Generated class for the CosntantsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CosntantsProvider {
  loading                 :  any
  DEVICE_DATETIME         = 'device_datetime'
  LOGED_IN_SALON_DETAIL   = 'salon_Details'
  SALON_TECHS             = 'salon_techs'
  APP_STATUS_SERVING      = "serving"
  APP_STATUS_FINISHED   	= "finished"
  APP_STATUS_ACCEPTED     = "accepted"
  APP_STATUS_REJECTED     = "rejected"
  APP_STATUS_DELETED      = "deleted"
  APP_STATUS_PENDING      = "pending"
  
  ACCEPT_BUTTON_TEXT      = "Accept"
  REMOVE_BUTTON_TEXT      = "Cancel"
  REJECT_BUTTON_TEXT      = "Reject"
  RESCHEDULE_BUTTON_TEXT  = "Reschedule"
 
 SUCCESS_STATUS           = "1"
 IS_LOGGED_IN             = "looged_in"
 LOGGED_IN_SALON_ID       = "logged_in_salon_id"
 LOGGED_IN_SALON_NAME     = "logged_in_salon_name"
 LOGGED_IN_SALON_STATUS   = "logged_in_salon_status"
 LOGGED_IN_SALON_ADDRESS  = "logged_in_salon_address"
 NO_APPOINTMENTS          = "No appointment exists."
 noInternetMsg            = "You're not connected to internet at the moment, Please try again later."
  
  constructor(  
          private http: Http,
          private toastCtrl: ToastController,
          public loadingCtrl: LoadingController,private datePipe:DatePipe) {
    
            this.loading = this.loadingCtrl.create({
            });
  }

// CUSTOME FUNCTIONS
    getCurrentDeviceDateTime(): string {
      let format = "yyyy-MM-dd HH:mm:ss";
      let currentDate = new Date();
      return this.datePipe.transform(currentDate,format)
    }

    getStringFromDateWithFormat(date,format) :string{
      return this.datePipe.transform(date,format)
    }

    get12HourFormatFrom24Hour(time){
      let hours = time.split(':')[0];
      var suffix = Number(hours) >= 12 ? "PM":"AM"; 
      var hour =  ((Number(hours) + 11) % 12 + 1);
      if (hour < 10){
        return "0" + hour +':'+time.split(':')[1]+' '+ suffix;
      }
      return hours = hour +':'+time.split(':')[1]+' '+ suffix;
    }

    get12HourFormatFrom24HourWithoutSuffix(time){
      let hours = time.split(':')[0];
      var hour =  ((Number(hours) + 11) % 12 + 1);
      if (hour < 10){
        return "0" + hour +':'+time.split(':')[1]+' ';
      }
      return hours = hour +':'+time.split(':')[1]+' ';
    }

    get12HourFormatSuffix(time){
      let hours = time.split(':')[0];
      var suffix = Number(hours) >= 12 ? "PM":"AM"; 
    
      return   suffix;
    }

    makeToastOnSuccess(message:string,duration:number,position:string) {
      let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position,
      dismissOnPageChange: true,
      cssClass: "customToastClassz"
      });
  
      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });
  
      toast.present();
   }
  
    makeToastOnFailure(message:string,duration:number,position:string) {
        let toast = this.toastCtrl.create({
        message: message,
        duration: duration,
        position: position,
        dismissOnPageChange: true,
        cssClass: "failureToastClass"
      });
  
      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });
  
      toast.present();
    }
  getLoggedInSalonID(): string {
    let loggedInsalon_ID = ""
    let _salon_details = localStorage.getItem('salon_Details')
    if (_salon_details == undefined){
      return ""
    }
    let loggedInSalonDetail:Salon = JSON.parse(_salon_details)
    loggedInsalon_ID = loggedInSalonDetail.sal_id
    if (loggedInsalon_ID == undefined){
      return ""
    }else{
     return loggedInsalon_ID
    }
  }
  showProgress(){
    console.log('loading');
  
    this.loading.present();
  }
  stopProgress(){
    console.log('dismissing');
    
    this.loading.dismiss();
  }

  presentToast(message, color) {
   let cssClass="customToastClassz";
    if(color==0){
      cssClass="failureToastClass";
    }else{
      cssClass="customToastClassz";
    }
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: "top",
      dismissOnPageChange: false,
      cssClass: cssClass
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();

  }

  setInLocalStorage(key: string, Value: any) {
    localStorage.setItem(key, JSON.stringify(Value))
  }
  getFromLocalStorage(key: string): any {
    const obj = localStorage.getItem(key)
    if (obj === undefined) {
      return ''
    }
    return JSON.parse(obj)
  }

}
