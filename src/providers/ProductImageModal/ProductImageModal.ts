import { PRODCUT_IMAGES } from '../SalonAppUser-Interface';
import { Platform } from 'ionic-angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { GlobalProvider } from '../global/global';
import { BehaviorSubject } from "rxjs";
import { Storage } from "@ionic/storage";

@Injectable()
export class ProductImageModal {

  /* Global Vairables */
  database: SQLiteObject
  private databaseReady: BehaviorSubject<boolean>;


  constructor(
    public http: HttpClient,
    private sqlite: SQLite,
    public sQLitePorter: SQLitePorter,
    public storage: Storage,
    public platform: Platform,
    public serviceManager: GlobalProvider, ) {
    this.databaseReady = new BehaviorSubject(false)
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;
          console.log('database is setup');
          this.databaseReady.next(true)

        }, err => {
          console.log('database is nullll');

        })
    });
  }

  getDatabaseState() {
    return this.databaseReady.asObservable()
  }

  createProductImagesTable() {
    console.log('came in createProductImagesTable Modal');
    let createTableQuery = "CREATE TABLE IF NOT EXISTS   `product_images` (`pi_id` int(11),`pi_name` varchar(250),`pi_caption` varchar(250),`pi_datetime` text,`p_id` int(11),`pi_created_at` text,`pi_modified_date` text)"
    let isTableCreated = false
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        isTableCreated = true
        console.log('product_images table created');
        
        return res
      })
      .catch(e => {
        let msg:string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)  
        }
        

        console.log('error: InsertInToProductImagesTable ' + e.message);
        let errorMessage: string = e.message
        if (errorMessage.trim().toLowerCase() === 'table product_images already exists'.trim().toLowerCase()) {
          isTableCreated = true
          return isTableCreated
        } else {
          isTableCreated = false
          return isTableCreated
        }
      });
  }

  InsertInToProductImagesTable(ProductImagesData) {
    if (!ProductImagesData || ProductImagesData.length === 0) {
      return 
    }
    console.log('insertionTest: ' + ProductImagesData.length + ' product_images were sent for dumping');

    let firstPart = "INSERT INTO `product_images` (`pi_id`, `pi_name`, `pi_caption`, `pi_datetime`, `p_id`, `pi_created_at`, `pi_modified_date`) VALUES ";
    let secondPart = ""
    console.log('ProductImagesData', ProductImagesData);
    let pi_ids = ""
    
    ProductImagesData.forEach(element => {
      let section: PRODCUT_IMAGES = element

        secondPart += '("' + section.pi_id + '", "' + section.pi_name + '", "' + section.pi_caption + '", "' + section.pi_datetime + '" , "' + section.p_id + '", "' + section.pi_created_at + '", "' + section.pi_modified_date + '"),';

        pi_ids += section.pi_id + ','
    });

    let finalQuery = firstPart + secondPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    pi_ids = pi_ids.slice(0, pi_ids.length - 1)

    console.log("sectionIDs", pi_ids)

    return this.deleteMultipleSections(pi_ids).then(tblProductImageDeleted => {
      let isDataInserted = false
      return this.database.executeSql(slicedQuery, [])
        .then(res => {
          console.log('insertionTest: ' + res.rowsAffected + ' product_images were dumped');
          res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false
          return isDataInserted
        })
        .catch(e => {
          console.log(e.message)
          this.serviceManager.sendErrorToServer(e.message, slicedQuery)  
          isDataInserted = false
          return e.message
        });

    })
  }

  getProductImage(p_id) {
    let Inserquery = "select * from product_images where p_id ="+p_id
    let ProductImages = []
    return this.database.executeSql(Inserquery, [])
      .then(ProductImagesArray => {
        for (var i = 0; i < ProductImagesArray.rows.length; i++) {
          let ProductImage = ProductImagesArray.rows.item(i)
          ProductImages.push(ProductImage)
        }
        return ProductImages
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Inserquery)  
        console.log('error:  ProductImages ' + e.message);

        return ProductImages

      });
  }

  TruncCateProductImagesTable() {
    let deleteQuery = "DELETE FROM `product_images` ;"

    let isDeleted = false
    
    return this.database.executeSql(deleteQuery, [])
      .then(res => {
        

        isDeleted = true
        return isDeleted
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteQuery)  
        console.log('error: product_images');

        isDeleted = false
        return e.message

      });
  }

  deleteMultipleSections(pi_ids) {

    let deleteSelectedQuery = 'DELETE FROM `product_images`  where pi_id IN (' + pi_ids + ');'
    return this.database.executeSql(deleteSelectedQuery, [])
      .then(res => {
        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery)  
        return false;
      });
  }

}
