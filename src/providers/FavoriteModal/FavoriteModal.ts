import { Customer, Posts, PINTEREST_OBJECT , Allfavorite} from '../SalonAppUser-Interface';
import { Platform } from 'ionic-angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { GlobalProvider } from '../global/global';
import { BehaviorSubject } from "rxjs/Rx";
import { Storage } from "@ionic/storage";

@Injectable()
export class FavoriteModal {

  database: SQLiteObject
  private databaseReady: BehaviorSubject<boolean>;


  constructor(
    public http: HttpClient,
    private sqlite: SQLite,
    public sQLitePorter: SQLitePorter,
    public storage: Storage,
    public platform: Platform,
    public SM: GlobalProvider,
    public serviceManager: GlobalProvider,
  ) {
    console.log('Hello FavoritePostProvider Provider');


    this.databaseReady = new BehaviorSubject(false)
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;
          console.log('database is setup');
          this.databaseReady.next(true)

        }, err => {
          console.log('database is nullll');

        })
    });
  }
  getDatabaseState() {
    return this.databaseReady.asObservable()
  }
  createPostTable() {
                                                        
    let createTableQuery = "CREATE TABLE IF NOT EXISTS  `posts` (`ip_id` int(11),`imageUrl` varchar(1000) DEFAULT NULL,`p_keywords` varchar(255) DEFAULT NULL,`ssc_id` int(4)  DEFAULT NULL,`p_setas_editor_img` int(10) DEFAULT '0',`favpost` tinyint(1)  NOT NULL DEFAULT 0 );"

    let isTableCreated = false
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        isTableCreated = true
        console.log('success: posts table created');
        return isTableCreated
      })
      .catch(e => {
        let msg:string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)  
        }
        console.log('error: in favroites'+e.message);
        
        let errorMessage: string = e.message
        if (errorMessage.trim().toLowerCase() === 'table `posts` already exists'.trim().toLowerCase()) {
          isTableCreated = true
          return isTableCreated
        } else {
          console.log('error: pinterest_pins' + e.message);
          isTableCreated = false
          return isTableCreated
        }



      });

  }
  InsertInTopostsTable(post: PINTEREST_OBJECT) {

    let basePart = "INSERT INTO `posts` (`ip_id`, `imageUrl`,`p_keywords`, `ssc_id`,`p_setas_editor_img`,`favpost`) VALUES"

    let valuesPart = '("'
      + post.ip_id + '", "'
      + post.imageUrl + '", "'
      + post.p_keywords + '", "'
      + post.ssc_id + '", "'
      + post.p_setas_editor_img + '", "'
      + post.favpost + '")';

    let finalQuery = basePart + valuesPart
    console.log('insert query');
    console.log(finalQuery);

    let isDataInserted = false
    return this.database.executeSql(finalQuery, [])
      .then(res => {
        console.log('success: insert in `posts` '+res.rowsAffected);
        
        res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false
        return isDataInserted
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, finalQuery)  
        isDataInserted = false
        return e.message

      });
  }

  InsertInToFavMulTable(favItem){
    let valuesPart = ''
    let basePart = "INSERT INTO `all_favorites` (`cf_id`, `cft_id`, `fav_id` ) VALUES"
    favItem.forEach(post => {
      + post.cf_id + '", "'
      + post.cft_id + '", "'
      + post.fav_id + '")';
    });
    favItem.forEach(post => {
      valuesPart += '("' + post.cf_id + '", "' + post.cft_id + '", "'   + post.fav_id + '"),';  
    });
    valuesPart = valuesPart.slice(0,valuesPart.length-1)
    let finalQuery = basePart + valuesPart
    console.log(finalQuery);
    console.log('insert_query');
    console.log(finalQuery);
    let isDataInserted = false
    return this.database.executeSql(finalQuery, [])
      .then(res => {
        console.log('success: insert in `favorite` '+res.rowsAffected);
        res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false
        return isDataInserted
      })
      .catch(e => {
        console.log('Errr',e.message)
        this.serviceManager.sendErrorToServer(e.message, finalQuery)  
        isDataInserted = false
        return e.message
      });

  }
  
  InsertBulkPostsInTable(posts: PINTEREST_OBJECT[]) {

    let valuesPart = ''
    let basePart = "INSERT INTO `posts` (`ip_id`, `imageUrl`,`p_keywords`, `ssc_id`,`p_setas_editor_img`,`favpost`) VALUES"
    posts.forEach(post => {
      valuesPart += '("' + post.ip_id + '", "' + post.imageUrl + '", "' + post.p_keywords + '", "' + post.ssc_id + '" , "' + post.p_setas_editor_img + '", "' + post.favpost + '"),';  
    });

    valuesPart = valuesPart.slice(0,valuesPart.length-1)
    let finalQuery = basePart + valuesPart
    console.log('insert query Posts');
    console.log(finalQuery);

    let isDataInserted = false
    return this.database.executeSql(finalQuery, [])
      .then(res => {
        console.log('success: posts Inserted '+res.rowsAffected);
        
        res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false
        return isDataInserted
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, finalQuery)  
        isDataInserted = false
        // this.SM.makeToastOnFailure(e.message,'top')
        return e.message

      });
  }

  getAllFavoritePosts() {
    let myFavoritePosts = []
    let selectQuery = "SELECT * FROM `posts`;"
    let mypost: PINTEREST_OBJECT
    return this.database.executeSql(selectQuery, [])
      .then(res => {
        
        for (var i = 0; i < res.rows.length; i++) {
          let salon: PINTEREST_OBJECT = res.rows.item(i)
          myFavoritePosts.push(salon)
        }
        
        return myFavoritePosts
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, selectQuery)  
        console.log('error: while fetching pinterest_pins '+e.message);

        return myFavoritePosts

      });
  }
  //  
  deletePost(ip_id) {
    let deleteQuery = 'DELETE FROM `posts` where `ip_id` ='+ip_id
    let isDeleted = false
    return this.database.executeSql(deleteQuery, [])
      .then(res => {
        console.log('success: post deleted '+ip_id);
        isDeleted = true
        return isDeleted
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteQuery)  
        isDeleted = false
        return e.message

      });
  }
  dropTable() {

    let deleteSelectedQuery = 'DROP TABLE `posts`;'
    return this.database.executeSql(deleteSelectedQuery, [])
      .then(res => {
        
        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery)  
        
        return false;
      });
  }
  makePostFavorite(pp_id, favUnFav) {
    let queyMakePostFavorite = 'UPDATE `posts` SET favpost =' + favUnFav+ ' where pp_id = '+pp_id;
    console.log('query ' + queyMakePostFavorite);

    return this.database.executeSql(queyMakePostFavorite, [])
      .then(res => {
        console.log(JSON.stringify(res));
        
        console.log('success: marked favorite');

        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, queyMakePostFavorite)  
        // this.SM.makeToastOnFailure(e.message, 'top')
        return false ;
      });
  }

  checkIfFavFashion(type) {
    let selectQuery=''
    selectQuery=  'SELECT *  FROM  `all_favorites`  where cft_id= '+type 
    console.log(selectQuery);
    let isFashionTrends = false
    return this.database.executeSql(selectQuery, [])
      .then(res => {
        console.log('totalFav '+res.rows.length);
        console.log('FavItems',JSON.stringify(res))
        if (res.rows.length > 0) {
          isFashionTrends=true
        }
        return isFashionTrends
      })
      .catch(e => {
        console.log('Errr',e.message)
        this.serviceManager.sendErrorToServer(e.message, selectQuery)  
        isFashionTrends = false
        return e.message
      });

    // return this.database.executeSql(selectQuery, [])
    //   .then(style => {
    //     //  alert('here'+style.rows.length)
    //     if (style.rows.length > 0) {
    //       for (var i = 0; i < style.rows.length; i++) {
    //         let subCategory: FBC_IMAGES = style.rows.item(i)
    //         if(subCategory.fav_id===undefined || subCategory.fav_id===null){
    //           subCategory.fav_id=subCategory.fbci_id
    //         }
    //         console.log('Items',JSON.stringify(subCategory))
    //         categorySubStyle.push(subCategory)
    //       }
    //       return categorySubStyle
    //     } else {
    //       return categorySubStyle
    //     }
    //   })
    //   .catch(e => {
    //     this.serviceManager.sendErrorToServer(e.message, selectQuery)
    //     console.log('error:  ' + e.message);
    //     return categorySubStyle
    //   });
  }

  

  createFavoriteTable() {                                                      
    let createTableQuery = "CREATE TABLE IF NOT EXISTS  `all_favorites` (`cf_id` int(11) DEFAULT NULL, `cft_id` int(11) DEFAULT NULL , `fav_id` int(11) DEFAULT NULL, `brandName`  varchar(64) , `image`  varchar(364) );"
    let isTableCreated = false
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        isTableCreated = true
        console.log('success: favorite table created');
        return isTableCreated
      })
      .catch(e => {
        let msg:string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)  
        }
        console.log('error: in favroites'+e.message);
        let errorMessage: string = e.message
        if (errorMessage.trim().toLowerCase() === 'table `posts` already exists'.trim().toLowerCase()) {
          isTableCreated = true
          return isTableCreated
        } else {
          console.log('error: pinterest_pins' + e.message);
          isTableCreated = false
          return isTableCreated
        }
      });
  }
  InsertInToFavoriteTable(favItem) {
    let basePart = "INSERT INTO `all_favorites` (`cf_id`, `cft_id`, `fav_id` ) VALUES"
    let valuesPart = '("'
      + favItem.cf_id + '", "'
      + favItem.cft_id + '", "'
      + favItem.fav_id + '")';
    let finalQuery = basePart + valuesPart
    console.log('insert_query');
    console.log(finalQuery);
    let isDataInserted = false
    return this.database.executeSql(finalQuery, [])
      .then(res => {
        console.log('success: insert in `favorite` '+res.rowsAffected);
        res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false
        return isDataInserted
      })
      .catch(e => {
        console.log('Errr',e.message)
        this.serviceManager.sendErrorToServer(e.message, finalQuery)  
        isDataInserted = false
        return e.message
      });
  }
  deleteFavorite(fav_id) {
    let deleteQuery = 'DELETE FROM `all_favorites` where `fav_id` ='+fav_id
    let isDeleted = false
    return this.database.executeSql(deleteQuery, [])
      .then(res => {
        console.log('success: post deleted '+fav_id);
        isDeleted = true
        return isDeleted
      })
      .catch(e => {
        console.log('ErrorIn deleting'+e.message)
        this.serviceManager.sendErrorToServer(e.message, deleteQuery)  
        isDeleted = false
        return e.message

      });
  }

}
