import { PRODUCT } from '../SalonAppUser-Interface';
import { Platform } from 'ionic-angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { GlobalProvider } from '../global/global';
import { BehaviorSubject } from "rxjs";
import { Storage } from "@ionic/storage";


@Injectable()
export class ProductModal {


  /* Global Vairables */
  database: SQLiteObject
  private databaseReady: BehaviorSubject<boolean>;
  public productSortingMethods = { "PriceLowToHigh": 1, "PriceHighToLow": 2, "productNameAsc": 3, "productNameDesc": 4, "productNewToOld": 5, "productOldToNew": 6 }

  constructor(
    public http: HttpClient,
    private sqlite: SQLite,
    public sQLitePorter: SQLitePorter,
    public storage: Storage,
    public platform: Platform,
    public serviceManager: GlobalProvider, ) {
    this.databaseReady = new BehaviorSubject(false)
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;
          console.log('database is setup');
          this.databaseReady.next(true)

        }, err => {
          console.log('database is nullll');

        })
    });
  }

  getDatabaseState() {
    return this.databaseReady.asObservable()
  }

  async createProductTable() {

    let createTableQuery = "CREATE TABLE IF NOT EXISTS  `products` (`p_id` int(11) ,`p_name` varchar(250) ,`p_description` text ,`p_image` varchar(250) ,`p_status` int(11) ,`p_cost` float ,`p_rrp` float ,`p_price` float ,`p_weight` varchar(100) ,`p_dimention` varchar(250) ,`pc_id` int(11) ,`pb_id` int(11) ,`p_created_at` text ,`p_modified_date` text, `isAddedtoCart` tinyint(1), `pb_name` text, `is_featured` tinyint(1) ) "
    let isTableCreated = false
    try {
      const res = await this.database.executeSql(createTableQuery, []);
      isTableCreated = true;
      console.log('products table created');
      return res;
    }
    catch (e) {
      let msg: string = e.message;
      if (!msg.includes('already exists')) {
        this.serviceManager.sendErrorToServer(e.message, createTableQuery);
      }
      console.log('error: InsertInToProductTable ' + e.message);
      let errorMessage: string = e.message;
      if (errorMessage.trim().toLowerCase() === 'table products already exists'.trim().toLowerCase()) {
        isTableCreated = true;
        return isTableCreated;
      }
      else {
        isTableCreated = false;
        return isTableCreated;
      }
    }
  }

  async InsertInToProductTable(ProductData: PRODUCT[]) {
    if (!ProductData || ProductData.length === 0) {
      return
    }

    let firstPart = 'INSERT INTO `products` (`p_id`, `p_name`, `p_description`, `p_image`, `p_status`, `p_cost`, `p_rrp`, `p_price`, `p_weight`, `p_dimention`, `pc_id`, `pb_id`, `p_created_at`, `p_modified_date`,`isAddedtoCart`, `pb_name`,`is_featured`) VALUES';
    let secondPart = ""

    let ProductIds = ""
    let zeroValue = 0
    console.log('insertionTest: ' + ProductData.length + ' Product were sent for dumping');

    ProductData.forEach(product => {
      secondPart += '("' + product.p_id + '", "' + product.p_name + '", "' + product.p_description.replace(/"/g, '').replace(/'/g, '') + '", "' + product.p_image + '" , "' + product.p_status + '", "' + Math.floor(product.p_cost) + '" , "' + Math.floor(product.p_rrp) + '", "' + Math.floor(product.p_price) + '", "' + product.p_weight + '", "' + product.p_dimention + '", "' + product.pc_id + '", "' + product.pb_id + '", "' + product.p_created_at + '", "' + product.p_modified_date + '", "' + zeroValue + '", "' + product.pb_name + '", "' + Number(product.is_featured) + '" ),';
      ProductIds += product.p_id + ','


    });

    let finalQuery = firstPart + secondPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    // console.log(slicedQuery);

    ProductIds = ProductIds.slice(0, ProductIds.length - 1)



    try {
      await this.delteDuplicateProduct(ProductIds);
      let isDataInserted = false;
      try {
        const res = await this.database.executeSql(slicedQuery, []);
        console.log('insertionTest: ' + res.rowsAffected + ' products were dumped');
        res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false;
        return isDataInserted;
      }
      catch (e) {
        this.serviceManager.sendErrorToServer(e.message, slicedQuery);
        isDataInserted = false;
        return e.message;
      }
    }
    catch (e_1) {
      this.serviceManager.sendErrorToServer(e_1.message, slicedQuery);
      return e_1.message;
    }
  }
  async getProductsForCategory(btc_id: string | number, lstProductID: string | number, start_at: string | number, orderBy: string) {
    let Inserquery = ''

    Inserquery = 'SELECT p.*, pc_name FROM `products` p, `product_categories` pc where p.pc_id = pc.pc_id AND  p.pc_id = ' + btc_id + '  order by ' + orderBy + '  limit ' + start_at + ', 8 '

    let beautyTips = []
    console.log(Inserquery);


    try {
      const tipsDetailDataSource = await this.database.executeSql(Inserquery, []);
      for (var i = 0; i < tipsDetailDataSource.rows.length; i++) {
        beautyTips.push(tipsDetailDataSource.rows.item(i));
      }
      console.log(beautyTips.length);
      return beautyTips;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, Inserquery);
      console.log('error:  tipsDetailDataSource ' + e.message);
      return beautyTips;
    }
  }
  async getProductsForBrand(pb_id: string | number, lstProductID: string | number, start_at: string | number, orderBy: string) {
    let Inserquery = ''

    Inserquery = 'SELECT p.*, pc_name FROM `products` p, `product_categories` pc where p.pc_id = pc.pc_id AND  p.pb_id = ' + pb_id + '   order by ' + orderBy + '  limit ' + start_at + ', 8 '
    let beautyTips = []
    console.log(Inserquery);


    try {
      const tipsDetailDataSource = await this.database.executeSql(Inserquery, []);
      for (var i = 0; i < tipsDetailDataSource.rows.length; i++) {
        beautyTips.push(tipsDetailDataSource.rows.item(i));
      }
      console.log(beautyTips.length);
      return beautyTips;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, Inserquery);
      console.log('error:  tipsDetailDataSource ' + e.message);
      return beautyTips;
    }
  }

  async getProductsByCategory(pc_id: string, lstProductID: string | number, start_at: string | number, orderBy: string) {
    let Inserquery = ''
    Inserquery = 'SELECT p.*, pc_name FROM `products` p, `product_categories` pc where p.pc_id = pc.pc_id AND  p.`pc_id` = ' + pc_id + '   order by ' + orderBy + '  limit ' + start_at + ', 8 '
    let beautyTips = []
    console.log(Inserquery);


    try {
      const tipsDetailDataSource = await this.database.executeSql(Inserquery, []);
      for (var i = 0; i < tipsDetailDataSource.rows.length; i++) {
        beautyTips.push(tipsDetailDataSource.rows.item(i));
      }
      console.log(beautyTips.length);
      return beautyTips;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, Inserquery);
      console.log('error:  tipsDetailDataSource ' + e.message);
      return beautyTips;
    }
  }
  async getProductDetail(p_id: string | number) {

    let Inserquery = 'SELECT p.*, pc_name FROM `products` p, `product_categories` pc where p.pc_id = pc.pc_id AND  p.p_id = ' + p_id
    let product: PRODUCT


    try {
      const products = await this.database.executeSql(Inserquery, []);
      if (products.rows.length > 0) {
        product = products.rows.item(0);
      }
      return product;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, Inserquery);
      console.log('error:  tipsDetailDataSource ' + e.message);
      return product;
    }
  }
  async getAllProducts(lstProductID, start_at: string | number, orderBy: string) {

    let Inserquery = ''

    Inserquery = 'SELECT p.*, pc_name FROM `products` p, `product_categories` pc where p.pc_id = pc.pc_id order by ' + orderBy + '  limit ' + start_at + ', 8 '
    console.log(Inserquery);

    let beautyTips = []

    try {
      console.log(Inserquery)
      const tipsDetailDataSource = await this.database.executeSql(Inserquery, []);
      for (var i = 0; i < tipsDetailDataSource.rows.length; i++) {
        beautyTips.push(tipsDetailDataSource.rows.item(i));

      }
      return beautyTips;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, Inserquery);
      console.log('error:  tipsDetailDataSource ' + e.message);
      return beautyTips;
    }
  }

  async searchProduct(productName: string) {

    // let Inserquery = 'select * from `products` where p_name like "%' + productName + '%" LIMIT 10'
    let Inserquery = 'select `p_id`, `p_name` , 1 AS p_type, pc_name from `products` as p, `product_categories` as pc where pc.pc_id = p.pc_id AND  p.p_name  like "%' + productName + '%" UNION select `pc_id` as p_id, `pc_name` as p_name, 0 AS p_type,pc_name  from `product_categories` pc1 where `pc1`.`pc_name`  like "%' + productName + '%" ORDER BY p_type  LIMIT 10'
    let products = []

    try {
      const tipsDetailDataSource = await this.database.executeSql(Inserquery, []);

      for (var i = 0; i < tipsDetailDataSource.rows.length; i++) {
        products.push(tipsDetailDataSource.rows.item(i));
      }
      return products;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, Inserquery);
      console.log('error:  tipsDetailDataSource ' + e.message);
      return products;
    }
  }
  async getMaxProductID() {
    let Query = 'select count(`p_id`) as maxID from `products` '
    let maxID = 0
    return this.database.executeSql(Query, []).then(res => {

      if (res.rows.item(0).maxID) maxID = res.rows.item(0).maxID
      return maxID
    }, e => {
      this.serviceManager.sendErrorToServer(e.message, Query);
      console.log('error:  tipsDetailDataSource ' + e.message);
      return maxID;
    })
  }
  async searchProductForLazyLoad(productName: string, lstProductID, start_at: string | number, orderBy: string) {


    let Inserquery = ''
    Inserquery = 'SELECT p.*, pc_name FROM `products` p, `product_categories` pc where p.pc_id = pc.pc_id AND  p.p_name  like "%' + productName + '%"   order by ' + orderBy + '  limit ' + start_at + ', 8 '
    let products = []
    console.log(Inserquery);

    try {
      const tipsDetailDataSource = await this.database.executeSql(Inserquery, []);

      for (var i = 0; i < tipsDetailDataSource.rows.length; i++) {
        products.push(tipsDetailDataSource.rows.item(i));
      }
      return products;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, Inserquery);
      console.log('error:  tipsDetailDataSource ' + e.message);
      return products;
    }
  }

  async updateProductTable(product: PRODUCT) {
    let updateQuery = ''
    updateQuery = "UPDATE `products` SET `isAddedtoCart` = " + Number(product.isAddedtoCart) + "  where p_id = " + product.p_id
    try {
      await this.database.executeSql(updateQuery, []).then(res => {

      }).catch(e => {

        this.serviceManager.sendErrorToServer(e.message, updateQuery);
        console.log('error: ' + e.message);
      });
      console.log('success: Cart updated');
      return true;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, updateQuery);
      console.log('error: ' + e.message);
      return false;
    }
  }

  async TruncateProductTable() {
    let deleteQuery = "DELETE FROM `products` ;"
    let isDeleted = false

    try {
      const res = await this.database.executeSql(deleteQuery, []);
      isDeleted = true;
      return isDeleted;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, deleteQuery);
      console.log('error: products');
      isDeleted = false;
      return e.message;
    }
  }

  async delteDuplicateProduct(ProductIds) {

    if (!ProductIds) {
      return
    }
    let deleteSelectedQuery = 'DELETE FROM `products`  where p_id IN (' + ProductIds + ');'
    try {
      const res = await this.database.executeSql(deleteSelectedQuery, []);

      return true;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery);
      return false;
    }
  }
  async getProductsForReorder(ProductIds: string) {

    if (!ProductIds || ProductIds.trim().length === 0) {
      return
    }
    let products: PRODUCT[] = []
    let query = 'SELECT p.*, pc_name FROM `products` p, `product_categories` pc where p.pc_id = pc.pc_id and p.p_id IN (' + ProductIds + ')'
    let deleteSelectedQuery = query
    console.log(query);

    try {
      const res = await this.database.executeSql(deleteSelectedQuery, []).then(tipsDetailDataSource => {
        for (var i = 0; i < tipsDetailDataSource.rows.length; i++) {
          products.push(tipsDetailDataSource.rows.item(i))
        }
        console.log(products.length);

      })

      return products
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery);
      return products
    }
  }

  async removeCartItemsAfterOrderPlaced() {
    let updateQuery = "UPDATE `products` SET `isAddedtoCart` =  0"


    try {
      const res = await this.database.executeSql(updateQuery, []);
      console.log('success: customer updated');
      return true;
    }
    catch (e) {
      this.serviceManager.sendErrorToServer(e.message, updateQuery);
      console.log('error: ' + e.message);
      return false;
    }
  }

}
