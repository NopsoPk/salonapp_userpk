import { PRODUCT_COLOR } from '../SalonAppUser-Interface';
import { Platform } from 'ionic-angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { GlobalProvider } from '../global/global';
import { BehaviorSubject } from "rxjs";
import { Storage } from "@ionic/storage";


@Injectable()
export class ProductColorModal {
  /* Global Vairables */
  database: SQLiteObject
  private databaseReady: BehaviorSubject<boolean>;


  constructor(
    public http: HttpClient,
    private sqlite: SQLite,
    public sQLitePorter: SQLitePorter,
    public storage: Storage,
    public platform: Platform,
    public serviceManager: GlobalProvider, ) {
    this.databaseReady = new BehaviorSubject(false)
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;
          console.log('database is setup');
          this.databaseReady.next(true)

        }, err => {
          console.log('database is nullll');

        })
    });
  }

  getDatabaseState() {
    return this.databaseReady.asObservable()
  }

  createProdoductColorTable() {
    console.log('came in createProdoductColorTable Modal');
    let createTableQuery = "CREATE TABLE IF NOT EXISTS  `product_colors` (`pcl_id` int(11),`pcl_image` varchar(250),`pcl_value` varchar(200),`pcl_code` varchar(100),`p_id` int(11),`pcl_created_at` text,`pcl_modified_date` text) ;"
    let isTableCreated = false
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        isTableCreated = true
        console.log('product_colors table created');
        
        return res
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }


        console.log('error: InsertInToProductColorTable ' + e.message);
        let errorMessage: string = e.message
        if (errorMessage.trim().toLowerCase() === 'table product_colors already exists'.trim().toLowerCase()) {
          isTableCreated = true
          return isTableCreated
        } else {
          isTableCreated = false
          return isTableCreated
        }
      });
  }

  InsertInToProductColorTable(productColorData) {
    if (!productColorData || productColorData.length === 0) {
      return
    }
    console.log('insertionTest: ' + productColorData.length + ' product_colors were sent for dumping');

    let firstPart = "INSERT INTO `product_colors` (`pcl_id`, `pcl_image`, `pcl_value`, `pcl_code`, `p_id`, `pcl_created_at`, `pcl_modified_date`) VALUES ";
    let secondPart = ""
    console.log('productColorData', productColorData);
    let sectionIDs = ""
    productColorData.forEach(element => {
      let section: PRODUCT_COLOR = element
      secondPart += '("' + section.pcl_id + '", "' + section.pcl_image + '", "' + section.pcl_value + '", "' + section.pcl_code + '" , "' + section.p_id + '", "' + section.pcl_created_at + '", "' + section.pcl_modified_date + '"),';
      sectionIDs += section.pcl_id + ','
    });

    let finalQuery = firstPart + secondPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    sectionIDs = sectionIDs.slice(0, sectionIDs.length - 1)

    console.log("sectionIDs", sectionIDs)

    return this.deleteMultipleSections(sectionIDs).then(tblHomeDeleted => {
      let isDataInserted = false
      return this.database.executeSql(slicedQuery, [])
        .then(res => {
          console.log('insertionTest: ' + res.rowsAffected + ' product_colors were dumped');
          res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false
          return isDataInserted
        })
        .catch(e => {
          this.serviceManager.sendErrorToServer(e.message, slicedQuery)
          isDataInserted = false
          return e.message
        });

    })
  }

  getProductColors(p_id) {
    let Inserquery = "select * from `product_colors` where p_id = "+p_id
    let productColors = []
    return this.database.executeSql(Inserquery, [])
      .then(productColorsArray => {
        for (var i = 0; i < productColorsArray.rows.length; i++) {
          let productColor = productColorsArray.rows.item(i)
          productColors.push(productColor)
        }
        return productColors
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Inserquery)
        console.log('error:  productColors ' + e.message);

        return productColors

      });
  }

  TruncProductColorTable() {
    let deleteQuery = "DELETE FROM `product_colors` ;"

    let isDeleted = false
    
    return this.database.executeSql(deleteQuery, [])
      .then(res => {
        

        isDeleted = true
        return isDeleted
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteQuery)
        console.log('error: product_colors');

        isDeleted = false
        return e.message

      });
  }

  deleteMultipleSections(salIds) {

    let deleteSelectedQuery = 'DELETE FROM `product_colors`  where pcl_id IN (' + salIds + ');'
    return this.database.executeSql(deleteSelectedQuery, [])
      .then(res => {
        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery)
        return false;
      });
  }

}
