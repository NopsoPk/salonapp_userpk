import { FavoriteModal } from "../providers/FavoriteModal/FavoriteModal";
import { CustomerModal } from '../providers/CustomerModal/CustomerModal';
import { HomeModal } from '../providers/homeModal/homeModal';
import { SqliteDbProvider } from '../providers/sqlite-db/sqlite-db';
import { SalonServicesModal } from '../providers/salon-services-modal/salon-services-modal';


import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { LatestFashionTrendPage } from '../pages/latest-fashion-trend/latest-fashion-trend'

import { HomePageDummyPage } from '../pages/home-page-dummy/home-page-dummy'

import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { FormsModule } from '@angular/forms';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler, Nav } from 'ionic-angular';
import { MyApp } from './app.component';
import { SQLite } from '@ionic-native/sqlite';
import { HttpModule } from "@angular/http"
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { AndroidPermissions} from '@ionic-native/android-permissions';
import { LazyLoadImageModule } from 'ng-lazyload-image';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';



import { IonicStorageModule } from '@ionic/storage';
import { DatePipe } from '@angular/common'
// import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import { Ionic2RatingModule } from 'ionic2-rating';
import { CosntantsProvider } from '../providers/cosntants/cosntants';
import { File } from '@ionic-native/file';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { GoogleAnalytics } from '@ionic-native/google-analytics';





import { EmailComposer } from '@ionic-native/email-composer';
import { Base64 } from '@ionic-native/base64';
import { FCM } from '@ionic-native/fcm';
import { CalendarComponent } from '../components/calendar/calendar'
import { TabsPage } from '../pages/tabs/tabs';
import { MainHomePage } from '../pages/main-home/main-home'
import { LoginSignUpPage } from '../pages/login-sign-up/login-sign-up'
import { RequestAppointmentPage } from '../pages/request-appointment/request-appointment';
import { PinActionSheetPage } from '../pages/pin-action-sheet/pin-action-sheet';
import { FavouriteFashionsTrendsPage } from '../pages/favourite-fashions-trends/favourite-fashions-trends';
import { HomePage } from '../pages/home/home';
import { AppointmentsPage } from '../pages/appointments/appointments';
import { AddAppointmentPage } from '../pages/add-appointment/add-appointment'
import { NotificationsPage } from '../pages/notifications/notifications';
import { ProfilePage } from '../pages/profile/profile';
import { TermsAndConditionsPage } from '../pages/terms-and-conditions/terms-and-conditions';
import { FeedbackPage } from '../pages/feedback/feedback';
import { PhoneNumberAuthenticationPage } from '../pages/phone-number-authentication/phone-number-authentication'
import { ActivateSalonPage } from '../pages/activate-salon/activate-salon'
import { SingUpPage } from '../pages/sing-up/sing-up'
import { BeautyTipsViewPage } from '../pages/beauty-tips-view/beauty-tips-view';
import { LatestTrendsPage } from '../pages/latest-trends/latest-trends'

import { CongratsreditsPage } from '../pages/congratsredits/congratsredits'

import { FavouriteBeautyTrendsPage } from '../pages/favourite-beauty-trends/favourite-beauty-trends'

import { LatestTrendsListingPage } from '../pages/latest-trends-listing/latest-trends-listing'
import { LatestTrendsArchiveCollectionPage } from './../pages/latest-trends-archive-collection/latest-trends-archive-collection';
import { LatestTrendsDetailPage } from './../pages/latest-trends-detail/latest-trends-detail';
import { LatestFashionBrandDetailsPage } from './../pages/latest-fashion-brand-details/latest-fashion-brand-details';
import { LatestFashionCollectionDetailPage } from './../pages/latest-fashion-collection-detail/latest-fashion-collection-detail';
import { LatestTrensTipsCategoryPage } from '../pages/latest-trens-tips-category/latest-trens-tips-category'
import { LatestTrensTipsDetailsPage } from '../pages/latest-trens-tips-details/latest-trens-tips-details'
import { ArchivesPage } from '../pages/archives/archives'
import { LookingForPage } from '../pages/looking-for/looking-for'
import { SalonDetailsPage } from '../pages/salon-details/salon-details'
import { SlotsViewPage } from '../pages/slots-view/slots-view'
import { ConfirmBookingPage } from '../pages/confirm-booking/confirm-booking';
import { ThanksVcPage } from '../pages/thanks-vc/thanks-vc'
import { MenuPage } from '../pages/menu/menu';
import { PrivacyPolicyPage } from '../pages/privacy-policy/privacy-policy';
import { SalonReviewsPage } from '../pages/salon-reviews/salon-reviews'
import { SalonAllServicesPage } from '../pages/salon-all-services/salon-all-services'
import { LatestTrendSubCatPage } from '../pages/latest-trend-sub-cat/latest-trend-sub-cat'
import { InActiveSalonPage } from '../pages/in-active-salon/in-active-salon'
import { RateAppointmentPage } from '../pages/rate-appointment/rate-appointment'
import { FavoriteSalonsPage } from "../pages/favorite-salons/favorite-salons";
import { SalonBeautyTrendsPage } from './../pages/salon-beauty-trends/salon-beauty-trends';
// import {BeautyTipsCategoryPage} from '../pages/trends-tips-category/trends-tips-category'
// import {BeautyTipsCategoryDetailsPage} from '../pages/trends-tips-category-details/trends-tips-category-details'


import { AllOffersPage } from '../pages/all-offers/all-offers';
import { GlobalServiceProvider } from '../providers/global-service/global-service';

import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Device } from '@ionic-native/device';
import { AppVersion } from '@ionic-native/app-version';
import { Keyboard } from '@ionic-native/keyboard';

import { DatePickerModule } from 'datepicker-ionic2';
import { MultiPickerModule } from 'ion-multi-picker';


//ionic cordova plugin add com-badrit-base64
//npm install --save @ionic-native/base64
import { from } from 'rxjs/observable/from';
import { GlobalProvider } from '../providers/global/global';

import { SubcategoriesPopOverPage } from "../pages/subcategories-pop-over/subcategories-pop-over";
import { ExpandableHeader } from '../components/expandable-header/expandable-header';
import { ExpandableComponent } from "../components/expandable/expandable";
import { IonicImageViewerModule } from 'ionic-img-viewer';

import { AppRate } from '@ionic-native/app-rate';
import { SalonCardComponent } from "../components/salon-card/salon-card";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ConfigModal } from "../providers/ConfigModal/ConfigModal";
import { BeautyTipsDetailPage } from "../pages/beauty-tips-detail/beauty-tips-detail";
import { BeautyTipsModal } from '../providers/BeautyTipsModal/BeautyTipsModal';
import { TipsDetailModal } from '../providers/TipsDetailModal/TipsDetailModal';
import { SafeHtmlPipe } from "../pipes/safe-html/safe-html";


import { AppointmentsCardPage } from '../pages/appointments-card/appointments-card'
import { AppointmentCardModel } from '../providers/AppointmentCardModel/AppointmentCardModel';
import { NopsoHideHeaderDirective } from "../directives/nopso-hide-header/nopso-hide-header";
import { ProductCategoryModal } from '../providers/ProductCategoryModal/ProductCategoryModal';
import { ProductModal } from '../providers/ProductModal/ProductModal';
import { ProductCategoriesPage } from "../pages/product-categories/product-categories";
import { ProductsPage } from "../pages/products/products";
import { ProductImageModal } from '../providers/ProductImageModal/ProductImageModal';
import { ProductColorModal } from '../providers/ProductColorModal/ProductColorModal';
import { ViewCartPage } from "../pages/view-cart/view-cart";
import { ShippingInfoPage } from "../pages/shipping-info/shipping-info";
import { ReviewOrderPage } from "../pages/review-order/review-order";
import { MyOrdersPage } from "../pages/my-orders/my-orders";
import { OrderDetailPage } from "../pages/order-detail/order-detail";
import { Diagnostic } from "@ionic-native/diagnostic";
import { ShippingAddressModal } from '../providers/ShippingAddressModal/ShippingAddressModal';
import { ProductdetailPage } from "../pages/productdetail/productdetail";
import { SocialSharing } from '@ionic-native/social-sharing';
import { PaymentMethodPage } from "../pages/payment-method/payment-method";
import { VisaCardPaymentMethodPage } from "../pages/visa-card-payment-method/visa-card-payment-method";
import { ThankYouPage } from "../pages/thank-you/thank-you";
import { CartModal } from "../providers/cartModal/cartModal";

import { WheelSelector } from '@ionic-native/wheel-selector';
import { FashionBrandModal } from "../providers/FashionBrandModal/FashionBrandModal";
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Network } from '@ionic-native/network';
import { ProductBrandModal } from '../providers/ProductBrandModal/ProductBrandModal';
import { TestCodePage } from "../pages/test-code/test-code";
import { PromotionModal } from '../providers/PromotionModal/PromotionModal';
import { PromotionsPage } from '../pages/promotions/promotions';
import { ParticlesProvider } from '../providers/particles/particles';
import { AppInfoPage } from '../pages/app-info/app-info';
import { PromotionDetailPage } from '../pages/promotion-detail/promotion-detail';
import { ProductSortingPage } from "../pages/product-sorting/product-sorting";
import { ForceUpdatePage } from "../pages/force-update/force-update";



@NgModule({
  declarations: [
    FavouriteBeautyTrendsPage,
    MyApp,
    MainHomePage,
    LoginSignUpPage,
    BeautyTipsViewPage,
    NopsoHideHeaderDirective,
    // BeautyTipsCategoryPage,
    // BeautyTipsCategoryDetailsPage,
    LatestTrendsPage,
    CongratsreditsPage,
    LatestTrendsArchiveCollectionPage,
    LatestTrendsDetailPage,
    LatestFashionBrandDetailsPage,
    LatestFashionCollectionDetailPage,
    LatestTrendsListingPage,
    LatestTrensTipsCategoryPage,
    LatestTrensTipsDetailsPage,
    FavouriteFashionsTrendsPage,
    ArchivesPage,
    SalonDetailsPage,
    LookingForPage,
    AppointmentsPage,
    AddAppointmentPage,
    SlotsViewPage,
    ConfirmBookingPage,
    ThanksVcPage,
    NotificationsPage,
    ProfilePage,
    HomePage,
    TabsPage,
    PhoneNumberAuthenticationPage,
    ActivateSalonPage,
    InActiveSalonPage,
    SingUpPage,
    MenuPage,
    PrivacyPolicyPage,
    TermsAndConditionsPage,
    FeedbackPage,
    
    LatestTrendSubCatPage,
    FeedbackPage, SalonReviewsPage,
    SalonAllServicesPage,
    RateAppointmentPage,
    PinActionSheetPage,
    RequestAppointmentPage,
    FavoriteSalonsPage,
    SubcategoriesPopOverPage,
    ExpandableComponent,
    SalonCardComponent,
    CalendarComponent,
    ExpandableHeader,
    AllOffersPage,
    
    BeautyTipsDetailPage,
    SafeHtmlPipe,
    AppointmentsCardPage,
    ProductCategoriesPage,
    ProductsPage,
    ViewCartPage,
    ShippingInfoPage,
    ReviewOrderPage,
    MyOrdersPage,
    OrderDetailPage,
    ProductdetailPage,
    PaymentMethodPage,
    VisaCardPaymentMethodPage,
    ThankYouPage,
    LatestFashionTrendPage,
    HomePageDummyPage,
    SalonBeautyTrendsPage,
    TestCodePage,
    PromotionsPage,
    AppInfoPage,
    PromotionDetailPage,
    ProductSortingPage,
    ForceUpdatePage,

  ],
  imports: [
    IonicImageViewerModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    Ionic2RatingModule,
    DatePickerModule,
    MultiPickerModule,
    LazyLoadImageModule,
    HttpClientModule,
    
    // HighlightModule,
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages: true,
      backButtonText: '',
      backButtonIcon: 'ios-arrow-back',
      iconMode: 'ios'
    }),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    FavouriteBeautyTrendsPage,
    MyApp,
    MainHomePage,
    LoginSignUpPage,
    BeautyTipsViewPage,
    // BeautyTipsCategoryPage,
    // BeautyTipsCategoryDetailsPage,
    LatestTrendsPage,
    CongratsreditsPage,
    FavouriteFashionsTrendsPage,
    LatestTrendsListingPage,
    LatestTrendsArchiveCollectionPage,
    LatestTrendsDetailPage,
    LatestFashionBrandDetailsPage,
    LatestFashionCollectionDetailPage,
    LatestTrensTipsCategoryPage,
    LatestTrensTipsDetailsPage,
    ArchivesPage,
    SalonDetailsPage,
    LookingForPage,
    AppointmentsPage,
    AddAppointmentPage,
    SlotsViewPage,
    ConfirmBookingPage,
    ThanksVcPage,
    NotificationsPage,
    ProfilePage,
    HomePage,
    TabsPage,
    PhoneNumberAuthenticationPage,
    ActivateSalonPage,
    InActiveSalonPage,
    SingUpPage,
    MenuPage,
    PrivacyPolicyPage,
    TermsAndConditionsPage,
    LatestTrendSubCatPage,
    FeedbackPage,

    FeedbackPage, SalonReviewsPage,
    SalonAllServicesPage,
    RateAppointmentPage,
    PinActionSheetPage,
    RequestAppointmentPage,
    FavoriteSalonsPage,
    SubcategoriesPopOverPage,
    AllOffersPage,    
    BeautyTipsDetailPage,
    AppointmentsCardPage,
    ProductCategoriesPage,
    ProductsPage,
    ViewCartPage,
    ShippingInfoPage,
    ReviewOrderPage,
    MyOrdersPage,
    OrderDetailPage,
    ProductdetailPage,
    PaymentMethodPage,
    VisaCardPaymentMethodPage,
    ThankYouPage,
    LatestFashionTrendPage,
    HomePageDummyPage,
    SalonBeautyTrendsPage,
    TestCodePage,
    PromotionsPage,
    AppInfoPage,
    PromotionDetailPage,
    ProductSortingPage,
    ForceUpdatePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SQLite,
    Diagnostic,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    // SpinnerDialog,
    GoogleAnalytics,
    NativePageTransitions,
    CosntantsProvider,
    DatePipe,
    File,
    FileTransfer,
    FilePath,
    Camera,
    FileTransferObject,
    Nav,
    EmailComposer,
    Base64,
    FCM,
    GlobalServiceProvider,
    LocationAccuracy,
    Geolocation,
    Device,
    AppVersion,
    // AdMobFree,
    Keyboard,
    GlobalProvider,
    HttpClient,
    SqliteDbProvider,
    SQLite,
    SQLitePorter,
    SalonServicesModal,
    HomeModal,
    CustomerModal,
    FavoriteModal,
    FavoriteModal,
    AppRate,
    ConfigModal,
    BeautyTipsModal,
    TipsDetailModal,
    AppointmentCardModel,
    ProductCategoryModal,
    ProductModal,
    ProductImageModal,
    ProductColorModal,
    ShippingAddressModal,
    SocialSharing,
    CartModal,

    WheelSelector,
    FashionBrandModal,
    InAppBrowser,
    AndroidPermissions,
    Network,
    ProductBrandModal,
    PromotionModal,
    ParticlesProvider,
  ]
})
export class AppModule { }
