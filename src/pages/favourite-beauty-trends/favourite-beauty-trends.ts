import { SqliteDbProvider } from './../../providers/sqlite-db/sqlite-db';
import {Salon, PINTEREST_OBJECT, Posts, Customer, SalonServices, CATEGORY_SUB_STYLE } from './../../providers/SalonAppUser-Interface';
import { PinActionSheetPage } from './../pin-action-sheet/pin-action-sheet';
import { SalonBeautyTrendsPage } from './../salon-beauty-trends/salon-beauty-trends';
import { MenuController} from 'ionic-angular';
import { Component } from '@angular/core';
import { App, IonicPage, LoadingController, NavController, NavParams, Platform, Alert, Events } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalServiceProvider } from '../../providers/global-service/global-service'
import { Storage } from '@ionic/storage';
import { GlobalProvider } from '../../providers/global/global'
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { config } from "../../providers/config/config";
import { PopoverController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { FavoriteModal } from '../../providers/FavoriteModal/FavoriteModal';
import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { MainHomePage } from '../main-home/main-home';
import { GoogleAnalytics } from '../../../node_modules/@ionic-native/google-analytics';
import { SalonServicesModal } from '../../providers/salon-services-modal/salon-services-modal';
import { Content } from 'ionic-angular';
import {  ViewChild, NgZone } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing';
import {  BEAUTY_TIPS, TIPS_DETAIL, NOPSO_Header_OPTIONS , FBC_IMAGES} from './../../providers/SalonAppUser-Interface';
import { timer } from 'rxjs/observable/timer';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { LoginSignUpPage } from '../login-sign-up/login-sign-up';

/**
 * Generated class for the FavouriteBeautyTrendsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favourite-beauty-trends',
  templateUrl: 'favourite-beauty-trends.html',
})
export class FavouriteBeautyTrendsPage {
 // public selectedCategory
  twoBeautyTips: any = [];
  public collectionName="";
  public slider: HTMLElement
  public nopsoHeaderOptions: NOPSO_Header_OPTIONS
  @ViewChild(Content) content: Content;
  ionScrollTop = 0
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  public unregisterBackButtonAction: any;
  public shouldShowMiniHeader = false
  public categorySubStyle: CATEGORY_SUB_STYLE[]
  public getPostsForKeyWordURL
  public postsArray = Array();
  public noPostsFound = false;
  public clearPostsAfterIntervval = 30 //minutes
  public infiniteScroll: any
  public myCustomer: Customer
  public PaginationLinks: any
  public cateogry = "";
  public isLoginSignUp="";
  public imageUrl = config.CategoryImageURL
  page = 0;
  maximumPages = 20
  public selectedSubCategory
  public selectedPost: PINTEREST_OBJECT
  public imageUrlKeyWord:any //for showing keyword tap search 
  public lastupdate_date=0;
  trendCall = 1
  trendOffset = 0
  trendLimit = 5
  
  public lastFetchDate:any

  constructor(
    public serviceManager: GlobalProvider,
    private menu: MenuController,
    public favModal: FavoriteModal,
    public http: Http,
    private socialSharing: SocialSharing,
    public globalSearch: GlobalServiceProvider,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public platform: Platform,
    public SM: GlobalProvider,
    public popoverCtrl: PopoverController,
    public modalCtrl: ModalController,
    public database: SqliteDbProvider,
    public favPostModal: FavoriteModal,
    public customerModal: CustomerModal,
    public salonServiceModal: SalonServicesModal,
    public events: Events,
    private zone: NgZone,
    private ga: GoogleAnalytics,
    private nativePageTransitions: NativePageTransitions,
  ){
      this.nopsoHeaderOptions = {
        miniHeaderHeight: '50pt',
        calledFrom: config.BeautyTipsViewPage,
      }
     // this.selectedCategory = this.navParams.get('serviceCat');
      this.collectionName='Name'

    
} 

  //custom back button for android
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    // this.navCtrl.pop();
    if(this.menu.isOpen()){
      this.menu.close();
    }else {
       if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
    }

  }
//end custom back button for android

postTaped( event, post){

  event.stopPropagation();

  let ssc_id = post.ssc_id
    let postObject: PINTEREST_OBJECT = post
    this.selectedPost = postObject
    var PinRelatedSalon: Salon[] = []
    let requiredSalonIDs = ''
    let SSCID = -1
    if (this.selectedSubCategory) {
      SSCID = this.selectedSubCategory.ssc_id
    } else {
      SSCID = postObject.ssc_id
    }
    this.database.getDatabaseState().subscribe(ready => {
      if (ready) {

        this.database.getSalSerSubCatFromSqliteDB(ssc_id).then(allSalonIDs => {

          allSalonIDs.forEach(element => {
            requiredSalonIDs += element.sal_id + ',';
          });
          requiredSalonIDs = requiredSalonIDs.slice(0, requiredSalonIDs.length - 1)
          if (requiredSalonIDs.length > 0) {
            // this.databse.getDatabaseState().subscribe(ready => {
            //   if (ready) {
            this.database.getSalonsListingFromSqliteDB(requiredSalonIDs).then(res => {
              if (res && res.length > 0) {
                PinRelatedSalon = res
                // PinRelatedSalon = PinRelatedSalon.slice(0, 5)
                this.salonServiceModal.getDatabaseState().subscribe(ready => {
                  if (ready) {


                    PinRelatedSalon.forEach(salon => {
                      this.salonServiceModal.getSalonServicesForSubCategory(salon.sal_id, SSCID).then(pinRelatedSers => {
                        return salon['pinRelatedServices'] = pinRelatedSers
                      })
                    });
                    this.nativePageTransitions.slide(this.options)
                    this.navCtrl.push(SalonBeautyTrendsPage, {
                      PinRelatedSalon: PinRelatedSalon,
                      cateogry: this.cateogry,
                      objPin: postObject,

                    })
                  }
                })
                // if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

                // const modal = this.modalCtrl.create(PinActionSheetPage, {
                //   PinRelatedSalon: PinRelatedSalon,
                //   cateogry: this.cateogry,
                //   objPin: postObject,
                // }, { cssClass: 'PopOverClass' });
                // modal.present()
              }
              // else {
              //   this.SM.makeToastOnFailure('No salon is associated with this style', 'top')
              // }
            })
          }
        })
      }
    })
}
ionViewDidEnter() {
  this.initializeBackButtonCustomHandler();
  let abc = false
  this.events.subscribe('canShowMiniHeader', (isHidden) => {
    this.zone.run(() => {
      this.shouldShowMiniHeader = isHidden
    });
  });
//page refresh on comming back from detail page
//End page refresh on comming back from detail page 
}
  ionViewDidLoad() {
    // this.content.ionScroll.subscribe(ev =>
    //   requestAnimationFrame(() => this.ionScrollChange(ev.scrollTop)));
  
    this.isLoginSignUp = this.serviceManager.getFromLocalStorage(this.serviceManager.IS_USER_LOGIN);

  
      this.customerModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.customerModal.getCustomer().then(customer => {
            this.myCustomer = customer
            //fetch from local side first then go to server
            this.salonServiceModal.getDatabaseState().subscribe(ready => {
              if (ready) {
               //get last fetch date
                this.categorySubStyle=[]
                //old search
               //this.salonServicesModal.getKeyWordsOnlyForLatestTrendsSearch(searchValue).then(res => {
                this.salonServiceModal.getFavouriteBeautyTrends('3').then(res => {
                  if (res && res.length > 0) {
                    this.categorySubStyle = res    
                    //show post from local databse
                    if (this.infiniteScroll) {
                      this.infiniteScroll.enable(true);
                    }
                    let categorySubStyleServer: CATEGORY_SUB_STYLE[] = []
                    this.categorySubStyle = res  
                    this.postsArray = res;
                    console.log('PostArray'+ JSON.stringify(this.postsArray))
                    
                    this.lastupdate_date = this.postsArray[this.postsArray.length - 1].update_date
          
                    this.noPostsFound = false
                    // this.getpostDetails();
                  
                  } else {
                    // this.getpostDetails();
                   
                  }   
                }, error => {
                })
              }
            })
           //later add in to else above
           // this.getFreshPostsFromServer()
          })
        }
      })
  }



//share contesnt on differnt platforms
getpostDetails() {
  //
  // this.lastFetchDate=''
  if (!this.lastFetchDate || (this.lastFetchDate && this.lastFetchDate.trim().length === 0)) {
    this.lastFetchDate=''
  }
 
  const params = {
    service: btoa('getPostForCategory'),
    last_fetched: btoa(this.lastFetchDate),
    // sc_id: btoa(this.sc_id),
    // sc_gender: btoa(this.gender),
    cust_id: btoa(this.myCustomer.cust_id ) 
  }
  if (!this.lastFetchDate || (this.lastFetchDate && this.lastFetchDate.trim().length === 0)) {
    params['limit'] = btoa(this.trendLimit.toString())
    params['offset'] = btoa(this.trendOffset.toString())
    if (this.trendCall === 1) this.SM.showProgress()
  } else {
    this.trendCall = -1
  }
  console.log('Params',JSON.stringify(params))
  this.SM.getData(params)
    .retryWhen((err) => {
      return err.scan((retryCount) => {
        retryCount += 1;
        if (retryCount < 3) {
          return retryCount;
        }
        else {
          throw (err);
        }
      }, 0).delay(1000)
    })
    .subscribe(
      (res) => {
    // here responce
   // console.log('PostResults:',''+JSON.stringify(res))
    
    
    if (this.trendCall === 1){
     
    }
    if (this.trendCall === 1) this.SM.stopProgress()
    
    if (!res.data.posts || res.data.posts.length === 0 ) {
      this.lastFetchDate=res.response_datetime
      this.saveDateToDb(res.response_datetime)

      return
    }else{
    }
    if( this.trendCall <= 3 && res.data.posts != undefined && res.data.posts.length > 0)
    {

    
      let categorySubStyleServer: CATEGORY_SUB_STYLE[] = []
      categorySubStyleServer = res.data.posts
  
      //save to local database
      this.salonServiceModal.getDatabaseState().subscribe(ready => {
       
        if (ready) {
              this.salonServiceModal.saveIntoCategorySubStyle(categorySubStyleServer).then(categoryInsertedSuccessfully => {
          })
        }
      })
      //end save to local database
      switch (this.trendCall) {

        case 1: //2nd call to fetch more 5 products
        console.log('Case','1')
         this.trendCall += 1
         this.trendOffset = 5
         this.postsArray =this.postsArray.concat(res.data.posts);
         this.getpostDetails()
         break;

       case 2: //3rd call 
      
         this.trendCall += 1
         this.trendLimit = 500
         this.trendOffset = 10
         
         this.getpostDetails()
         this.postsArray =this.postsArray.concat(res.data.posts);
         this.lastupdate_date = this.postsArray[this.postsArray.length - 1].update_date
         break;
       case 3: 
         this.trendLimit = 500
         this.trendOffset += 500
        
         this.saveDateToDb(res.response_datetime)
         if (res.data.posts && res.data.posts.length >= 500) {
        
           this.getpostDetails()
          
         } 
         else{
           this.lastFetchDate=res.response_datetime
         }
          
         break;
       default:
       console.log('Case','defauly')
         break;
     }
    } else 
    {
    
    }

      },
      (error) => {
        this.SM.stopProgress()
        
        this.SM.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
        console.log('something went wrong', error);
      })
}
saveDateToDb(date){
  
  
  this.salonServiceModal.getDatabaseState().subscribe(ready => {
    if (ready) {
      let dateObject={
        // dates_id:this.sc_id,
        // gender:this.gender,
        t_date:date,
      }
      // ValuesPart += '("' + dateObj.dates_id + '", "'  + dateObj.gender + '", "'  + dateObj.t_date + '"),';
      console.log('dateObject',JSON.stringify(dateObject))
      this.salonServiceModal.saveIntoTrendPostDatesTable(dateObject).then(categoryInsertedSuccessfully => {
      
      })
    }
  })
}
wentToLoadMore = false
loadMorePosts(infiniteScroll) {

  // if (this.wentToLoadMore || this.lastupdate_date === -1) {
  //   if (infiniteScroll) {
  //     try {
  //       infiniteScroll.complete();
  //     } catch (ex) {
  //     }
  //   }
  //   return
  // }
  // this.infiniteScroll = infiniteScroll
  // this.getPostFromDb();
 

}
getPostFromDb(){
  this.salonServiceModal.getDatabaseState().subscribe(ready => {
    if (ready) {
      this.categorySubStyle=[]
      this.salonServiceModal.getFavouriteBeautyTrends('3').then(res => {
       this.doRestOfThings(res)
      }, error => {
      })
    }
  })
}
doRestOfThings(res) {
  this.postsArray =  this.postsArray.concat(res)
  this.infiniteScroll.complete()
  if (res && res.length > 0) {
    this.categorySubStyle = res    
    this.categorySubStyle = res  
    
    console.log('PostArray'+JSON.stringify(this.postsArray))

    this.noPostsFound = false
    this.lastupdate_date = this.postsArray[this.postsArray.length - 1].update_date
  } else {
    this.noPostsFound = true
  } 

  if (res && res.length < 8) {
    this.infiniteScroll.enable(false)
  }
  timer(4000).subscribe(() => {
    this.wentToLoadMore = false

  })


}
//app buttons
btnShareTapped(event, latestTrends , keyword){
  event.stopPropagation();
  let loading = this.loadingController.create({ content: "Please Wait..." });
  loading.present();
  timer(1500).subscribe(() => {
    loading.dismiss()
  })
    let productSshareURL = config.shareLatestTrendsURL + latestTrends.ip_id + '/' + latestTrends.p_keywords.replace(' ', '-')
    this.socialSharing.share('This is ' + this.myCustomer.cust_name + '.  I am using BeautyApp.pk on my phone and seen something interesting for you.  Please check the attached.\n' + productSshareURL, 'image', latestTrends.imageUrl).then(() => {
    }).catch(er => {
    })
  
 }
 btnFavTapped(latestTrends , keyword){ 
 }
btnShareFacebookTapped() {
  this.socialSharing.shareViaFacebook('message', 'image', 'url').then(() => {

  }).catch(er => {

  })
}
btnShareWhatsAppTapped() {
  this.socialSharing.shareViaWhatsApp('message', 'image', 'url').then(() => {

  }).catch(er => {

  })
}
btnShareTwitterTapped() {
  this.socialSharing.shareViaTwitter('message', 'image', 'url').then(() => {

  }).catch(er => {
  })
}

//favourite section
makeFavoriteTapped(event, collection, index:number) {

  if(!this.isLoginSignUp){
    this.navCtrl.push(LoginSignUpPage, { animate: false })
    return
  }
  
  // if(!this.isLoginSignUp){
  //   this.navCtrl.push(LoginSignUpPage, { animate: false })
  //   return
  // }
  

  event.stopPropagation();
  let itemBackUp=collection;
  this.setFavToDbAndServer(itemBackUp)
  // let mycollection: FBC_IMAGES = collection
  let makeSalonFavorite: number
  if (collection.fav_id !== 0  ) {
    this.postsArray[index].fav_id = 0
  } else {
    this.postsArray[index].fav_id = collection.fbci_id
  }
}
setFavToDbAndServer(collection){
    let item={
      fav_id:collection.ip_id,
      cft_id:3,
    }
    if(collection.fav_id == 0){
      this.setFavToDb(item)
      this.setFavToServer(item, 1)
    }else{
     this.removeFavFromDb(item)
     this.setFavToServer(item, 0)
    }
}
removeFavFromDb(item){
  this.favModal.getDatabaseState().subscribe(ready => {
    if (ready) {
      this.favModal.deleteFavorite(item.fav_id).then(isSalonMarkedFavorite => {
        if (isSalonMarkedFavorite) {
          //this.removeFromFavFashion(item);
        }
      })
    }
  })
}

setFavToDb(item){
  this.favModal.getDatabaseState().subscribe(ready => {
    if (ready) {
      this.favModal.InsertInToFavoriteTable(item).then(isSalonMarkedFavorite => {
        if (isSalonMarkedFavorite) {
          // this.setFavFashions(item)
        }
      })
    }
  })
}
 
setFavToServer(item, status){
const params = {
  service: btoa('favorites'),
  fav_id: btoa(item.fav_id),
  cft_id: btoa('3'),
  cust_id: btoa(this.myCustomer.cust_id),
  favorite:btoa(status),
}
console.log('params'+JSON.stringify(params))
// if (this.trendCall === 1) this.SM.showProgress()
this.SM.getData(params)
  .retryWhen((err) => {
    return err.scan((retryCount) => {
      retryCount += 1;
      if (retryCount < 3) {
        return retryCount;
      }
      else {
        throw (err);
      }
    }, 0).delay(1000)
  })
  .subscribe(
    (res) => {
      console.log('Response'+JSON.stringify(res))
    },
    (error) => {
      this.SM.stopProgress()
      this.SM.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

  
    })
}
//end favorite post section

  //below all is static code
  btnBackTapped() {
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
  }

}
