import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavouriteBeautyTrendsPage } from './favourite-beauty-trends';

@NgModule({
  declarations: [
    FavouriteBeautyTrendsPage,
  ],
  imports: [
    IonicPageModule.forChild(FavouriteBeautyTrendsPage),
  ],
})
export class FavouriteBeautyTrendsPageModule {}
