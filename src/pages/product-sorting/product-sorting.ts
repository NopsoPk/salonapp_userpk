import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions';
import { MainHomePage } from '../main-home/main-home';
import { Storage } from '@ionic/storage';
import { GlobalProvider } from '../../providers/global/global';
/**
 * Generated class for the ProductSortingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product-sorting',
  templateUrl: 'product-sorting.html',
})
export class ProductSortingPage {
  public sortProductOn: number
  public productSortingMethods = { "PriceLowToHigh": 1, "PriceHighToLow": 2, "productNameAsc": 3, "productNameDesc": 4, "productNewToOld": 5, "productOldToNew": 6 }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public storage: Storage,
    public serviceManager: GlobalProvider,
    public nativePageTransitions: NativePageTransitions,
  ) {
    Object.freeze(this.productSortingMethods)
    // this.sortProductOn = this.productSortingMethods.productNewToOld

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductSortingPage');
  }

  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 700,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };
    if (this.platform.is(`ios`)) {
      this.nativePageTransitions.slide(options)
    }

    if (this.navCtrl.canGoBack()) {
      if (this.sortProductOn && this.sortProductOn > 0) this.navCtrl.getPrevious().data.isSortingSelected = true
        else this.navCtrl.getPrevious().data.isSortingSelected = false
      this.navCtrl.pop()
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
  }
  productSortingMethodsChanged() {
    this.serviceManager.setInLocalStorage('productSorting',this.sortProductOn)
    // this.storage.set('productSorting',this.sortProductOn.toString())
    // alert(this.sortProductOn)
  }
}
