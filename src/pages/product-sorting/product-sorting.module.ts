import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductSortingPage } from './product-sorting';

@NgModule({
  declarations: [
    ProductSortingPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductSortingPage),
  ],
})
export class ProductSortingPageModule {}
