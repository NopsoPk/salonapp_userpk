import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PinActionSheetPage } from './pin-action-sheet';

@NgModule({
  declarations: [
    PinActionSheetPage,
  ],
  imports: [
    IonicPageModule.forChild(PinActionSheetPage),
  ],
})
export class PinActionSheetPageModule {}
