import { Http } from '@angular/http';
import { GlobalProvider } from './../../providers/global/global';
import { RequestAppointmentPage } from './../request-appointment/request-appointment';
import { PINTEREST_OBJECT, Category, Customer, SalonServices,SALON_CARD_OPTIONS, TIPS_DETAIL, BEAUTY_TIPS } from './../../providers/SalonAppUser-Interface';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { SalonDetailsPage } from './../salon-details/salon-details';
import { config } from './../../providers/config/config';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events } from 'ionic-angular';
import { Salon } from '../../providers/SalonAppUser-Interface';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db';
import { GoogleAnalytics } from '../../../node_modules/@ionic-native/google-analytics';
import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { MainHomePage } from '../main-home/main-home';
import { Platform } from 'ionic-angular/platform/platform';
@IonicPage()
@Component({
  selector: 'page-pin-action-sheet',
  templateUrl: 'pin-action-sheet.html',
})
export class PinActionSheetPage {
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };

  public salonImagesURL = config.salonImgUrl;
  public PinRelatedSalons: Salon[]
  public objPin: PINTEREST_OBJECT
  public cateogry: Category
  public myCustomer: Customer
  public salonCardOption: SALON_CARD_OPTIONS
  public isFromBeautyTip: false 
  public selectedTipDetail: TIPS_DETAIL
  public shouldShowPostsInEnglish: false;
  public ShouldShowSalonListing = true
  public beautyTipsCateogry: BEAUTY_TIPS 


  
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public SM: GlobalProvider,
    public salonModal: SqliteDbProvider,
    public http: Http,
    public nativePageTransitions: NativePageTransitions,
    private ga: GoogleAnalytics,
    public events: Events,
    public customerModal: CustomerModal,
    public platform: Platform,
  ) {
    // alert('Here now')
    this.PinRelatedSalons = []
    this.events.subscribe('sendCustomer', customer => {
      if (customer) {
        this.myCustomer = customer
      }
    })
    this.events.publish('getCustomer')
    if (!this.myCustomer) {
      this.customerModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.customerModal.getCustomer().then(customer => {
            this.myCustomer = customer
            // alert('Customer'+JSON.stringify( this.myCustomer))
          })
        }
      })
      
    }

    // this.PinRelatedSalons = this.SM.getFromLocalStorage('testSalons')
    // this.objPin = this.SM.getFromLocalStorage('TestobjPin')
    // this.cateogry = this.SM.getFromLocalStorage('TestCategory')

    this.PinRelatedSalons = this.navParams.get('PinRelatedSalon')
    this.objPin = this.navParams.get('objPin')
    this.cateogry = this.navParams.get('cateogry')
    this.isFromBeautyTip = this.navParams.get('isFromBeautyTip')


    if (this.isFromBeautyTip) {
      this.selectedTipDetail = this.navParams.get('selectedTipDetail')
      this.shouldShowPostsInEnglish = this.navParams.get('shouldShowPostsInEnglish')
      this.ShouldShowSalonListing = false
      this.beautyTipsCateogry = this.navParams.get('cateogry')
      this.googleAnalytics( 'visited  salon-listing-beauty-tips')
    }else{
      this.googleAnalytics( 'visited  salon-listing-latest-trend')
    }
    
    this.PinRelatedSalons = this.PinRelatedSalons.sort(this.compare)
    this.salonCardOption = {
      CardHeight: '100vh',
      calledFrom: config.PinRelatedSalonPage,
      ShouldShowSalonImage: true,
      isViewPinRelatedSalon: true,
      shouldScroll: true,
      salon: this.PinRelatedSalons,
    }
    // this.SM.setInLocalStorage('testSalons', this.PinRelatedSalons)
    // this.SM.setInLocalStorage('TestobjPin', this.objPin)
    // this.SM.setInLocalStorage('TestCategory', this.cateogry)
    // console.log(JSON.stringify(this.PinRelatedSalons));
  }
  ionViewDidLoad() {
    // alert('testing:3:pinaction')
    console.log('ionViewDidLoad PinActionSheetPage');
  }
  close() {
    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 400,
      slowdownfactor: 3,
      slidePixels: 0,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0, // not to include this top section in animation 
      fixedPixelsBottom: 0 // not to include this Bottom section in animation 
    };
    if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
    this.viewCtrl.dismiss();
  }
  btnBookAppointmentTapped(salon) {
     let customerName='test'
     //this.myCustomer.cust_name
    let eventAction = customerName +' is interested in Booking Appointment with '+salon.sal_name
    this.googleAnalytics(eventAction)
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(SalonDetailsPage, {
      selectedSalon: salon,
      sal_id: salon.sal_id
    });

  }

  requestAppointmentTapped(salon) {
    
    let customerName='test'
//this.myCustomer.cust_name 
    let eventAction = customerName +' reuested Appointment with '+salon.sal_name
    this.googleAnalytics(eventAction)
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(RequestAppointmentPage, {
      selectedSalon: salon,
      objPin: this.objPin
    });
  }
  RoundDistanceToTwoDecimal(distance) {
    let _distace = Number(distance).toFixed(1)
    return _distace !== undefined ? _distace : distance

  }
  compare(a, b) {
    const firsSalon: Salon = a;
    const secondSalon: Salon = b

    const genreA = Number(firsSalon.distance)
    const genreB = Number(secondSalon.distance)

    let comparison = 0;
    if (genreA > genreB) {
      comparison = 1;
    } else if (genreA < genreB) {
      comparison = -1;
    }
    return comparison;
  }
  makeSalonFavoriteTapped(salon) {
    console.log(JSON.stringify(salon));

    let mySalon: Salon = salon
    let makeSalonFavorite :number
    if (mySalon.favsal === 1) {
      makeSalonFavorite = 0
    } else {
      makeSalonFavorite = 1
    }
    console.log(makeSalonFavorite);
    

    let params = {
      service: btoa('favorite'),
      sal_id: btoa(mySalon.sal_id),
      cust_id: btoa(this.myCustomer.cust_id),
      favorite: btoa(makeSalonFavorite.toString()),
      
    }
    this.SM.getData(params).subscribe(res => {
      if (res['status'] === '1') {
        
        this.salonModal.getDatabaseState().subscribe(ready => {
          if (ready) {
            this.salonModal.makeSalonFavorite(mySalon.sal_id, makeSalonFavorite).then(isSalonMarkedFavorite => {
              if (isSalonMarkedFavorite) {
                this.PinRelatedSalons.forEach(salon => {
                  if (salon.sal_id === mySalon.sal_id) {
                    return salon.favsal = makeSalonFavorite
                  }
                });
                // this.salonModal.getSalonDetailsFrom().then()
              }
            })
          }
        })
      }
      
      })
  }
  googleAnalytics( message) {
    let cust_name='test'
    this.ga.trackView(cust_name + message);
    this.ga.trackTiming('cat: tracking timing', 600000, 'variable: not sure what will go here', 'label: and the same')
    this.ga.debugMode()
    this.ga.setAllowIDFACollection(true)
    // this.ga.setUserId(this.myCustomer.cust_id)


    // this.ga.trackView(this.myCustomer.cust_name + ' visited Salon Detail Page');
    
    

  }

  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };

    if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop({
        animate: false,
        animation: 'ios-transition',
        direction: 'back',
        duration: 500
      })
      // this.navCtrl.pop({ animate: true, direction: "back" });  
    } else {
      //set root page
      this.navCtrl.setRoot(MainHomePage)
    }



  }
  ShouldShowSalonListingTapped() {
    this.ShouldShowSalonListing = true
  }
}
