import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BeautyTipsViewPage } from './beauty-tips-view';

@NgModule({
  declarations: [
    BeautyTipsViewPage,
  ],
  imports: [
    IonicPageModule.forChild(BeautyTipsViewPage),
  ],
})
export class BeautyTipsViewPageModule {}
