import { Component, NgZone, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import { PRODUCT, Customer, SHIPPING_ADDRESS_DATA, PROMOTIONS, APP_CONFIG } from '../../providers/SalonAppUser-Interface';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions';
import { MainHomePage } from '../main-home/main-home';
import { PLACE_ORDER_PARAM, ORDER_ITEM } from '../../providers/params-interface/params-interface';
import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { GlobalProvider } from '../../providers/global/global';
import { ProductModal } from '../../providers/ProductModal/ProductModal';
import { Keyboard } from '@ionic-native/keyboard';
import { ShippingInfoPage } from '../shipping-info/shipping-info';
import { ShippingAddressModal } from '../../providers/ShippingAddressModal/ShippingAddressModal';
import { CartModal } from '../../providers/cartModal/cartModal';
import { config } from '../../providers/config/config';
import { ProductdetailPage } from '../productdetail/productdetail';
import { configJazzCash, JAZZ_Cash_INTERFACE } from "../../providers/configJazzCash/configJazzCash";
import { DatePipe } from '@angular/common';
import { Http, Headers, RequestOptions, HttpModule } from '@angular/http';
import { InAppBrowser, InAppBrowserObject, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { ThankYouPage } from '../thank-you/thank-you';
import { GlobalServiceProvider } from '../../providers/global-service/global-service';
import { Platform } from 'ionic-angular/platform/platform';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { Select } from 'ionic-angular';
import { timer } from 'rxjs/observable/timer';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-review-order',
  templateUrl: 'review-order.html',
})
export class ReviewOrderPage {
  @ViewChild(Content) content: Content;
  // @ViewChild(Select) select: Select;
  // @ViewChild('secondSelectPayment') secondSelectPayment: any

  public isPayementProcessing = false
  public maxtime: any = 30
  public productIDS = ''
  public responsePlaceOrder: any
  public alertShown: boolean = false;
  public unregisterBackButtonAction: any;
  public productPaymentMethodsForServer = { "COD": 1, "jazzCashMobileAccount": 2, "jazzCashVoucher": 3, "DebitCreditCard": 4 }
  public productPaymentMethodsOptions = { "Mobile_Account": "Jazz Cash Mobile", "Jazz_Cash_Voucher": "Jazz Cash Agent", "Debit_Credit_Card": "Debit/Credit Card", "Cash_On_Delievery": "Cash on Delivery *" }
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };

  public o_id: any
  public CartItems: PRODUCT[] = []
  public params: PLACE_ORDER_PARAM
  public myCustomer: Customer

  trendCall = 1
  public isFirstRequest = true;
  public shouldOrderRequest = true
  public currencySymbol = ''

  public defaultAddress: SHIPPING_ADDRESS_DATA
  public updatedAddress: SHIPPING_ADDRESS_DATA
  public productsImageURL = config.prouductImages

  // Payment method variables

  public ngModalPaymentMethod: string;
  public txtPaymentMobileNumber = ''
  public selectedPaymentMethod = 0
  public loading: any
  public orderSubTotal = 0.0

  public orderDeliveryCharges = 0.0
  public orderGrandTotal = 0.0

  public showPaymentInstruction = false
  public selectedPromotion: PROMOTIONS
  public selectedPromotionAmount = 0
  public dicounAmounttOnVoucher = 0
  public usedCreditStore = 0
  public appConfig: APP_CONFIG

  public ngModelUsePromotion: boolean = false
  public ngModelUseCredit: boolean = false
  public promotionResponse: any
  // public selectedPromotions: PROMOTIONS
  public availableStoredCredit = 0

  public txtApplyVoucher = ''
  public ShippingAddressArray: SHIPPING_ADDRESS_DATA[] = []

  constructor(
    private menu: MenuController,
    private loadingController: LoadingController,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public serviceManager: GlobalProvider,
    public customerModal: CustomerModal,
    public productModal: ProductModal,
    public nativePageTransitions: NativePageTransitions,
    public keyboard: Keyboard,
    public shippingAddressModal: ShippingAddressModal,
    private zone: NgZone,
    public cartModal: CartModal,
    public datePipe: DatePipe,
    public http: Http,
    private inAppBrowser: InAppBrowser,
    public gsp: GlobalServiceProvider,
    public platform: Platform,

  ) {
    this.params = {}
    Object.freeze(this.productPaymentMethodsForServer)
    Object.freeze(this.productPaymentMethodsOptions)

    this.ngModalPaymentMethod = ''
    // params
    this.CartItems = this.navParams.get('CartItems')
    this.orderSubTotal = this.navParams.get('orderSubTotal')
    this.ngModelUsePromotion = this.navParams.get('ngModelUsePromotion')
    this.ngModelUseCredit = this.navParams.get('ngModelUseCredit')
    this.promotionResponse = this.navParams.get('promotionResponse')



    this.orderGrandTotal = this.orderSubTotal + this.orderDeliveryCharges


    console.log('orderSubTotal ' + this.orderSubTotal);
    console.log('orderDeliveryCharges ' + this.orderDeliveryCharges);
    console.log('orderGrandTotal ' + this.orderGrandTotal);


    if (!this.CartItems) this.CartItems = []
    this.getCustomer()
  }

  ionViewWillEnter() {

    this.getCurrencyDetail()

    let updatedAddress: SHIPPING_ADDRESS_DATA = this.navParams.get('updatedAddress')

    if (updatedAddress) {
      this.zone.run(() => {
        this.defaultAddress = updatedAddress

      });
    }

  }

  ionViewDidEnter() {
    this.appConfig = this.serviceManager.getFromLocalStorage('isAppConfigs')

    this.initializeBackButtonCustomHandler();

    this.calculatePromotionsAndStoreCredits();
    this.getDeliveryAddress()
  }

  window: any;
  private calculatePromotionsAndStoreCredits() {

    this.orderGrandTotal = this.orderSubTotal + this.orderDeliveryCharges

    if (this.ngModelUsePromotion) {
      let promotion: PROMOTIONS = this.navParams.get('selectedPromotions')
      let _tempPromotionalAmount = this.orderSubTotal * (promotion.pc_percent / 100)
      this.selectedPromotionAmount = Math.round(_tempPromotionalAmount);
      if (this.selectedPromotionAmount > promotion.pc_max_value) this.selectedPromotionAmount = promotion.pc_max_value

      this.orderGrandTotal = this.orderGrandTotal - this.selectedPromotionAmount
      this.params.pc_id = btoa(promotion.pc_id.toString())
      this.params.o_discount = btoa(this.selectedPromotionAmount.toString())
    } else if (!this.ngModelUsePromotion && this.voucherPromotion) {
      let promotion: PROMOTIONS = this.voucherPromotion

      let _tempPromotionalAmount = this.orderSubTotal * (promotion.pc_percent / 100);
      this.dicounAmounttOnVoucher = Math.round(_tempPromotionalAmount);

      if (this.dicounAmounttOnVoucher > promotion.pc_max_value) this.dicounAmounttOnVoucher = promotion.pc_max_value

      this.orderGrandTotal = this.orderGrandTotal - this.dicounAmounttOnVoucher
      this.params.pc_id = btoa(promotion.pc_id.toString())
      this.params.o_discount = btoa(this.dicounAmounttOnVoucher.toString())

    }
    if (this.ngModelUseCredit && this.promotionResponse && this.promotionResponse.cust_store_credit > 0) {

      this.availableStoredCredit = this.promotionResponse.cust_store_credit;

      if (this.orderGrandTotal > this.availableStoredCredit) {
        this.usedCreditStore = Math.round(this.availableStoredCredit);
        this.orderGrandTotal = this.orderGrandTotal - this.usedCreditStore
      }
      else if (this.availableStoredCredit > this.orderGrandTotal) {
        this.usedCreditStore = Math.round(this.orderGrandTotal);
        this.orderGrandTotal = 0

      }
    }




  }

  ionViewDidLoad() {

    this.shippingAddressModal.getDatabaseState().subscribe(ready => {
      if (ready) {

        this.shippingAddressModal.getDefaultShippingAddress().then(defaultAddress => {
          if (defaultAddress) this.defaultAddress = defaultAddress

        })
      }
    })

  }
  // ionViewWillLeave() {
  //   this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  // }
  // public initializeBackButtonCustomHandler(): void {
  //   this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
  //     this.customHandleBackButton();
  //   }, 10);
  // }
  // private customHandleBackButton(): void {
  //   alert('here')
  //   this.navCtrl.pop();
  // }



  //custom back button for android
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {

    if (!this.isPayementProcessing) {
      // this.navCtrl.pop();
      if (this.menu.isOpen()) {
        this.menu.close();
      } else {
        if (this.navCtrl.canGoBack()) {
          this.navCtrl.pop();
        } else {
          this.navCtrl.setRoot(MainHomePage)
        }
      }
    }
  }
  //end custom back button for android
  openWebpage(url: string) {
    const options: InAppBrowserOptions = {
      zoom: 'no',
      location: 'no',
      toolbar: 'no'
    };
    const browser = this.inAppBrowser.create(url, '_self', options);
    //browser.on('').subscribe
  }
  CalculatePrice(itemPrice, Qty) {

    return itemPrice * Qty
  }

  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };


    if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

    if (this.navCtrl.canGoBack()) {

      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
  }
  placeOrderTapped() {

    // this.paymentMethodChanged()

    if (!this.myCustomer) {
      // this.serviceManager.makeToastOnFailure(AppMessages.msgCustomerDataMissing)
      return
    }
    if (!this.defaultAddress) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgShippingAddressMissing)
      return
    }
    if ((this.selectedPaymentMethod < 1 || this.selectedPaymentMethod > 4) && this.orderGrandTotal > 0) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgSelectPaymentMethod)
      return
    }


    if ((this.selectedPaymentMethod === this.productPaymentMethodsForServer.jazzCashMobileAccount || this.selectedPaymentMethod === this.productPaymentMethodsForServer.jazzCashVoucher) && (!this.txtPaymentMobileNumber || this.txtPaymentMobileNumber.trim().length === 0)) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgMobileNumberForPayment)
      return
    }

    if ((this.selectedPaymentMethod === this.productPaymentMethodsForServer.jazzCashMobileAccount || this.selectedPaymentMethod === this.productPaymentMethodsForServer.jazzCashVoucher) && this.txtPaymentMobileNumber.length < 11) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgCustomerMobileNumber)
      return
    }
    if (this.selectedPaymentMethod === this.productPaymentMethodsForServer.DebitCreditCard) {


    }

    let orderItems: ORDER_ITEM[] = []
    this.CartItems.forEach(prod => {
      if (prod.isChecked) this.productIDS += prod.p_id + ','
      let orderItem: ORDER_ITEM = {

      }
      orderItem.p_id = btoa(prod.p_id.toString())
      orderItem.od_quantity = btoa(prod.p_quantity.toString())
      orderItem.od_price = btoa((prod.p_price).toString())
      orderItem.od_name = btoa(prod.p_name)
      orderItem.od_image = btoa(prod.p_image)

      orderItems.push(orderItem)
    });

    // this.orderGrandTotal = this.orderSubTotal + this.orderDeliveryCharges

    this.params.service = btoa('place_order')
    this.params.o_address = btoa(this.defaultAddress.o_address)
    this.params.o_city = btoa(this.defaultAddress.o_city)
    this.params.o_province = btoa(this.defaultAddress.o_province)
    let _appConfigDeliveryCharges = this.appConfig.delivery_charges
    if (!_appConfigDeliveryCharges) _appConfigDeliveryCharges = 0
    this.params.o_payable_amount = btoa((this.orderSubTotal + this.orderDeliveryCharges + _appConfigDeliveryCharges - this.usedCreditStore - this.selectedPromotionAmount).toString())
    this.params.o_cust_store_credit = btoa(this.usedCreditStore.toString())
    // this.params.o_discount = btoa(this.selectedPromotionAmount.toString())
    this.params.o_cod_charges = btoa(this.orderDeliveryCharges.toString())
    if (this.orderGrandTotal === 0) this.selectedPaymentMethod = 5
    //  alert('orderGrandTotal'+this.selectedPaymentMethod+'orderGrandTotal'+this.orderGrandTotal)
    if (this.defaultAddress) {
      this.params.o_contact_person = btoa(this.defaultAddress.o_contact_person)
      this.params.o_phone = btoa(this.defaultAddress.o_phone)
    } else {
      this.params.o_contact_person = btoa(this.myCustomer.cust_name)
      this.params.o_phone = btoa(this.myCustomer.cust_phone)
    }

    this.params.cust_id = btoa(this.myCustomer.cust_id.toString())
    this.params.o_amount = btoa(this.orderSubTotal.toString())
    this.params.o_delivery_charges = btoa(_appConfigDeliveryCharges.toString())
    this.params.o_items = orderItems
    this.params.pt_id = btoa(this.selectedPaymentMethod.toString())
    this.params.o_payment_mobile = btoa(this.txtPaymentMobileNumber)
    if (this.selectedPaymentMethod === this.productPaymentMethodsForServer.jazzCashMobileAccount) {
      this.showPaymentInstruction = true
    } else {
      this.showPaymentInstruction = false
      this.serviceManager.showProgress()
    }

    // console.log('params-all', JSON.stringify(this.params))

    this.serviceManager.getData(this.params).subscribe(res => {

      if (this.selectedPaymentMethod === 4) {

        this.responsePlaceOrder = res
        // console.log('response:', JSON.stringify(res))
        if (res.o_status != undefined && res.o_status.length > 0) {
          if (!this.showPaymentInstruction) this.serviceManager.stopProgress()
          this.o_id = res.o_id;
          this.getOrderStatus();
          this.isPayementProcessing = true;
          this.openWebpage(res.url)
        }
      } else {
        this.showPaymentInstruction = false
        if (!this.showPaymentInstruction) this.serviceManager.stopProgress()
        if (Number(res.status) === 1) {
          this.productModal.removeCartItemsAfterOrderPlaced().then(isDone => {
            console.log('good luck');
            let options: NativeTransitionOptions = {
              direction: 'right',
              duration: 500,
              slowdownfactor: 3,
              slidePixels: 20,
              iosdelay: 100,
              androiddelay: 150,
              fixedPixelsTop: 0,
              fixedPixelsBottom: 0
            };
            //commit go to next page by me(rizwan)
            if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

            this.navCtrl.setRoot(ThankYouPage, {

              res: res
            }).then(done => {
              this.productIDS = this.productIDS.slice(0, this.productIDS.length - 1)
              this.cartModal.delteItemWhoseOrderIsPlaced(this.productIDS)
            })
          })
        } else {
          // elem.style.display = "block";
          this.serviceManager.makeToastOnFailure(res.error)
          console.log('status is zero');

        }

      }


    }, e => {
      console.log(e)
      // elem.style.display = "block";
      this.showPaymentInstruction = false
      this.serviceManager.stopProgress()
    })
  }
  cancelPayment() {
    this.getOrderStatusOnCancel();

  }

  // doTransactionWithJazzCashMobileAccount() {
  //   let myTxnDate = new Date()
  //   // let TxnDateTime = this.datePipe.transform(myTxnDate, 'yyyyMMddHHmmss');
  //   // console.log('abcabc');
  //   // console.log(TxnDateTime);
  //   // console.log(myTxnDate.getMinutes())
  //   // myTxnDate.setMinutes(5,0,0) //= myTxnDate.getMinutes() + 4
  //   // console.log(myTxnDate.getMinutes())
  //   // console.log(myTxnDate.getMinutes())


  //   let objMobileAccount: JAZZ_Cash_INTERFACE = {

  //   }
  //   let pp_Amount = this.orderTotalPrice
  //   let pp_BankID = ''
  //   let pp_BillReference = 1234
  //   let pp_Description = 'This is just Test Description'
  //   let pp_Language = configJazzCash.language
  //   let pp_MerchantID = configJazzCash.pp_MerchantID
  //   let pp_Password = configJazzCash.pp_Password
  //   let pp_ProductID = 2342323
  //   let pp_ReturnURL = configJazzCash.pp_ReturnURL
  //   let pp_SubMerchantID = ''
  //   let pp_TxnCurrency = configJazzCash.TxnCurrency
  //   let pp_TxnDateTime = this.datePipe.transform(myTxnDate, 'yyyyMMddHHmmss')
  //   let pp_TxnExpiryDateTime = this.datePipe.transform(myTxnDate.setMinutes(myTxnDate.getMinutes() + configJazzCash.expireDuration), 'yyyyMMddHHmmss')
  //   let pp_TxnType = configJazzCash.pp_TxnType
  //   let pp_Version = configJazzCash.Version


  //   objMobileAccount.pp_Amount = pp_Amount * 100
  //   // objMobileAccount.pp_BankID = pp_BankID //this.calculateHash('')
  //   objMobileAccount.pp_BillReference = pp_BillReference // this.calculateHash(12345)
  //   objMobileAccount.pp_Description = pp_Description // this.calculateHash('This is just Test Description')
  //   objMobileAccount.pp_DiscountBank = '' // this.calculateHash('This is just Test Description')
  //   objMobileAccount.pp_DiscountedAmount = '' // this.calculateHash('This is just Test Description')

  //   objMobileAccount.pp_Language = pp_Language//this.calculateHash(configJazzCash.language)
  //   objMobileAccount.pp_MerchantID = pp_MerchantID //this.calculateHash(configJazzCash.pp_MerchantID)
  //   objMobileAccount.pp_Password = pp_Password //this.calculateHash(configJazzCash.pp_Password)
  //   // objMobileAccount.pp_ProductID = pp_ProductID //this.calculateHash('')
  //   objMobileAccount.pp_ReturnURL = pp_ReturnURL // this.calculateHash()
  //   objMobileAccount.pp_SubMerchantID = pp_SubMerchantID// this.calculateHash('')

  //   objMobileAccount.pp_TxnCurrency = pp_TxnCurrency  //this.calculateHash(configJazzCash.TxnCurrency)

  //   objMobileAccount.pp_TxnDateTime = pp_TxnDateTime// this.calculateHash()
  //   objMobileAccount.pp_TxnExpiryDateTime = pp_TxnExpiryDateTime // this.calculateHash()
  //   objMobileAccount.pp_TxnRefNo = 'TXN20190104121906' + Math.random() //'zaheer sb'//this.calculateHash('') //zaheer sb
  //   objMobileAccount.pp_TxnType = pp_TxnType //this.calculateHash(configJazzCash.pp_TxnType)
  //   objMobileAccount.pp_Version = pp_Version //this.calculateHash(configJazzCash.Version)

  //   let secureHash = configJazzCash.secretKey
  //   for (var key in objMobileAccount) {
  //     if (objMobileAccount.hasOwnProperty(key)) {
  //       // console.log(key + " -> " + objMobileAccount[key]);
  //       let valueValue = objMobileAccount[key].toString()
  //       if (valueValue && valueValue.length > 0) {
  //         secureHash += '&' + valueValue
  //         // console.log('secureHash '+secureHash);

  //       } else {
  //         console.log('is in else');

  //       }
  //     }
  //   }

  //   console.log('secureHash');
  //   console.log(secureHash);
  //   // forge.util.encodeUtf8('Hi server!'

  //   objMobileAccount.pp_SecureHash = this.calculateHash(secureHash)

  //   objMobileAccount.ppmpf_1 = '03047660078' //this.calculateHash()
  //   objMobileAccount.ppmpf_2 = '' //this.calculateHash('')
  //   objMobileAccount.ppmpf_3 = '' // this.calculateHash('')
  //   objMobileAccount.ppmpf_4 = '' // this.calculateHash('')
  //   objMobileAccount.ppmpf_5 = '' // this.calculateHash('')
  //   // alert(objMobileAccount.ppmpf_1)
  //   // alert(this.txtPaymentMobileNumber)
  //   // console.log('pp_Securehash ' + objMobileAccount.pp_SecureHash);

  //   // console.log(JSON.stringify(objMobileAccount));

  //   // this.serviceManager.getDataJazCash(configJazzCash.baseURL, objMobileAccount).subscribe(res => {
  //   //   alert('mash allah')
  //   //   console.log(res);

  //   // }, er => {
  //   //     console.log(er);

  //   //   })


  //   // this.http.post(configJazzCash.baseURL, objMobileAccount)
  //   //   .map(Response => Response.json())
  //   //   .subscribe(res => {
  //   //     console.log(JSON.stringify(res))
  //   //   }, er => {
  //   //     console.log(JSON.stringify(er))
  //   //   })


  //   // let Amount = 10 * 100; //Last two digits will be considered as Decimal
  //   // let BillReference = "OrderID";
  //   // let Description = configJazzCash.description;
  //   // let Language = configJazzCash.language;
  //   // let TxnCurrency = configJazzCash.TxnCurrency;
  //   // let TxnDateTime = date('YmdHis');
  //   // let TxnExpiryDateTime = date('YmdHis', strtotime('+8 Days'));
  //   // let TxnRefNumber = "TXN".date('YmdHis');
  //   // let TxnType = configJazzCash.TxnType
  //   // let Version = configJazzCash.Version
  //   // let SubMerchantID = configJazzCash.SubMerchantID
  //   // let DiscountedAmount = "";
  //   // let DiscountedBank = "";
  //   // let ppmpf_1 = "";
  //   // let ppmpf_2 = "";
  //   // let ppmpf_3 = "";
  //   // let ppmpf_4 = "";
  //   // let ppmpf_5 = "";

  //   // this.post_new("https://sandbox.jazzcash.com.pk/payaxisapplicationapi/api/Payment/DoTransaction",objMobileAccount,"POST")
  //   this.post_to_url(configJazzCash.baseURL, objMobileAccount)
  // }

  // post_new(path, params, method) {

  //   var form = document.createElement("form");
  //   form.setAttribute("method", method);
  //   form.setAttribute("action", path);

  //   for (var key in params) {
  //     if (params.hasOwnProperty(key)) {
  //       var hiddenField = document.createElement("input");
  //       hiddenField.setAttribute("type", "text");
  //       hiddenField.setAttribute("name", key);
  //       hiddenField.setAttribute("value", params[key]);

  //       form.appendChild(hiddenField);
  //     }
  //   }
  //   var field = document.createElement("input");
  //   field.setAttribute("type", "submit");
  //   form.appendChild(field)


  //   document.body.appendChild(form);
  //   console.log(document);

  //   form.submit()
  //   try {
  //   } catch (error) {
  //     console.log(JSON.stringify(error));

  //   }

  //   // alert('sending complete')
  //   // this.sendData(params)
  // }
  // sendData(data) {
  //   var XHR = new XMLHttpRequest();
  //   var urlEncodedData = "";
  //   var urlEncodedDataPairs = [];
  //   var name;

  //   // Turn the data object into an array of URL-encoded key/value pairs.
  //   for (name in data) {
  //     urlEncodedDataPairs.push(encodeURIComponent(name) + '=' + encodeURIComponent(data[name]));
  //   }

  //   // Combine the pairs into a single string and replace all %-encoded spaces to 
  //   // the '+' character; matches the behaviour of browser form submissions.
  //   urlEncodedData = urlEncodedDataPairs.join('&').replace(/%20/g, '+');

  //   // Define what happens on successful data submission
  //   XHR.addEventListener('load', function (event) {
  //     // alert('Yeah! Data sent and response loaded.');
  //   });

  //   // Define what happens in case of error
  //   XHR.addEventListener('error', function (event) {
  //     // alert('Oops! Something goes wrong.');
  //   });

  //   // Set up our request
  //   XHR.open('POST', configJazzCash.baseURL);

  //   // Add the required HTTP header for form data POST requests
  //   XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

  //   // Finally, send our data.
  //   XHR.send(urlEncodedData);
  // }

  // post_to_url(path, params) {

  //   let method = 'POST';

  //   var form = document.createElement('form');

  //   // Move the submit function to another variable
  //   // so that it doesn't get overwritten.
  //   form._submit_function_ = form.submit;

  //   form.setAttribute('method', method);
  //   form.setAttribute('action', path);
  //   // form.setAttribute('target', '_blank');

  //   for (var key in params) {
  //     var hiddenField = document.createElement('input');
  //     hiddenField.setAttribute('type', 'hidden');
  //     hiddenField.setAttribute('name', key);
  //     hiddenField.setAttribute('value', params[key]);

  //     form.appendChild(hiddenField);
  //   }

  //   var field = document.createElement("input");
  //   field.setAttribute("type", "submit");
  //   form.appendChild(field)

  //   document.body.appendChild(form);
  //   console.log(document.body);

  //   form._submit_function_(); // Call the renamed function.
  //   form.submit()
  // }

  btnCancelTapped() {

    let alert = this.alertCtrl.create({
      title: 'Canceling Payment',
      message: 'Are you sure you want to cancel  payment processing?',
      buttons: [
        {
          text: 'NO',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'YES',
          handler: () => {
            //cancel payemnt request
            this.isPayementProcessing = false;
            this.shouldOrderRequest = false

            timer(1000).subscribe(() => {
              this.onOrderCancel();
            })
            // this.loading = this.loadingController.create({ content: "Please wait..." });
            // this.loading.present();
            const params = {
              service: btoa('update_order_status'),
              cust_id: btoa(this.myCustomer.cust_id),
              o_id: btoa(this.o_id),
              o_status: btoa('deleted'),
            }
            // this.serviceManager.showProgress()
            this.serviceManager.getData(params)
              .retryWhen((err) => {
                return err.scan((retryCount) => {
                  retryCount += 1;
                  if (retryCount < 3) {
                    return retryCount;
                  }
                  else {
                    throw (err);
                  }
                }, 0).delay(1000)
              })
              .subscribe(
                (res) => {
                  // this.loading.dismissAll();
                  // if (res.status === "1" || res.status === 1) {
                  //   this.onOrderCancel();
                  // }
                  // this.onOrderCancel();
                },
                (error) => {
                  // this.onOrderCancel();
                  // this.loading.dismissAll();
                  console.log(error);
                  this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
                  console.log('something went wrong', error);
                },
                () => {
                  //   this.serviceManager.stopProgress()
                }
              )
          }
        }
      ]
    });
    alert.present();
  }
  calculateHash(input: string | number) {
    var forge = require('node-forge');
    // forge.util.encodeUtf8('Hi server!'
    forge.util.encodeUtf8('')
    var hmac = forge.hmac.create();
    hmac.start(configJazzCash.digestAlgorithm, configJazzCash.secretKey);
    hmac.update(input);
    // console.log(hmac.digest().toHex());
    return hmac.digest().toHex()
  }
  openInappBrowser() {

  }
  getCustomer() {

    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(customer => {
          if (customer) {
            this.myCustomer = customer
          }
        })
      }
    })
  }
  handleReturnkey() {
    console.log('return key is tapped');
    this.keyboard.close()
  }
  btnAddAddressClicked() {
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(ShippingInfoPage)
  }
  btnEditAddressTapped() {

    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(ShippingInfoPage, {
      isRequestforNewOrEditAddress: this.defaultAddress
    })
  }
  removeAllAddress() {
    this.shippingAddressModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.shippingAddressModal.TruncateShippingAddressTable()
      }
    })
  }
  showProductDetailTapped(cartItem: PRODUCT, index) {

    this.productModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.productModal.getProductDetail(cartItem.p_id).then(product => {

          this.nativePageTransitions.slide(this.options)
          this.navCtrl.push(ProductdetailPage, {
            product: cartItem,
            isFromReviewOrder: true
          })
        })
      }
    })
  }
  paymentMethodChanged() {
    switch (this.ngModalPaymentMethod.trim()) {
      case this.productPaymentMethodsOptions.Mobile_Account:
        this.selectedPaymentMethod = this.productPaymentMethodsForServer.jazzCashMobileAccount
        this.orderDeliveryCharges = 0

        this.calculatePromotionsAndStoreCredits();

        break;
      case this.productPaymentMethodsOptions.Jazz_Cash_Voucher:
        this.selectedPaymentMethod = this.productPaymentMethodsForServer.jazzCashVoucher
        this.orderDeliveryCharges = 0

        this.calculatePromotionsAndStoreCredits();

        break;
      case this.productPaymentMethodsOptions.Debit_Credit_Card:

        this.selectedPaymentMethod = this.productPaymentMethodsForServer.DebitCreditCard
        this.orderDeliveryCharges = 0

        this.calculatePromotionsAndStoreCredits();
        break;
      case this.productPaymentMethodsOptions.Cash_On_Delievery:
        this.selectedPaymentMethod = this.productPaymentMethodsForServer.COD
        this.orderDeliveryCharges = config.deliveryCharges
        this.calculatePromotionsAndStoreCredits();

        break;

      default:
        this.selectedPaymentMethod = 0
        this.orderDeliveryCharges = 0

        this.calculatePromotionsAndStoreCredits();
        break;
    }

  }
  shouldChangeCharacter(search) {
    if (this.txtPaymentMobileNumber.length == 1) {
      if (this.txtPaymentMobileNumber.trim() != '0') {
        this.txtPaymentMobileNumber = '';
        this.serviceManager.makeToastOnFailure(AppMessages.msgVerifyCustomerMobileNumber)
      }
    } else if (this.txtPaymentMobileNumber.length == 2) {
      if (this.txtPaymentMobileNumber != '03') {
        this.txtPaymentMobileNumber = '';

        this.serviceManager.makeToastOnFailure(AppMessages.msgMobileNumberStartingDigits)

      }
    }
  }
  getCurrencyDetail() {
    this.currencySymbol = this.gsp.currencySymbol

    if (!this.currencySymbol || this.currencySymbol.length === 0) {
      let _appConfig = this.serviceManager.getFromLocalStorage('isAppConfigs')
      if (_appConfig) {
        if (_appConfig && _appConfig.currency) {
          this.gsp.currencySymbol = _appConfig.currency
          this.gsp.distance_unit = _appConfig.distance_unit

        }
      }
    }
  }

  getOrderStatus() {

    // this.lastFetchDate=''

    const params = {
      service: btoa('get_order_status'),
      o_id: btoa(this.o_id),

    }

    // if (this.trendCall === 1) this.SM.showProgress()
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          // console.log('ResponseOrderStatus', JSON.stringify(res))
          if (res.o_status != undefined && res.o_status.length > 0) {
            console.log('Its here in if')
            //for a while
            // this.onOrderSuccess();

            if (res.o_status == "Pending") {
              if (this.shouldOrderRequest) {
                if (this.isFirstRequest) {
                  this.isFirstRequest = false
                  //
                  timer(30000).subscribe(() => {
                    console.log('Getting-order-status-first-time')
                    //
                    this.getOrderStatus();
                  })
                } else {
                  timer(3000).subscribe(() => {
                    console.log('Getting-order-status')
                    this.getOrderStatus();
                  })
                }
              }
            } else if (res.o_status == "Placed") {
              //if payment was  in processing then move ot succes else user has canceled the payment.
              if (this.isPayementProcessing) {
                this.onOrderSuccess(res);
              }

              this.shouldOrderRequest = false
            } else {
              //if payment was  in processing then move ot failed else user has canceled the payment.
              if (this.isPayementProcessing) {
                this.onOrderFailed(res);
              }
              //temprarorily
              // alert('transaction-failed-1')
              this.shouldOrderRequest = false
            }

          } else {
            console.log('Its here in else')
          }
        },
        (error) => {
          this.serviceManager.stopProgress()
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)


        })
  }

  getOrderStatusOnCancel() {

    // this.lastFetchDate=''
    this.loading = this.loadingController.create({ content: "Please wait..." });
    this.loading.present();
    const params = {
      service: btoa('get_order_status'),
      o_id: btoa(this.o_id),

    }

    // if (this.trendCall === 1) this.SM.showProgress()
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          this.loading.dismissAll();

          if (res.o_status != undefined && res.o_status.length > 0) {
            if (res.o_status == "Pending") {
              // if(this.shouldOrderRequest){
              //   if(this.isFirstRequest){
              //     this.isFirstRequest=false
              //     timer(30000).subscribe(() => {
              //       console.log('Getting-order-status-first-time')
              //       this.getOrderStatus();
              //      })
              //   }else{
              //     timer(3000).subscribe(() => {
              //       console.log('Getting-order-status')
              //       this.getOrderStatus();
              //      })
              //   }
              // }
              this.btnCancelTapped()
            } else if (res.o_status == "Placed") {
              this.onOrderSuccess(res);
              this.shouldOrderRequest = false
            } else {
              this.onOrderFailed(res);
              this.shouldOrderRequest = false
              //temprarorily
              // alert('transaction-failed-1')
            }

          }
        },
        (error) => {
          this.loading.dismissAll();
          this.serviceManager.stopProgress()
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)


        })
  }
  onOrderFailed(response) {
    // console.log('response', JSON.stringify(response))
    this.shouldOrderRequest = false
    this.productModal.removeCartItemsAfterOrderPlaced().then(isDone => {
      console.log('failed ');
      let options: NativeTransitionOptions = {
        direction: 'right',
        duration: 500,
        slowdownfactor: 3,
        slidePixels: 20,
        iosdelay: 100,
        androiddelay: 150,
        fixedPixelsTop: 0,
        fixedPixelsBottom: 0
      };

      if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
      this.navCtrl.setRoot(ThankYouPage, {
        message: response,
        res: response
      }).then(done => {
        this.productIDS = this.productIDS.slice(0, this.productIDS.length - 1)
        this.cartModal.delteItemWhoseOrderIsPlaced(this.productIDS)
      })
    })


  }
  onOrderCancel() {
    this.isPayementProcessing = false;
    this.shouldOrderRequest = false
    //stay user here not push to previous page
    // this.productModal.removeCartItemsAfterOrderPlaced().then(isDone => {
    //   console.log('good luck');
    //   let options: NativeTransitionOptions = {
    //     direction: 'left',
    //     duration: 500,
    //     slowdownfactor: 3,
    //     slidePixels: 20,
    //     iosdelay: 100,
    //     androiddelay: 150,
    //     fixedPixelsTop: 0,
    //     fixedPixelsBottom: 0
    //   };
    //   if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
    //   this.navCtrl.setRoot(MainHomePage).then(done => {
    //     this.productIDS = this.productIDS.slice(0, this.productIDS.length - 1)
    //     this.cartModal.delteItemWhoseOrderIsPlaced(this.productIDS)
    //   })
    // })


  }
  onOrderSuccess(response) {

    this.shouldOrderRequest = false
    this.productModal.removeCartItemsAfterOrderPlaced().then(isDone => {
      console.log('good luck');
      let options: NativeTransitionOptions = {
        direction: 'right',
        duration: 500,
        slowdownfactor: 3,
        slidePixels: 20,
        iosdelay: 100,
        androiddelay: 150,
        fixedPixelsTop: 0,
        fixedPixelsBottom: 0
      };

      if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options)


      this.navCtrl.setRoot(ThankYouPage, {
        res: response
      }).then(done => {
        this.productIDS = this.productIDS.slice(0, this.productIDS.length - 1)
        this.cartModal.delteItemWhoseOrderIsPlaced(this.productIDS)
      })
    })
  }
  voucherPromotion: PROMOTIONS
  invalidVoucherCount = 0
  btnApplyVoucher() {
    if (this.txtApplyVoucher.trim().length === 0) {
      this.serviceManager.makeToastOnFailure('voucher number is required')
      return
    }
    if (this.invalidVoucherCount >= 3) {
      this.dicounAmounttOnVoucher = null
      this.ngModelUsePromotion = false
      this.serviceManager.makeToastOnFailure('You reached maximum limit to enter coupon number.')
      return
    }
    // this.ngModelUsePromotion = true
    const params = {
      service: btoa('validate_promo_code'),
      pc_code: btoa(this.txtApplyVoucher),

    }
    this.serviceManager.showProgress()
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          this.serviceManager.stopProgress()
          if (Number(res.status) === 1 && res.promo && !this.ngModelUsePromotion) {
            this.voucherPromotion = res.promo
            this.serviceManager.makeToastOnSuccess(res.msg)
            this.calculatePromotionsAndStoreCredits()

          } else {
            this.invalidVoucherCount += 1
            this.serviceManager.makeToastOnFailure(res.msg)
          }

        },
        (error) => {
          this.serviceManager.stopProgress()
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)


        })
  }
  getDeliveryAddress() {
    this.shippingAddressModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.shippingAddressModal.getShippingAddress().then(sa => {
          if (sa) this.ShippingAddressArray = sa
          this.ShippingAddressArray.forEach(sa => {
            console.log(sa.o_completeAddress)
            console.log(sa.o_id)
          });
        })


      }
    })
  }
  public caseAddress = 0
  shippingAddressSelected(shipAddres: SHIPPING_ADDRESS_DATA, index) {
    this.caseAddress = 0
    this.shouldShowPreviousDeliveryAddress = !this.shouldShowPreviousDeliveryAddress
    this.defaultAddress = shipAddres
    /**
      // this.selectedPreviousAddress = shipAddres
      {
  
        switch (this.caseAddress) {
          case 0:// for using previous address without any change
            // this.selectedAdress = this.selectedPreviousAddress
            // this.goBackToReviewOrder()
            break;
          case 1: // for updating shipping address
  
            if (this.validateAddressInformation()) {
              // this.defaultAddress = shipAddres
              this.defaultAddress.isdefaultaddress = this.markthisAsDefaultAddress
              this.defaultAddress.o_contact_person = this.customerName
              this.defaultAddress.o_phone = this.customerPhone
  
              this.defaultAddress.o_city = this.city
              this.defaultAddress.o_province = this.gender
              this.defaultAddress.o_address = this.shippingAddress
  
              this.defaultAddress.o_completeAddress = this.shippingAddress
              this.shippingAddressModal.updateShippingAddress(this.selectedAdress).then(() => {
                this.goBackToReviewOrder()
              })
  
            }
            break;
          case 2: // simple insert a new address
            if (this.validateAddressInformation()) {
  
              let newAddressArray: SHIPPING_ADDRESS_DATA[] = []
              let newAddress: SHIPPING_ADDRESS_DATA = {
  
              }
              newAddress.o_contact_person = this.customerName
              newAddress.o_phone = this.customerPhone
              newAddress.isdefaultaddress = this.markthisAsDefaultAddress
              newAddress.o_city = this.city
              newAddress.o_province = this.province
              newAddress.o_address = this.shippingAddress
              newAddress.o_completeAddress = this.shippingAddress // this.customerName + ',\n' + this.shippingAddress + this.city + ', ' + this.province + ',\n #' + this.customerPhone
  
              this.selectedAdress = {
  
              }
              this.selectedAdress = newAddress
              newAddressArray.push(newAddress)
              this.shippingAddressModal.getDatabaseState().subscribe(ready => {
                if (ready) {
                  this.shippingAddressModal.InsertInToShippingAddressTable(newAddressArray).then(() => {
                    this.goBackToReviewOrder()
                  })
                }
              })
            }
            break;
  
          default:
            break;
        }
  
  
      }
      */

  }



  btnMoreOptionTapped(event: string, shipAddres: SHIPPING_ADDRESS_DATA) {
    if (event.trim() === 'Delete') {
      let alert = this.alertCtrl.create({
        title: 'Warning',
        message: 'Surely want to remove this address?',
        buttons: [
          {
            text: 'NO',
            role: 'cancel',
            handler: () => {
            }
          },
          {
            text: 'YES',
            handler: () => {
              this.shippingAddressModal.getDatabaseState().subscribe(ready => {
                if (ready) {
                  this.shippingAddressModal.deleteAddress(shipAddres.o_completeAddress).then(() => {
                    this.shippingAddressModal.getShippingAddress().then(sa => {
                      if (sa) this.ShippingAddressArray = sa

                    })
                  })

                }
              })
            }
          }
        ]
      });
      alert.present();


    } else if (event.trim() === 'Edit') {

      this.caseAddress = 1
      this.defaultAddress = shipAddres
      this.btnEditAddressTapped()

      // this.IsNewShippingAddress = true;
      // this.selectedAdress = shipAddres
      // this.customerName = shipAddres.o_contact_person
      // this.customerPhone = shipAddres.o_phone
      // this.city = shipAddres.o_city
      // this.gender = shipAddres.o_province
      // this.shippingAddress = shipAddres.o_address
      // alert('updated address')
    }


  }
  public shouldShowPreviousDeliveryAddress = false
  btnShowPreviousDeliveryAddressTapped() {
    this.shouldShowPreviousDeliveryAddress = !this.shouldShowPreviousDeliveryAddress
  }
}




