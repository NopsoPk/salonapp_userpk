import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LatestTrendsDetailPage } from './latest-trends-detail';

@NgModule({
  declarations: [
    LatestTrendsDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(LatestTrendsDetailPage),
  ],
})
export class LatestTrendsDetailPageModule {}
