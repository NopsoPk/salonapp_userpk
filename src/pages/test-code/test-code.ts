import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the TestCodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-test-code',
  templateUrl: 'test-code.html',
})
export class TestCodePage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public serviceManager:GlobalProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TestCodePage');
    this.serviceManager.makeToastOnFailure('zahid shabbir & Shahry khalid zahid shabbir & Shahry khalid zahid shabbir & Shahry khalid zahid shabbir & Shahry khalid zahid shabbir & Shahry khalid zahid shabbir & Shahry khalid zahid shabbir & Shahry khalid zahid shabbir & Shahry khalid')
  }

}
