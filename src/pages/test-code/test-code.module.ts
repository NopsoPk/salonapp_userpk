import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TestCodePage } from './test-code';

@NgModule({
  declarations: [
    TestCodePage,
  ],
  imports: [
    IonicPageModule.forChild(TestCodePage),
  ],
})
export class TestCodePageModule {}
