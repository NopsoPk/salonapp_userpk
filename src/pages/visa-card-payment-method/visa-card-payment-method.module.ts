import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VisaCardPaymentMethodPage } from './visa-card-payment-method';

@NgModule({
  declarations: [
    VisaCardPaymentMethodPage,
  ],
  imports: [
    IonicPageModule.forChild(VisaCardPaymentMethodPage),
  ],
})
export class VisaCardPaymentMethodPageModule {}
