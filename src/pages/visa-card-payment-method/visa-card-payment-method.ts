import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { MainHomePage } from '../main-home/main-home';
import { ThankYouPage } from '../thank-you/thank-you';
import { Platform } from 'ionic-angular/platform/platform';

@IonicPage()
@Component({
  selector: 'page-visa-card-payment-method',
  templateUrl: 'visa-card-payment-method.html',
})
export class VisaCardPaymentMethodPage {

  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  
  public txtAccountNumber = ''
  public txtCNIC = ''
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private nativePageTransitions: NativePageTransitions,
    public platform: Platform,
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VisaCardPaymentMethodPage');
  }
  btnClearTextTapped(clearText) {
    clearText === 'txtCNIC' ? this.txtCNIC = '' : this.txtAccountNumber = '' 
  }

  onBtnPlaceOrderClick() {
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(ThankYouPage)
  }
  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };


    if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

    if (this.navCtrl.canGoBack()) {

      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
  }
}
