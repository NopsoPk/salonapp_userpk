import { Category, Sal_Techs, Services, Tech_appointments, Salon, OFFERS, SalonTechnicain } from './../../providers/SalonAppUser-Interface';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Platform, Navbar } from 'ionic-angular';
import { SlotsViewPage } from '../../pages/slots-view/slots-view'
import { MainHomePage } from '../../pages/main-home/main-home'


import { retry } from 'rxjs/operators/retry';
import { GlobalProvider } from '../../providers/global/global';
import { SalonServicesModal } from '../../providers/salon-services-modal/salon-services-modal';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db'
import { config } from '../../providers/config/config';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
@IonicPage()
@Component({
  selector: 'page-add-appointment',
  templateUrl: 'add-appointment.html',
})
export class AddAppointmentPage {
  @ViewChild(Navbar) navBar: Navbar;
 
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };

  baseUrl=config.TechImageURL;
  objSubStyle: any
  objPin: any
  pinImage: any
  rating: any
  dummyItemInList: any;
  public allTechs: Sal_Techs[];
  rate: any;
  public selectedTechIndex = 0;
  firstCall = true
  cust_id: string;
  public salonTechs: any[];
  public selectedSalon: Salon
  public salonForComponent: Salon
  techSelected: Sal_Techs;
  _services: any[]
  servicesCountByCategory = [];
  _mainService: any[]
  salonServices: any[]
  //techServices: any[];
  subStyleServices = []
  servicesSortedByCat = []
  Categories = []
  selectedServicesObjects = []
  totalPrice = 0.0
  totalTime = 0
  selectedTech: Sal_Techs;
  selectedTechServices: Services[]
  appointment: Tech_appointments;
  isAppoTechChanged = false
  public offersObjects: OFFERS[];
  public unregisterBackButtonAction: any;
  public rescheduleAppointment: Tech_appointments
  //# NaParams
  isRescheduleAppointment = false
  //appServices = []
  constructor(
    public sqlProvider: SqliteDbProvider,
    public navCtrl: NavController,
    public toast: ToastController,
    public navParams: NavParams,
    public platform: Platform,
    public serviceManager: GlobalProvider,
    private nativePageTransitions: NativePageTransitions,
    private salonServiceModal: SalonServicesModal,
    
  ) {
    // this.techServices = []
    this.selectedSalon = this.navParams.get("selectedSalon");
    this.salonForComponent = this.navParams.get('salonForComponent');
    this.rescheduleAppointment = this.navParams.get('rescheduleAppointment')
    this.selectedServicesObjects = this.navParams.get("selectedServicesObjects");
    this.salonServiceModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServiceModal.getSalonOffers(this.selectedSalon.sal_id).then(offersObjects => {
          this.offersObjects = offersObjects
        })
      }
    })
    this.allTechs = []

    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.sqlProvider.getSalonTechnicianBySalId(this.selectedSalon.sal_id).then(res => {
          this.allTechs = res;

        })
      }
    })

    // this.allTechs = this.selectedSalon['techs'];//06/09/2018

    this.allTechs.forEach(myTech => {
      return this.selectedTech && this.selectedTech.tech_id === myTech.tech_id ? myTech.isSelected = true : myTech.isSelected = false
      // return myTech.isSelected = false
    });

    // console.log(JSON.stringify(this.allTechs));
    // this.isRescheduleAppointment = this.navParams.get("isRescheduleAppointment");
    //this.appServices = this.navParams.get("appServices");
    // this.cust_id = navParams.get('cust_id');
    // this.appointment = navParams.get('existingAppo');
    this.salonTechs = this.allTechs;
    /* Finding selected to show to user */
    if (this.appointment !== undefined) {
      // reschedule appointment
      // this.totalTime = Number(this.appointment.app_est_duration);
      // this.totalPrice = Number(this.appointment.app_price);
      for (let i = 0; i < this.salonTechs.length; i++) {
        let element = this.salonTechs[i];
        if (this.appointment.tech_id == element.tech_id) {
          this.selectedTechIndex = i;
          this.selectedTech = element;
          break;
        }
      }
    } else if (this.salonTechs.length > 0) {
      /* in case tech not found then need to show first tech */
      this.selectedTechIndex = 0;
      this.selectedTech = this.salonTechs[0];
    }
    /* populating tech services when tech is found and identified*/
    if (this.salonTechs.length > 0) {
      this.selectedTechDetail(this.salonTechs[this.selectedTechIndex], this.selectedTechIndex);
    }
  }

  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };


    if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(AddAppointmentPage)
    }

  }

  //custom back button 
  ionViewDidEnter() {

    this.initializeBackButtonCustomHandler();
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    // this.navCtrl.pop();
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
    

  }
  scrollTo(index) {
    this.selectedTechIndex = index;
  }
  goBack() {
    this.navCtrl.pop();
  }
  selectedTechDetail(selectedTechnician, index) {
    
    if (this.selectedTechIndex == index && !this.firstCall) {
      return;
    }
    this.firstCall = false
    this.selectedTechIndex = index;
    this.selectedTech = selectedTechnician;
    this.totalPrice = 0;
    this.totalTime = 0;
    this.selectedTech = selectedTechnician
    if (!this.appointment) {
      //  this.selectedServicesObjects = [];
    }
    this.subStyleServices = []
    // }
    this.servicesSortedByCat = [];
    this.servicesCountByCategory = []
    this.Categories.forEach(cat => {
      let catgegory: Services = cat
      var count = 0
      this.servicesSortedByCat.forEach(ser => {
        let service: Services = ser
        if (catgegory.ssc_id === service.ssc_id) {
          count += 1
        } else {
          return
        }
      });
      this.servicesCountByCategory.push(count)
    });
    this.isAppoTechChanged = true
  }
  OnTechSecltion(objTech, techIndex) {
    this.allTechs.map(tech => {
      if (tech === objTech) {
        tech.isSelected = !tech.isSelected
      } else {
        tech.isSelected = false

      }
    })
    if (objTech.isSelected) {
      this.techSelected = objTech;
    } else {
      this.techSelected = null
    }
    this.selectedTech = objTech;
  }
  btnNextTappe() {
    if (!this.techSelected) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgSelectMakeArtist)

      return
    }
    this.nativePageTransitions.slide(this.options)
    /** 
    console.log("Tech", JSON.stringify(this.selectedTech))
    console.log("salonForComponent",JSON.stringify(this.salonForComponent))

    console.log("salonForComponent",JSON.stringify(this.salonForComponent))
    console.log("selectedServicesObjects",JSON.stringify(this.selectedServicesObjects))
    console.log("selectedSalon",JSON.stringify(this.selectedSalon))
    console.log("rescheduleAppointment",JSON.stringify(this.rescheduleAppointment))
    console.log("offersObjects",JSON.stringify(this.offersObjects))
*/
    this.navCtrl.push(SlotsViewPage, {

      selectedTech: this.selectedTech,
      salonForComponent: this.salonForComponent,
      selectedServicesObjects: this.selectedServicesObjects,
      selectedSalon: this.selectedSalon,
      rescheduleAppointment: this.rescheduleAppointment,
      offersObjects: this.offersObjects
    })
  }
}