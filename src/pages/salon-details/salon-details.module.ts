import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalonDetailsPage } from './salon-details';

@NgModule({
  declarations: [
    SalonDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(SalonDetailsPage),
  ],
})
export class SalonDetailsPageModule {}
