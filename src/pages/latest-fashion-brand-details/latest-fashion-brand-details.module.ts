import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LatestFashionBrandDetailsPage } from './latest-fashion-brand-details';

@NgModule({
  declarations: [
    LatestFashionBrandDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(LatestFashionBrandDetailsPage),
  ],
})
export class LatestFashionBrandDetailsPageModule {}
