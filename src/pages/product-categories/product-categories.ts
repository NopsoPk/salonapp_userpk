import { NOPSO_Header_OPTIONS, PRODUCT_CATEGORY, PRODUCT, PRODUCT_BRANDS } from './../../providers/SalonAppUser-Interface';
import { GlobalProvider } from './../../providers/global/global';
import { CustomerModal } from './../../providers/CustomerModal/CustomerModal';
import { MainHomePage } from './../main-home/main-home';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Navbar, Events } from 'ionic-angular';
import { BeautyTipsDetailPage } from '../beauty-tips-detail/beauty-tips-detail';
import { config } from '../../providers/config/config';
import { ProductModal } from '../../providers/ProductModal/ProductModal';
import { ProductCategoryModal } from '../../providers/ProductCategoryModal/ProductCategoryModal';
import { ProductsPage } from '../products/products';
import { ProductColorModal } from '../../providers/ProductColorModal/ProductColorModal';
import { ProductImageModal } from '../../providers/ProductImageModal/ProductImageModal';
import { ViewCartPage } from '../view-cart/view-cart';
import { Content } from 'ionic-angular';
import { CartModal } from '../../providers/cartModal/cartModal';
import { Platform } from 'ionic-angular';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { ProductBrandModal } from '../../providers/ProductBrandModal/ProductBrandModal';
import { MenuController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-product-categories',
  templateUrl: 'product-categories.html',
})
export class ProductCategoriesPage {

  @ViewChild(Navbar) navBar: Navbar;
  @ViewChild(Content) content: Content;

  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };

  public ProductCategories: PRODUCT_CATEGORY[] = []
  public ProductBrands: PRODUCT_BRANDS[] = []
  public categoryImageUrl = config.prouductImages
  public nopsoHeaderOptions: NOPSO_Header_OPTIONS
  public shouldShowMiniHeader = false
  public shouldShowMiniHeaderPrevious = false
  public CartItems: PRODUCT[] = []
  public myScrollToTop = 0
  public isForProductCategory = true;
  constructor(
    private menu: MenuController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private zone: NgZone,
    public events: Events,
    public customerModal: CustomerModal,
    public serviceManager: GlobalProvider,
    private nativePageTransitions: NativePageTransitions,
    public productCatModal: ProductCategoryModal,
    public productBrandModal: ProductBrandModal,
    public ProductModal: ProductModal,
    public cartModal: CartModal,
    public productColorModal: ProductColorModal,
    public productImageModal: ProductImageModal,
    public platform: Platform,
  ) {
    this.nopsoHeaderOptions = {
      miniHeaderHeight: '50pt',
      calledFrom: config.ProductCategoriesPage,
    }
    // this.serviceManager.removeFromStorageForTheKey(this.serviceManager.PRODUCT_CATEGORIES_LAST_FETCHED_DATE)
    // this.serviceManager.removeFromStorageForTheKey(this.serviceManager.TIPS_DETAILS_LAST_FETCHED_DATE)
    let isFromCategory = this.navParams.get('isForProductCategory')
    if (isFromCategory) {
      this.isForProductCategory = true;
      let isBTFetched = this.serviceManager.getFromLocalStorage(this.serviceManager.PRODUCT_CATEGORIES_LAST_FETCHED_DATE)
      if (isBTFetched) {
        this.productCatModal.getDatabaseState().subscribe(ready => {
          if (ready) {
            this.productCatModal.getProductCategory().then(beautyTips => {
              this.ProductCategories = beautyTips
              // this.ProductCategories = this.ProductCategories.concat(this.ProductCategories)
              this.getProdCategoriesFromServer()

            })
          }
        })
      } else {
        this.getProdCategoriesFromServer()
      }
    } else {
      
      this.isForProductCategory = false
      let isBTFetched = this.serviceManager.getFromLocalStorage(this.serviceManager.PRODUCT_BRANDS_LAST_FETCHED_DATE)
      if (isBTFetched) {
        this.productBrandModal.getDatabaseState().subscribe(ready => {
          if (ready) {
            this.productBrandModal.getProductAllBrands().then(productBrands => {
              this.ProductBrands = productBrands
              this.getProdBrandsFromServer()

            })
          }
        })
      } else {
        this.getProdBrandsFromServer()
      }
    }
  }

  public unregisterBackButtonAction: any;

  //custom back button for android
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    // alert('custom-back-button')
    // this.navCtrl.pop();
    if(this.menu.isOpen()){
      this.menu.close();
    }else {
       if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
    }
  }
  //end custom back button for android

  ionViewDidLoad() {
    
  }
  ionViewWillEnter() {
    this.shouldShowMiniHeader = this.shouldShowMiniHeaderPrevious
  }

  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
    this.content.scrollTo(0, 0).then(res => {
      this.content.resize()
      this.content.scrollTo(0, this.myScrollToTop, 900).then(res => {

      })
    })
    this.events.subscribe('canShowMiniHeader', (isHidden) => {
      this.zone.run(() => {
        this.shouldShowMiniHeader = isHidden
      });
    });

    this.productCatModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.cartModal.getCartItems().then(cartItems => {
          this.CartItems = cartItems
        })
      }
    })
  }
  goBack() {
    this.navCtrl.canGoBack() ? this.navCtrl.pop() : this.navCtrl.setRoot(MainHomePage)
  }

  categoryClicked(selectedProductCategory, index) {
    this.myScrollToTop = this.content.scrollTop
    this.nativePageTransitions.slide(this.options)
    this.shouldShowMiniHeaderPrevious = this.shouldShowMiniHeader
    this.navCtrl.push(ProductsPage, {
      selectedProductCategory: selectedProductCategory
    })
  }

  brandClicked(selectedProductBrand, index) {
    this.myScrollToTop = this.content.scrollTop
    this.nativePageTransitions.slide(this.options)
    this.shouldShowMiniHeaderPrevious = this.shouldShowMiniHeader
    this.navCtrl.push(ProductsPage, {
      selectedProductBrand: selectedProductBrand
    })
  }

  getProdCategoriesFromServer() {
    // let lastFetchDate = this.serviceManager.getFromLocalStorage(this.serviceManager.LAST_FETCH_DATE_FOR_BEAUTY_TIPS)
    let lastFetchDate = this.serviceManager.getFromLocalStorage(this.serviceManager.PRODUCT_CATEGORIES_LAST_FETCHED_DATE)
    let responseTime = lastFetchDate ? lastFetchDate : ''
    const params = {
      service: btoa('get_product_categories'),
      last_fetched: btoa(responseTime)
    }
    !lastFetchDate || lastFetchDate.length === 0 ? this.serviceManager.showProgress() : ''
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          //added by rizwan 
        
          if (res.inactive_categories && res.inactive_categories.length > 0) {
            this.productCatModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.productCatModal.delteDuplicateCategories(res.inactive_categories).then(() => {
                  
                });
              }
            });
          }
          //end added by rizwan

          !lastFetchDate || lastFetchDate.length === 0 ? this.serviceManager.stopProgress() : ''
          if (!res.ProductCategories || res.ProductCategories.length === 0) {
            this.serviceManager.setInLocalStorage(this.serviceManager.PRODUCT_CATEGORIES_LAST_FETCHED_DATE, res.response_datetime)
            this.getProductsFromServer()
            return
          }
          this.ProductCategories=  []
          this.ProductCategories = res.ProductCategories
          // this.ProductCategories = this.ProductCategories.concat(this.ProductCategories)
          this.productCatModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.productCatModal.InsertInToProductCategoryTable(this.ProductCategories).then(done => {
                this.serviceManager.setInLocalStorage(this.serviceManager.PRODUCT_CATEGORIES_LAST_FETCHED_DATE, res.response_datetime)
              }).then(jobsDone => {
                this.getProductsFromServer()
              })
            }
          })
        },
        (error) => {
          this.serviceManager.stopProgress()
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        })
  }
  getProdBrandsFromServer() {
    let lastFetchDate = this.serviceManager.getFromLocalStorage(this.serviceManager.PRODUCT_BRANDS_LAST_FETCHED_DATE)
    let responseTime = lastFetchDate ? lastFetchDate : ''
    const params = {
      service: btoa('get_product_brands'),
      last_fetched: btoa(responseTime)
    }
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {

          console.log(res);
          if (res.inactive_brands && res.inactive_brands.length > 0) {
            this.productBrandModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.productBrandModal.delteDuplicateProductBrands(res.inactive_brands).then(() => {                  
                });
              }
            });
          }                  
          if (!res.ProductBrands) {
            this.serviceManager.setInLocalStorage(this.serviceManager.PRODUCT_BRANDS_LAST_FETCHED_DATE, res.response_datetime)
            return
          }


          if (!res.ProductBrands || res.ProductBrands.length === 0) return

          this.productBrandModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.productBrandModal.InsertInToProductBrandsTable(res.ProductBrands).then(done => {
                this.serviceManager.setInLocalStorage(this.serviceManager.PRODUCT_BRANDS_LAST_FETCHED_DATE, res.response_datetime)
              })
            }
          })
        },
        (error) => {
          this.serviceManager.stopProgress()
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        })
  }
  updateUrl() {

    this.categoryImageUrl = config.prouductImages


  }
  getProductsFromServer() {

    // if(true)return 
    let lastFetchDate = this.serviceManager.getFromLocalStorage(this.serviceManager.PRODUCTS_LAST_FETCHED_DATE)
    let responseTime = lastFetchDate ? lastFetchDate : ''

    const params = {
      service: btoa('get_products'),
      last_fetched: btoa(responseTime)
    }

    !lastFetchDate || lastFetchDate.length === 0 ? this.serviceManager.showProgress() : ''

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {

          !lastFetchDate || lastFetchDate.length === 0 ? this.serviceManager.stopProgress() : ''
          if (!res.products) {
            this.serviceManager.setInLocalStorage(this.serviceManager.PRODUCTS_LAST_FETCHED_DATE, res.response_datetime)
            return
          }
          let beautyTipsDetail: PRODUCT[] = res.products

          this.ProductModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.ProductModal.InsertInToProductTable(beautyTipsDetail).then(done => {
                this.serviceManager.setInLocalStorage(this.serviceManager.PRODUCTS_LAST_FETCHED_DATE, res.response_datetime)
                res.product_images && this.productImageModal.InsertInToProductImagesTable(res.product_images)
                res.product_colors && this.productColorModal.InsertInToProductColorTable(res.product_colors)
              })
            }
          })
        },
        (error) => {
          this.serviceManager.stopProgress()
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        })
  }
  btnBackTapped() {
    
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };
    if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
  }

  btnViewCartTapped() {
    if (!this.CartItems || this.CartItems.length === 0) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgNoProductInCart)
      return
    }
    this.shouldShowMiniHeaderPrevious = this.shouldShowMiniHeader
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(ViewCartPage, {
      CartItems: this.CartItems,
      selectedProduct: null
    })
  }
}
