import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LatestTrendsArchiveCollectionPage } from './latest-trends-archive-collection';

@NgModule({
  declarations: [
    LatestTrendsArchiveCollectionPage,
  ],
  imports: [
    IonicPageModule.forChild(LatestTrendsArchiveCollectionPage),
  ],
})
export class LatestTrendsArchiveCollectionPageModule {}
