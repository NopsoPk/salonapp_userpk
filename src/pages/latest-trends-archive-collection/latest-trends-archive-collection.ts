
import { CustomerModal } from './../../providers/CustomerModal/CustomerModal';
import { SalonServicesModal } from './../../providers/salon-services-modal/salon-services-modal';
import { GlobalProvider } from './../../providers/global/global';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalServiceProvider } from '../../providers/global-service/global-service'
import { MenuController } from 'ionic-angular';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db'
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { GoogleAnalytics } from '../../../node_modules/@ionic-native/google-analytics';
import { IonicPage, NavController, NavParams, Navbar, Content , Events, LoadingController, PopoverController, Platform} from 'ionic-angular';
import { Component, ViewChild, NgZone } from '@angular/core';
import {  BEAUTY_TIPS, TIPS_DETAIL, NOPSO_Header_OPTIONS } from './../../providers/SalonAppUser-Interface';
import { config } from '../../providers/config/config';
import { MainHomePage } from './../main-home/main-home';
import { LatestTrendsDetailPage } from './../latest-trends-detail/latest-trends-detail';
import { SALON_SERVICES_SUB_CATEGORIES, LATEST_TRENDS_STYLES, SalonServices, Salon, SALON_SERVICES_SEARCH, ServiceSubCATEGORIES, Customer, Homepage, CATEGORY_SUB_STYLE, Category } from './../../providers/SalonAppUser-Interface';
import { AppMessages } from '../../providers/AppMessages/AppMessages';

/**
 * Generated class for the LatestTrendsArchiveCollectionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-latest-trends-archive-collection',
  templateUrl: 'latest-trends-archive-collection.html',
})
export class LatestTrendsArchiveCollectionPage {
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  public unregisterBackButtonAction: any;
  public imageUrl = config.CategoryImageURL
  public LatestTrendsStyles = Array();
  public LatestTrendsStylesMale = Array();
  public LatestTrendsStylesFemale = Array();
  public LatestTrendsSubStyles = Array();
  public StylesBackup = Array();
  public myCustomer: Customer
  public cateogry = ""
  public defaultImage = 'default.png'
  public shouldShowMenTrends: boolean
  public shouldShowWomenTrends: boolean
  public GenderBasedTrends: string
  public parentPage: Homepage
  public parentPageDescription
  public SALON_SERVICE_SEARCH: SALON_SERVICES_SEARCH[]
  public ServiceSubCATEGORIE: ServiceSubCATEGORIES[]
  public selectedStyle: Category
  public noStyleFound = false
  public HeaderHeightIsZero = false
  public isAppLaunchedFirstTime = 'isAppLaunchedFirstTime'
  public myScrollToTop = 0
  twoBeautyTips: any = [];
  public slider: HTMLElement
  public nopsoHeaderOptions: NOPSO_Header_OPTIONS
  @ViewChild(Content) content: Content;
  @ViewChild(Navbar) navBar: Navbar;
  ionScrollTop = 0
  public isFirstTime=false
  public shouldShowMiniHeader = false
 
  constructor(
    private menu: MenuController,
    public platform: Platform,
    private zone: NgZone,
    public http: Http,
    public sqlProvider: SqliteDbProvider,
    public salonServicesModal: SalonServicesModal,
    public globalSearch: GlobalServiceProvider,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public serviceManager: GlobalProvider,
    private nativePageTransitions: NativePageTransitions,
    public customerModal: CustomerModal,
    public ga: GoogleAnalytics,
    public events: Events,
    public popoverCtrl: PopoverController,
    
    ) {
      this.nopsoHeaderOptions = {
        miniHeaderHeight: '50pt',
        calledFrom: config.BeautyTipsViewPage,
      }
      this.isFirstTime=true;
      // this.GenderBasedTrends=navParams.get('selectedGender')

  }

  

  ionViewDidEnter() {
    let abc = false
    this.events.subscribe('canShowMiniHeader', (isHidden) => {
      this.zone.run(() => {
        this.shouldShowMiniHeader = isHidden
      });
    });
 //page refresh on comming back from detail page
 if(!this.isFirstTime){
  //this.scrollToTop();
 
  let options: NativeTransitionOptions = {
    direction: 'top',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 150,
    androiddelay: 0,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 0
    };
  this.nativePageTransitions.fade(options);
  this.navCtrl.setRoot(LatestTrendsArchiveCollectionPage, {}, {animate: false});

  //this.navCtrl.setRoot(this.navCtrl.getActive().component);
}else{
  this.isFirstTime=false
}
//End page refresh on comming back from detail page

  }
  //custom back button for android
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  
  private customHandleBackButton(): void {
    // this.navCtrl.pop();
    if(this.menu.isOpen()){
      this.menu.close();
    }else {
       if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
    }

  }
//end custom back button for android

  getCustomer() {
    if (this.myCustomer && this.LatestTrendsStyles && this.LatestTrendsStyles.length > 1) {
      console.log('went back');
      return
    }

    console.log('came in getCustomer');
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(customer => {
          if (customer) {
            this.myCustomer = customer
            this.googleAnalytics()
            if (!this.LatestTrendsStyles || this.LatestTrendsStyles.length === 0){
              this.getLatestTrendsSTylesFromDB();
              
            }
        

          } else {

            // this.serviceManager.makeToastOnFailure(AppMessages.msgCustomerDataMissing)
            // this.navCtrl.setRoot(ActivateSalonPage)
          }
        }, error => {
          console.log('error: while getting customer');
        })
      }
    })
  }
  googleAnalytics() {
    this.ga.startTrackerWithId(config.GoogleAnalyticsAppID)
      .then(() => {
        console.log('Google analytics is ready now');
        this.ga.trackView(this.myCustomer.cust_name + ' on latest-trends screen');
        this.ga.trackEvent('cat: ionViewWill Enter', 'act: loads everytime', 'lab: general label', 200, true)
        this.ga.trackTiming('cat: tracking timing', 600000, 'variable: not sure what will go here', 'label: and the same')
        this.ga.debugMode();
        this.ga.setAllowIDFACollection(true);
        this.ga.setUserId(this.myCustomer.cust_id)
        // Tracker is ready
        // You can now track pages or set additional information such as AppVersion or UserId
      })
      .catch(e => {
        console.log('Error starting GoogleAnalytics', e)
      });
  }
  getLatestTrendsSTylesFromDB() {
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServicesModal.createServiceCategoryTable().then(res => {
          this.salonServicesModal.getServiceCategoriesArchive().then(serviceCategories => {

         
            // serviceCategories.forEach(category => {
            //   return category.expanded = false
            // });
            serviceCategories.forEach(category => {

              if(category.sc_gender== 2 ){//gender base sorting
                this.LatestTrendsStylesFemale.push(category)
               // this.FashionStyFeatureItemMen.push(category)
              }else{//for male rest is same 
              //push trends list men
              this.LatestTrendsStylesMale.push(category)
              }

              //category.sc_gender == 2 ? this.LatestTrendsStylesFemale.push(category) : this.LatestTrendsStylesMale.push(category)
            });

           // this.GenderBasedTrends=

            let gender = this.navParams.get('selectedGender');
            if (gender && gender === 1) {
              this.GenderBasedTrends = 'man'
              this.LatestTrendsStyles = []
              this.LatestTrendsStyles = this.LatestTrendsStylesMale
             // this.ShowMenTrends()
             this.menTrendsTapepd()

            } else {
              
              this.GenderBasedTrends = 'woman'
              this.LatestTrendsStyles = []
              this.LatestTrendsStyles = this.LatestTrendsStylesFemale
              this.womenTrendsTapepd()
              //this.ShowWomenTrends()
            }
          })
        })
      }
    })
  }
  ionViewDidLoad() {

    this.content.ionScroll.subscribe(ev =>
      requestAnimationFrame(() => this.ionScrollChange(ev.scrollTop))

    );
   this.getCustomer();
  // this.getLatestTrendsSTylesFromDB();
  }
  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };


    if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

    if (this.navCtrl.canGoBack()) {

      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
  }
  MainCtrl($scope, $ionicScrollDelegate) {
    console.log('scroll did begin');

    $scope.scrollMainToTop = function () {
      $ionicScrollDelegate.$getByHandle('mainScroll').scrollTop();
    };
    $scope.scrollSmallToTop = function () {
      $ionicScrollDelegate.$getByHandle('small').scrollTop();
    };
  }
  ionScrollChange(scrollTop) {
    
    this.zone.run(() => {
      this.ionScrollTop = scrollTop
    });
  }
  menTrendsTapepd() {
    this.slider = document.getElementById('slider_collection');
    this.slider.style.cssFloat = 'left';
    this.LatestTrendsStyles = []
    this.LatestTrendsStyles = this.LatestTrendsStylesMale;
    !this.LatestTrendsStyles || this.LatestTrendsStyles.length === 0 ? this.noStyleFound = true : this.noStyleFound = false
    this.shouldShowMenTrends = true
    this.shouldShowWomenTrends = false

    
  }
  womenTrendsTapepd() {
    this.slider = document.getElementById('slider_collection');
    this.slider.style.cssFloat = 'right';
    this.LatestTrendsStyles = []
    this.LatestTrendsStyles = this.LatestTrendsStylesFemale;
    !this.LatestTrendsStyles || this.LatestTrendsStyles.length === 0 ? this.noStyleFound = true : this.noStyleFound = false
    this.shouldShowMenTrends = false
    this.shouldShowWomenTrends = true

    //this.ShowWomenTrends()
  }
  categoryClicked(item, index){
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push( LatestTrendsDetailPage, {
      serviceCat: item
    });
     //if (selectedSubStyles) {
    //this.serviceManager.setInLocalStorage(this.serviceManager.OBJ_SUB_STYLE, selectedSubStyles)
    // }
  }
}
