import { CustomerModal } from './../../providers/CustomerModal/CustomerModal';
import { SalonServicesModal } from './../../providers/salon-services-modal/salon-services-modal';
import { Keyboard } from '@ionic-native/keyboard';

import { LatestTrensTipsCategoryPage } from './../latest-trens-tips-category/latest-trens-tips-category';
import { config } from './../../providers/config/config';
import { MainHomePage } from './../main-home/main-home';
import { GlobalProvider } from './../../providers/global/global';
import { Component, OnInit, Input, Output, EventEmitter, trigger, state, style, transition, animate } from '@angular/core';
import { App, IonicPage, LoadingController, NavController, NavParams, Platform, PopoverController, Alert, Events } from 'ionic-angular';
import { LatestTrendSubCatPage } from '../latest-trend-sub-cat/latest-trend-sub-cat'
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalServiceProvider } from '../../providers/global-service/global-service'

import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db'
import { SALON_SERVICES_SEARCH, ServiceSubCATEGORIES, Customer, Homepage, CATEGORY_SUB_STYLE, Category } from './../../providers/SalonAppUser-Interface';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { GoogleAnalytics } from '../../../node_modules/@ionic-native/google-analytics';
import { Content } from 'ionic-angular';
import { ViewChild } from '@angular/core';

import * as $ from 'jquery'
import { AppMessages } from '../../providers/AppMessages/AppMessages';

@IonicPage()
@Component({
  selector: 'page-latest-trends',
  templateUrl: 'latest-trends.html',
  animations: [
    trigger('expand', [
      state('ActiveGroup', style({ opacity: '1', height: '*' })),
      state('NotActiveGroup', style({ opacity: '0', height: '0', overflow: 'hidden' })),
      transition('ActiveGroup <=> NotActiveGroup', animate('600ms ease-in-out'))
    ]),
  ]
})
export class LatestTrendsPage {

  isAndroidPlatForm = true;

  @ViewChild(Content) content: Content;
  state: string = 'NotActiveGroup';

  itemExpandHeight: number = 200;
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };


  public imageUrl = config.CategoryImageURL
  public LatestTrendsStyles = Array();

  public LatestTrendsStylesMale = Array();
  public LatestTrendsStylesFemale = Array();


  public LatestTrendsSubStyles = Array();


  public StylesBackup = Array();
  public myCustomer: Customer
  public cateogry = ""
  public defaultImage = 'default.png'
  public txtSearchSalon: string = ''
  public shouldShowMenTrends: boolean
  public shouldShowWomenTrends: boolean
  // public shouldShowAllTrends: boolean
  public GenderBasedTrends: string
  public showTagsPopOver = false
  public parentPage: Homepage
  public parentPageDescription
  public filteredTags = []
  public SALON_SERVICE_SEARCH: SALON_SERVICES_SEARCH[]
  public ServiceSubCATEGORIE: ServiceSubCATEGORIES[]

  public selectedStyle: Category
  public noStyleFound = false
  public HeaderHeightIsZero = false
  public isAppLaunchedFirstTime = 'isAppLaunchedFirstTime'
  // public isHeaderHidden: boolean = false
  public myScrollToTop = 0

  constructor(
    public http: Http,
    public sqlProvider: SqliteDbProvider,
    public salonServicesModal: SalonServicesModal,
    public globalSearch: GlobalServiceProvider,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public serviceManager: GlobalProvider,
    private nativePageTransitions: NativePageTransitions,
    public platform: Platform,
    public keyboard: Keyboard,
    public customerModal: CustomerModal,
    public ga: GoogleAnalytics,
    public popoverCtrl: PopoverController,
    public events: Events,
    public plt: Platform,
  ) {
    //  alert('Page-1st')
    if (this.plt.is('ios')) {
      this.isAndroidPlatForm = false;
      console.log('I am an iOS device!');
    } else {
      this.isAndroidPlatForm = true;
    }



    this.filteredTags = []
    this.LatestTrendsStyles = []
    this.events.subscribe('HeaderIsHidden', isHidden => {

      if (isHidden) {
        var x = document.getElementById("HideThisBaster");
        x.style.display = "block";
      }
      else {

        var x = document.getElementById("HideThisBaster");
        x.style.display = "none";
      }
    });

    this.parentPage = this.navParams.get('parentPage')
    if (this.parentPage) {
      this.parentPageDescription = this.parentPage.sectionDescription
    } else {
      let description = this.serviceManager.getFromLocalStorage(this.serviceManager.LATEST_TRENDS_DESCRIPTION)
      if (description) {
        this.parentPageDescription = description
      }
    }
  }
  ionViewWillEnter() {

    this.content.scrollTo(0, 0).then(res => {
      this.content.resize()
      this.content.scrollTo(0, this.myScrollToTop, 900).then(res => {

      })
    })

    if (this.selectedStyle) {
      this.expandItem(this.selectedStyle)
    }

    this.getCustomer()
  }
  ionViewDidLoad() {
    var x = document.getElementById("HideThisBaster");
    x.style.display = "none";
    console.log('ionViewDidLoad LatestTrendsPage');


  }
  ionViewWillLeave() {
    this.content.scrollToTop()
  }

  fileteLatestTrendStyles(serviceCategories) {
    this.LatestTrendsStylesMale = [];
    this.LatestTrendsStylesFemale = [];



  }
  getLatestTrendsSTylesFromDB() {
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServicesModal.createServiceCategoryTable().then(res => {
          this.salonServicesModal.getServiceCategories().then(serviceCategories => {

            serviceCategories.forEach(category => {
              return category.expanded = false
            });
            serviceCategories.forEach(category => {

              category.sc_gender == 2 ? this.LatestTrendsStylesFemale.push(category) : this.LatestTrendsStylesMale.push(category)
            });
            let gender = Number(this.myCustomer.cust_gender)
            if (gender && gender === 1) {
              this.GenderBasedTrends = 'man'
              this.LatestTrendsStyles = []
              this.LatestTrendsStyles = this.LatestTrendsStylesMale
              this.ShowMenTrends()

            } else {
              this.GenderBasedTrends = 'woman'
              this.LatestTrendsStyles = []
              this.LatestTrendsStyles = this.LatestTrendsStylesFemale
              this.ShowWomenTrends()
            }




          })
        })
      }
    })
  }
  getLatestesTrendsCategoriesFromServer() {

    let tagURL = 'http://www.laravel.nopso.qwpcorp.com/InstaApi/public/api/serviceCat';

    let loading = this.loadingController.create({ content: "Please Wait..." });
    loading.present();
    console.log('__url ', tagURL);

    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');

    let options = new RequestOptions({ headers: headers });

    this.http.get(tagURL, options)
      .map(Response => Response.json())
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(res => {

        loading.dismiss()
        if (res.data.categories != undefined) {
          if (res.data.categories.length > 0) {
            this.serviceManager.setInLocalStorage(this.isAppLaunchedFirstTime, this.isAppLaunchedFirstTime)
            this.LatestTrendsStyles = []
            this.LatestTrendsStyles = res.data.categories;
            this.LatestTrendsStyles.forEach(category => {
              return category.expanded = false
            });

            this.salonServicesModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.salonServicesModal.saveIntoServiceCategoryTable(this.LatestTrendsStyles).then(serviceCategories => {
                  console.log('success: jobs done');

                })
              }
            })
            // console.log(JSON.stringify(this.LatestTrendsStyles));
          } else {
            this.LatestTrendsStyles = []
            console.log('No Latest Trend...');
            this.LatestTrendsStyles.length === 0 ? this.noStyleFound = true : this.noStyleFound = false
          }
        }

      }, error2 => {
        this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
        loading.dismiss()
      }
      )
  }

  mainCategoryClicked(index) {

    this.navCtrl.push(LatestTrendSubCatPage, {
      selectedCat: this.LatestTrendsStyles[index]
    });
  }

  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };


    if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

    if (this.navCtrl.canGoBack()) {

      this.navCtrl.pop();
    } else {

      this.navCtrl.setRoot(MainHomePage)
    }

  }


  shouldBeginEditing(value) {

  }
  endEndEditing(value) {
    if (this.txtSearchSalon && this.txtSearchSalon.trim().length === 0) {
      this.filteredTags = []
      this.showTagsPopOver = false
    }
  }

  shouldChangeCharacter(search) {
    let searchGender = "2";

    if (this.shouldShowMenTrends) {
      searchGender = "1";
    } else {
      searchGender = "2";
    }


    console.log("Gender", searchGender)
    let searchValue: string = search.value
    if (searchValue.trim().length === 0) {
      this.filteredTags = []
      this.showTagsPopOver = false
      return
    }


    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {


        //old search
        // this.salonServicesModal.getKeyWordsOnlyForLatestTrendsSearch(searchValue).then(res => {

        this.salonServicesModal.getSearchForLatestTrendsSearch(searchValue, searchGender).then(res => {
          if (res && res.length > 0) {
            this.filteredTags = res

            this.showTagsPopOver = true
            this.noStyleFound = false

          } else {
            this.filteredTags = []
            this.noStyleFound = true
            // this.showTagsPopOver = false
          }
          console.log('showTagsPopOver', this.showTagsPopOver);

        }, error => {
          console.log('error while getting Search keywords');

        })

      }
    })

  }


  handleReturnkey() {
    this.keyboard.close()
  }
  menLatestTrendsTapped() {
    this.LatestTrendsStyles = []
    this.filteredTags = []
    this.txtSearchSalon = ''
    this.showTagsPopOver = false
    this.LatestTrendsStyles = this.LatestTrendsStylesMale;
    this.ShowMenTrends()

  }


  wommenLatestTrendsTapped() {
    this.LatestTrendsStyles = []
    this.filteredTags = []
    this.txtSearchSalon = ''
    this.showTagsPopOver = false
    this.LatestTrendsStyles = this.LatestTrendsStylesFemale;
    this.ShowWomenTrends()

  }
  ShowMenTrends() {

    !this.LatestTrendsStyles || this.LatestTrendsStyles.length === 0 ? this.noStyleFound = true : this.noStyleFound = false
    this.shouldShowMenTrends = true
    this.shouldShowWomenTrends = false

  }

  ShowWomenTrends() {

    !this.LatestTrendsStyles || this.LatestTrendsStyles.length === 0 ? this.noStyleFound = true : this.noStyleFound = false
    this.shouldShowMenTrends = false
    this.shouldShowWomenTrends = true

  }

  categoryClicked(index) {

    console.log('Cats...zz');
    console.log(this.LatestTrendsStyles[index]);
    let selectedSubStyles = this.LatestTrendsStyles[index]

    if (selectedSubStyles) {
      this.serviceManager.setInLocalStorage(this.serviceManager.OBJ_SUB_STYLE, selectedSubStyles)
    }
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(LatestTrensTipsCategoryPage, {
      subCat: this.LatestTrendsStyles[index]
    });
  }

  showPostForSearchKeyWords(SearchObject, ind) {


    console.log(JSON.stringify(SearchObject));





    let baseURLTogetPinsForSubStyle = config.getPinsForSubStyleURL + SearchObject.keyword + '/' + this.myCustomer.cust_id
    console.log(baseURLTogetPinsForSubStyle);
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(LatestTrensTipsCategoryPage, {
      getPinsForSubStyleURL: baseURLTogetPinsForSubStyle,
      cateogry: SearchObject.keyword
    });

  }

  public highlight(SubStyleName: string) {
    if (!this.txtSearchSalon) {
      return SubStyleName;
    }
    return SubStyleName.replace(new RegExp(this.txtSearchSalon, "gi"), match => {
      return '<span class="highlightText">' + match + '</span>';
    });
  }

  getCustomer() {
    if (this.myCustomer && this.LatestTrendsStyles && this.LatestTrendsStyles.length > 1) {
      console.log('went back');

      return
    }
    console.log('came in getCustomer');


    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(customer => {
          if (customer) {
            this.myCustomer = customer
            this.googleAnalytics()
            if (!this.LatestTrendsStyles || this.LatestTrendsStyles.length === 0) this.getLatestTrendsSTylesFromDB();



            // this.salonServicesModal.getSubCategories().then(res => {
            //   // this.LatestTrendsStyles = res
            //   // this.StylesBackup = res

            //   this.MenStyles = this.LatestTrendsStyles.filter(function (style) {
            //     return (Number(style.ssc_gender) === 1 || Number(style.ssc_gender) === 3)
            //   })

            //   this.WomenStyles = this.LatestTrendsStyles.filter(function (style) {
            //     return (Number(style.ssc_gender) === 2 || Number(style.ssc_gender) === 3)
            //   })

            //   /* Showing filtered Styles to customer respective of gender  */
            //   let gender = Number(this.myCustomer.cust_gender)
            //   if (gender && gender === 1) {
            //     this.GenderBasedTrends = 'man'
            //     this.ShowMenTrends()

            //   } else {
            //     this.GenderBasedTrends = 'woman'
            //     this.ShowWomenTrends()
            //   }

            // })
          } else {

            // this.serviceManager.makeToastOnFailure(AppMessages.msgCustomerDataMissing)
            // this.navCtrl.setRoot(ActivateSalonPage)
          }
        }, error => {
          console.log('error: while getting customer');
        })
      }
    })
  }
  googleAnalytics() {
    this.ga.startTrackerWithId(config.GoogleAnalyticsAppID)
      .then(() => {
        console.log('Google analytics is ready now');
        this.ga.trackView(this.myCustomer.cust_name + ' on latest-trends screen');
        this.ga.trackEvent('cat: ionViewWill Enter', 'act: loads everytime', 'lab: general label', 200, true)
        this.ga.trackTiming('cat: tracking timing', 600000, 'variable: not sure what will go here', 'label: and the same')
        this.ga.debugMode();
        this.ga.setAllowIDFACollection(true);
        this.ga.setUserId(this.myCustomer.cust_id)
        // Tracker is ready
        // You can now track pages or set additional information such as AppVersion or UserId

      })
      .catch(e => {
        console.log('Error starting GoogleAnalytics', e)

      });

  }
  // presentPopover(myEvent, objcategory) {
  //   console.log(JSON.stringify(objcategory));
  //   this.salonServicesModal.getDatabaseState().subscribe(ready => {
  //     if (ready) {
  //       this.salonServicesModal.getSubCategoriesOfCategory(objcategory.sc_id).then(res => {
  //         console.log(res.length);
  //         let popover = this.popoverCtrl.create(SubcategoriesPopOverPage, {


  //           subCategories: res,
  //           scrollHeight: res.length * 30
  //         }, {
  //             cssClass: 'subCategoryPopOverClass'
  //           });
  //         popover.present({
  //           ev: myEvent,

  //         });

  //       })
  //     }
  //   })



  // }

  expandItem(style) {
    this.selectedStyle = style
    this.state = this.state == 'NotActiveGroup' ? 'ActiveGroup' : 'NotActiveGroup';

    // $( "button.ExpandAble" ).children().css( "background-color", "red" );
    $("#book").slideDown("slow", function () {
      // Animation complete.
    });

    if (!style.expanded) {
      console.log('came in ifff');

      this.LatestTrendsSubStyles = []
      this.salonServicesModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.salonServicesModal.getSubCategoriesOfCategory(style.sc_id).then(res => {
            this.LatestTrendsSubStyles = res

            this.CategoryExpandableToggle(style)


          })
        }
      })
    } else {
      console.log('came in else');

      this.LatestTrendsSubStyles = []
      this.CategoryExpandableToggle(style)
    }
  }
  CategoryExpandableToggle(item) {
    this.LatestTrendsStyles.map((cateogry) => {

      if (item == cateogry) {
        cateogry.expanded = !cateogry.expanded;
      } else {
        cateogry.expanded = false;
      }

      return cateogry;

    });
  }


  showPostsForStyles(subStyle1) {
    let subStyle;

    if (subStyle1.keyword) {
      subStyle = {
        ssc_name: subStyle1.keyword,
        ssc_id: subStyle1.refer_id,
        ssc_image: subStyle1.image_url
      }
    } else {
      subStyle = subStyle1;
    }

    this.myScrollToTop = this.content.scrollTop

    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(LatestTrensTipsCategoryPage, {
      subCat: subStyle
    });
  }

  isOpen = false
  here() {
    this.isOpen = !this.isOpen
    this.isOpen === true ? $('#zahid').removeClass('open') : $('#zahid').addClass('open');
  }

  MainCtrl($scope, $ionicScrollDelegate) {
    console.log('scroll did begin');

    $scope.scrollMainToTop = function () {
      $ionicScrollDelegate.$getByHandle('mainScroll').scrollTop();
    };
    $scope.scrollSmallToTop = function () {
      $ionicScrollDelegate.$getByHandle('small').scrollTop();
    };
  }
}
