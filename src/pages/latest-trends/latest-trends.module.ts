import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LatestTrendsPage } from './latest-trends';

@NgModule({
  declarations: [
    LatestTrendsPage,
  ],
  imports: [
    IonicPageModule.forChild(LatestTrendsPage),
  ],
})
export class LatestTrendsPageModule {}
