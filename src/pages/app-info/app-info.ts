import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions';
import { MainHomePage } from '../main-home/main-home';
import { GlobalProvider } from '../../providers/global/global';
import { ViewController } from 'ionic-angular/navigation/view-controller';

import { Storage } from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen';



@IonicPage()
@Component({
  selector: 'page-app-info',
  templateUrl: 'app-info.html',
})
export class AppInfoPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public nativePageTransitions: NativePageTransitions,
    public serviceManager: GlobalProvider,
    public viewCtrl: ViewController,
    public storage: Storage,
    public splashScreen: SplashScreen

  ) {
  }
  selectedEnviroment: number
  ionViewDidLoad() {
    // this.selectedEnviroment = this.serviceManager.getFromLocalStorage('zahidzahidzahid')
    this.selectedEnviroment = Number(localStorage.getItem('zahidzahidzahid'))


    if (!this.selectedEnviroment) this.selectedEnviroment = 1

    console.log('ionViewDidLoad AppInfoPage');
  }
  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };

    if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
  }

  switchEnviroment(environment: number) {
    this.selectedEnviroment = environment
    this.viewCtrl.dismiss().then(() => {
      window['sqlitePlugin'].deleteDatabase({ name: "ionicdb.db", location: 1 }, this.onSuccess(), this.onERror());

    });

    // switch (environment) {
    //   case 1:  // this means enviroment is connected with live api
    //     this.viewCtrl.dismiss().then(() => {
    //       window['sqlitePlugin'].deleteDatabase({ name: "ionicdb.db", location: 1 }, this.onSuccess(), this.onERror());

    //     });
    //     break;
    //   case 2: // this means enviroment is connect with Dev Mode
    //     this.viewCtrl.dismiss().then(() => {

    //       window['sqlitePlugin'].deleteDatabase({ name: "ionicdb.db", location: 1 }, this.onSuccess(), this.onERror());

    //     });
    //     break;
    //   default:
    //     // this.serviceManager.setInLocalStorage('zahidzahidzahid', 1)
    //     localStorage.setItem('zahidzahidzahid', '1')

    //     this.viewCtrl.dismiss().then(() => {

    //       window['sqlitePlugin'].deleteDatabase({ name: "ionicdb.db", location: 1 }, this.onSuccess(), this.onERror());

    //     });
    //     break;
    // }
  }
  onERror() {

  }
  onSuccess() {
    localStorage.clear()
    this.storage.remove('isLogin')

    this.serviceManager.removeFromStorageForTheKey('isLogin')
    this.splashScreen.show()
    window.location.reload();

    localStorage.setItem('zahidzahidzahid', this.selectedEnviroment.toString())

    this.navCtrl.setRoot(MainHomePage).then(() => {
      // this.serviceManager.setInLocalStorage('zahidzahidzahid', this.selectedEnviroment)
      // localStorage.setItem('zahidzahidzahid', this.selectedEnviroment.toString())
    })
  }
}
