import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LookingForSearchPage } from './looking-for-search';

@NgModule({
  declarations: [
    LookingForSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(LookingForSearchPage),
  ],
})
export class LookingForSearchPageModule {}
