import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RateAppointmentPage } from './rate-appointment';

@NgModule({
  declarations: [
    RateAppointmentPage,
  ],
  imports: [
    IonicPageModule.forChild(RateAppointmentPage),
  ],
})
export class RateAppointmentPageModule {}
