import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePageDummyPage } from './home-page-dummy';

@NgModule({
  declarations: [
    HomePageDummyPage,
  ],
  imports: [
    IonicPageModule.forChild(HomePageDummyPage),
  ],
})
export class HomePageDummyPageModule {}
