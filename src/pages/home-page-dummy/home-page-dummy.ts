import { CustomerModal } from './../../providers/CustomerModal/CustomerModal';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';

import { Allfavorite } from './../../providers/SalonAppUser-Interface';
import { Component } from '@angular/core';
import { NavController, MenuController, ViewController, NavParams, LoadingController, Platform, Content, App } from 'ionic-angular';
import { CosntantsProvider } from "../../providers/cosntants/cosntants";
import { SingUpPage } from '../sing-up/sing-up'
import { FavoriteModal } from '../../providers/FavoriteModal/FavoriteModal';
import { MainHomePage } from '../main-home/main-home';
import { Storage } from '@ionic/storage';
import { Navbar } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { Events } from 'ionic-angular';
import { DatePipe } from '@angular/common'
import { GlobalProvider } from '../../providers/global/global'
import { Http } from '@angular/http';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { Customer, AUTH_CODE_VALIDATION } from '../../providers/SalonAppUser-Interface';
import { Geolocation } from '@ionic-native/geolocation';

import { ActivateSalonPage } from '../activate-salon/activate-salon';
import { Keyboard } from '@ionic-native/keyboard';
import { config } from '../../providers/config/config';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { timer } from 'rxjs/observable/timer';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { CongratsreditsPage } from '../congratsredits/congratsredits';
import { Body } from '@angular/http/src/body';

declare var SMS: any;

@Component({
  selector: 'page-home-page-dummy',
  templateUrl: 'home-page-dummy.html',
})
export class HomePageDummyPage {
  @ViewChild(Navbar) navBar: Navbar;
  @ViewChild(Content) content: Content;

  // @ViewChild('inputFirstDigit') inputFirstDigit: any;
  // @ViewChild('inputSecondDigit') inputSecondDigit: any;;
  // @ViewChild('inputThirdDigit') inputThirdDigit: any;;
  // @ViewChild('inputForthDigit') inputForthDigit: any;;

  public otpFirstDigit = ''
  public otpSecondDigit = ''
  public otpThirdDigit = ''
  public otpForthDigit = ''
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  public longitude: any;
  public latitude: any;
  blockAttempts = 0;
  isBlock = false;
  blockTime = '';
  blockTimeRemeaning = 0;
  blockAttempts_code = 0;
  isBlock_code = false;
  blockTime_code = '';
  blockTimeRemeaning_code = 0;
  response: string;
  gcmTocken: any;
  deviceType: string;
  status: string;
  responseJson: any[];
  customerIfExist: any[];
  code: string;
  authCode: string;
  phone_number: '';
  index1: string;
  index2: string;
  index3: string;
  index4: string;
  cusror1: boolean;
  cusror2: boolean;
  cusror3: boolean;
  cusror4: boolean;
  api_key: string = '';
  public items = ['1', '2', '3', '4', '5', '6', '7', '8', '9', 'reset', '0', 'clear']
  public isUserBLocked = false;
  public unregisterBackButtonAction: any;
  public objAuthCodeValidtion: AUTH_CODE_VALIDATION
  public isAlreadySentYourCodeForValidation = false
  setBackButtonAction() {
    this.navBar.backButtonClick = () => {
      this.navCtrl.pop()
    }
  }
  constructor(

    public appCtrl: App,
    private geolocation: Geolocation,
    public global: GlobalProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public favModel: FavoriteModal,
    private loadingController: LoadingController,
    private viewCtrl: ViewController,
    public storage: Storage,
    private constProvider: CosntantsProvider,
    public events: Events,
    public platForm: Platform,
    private menu: MenuController,
    public serviceManager: GlobalProvider,
    private nativePageTransitions: NativePageTransitions,
    public http: Http,
    public keyboard: Keyboard,
    public datePipe: DatePipe,
    public customerModal: CustomerModal,
    public androidPermissions: AndroidPermissions,
    public platform: Platform,
  ) {
    this.storage.get('api_key').then((value) => {
      if (value != undefined && value != null) {
        this.api_key = value;
      }
    })
    // this.constProvider.makeToastOnFailure('Activation code sent successfully!', 1);
    this.phone_number = navParams.get('PhoneNumber');
    console.log("UserEnterPhone", this.phone_number);
    this.index1 = '';
    this.index2 = '';
    this.index3 = '';
    this.index4 = '';
    this.authCode = '';
    this.cusror1 = true
    this.cusror2 = false;
    this.cusror3 = false;
    this.cusror4 = false;
    if (platForm.is('ios')) {
      this.deviceType = "1";
    }
    if (platForm.is('android')) {
      this.deviceType = "2";
    }
    this.createTables()
  }
 

  createTables() {
    this.favModel.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.favModel.createFavoriteTable();
      }
    })
  }

  getCurrentLocation() {

    try {
      this.geolocation.getCurrentPosition({ maximumAge: 3000, timeout: 6000, enableHighAccuracy: true }).then(location => {
        this.latitude = location.coords.latitude
        this.longitude = location.coords.longitude
        // alert(this.latitude + ' : ' + this.longitude)

      }).catch(error => {

        // this.showError(error)
        // this.latitude = ''
        // this.longitude = ''
        // this.showError(error)
      })
    } catch (error) {
    }
  }
  // showError(error) {
  //   switch (error.code) {
  //     case error.PERMISSION_DENIED:
  //       this.serviceManager.makeToastOnFailure('You have denied location permission, please enable location permission', 'top')
  //       break;
  //     case error.POSITION_UNAVAILABLE:

  //       this.serviceManager.makeToastOnFailure('Location information is unavailable. please try again', 'top')
  //       break;
  //     case error.TIMEOUT:
  //       this.serviceManager.makeToastOnFailure('Cannot get your current location, please! enable your location.', 'top')
  //       break;
  //     case error.UNKNOWN_ERROR:
  //       this.serviceManager.makeToastOnFailure('Cannot get your current location, please! enable your location.', 'top')
  //       break;
  //     default:
  //       this.serviceManager.makeToastOnFailure('Cannot get your current location, please! enable your location.', 'top')
  //   }
  // }



  parseDate(dateString) {
    var time = Date.parse(dateString);
    if (!time) {
      time = Date.parse(dateString.replace("T", " "));
      if (!time) {
        let bound = dateString.indexOf('T');
        var dateData = dateString.slice(0, bound).split('-');
        var timeData = dateString.slice(bound + 1, -1).split(':');

        time = Date.UTC(dateData[0], dateData[1] - 1, dateData[2], timeData[0], timeData[1], timeData[2]);
      }
    }
    return time;
  }


  ionViewWillEnter() {
    // this.UpdateComponent()
    // let myDate = '2018/12/24 12:53';
    // this.objAuthCodeValidtion = this.serviceManager.getFromLocalStorage('AUTH_CODE_VALIDATION')
    // alert(this.objAuthCodeValidtion.currentServerTime)
    // let guess = this.objAuthCodeValidtion.currentServerTime.toLowerCase().replace(/-|'/g,'/');
    // alert(Date.parse(guess))

    // this.keyboard.show();

    this.platForm.ready().then(() => {

      this.getCurrentLocation();

      if (SMS) SMS.startWatch(() => {
        // alert('watching started');
      }, Error => {
        // alert('failed to start watching');
      });

    })
    this.getBlockedAttempts();


    this.getAttempts_code();
    let objAuthCodeValidtion: AUTH_CODE_VALIDATION = this.serviceManager.getFromLocalStorage('AUTH_CODE_VALIDATION')
    if (objAuthCodeValidtion) {
      this.objAuthCodeValidtion = objAuthCodeValidtion
    }
  }
  millisToMinutesAndSeconds(millis) {
    this.blockTimeRemeaning = Math.floor(millis / 60000);
    //console.log('Diff in minutes',minutes);
  }
  getBlockTime() {
    // alert('248')
    let date = new Date();
    let currentDateTimeInString = this.datePipe.transform(date, 'yyyy-MM-dd HH:mm:ss');
    return this.blockTimeRemeaning = Math.floor((new Date(currentDateTimeInString).getTime() - new Date(this.blockTime).getTime()) / 60000);

    // let dt1 = new Date(currentDateTimeInString);
    // let dt2 = new Date(this.blockTime);
    // let diffinmil = new Date(currentDateTimeInString).getTime() - new Date(this.blockTime).getTime();
    // console.log("diffinmil", diffinmil);

    // this.millisToMinutesAndSeconds(diffinmil);
    // let diff = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
    // console.log("difference", diff);
  }

  getBlockTime_code() {
    let date = new Date();
    let currentDateTimeInString = this.datePipe.transform(date, 'yyyy-MM-dd HH:mm:ss');

    return this.blockTimeRemeaning = Math.floor((new Date(currentDateTimeInString).getTime() - new Date(this.blockTime_code).getTime()) / 60000);

    // let dt1 = new Date(currentDateTimeInString);
    // let dt2 = new Date(this.blockTime_code);

    // let diffinmil = new Date(currentDateTimeInString).getTime() - new Date(this.blockTime_code).getTime();
    // console.log("diffinmil", diffinmil);
    // this.millisToMinutesAndSeconds(diffinmil);
    // let diff = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
    // console.log("difference", diff);
  }

  getBlockedAttempts() {
    this.storage.get('blockAttempts').then((value) => {
      this.blockAttempts = value;
    });

    this.storage.get('blockTime').then((value) => {
      this.blockTime = value;
    });



    this.storage.get('isBlock').then((value) => {
      this.isBlock = value;
    });
  }
  getAttempts_code() {
    this.storage.get('blockAttempts_code').then((value) => {
      this.blockAttempts_code = value;
    });

    this.storage.get('blockTime_code').then((value) => {
      this.blockTime_code = value;
    });



    this.storage.get('isBlock_code').then((value) => {
      this.isBlock_code = value;
    });
  }


  ionViewDidLeave() {

    // Don't forget to return the swipe to normal, otherwise 
    // the rest of the pages won't be able to swipe to open menu
    this.menu.swipeEnable(true);
    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(true, 'menu1');
  }
  ionViewDidLoad() {

    // setTimeout(() => {
    //   this.keyboard.show()
    //   this.inputFirstDigit.setFocus();
    //   if (this.platForm.is('android')) {
    //     this.content.scrollTo(0, 0).then(res => {
    //       this.content.resize()
    //       this.content.scrollTo(0, 120, 900).then(res => {

    //       })
    //     })
    //   }
    // }, 1000);

  }
  ionViewDidEnter() {

    this.platform.ready().then((readySource) => {

      if (SMS) SMS.startWatch(() => {
        // alert('watching started');
      }, Error => {
        // alert('failed to start watching');
      });

      document.addEventListener('onSMSArrive', (e: any) => {
        var sms = e.data;

        console.log(JSON.stringify(sms));
        // alert(JSON.stringify(sms));
        let smsBody: string = sms.body
        let splismsbod = smsBody.split('.')
        // alert('your code is: ' + splismsbod[0].slice(splismsbod[0].length - 4, splismsbod[0].length))
        let finalCode = splismsbod[0].slice(splismsbod[0].length - 4, splismsbod[0].length)
        if (/^\d*$/.test(finalCode)) {
          this.authCode = finalCode

          this.otpFirstDigit = this.authCode.charAt(0)
          this.otpSecondDigit = this.authCode.charAt(1)
          this.otpThirdDigit = this.authCode.charAt(2)
          this.otpForthDigit = this.authCode.charAt(3)


          // this.index1 = this.authCode.charAt(0)
          // this.cusror2 = true
          // this.index2 = this.authCode.charAt(1)
          // this.cusror3 = true
          // this.index3 = this.authCode.charAt(2)
          // this.cusror4 = true
          // this.index4 = this.authCode.charAt(3)
          // this.resetCusrsor()

          this.validateAuthenticationCodeFromServer()

          // setTimeout(arguments.callee, 60000);

          // timer(400).subscribe()
          // for (var i = 0, len = this.authCode.length; i < len; i += 1) {
          //   timer(400).subscribe(() => {
          //     this.number1(null, this.authCode.charAt(i))
          //   })
          // }
        } else {
          console.log('is in else');

        }
      });

    });

    this.menu.swipeEnable(false);
    this.initializeBackButtonCustomHandler();
  }

  ionViewWillLeave() {
    // Unregister the custom back button action for this page
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platForm.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    // this.navCtrl.pop();
    if (this.menu.isOpen()) {
      this.menu.close();
    } else {
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop();
      } else {
        this.navCtrl.setRoot(MainHomePage)
      }
    }

  }



  number1(event, number) {
    console.log((number));
    if (event) event.stopPropagation();
    this.resetCusrsor();
    if (number === 'clear') {
      this.clearLastIndex()

    } else if (number === 'reset') {
      this.clearInput()
    } else {
      if (this.authCode.length <= 3) this.authCode = this.authCode + number;
      switch (this.authCode.length) {
        case 0:

          this.cusror1 = true;
          break;
        case 1:

          this.index1 = this.authCode.substring(0, 1);
          this.cusror2 = true;
          break;
        case 2:

          this.index2 = this.authCode.substring(1, 2);
          this.cusror3 = true;
          break;
        case 3:

          this.cusror4 = true;
          this.index3 = this.authCode.substring(2, 3);
          break;
        case 4:
          this.index4 = this.authCode.substring(3, 4);
          // if (this.isCameToGetLocation) { alert('is trying to get location please wait');return}
          this.objAuthCodeValidtion = this.serviceManager.getFromLocalStorage('AUTH_CODE_VALIDATION')
          if (this.objAuthCodeValidtion) {


            let timeDifference = (Date.parse(this.objAuthCodeValidtion.currentServerTime.toLowerCase().replace(/-|'/g, '/')) - Date.parse(this.objAuthCodeValidtion.serverTimeWhenUserWasBlocked.toLowerCase().replace(/-|'/g, '/'))) / 60000
            // alert(timeDifference + ' > ' + config.blockTimeOnInCorrectCode + ' please unblock this user for activation')
            if (this.objAuthCodeValidtion.isUserBlocked && (timeDifference >= config.blockTimeOnInCorrectCode)) {
              // alert(timeDifference + ' > ' + config.blockTimeOnInCorrectCode + ' please unblock this user for activation')
              this.objAuthCodeValidtion.isUserBlocked = false
              this.objAuthCodeValidtion.serverTimeWhenUserWasBlocked = ''
              this.serviceManager.setInLocalStorage('AUTH_CODE_VALIDATION', this.objAuthCodeValidtion)
            }
            if (this.objAuthCodeValidtion && this.objAuthCodeValidtion.isUserBlocked) {
              this.serviceManager.makeToastOnFailure(AppMessages.msgWrongCodeEntrance);
              this.clearInput()
              return
            }
          }

          this.validateAuthenticationCodeFromServer()


          /** if use us blocked then 
           *     check if blocked atttempts are  5 : if true then block user for 30 minutes
           *      if blocked attempts are >= 5 then check remaining blocked time is more than
           */

          break;

        default:
          break;
      }
      // this.authCode = this.authCode + number;

      // this.resetCusrsor();
      // if (this.authCode.length < 4) {
      //   this.authCode = this.authCode + number;
      // }

      // if (this.authCode.length == 0) {
      //   console.log("length 0");
      //   this.cusror1 = true;
      // }
      // else if (this.authCode.length == 1) {
      //   console.log("length 1");
      //   this.index1 = this.authCode.substring(0, 1);
      //   this.cusror2 = true;
      // } else if (this.authCode.length == 2) {
      //   console.log("length 2");
      //   this.index2 = this.authCode.substring(1, 2);
      //   this.cusror3 = true;
      // } else if (this.authCode.length == 3) {
      //   console.log("length 3");
      //   this.cusror4 = true;
      //   this.index3 = this.authCode.substring(2, 3);
      // } else
      //   if (this.authCode.length == 4) {
      //   console.log("length 4");
      //   alert('here')
      //   this.index4 = this.authCode.substring(3, 4);


      //   //check all attempts 

      //   if (!this.isBlock_code) { // we check thsi on ionWillEnter
      //     this.resgisterUser();
      //   } else {
      //     this.getBlockTime_code();
      //     if (this.blockTimeRemeaning_code >= 5) {
      //       this.resetBlock_code();
      //       this.resgisterUser();
      //     } else {
      //       this.serviceManager.makeToastOnFailure("You have entered wrong authentication code 4 times.  Please wait 5 minutes to enter correct code again.", 0);
      //       console.log('IF', 'in if' + this.blockTime_code + 'hello');
      //     }

      //   }



      //   // this.navCtrl.push(SingUpPage);
      // } else if (this.authCode.length > 4) {
      //   // this.index2=this.authCode.substring(1,2);
      //   //its exceed the limit
      //   console.log("length more than 4", this.authCode.length);
      // }
    }
  }

  clearLastIndex() {
    this.resetCusrsor();
    if (this.authCode.length > 0) {
      this.authCode = this.authCode.slice(0, -1);
      this.resetValue();
    } else {
      this.cusror1 = true;
    }
  }
  resetValue() {
    this.index1 = '';
    this.index2 = '';
    this.index3 = '';
    this.index4 = '';
    console.log("length 4" + this.authCode, "length:" + this.authCode.length);
    if (this.authCode.length == 0) {
      this.cusror1 = true;
    }
    else if (this.authCode.length == 1) {
      console.log("length 1");
      this.cusror2 = true;
      this.index1 = this.authCode.substring(0, 1);
    } else if (this.authCode.length == 2) {
      this.cusror3 = true;
      console.log("length 2");
      this.index1 = this.authCode.substring(0, 1);
      this.index2 = this.authCode.substring(1, 2);
    } else if (this.authCode.length == 3) {
      this.cusror4 = true;
      console.log("length 3");
      this.index1 = this.authCode.substring(0, 1);
      this.index2 = this.authCode.substring(1, 2);
      this.index3 = this.authCode.substring(2, 3);
    } else if (this.authCode.length == 4) {
      console.log("length 4");
      this.index1 = this.authCode.substring(0, 1);
      this.index2 = this.authCode.substring(1, 2);
      this.index3 = this.authCode.substring(2, 3);
      this.index4 = this.authCode.substring(3, 4);
    }
  }
  clearInput() {
    this.authCode = '';
    this.index1 = '';
    this.index2 = '';
    this.index3 = '';
    this.index4 = '';
    this.resetCusrsor();
    this.cusror1 = true;
    this.authCode = '';
  }
  resetCusrsor() {
    this.cusror1 = false;
    this.cusror2 = false;
    this.cusror3 = false;
    this.cusror4 = false;
  }
  getLength(index) {
    console.log("indexSS", index);
    if (index.length == 0) {
      return true;
    } else {
      return false;
    }
  }
  validateAuthenticationCodeFromServer() {
    if (!this.latitude || !this.longitude) {
      this.latitude = config.defaultLatitude
      this.longitude = config.defaultLongitude

      // alert('is in getting location')
      // this.getCurrentLocation()
      // return

    }
    this.content.scrollTo(0, 0).then(res => {
      this.content.resize()
      this.content.scrollTo(0, 0, 900).then(res => {

      })
    })
    // this.content.scrollToTop()
    if (this.isAlreadySentYourCodeForValidation) { return }
    let notificationController = this.loadingController.create({
      content: "Authenticating..."
    });
    notificationController.present();
    let token = this.serviceManager.getFromLocalStorage(this.serviceManager.GCM_TOKEN);

    var params = {
      service: btoa('validate_auth_code'),
      phone_number: btoa(this.phone_number),
      api_key: btoa(this.api_key),
      cust_device_id: btoa(token),
      device_datetime: btoa(this.serviceManager.getCurrentDateTime()),
      auth_code: btoa(this.authCode),
      cust_device_type: btoa(this.deviceType),// for now its 2 but its will cahnge 
      // for now its 2 but its will cahnge 
      cust_lat: btoa(this.latitude),
      cust_lng: btoa(this.longitude)
    }
    console.log('param', JSON.stringify(params))
    this.isAlreadySentYourCodeForValidation = true;
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe((response) => {
        console.log('response', JSON.stringify(response))
        //  notificationController.dismiss();
        notificationController.dismiss();
        this.isAlreadySentYourCodeForValidation = false;
        this.response = response.status;
        if (Number(response.status) === 1) {

          if (response.favorites) {
            this.saveIntoFavourites(response.favorites)
          }

          let customer = response.customer
          if (customer.cust_lat.trim().length === 0 || customer.cust_lng.trim().length === 0 && this.latitude) {
            customer.cust_lat = this.latitude
            customer.cust_lng = this.longitude
          }
          console.log('ResponseAll', JSON.stringify(response))
          console.log('ResponsePromo', JSON.stringify(response.promo_code))
          let promo_code = response.promo_code;
          if (promo_code && promo_code.pc_id != undefined) {
            console.log('if')
            this.saveCustomerIntoDB(customer, response.promo_code)
          } else {
            console.log('else')
            promo_code = null
            this.saveCustomerIntoDB(customer, null)
          }

        } else {
          // setTimeout(() => {
          //   this.keyboard.show()
          //   this.inputFirstDigit.setFocus()
          //   this.otpForthDigit = ''
          //   this.otpThirdDigit = ''
          //   this.otpSecondDigit = ''
          //   this.otpFirstDigit = ''
          //   this.inputFirstDigit.setFocus()
          //   // this.inputForthDigit.setFocus();
          // }, 1000);
          this.clearInput();
          this.objAuthCodeValidtion = this.serviceManager.getFromLocalStorage('AUTH_CODE_VALIDATION')
          if (this.objAuthCodeValidtion) {
            this.objAuthCodeValidtion.wrongAuthCodeEnteredCount += 1
            if (this.objAuthCodeValidtion.wrongAuthCodeEnteredCount >= 5) {
              this.objAuthCodeValidtion.isUserBlocked = true;
              this.objAuthCodeValidtion.serverTimeWhenUserWasBlocked = response.response_datetime;
              this.objAuthCodeValidtion.currentServerTime = response.response_datetime;

            }
            this.serviceManager.setInLocalStorage('AUTH_CODE_VALIDATION', this.objAuthCodeValidtion)
          } else {
            // for the first time user has entered wrong activation code.
            this.objAuthCodeValidtion = {

            }
            this.objAuthCodeValidtion.serverTimeWhenUserWasBlocked = response.response_datetime;
            this.objAuthCodeValidtion.currentServerTime = response.response_datetime;
            this.objAuthCodeValidtion.wrongAuthCodeEnteredCount = 1
            this.serviceManager.setInLocalStorage('AUTH_CODE_VALIDATION', this.objAuthCodeValidtion)
          }
          this.serviceManager.makeToastOnFailure(response.error)
        }


      },
        error => {
          notificationController.dismiss();

          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        }

      );
    // 
  }

  saveIntoFavourites(AllFav) {
    let favData: Allfavorite[] = []
    favData = AllFav
    console.log('allFavourite', JSON.stringify(favData))
    console.log('TotalItems', favData.length)
    this.favModel.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.favModel.InsertInToFavMulTable(favData).then(isSalonMarkedFavorite => {
          if (isSalonMarkedFavorite) {
            // this.salonModal.getSalonDetailsFrom().then()
          }
        })
      }
    })

  }

  checkAuthStatus(response) {
    console.log('ResponseCustomer', JSON.stringify(response))

    if (this.response == '1') {
      console.log('testCustomerObject ');
      console.log(JSON.stringify(response.customer));

      //alert('Here'+JSON.stringify(response.customer))
      "customer" in this.responseJson ? this.customerAlreadyExist(response.customer, response.message) : this.customerNotExists();
    } else {
      this.clearInput();
      this.serviceManager.makeToastOnFailure(response.error);
    }
  }
  customerAlreadyExist(customer: Customer, message) {
    /* new code */


    if (customer.cust_lat.trim().length === 0 || customer.cust_lng.trim().length === 0 && this.latitude) {
      customer.cust_lat = this.latitude
      customer.cust_lng = this.longitude
    }
    this.saveCustomerIntoDB(customer, null)





    /**
     * old code
     */
    // this.serviceManager.setInLocalStorage(this.serviceManager.CUSTOMER_DATA, customer)

    // this.storage.set(this.serviceManager.CUSTOMER_DATA, customer);



    // console.log("Customer already there, be carefull ...", customer);
    //   this.constProvider.makeToastOnFailure("Authnitication success!");
    // this.checkCustomerData()

  }



  customerNotExists() {
    console.log('settingNav', this.phone_number);


    //dummy code 
    // this.appCtrl.getRootNav().setRoot(MainHomePage);
    //below code was actual
    this.navCtrl.push(MainHomePage,
      {
        PhoneNumber: this.phone_number
      }).then(() => {
        // first we find the index of the current view controller:
        const index = this.viewCtrl.index;
        // then we remove it from the navigation stack
        this.navCtrl.remove(1);
        this.navCtrl.remove(0);
      });
    console.log("Customer is not  there, you have to  create new one")
  }
  resetBlock() {
    this.storage.set('blockTime', '');
    this.storage.set('isBlock', false);
    this.storage.set('blockAttempts', '');
  }

  resetBlock_code() {
    this.storage.set('blockTime_code', '');
    this.storage.set('isBlock_code', false);
    this.storage.set('blockAttempts_code', '');
  }

  resendActivationCode() {
    this.getBlockTime();
    if (!this.isBlock) {
      this.resendCode();
    } else {

      if (this.blockTimeRemeaning >= 60) {
        this.resetBlock();
        this.resendCode();
      } else {
        this.serviceManager.makeToastOnFailure(AppMessages.msgFourTimesAuthentication);
        console.log('IF', 'in if' + this.blockTime + 'hello');
      }

    }

  }


  setAttempts() {
    this.storage.get('blockAttempts').then((value) => {
      if (value != undefined && value != null) {
        value = (value + 1);
      } else {
        value = 1;
      }

      if (value >= 4) {
        let blockTime1 = this.serviceManager.getCurrentDateTime();
        this.storage.set('blockTime', blockTime1);
        this.storage.set('isBlock', true);
      } else {
        this.storage.set('isBlock', false);
      }

      this.storage.set('blockAttempts', value);
    });
  }

  setAttempts_code() {
    this.storage.get('blockAttempts_code').then((value) => {
      if (value != undefined && value != null) {
        value = (value + 1);
      } else {
        value = 1;
      }

      if (value >= 3) {
        let blockTime1 = this.serviceManager.getCurrentDateTime();
        this.storage.set('blockTime_code', blockTime1);
        this.storage.set('isBlock_code', true);
      } else {
        this.storage.set('isBlock_code', false);
      }

      this.storage.set('blockAttempts_code', value);
    });
  }
  resendCode() {
    let notificationController = this.loadingController.create({
      content: "Resending the activation code."
    });
    notificationController.present();
    var params = {
      service: btoa('send_sms'),
      phone_number: btoa(this.phone_number),
    }
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe((response) => {
        this.status = response.status;
        notificationController.dismiss();
        console.log('AllResponse', response);
        this.serviceManager.makeToastOnFailure('Activation code is sent to your number');
        if (!this.isBlock) {
          this.setAttempts();
        }
        this.getBlockedAttempts();
      },
        error => {
          notificationController.dismiss();
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        }
      );
  }

  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };
    if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      // this.navCtrl.setRoot(ActivateSalonPage)
    }
  }

  getCoordinatesHome(event) {
    event.stopPropagation();
    // This output's the X coord of the click
    console.log(event.clientX);
    // This output's the Y coord of the click
    console.log(event.clientY);
    // alert('getCoordinatesHome')
  }

  saveCustomerIntoDB(customer: Customer, promo_code) {
    if (customer) {
      this.customerModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.customerModal.createCustomerTable().then(isTableCreated => {
            if (isTableCreated === true) {
              this.customerModal.InsertInToCustomerTable(customer).then(isCustomerInserted => {
                if (isCustomerInserted === true) {
                  console.log('success: InsertInToCustomerTable ');
                  this.serviceManager.setInLocalStorage(this.serviceManager.IS_USER_LOGIN, true)
                  this.storage.set('isLogin', true);
                  this.events.publish('customer:changed', customer);
                  // this.appCtrl.getRootNav().setRoot(MainHomePage);
                  if (promo_code && promo_code.pc_id != undefined) {

                    this.nativePageTransitions.slide(this.options)
                    this.navCtrl.push(CongratsreditsPage, { myCustomer: customer, promo_code: promo_code })
                      .then(() => {
                        this.navCtrl.remove(1);
                        this.navCtrl.remove(0);
                      });
                  } else {
                    // alert('pushing-here')
                    this.navCtrl.push(MainHomePage);
                    // this.navCtrl.push(MainHomePage, { myCustomer: customer })
                    //   .then(() => {
                    //     this.navCtrl.remove(1);
                    //     this.navCtrl.remove(0);
                    //   });
                  }
                  // this.nativePageTransitions.slide(this.options)
                  // this.navCtrl.push(MainHomePage, { myCustomer: customer })
                  //   .then(() => {
                  //     this.navCtrl.remove(1);
                  //     this.navCtrl.remove(0);
                  //   });

                } else {
                  console.log('error: InsertInToCustomerTable ', isCustomerInserted);

                }


              }, error => {
                console.log('error: ', error.message);

              })
            }

          })
        }
      })
    }

  }
  onClick(event) {
    event.stopPropagation();
  }
  shouldChangeCharacter(value) {
    console.log(value);

  }
  firstDigitTapped() {
    // alert('1st-tap')
    // if (/^\d*$/.test(this.otpFirstDigit) === false) {
    //   this.otpFirstDigit = ''
    //   return
    // }
    // if (this.otpFirstDigit.trim().length === 0) return
    // this.authCode += this.otpFirstDigit
    // this.setFocusOnSecondInput()
  }


  secondDigitTapped() {
    // alert('2nd-tap')
    // if (/^\d*$/.test(this.otpSecondDigit) === false) {
    //   this.otpSecondDigit = ''
    //   return
    // }
    // if (event && event.keyCode === 8) {
    //   this.authCode = this.otpThirdDigit.slice(0, 0)
    //   // alert(this.authCode)
    //   this.setFocusOnFirstInput()
    //   return
    // }
    // if (this.otpSecondDigit.trim().length > 0) {
    //   this.authCode += this.otpSecondDigit
    //   this.setFocusOnThirdInput()
    // }
  }

  thirdDigitTapped() {
  
  //   // if (/^\d*$/.test(this.otpThirdDigit) === false) {
  //   //   this.otpThirdDigit = ''
  //   //   return
  //   // }
  //   // if (event && event.keyCode === 8) {
  //   //   this.setFocusOnSecondInput()
  //   //   this.authCode = this.otpThirdDigit.slice(0, 1)
  //   //   return
  //   // }
  //   // if (this.otpSecondDigit.trim().length > 0) { 

  //   // }
  //   // if (this.otpThirdDigit.trim().length > 0) { 

  //   //   this.authCode += this.otpThirdDigit
  //   //   this.setFocusOnForthnput()
  //   // }
  // }


  // forthDigitTapped() {
  //   // alert('forth-tapped')
  //   this.authCode = '1234';
  //   // this.validateAuthenticationCodeFromServer()

  //   // if (/^\d*$/.test(this.otpForthDigit) === false) {
  //   //   this.otpForthDigit = ''
  //   //   return
  //   // }
  //   // if (event && event.keyCode === 8) {
  //   //   this.setFocusOnThirdInput()
  //   //   return
  //   // }

  //   // if (this.otpForthDigit.trim().length > 0) { 

  //   //   this.authCode += this.otpForthDigit
  //   // }

  //   // if (this.authCode.trim().length === 4) {
  //   //   this.validateAuthenticationCodeFromServer()
  //   // }

    // call here api to verify OTP

  }

  // setFocusOnFirstInput() {
  //   setTimeout(() => {
  //     this.keyboard.show()
  //     this.inputFirstDigit.setFocus();
  //   }, 100);
  // }
  // setFocusOnSecondInput() {
  //   setTimeout(() => {
  //     this.keyboard.show()
  //     this.inputSecondDigit.setFocus();
  //   }, 100);
  // }
  // setFocusOnThirdInput() {
  //   setTimeout(() => {
  //     this.keyboard.show()
  //     this.inputThirdDigit.setFocus();
  //   }, 100);
  // }
  // setFocusOnForthnput() {
  //   setTimeout(() => {
  //     this.keyboard.show()
  //     this.inputForthDigit.setFocus();
  //   }, 100);
  // }

  contentClick() {
    // alert('content-click')
  }


  homePageTaped() {
    this.authCode = '1234';
    this.validateAuthenticationCodeFromServer()
    // alert('here') 
    

  }
  endEndEditing(any) {
    // alert('going to end editor')
    this.keyboard.close()
    this.content.scrollTo(0, 0).then(res => {
      this.content.resize()
      this.content.scrollTo(0, 0, 900).then(res => {

      })
    })
  }
}