import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Customer, ORDER_ITEMS, SHIPPING_ADDRESS_DATA, MY_ORDERS, PRODUCT } from '../../providers/SalonAppUser-Interface';
import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { MainHomePage } from '../main-home/main-home';
import { ShippingAddressModal } from '../../providers/ShippingAddressModal/ShippingAddressModal';
import { OrderDetailPage } from '../order-detail/order-detail';
import { ViewCartPage } from '../view-cart/view-cart';
import { GlobalServiceProvider } from '../../providers/global-service/global-service';
import { CosntantsProvider } from '../../providers/cosntants/cosntants';
import { config } from '../../providers/config/config';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { Platform } from 'ionic-angular/platform/platform';
import { MenuController } from 'ionic-angular';
import { LoginSignUpPage } from '../login-sign-up/login-sign-up';

@IonicPage()
@Component({
  selector: 'page-my-orders',
  templateUrl: 'my-orders.html',
})
export class MyOrdersPage {
  public unregisterBackButtonAction: any;
  orderStatus = {
    Pending: 'Pending',
    Placed: 'Placed',
    Processing: 'Processing',
    Cancelled: 'Cancelled',
    Dispatched: 'Dispatched',
    Delivered: 'Delivered',
    Failed: 'Failed'
  }
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  public publicUser=false
  public myCustomer: Customer
  public myOrders: MY_ORDERS[] = []
  public myOrdersTemp = []
  public loading: any
  public ShppingAddress: SHIPPING_ADDRESS_DATA[] = []
  public isFirstCall = true
  public noOrderAvailable = false
  public currencySymbol = ''
  public deliveryCharges = config.deliveryCharges
  constructor(
    private loadingController: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public serviceManager: GlobalProvider,
    public customerModal: CustomerModal,
    public shippingAddressModal: ShippingAddressModal,
    public nativePageTransitions: NativePageTransitions,
    public alertCtrl: AlertController,
    public gsp: GlobalServiceProvider,
    public constants: CosntantsProvider,
    public platform: Platform,
    private menu: MenuController,
  ) {
  }

  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }

  //custom back button for android
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    // this.navCtrl.pop();
    if (this.menu.isOpen()) {
      this.menu.close();
    } else {
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop();
      } else {
        this.navCtrl.setRoot(MainHomePage)
      }
    }
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyOrdersPage');
  }
  
  goToRegisterScreen(){
    this.navCtrl.push(LoginSignUpPage, { animate: false })
  }

  ionViewWillEnter() {
    let isLoginSignUp = this.serviceManager.getFromLocalStorage(this.serviceManager.IS_USER_LOGIN);
    if(!isLoginSignUp){
      this.publicUser=true
    }else{
      this.publicUser=false
    }

    this.getCurrencyDetail()
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {

        this.customerModal.getCustomer().then(customer => {

          // if(!customer){
          //   // alert('customer not exist')
          //   this.publicUser=true
          // }else{
          //   this.publicUser=false
          // }

          this.myCustomer = customer;
          this.getMyOrders()
        })
      }
    })

  }
  getMyOrders() {
    if (!this.myCustomer) {
      // // alert('customer ni mila')
      return 
    }
    const params = {
      service: btoa('get_orders'),
      cust_id: btoa(this.myCustomer.cust_id),
      last_fetched: ''
    }
    this.serviceManager.showProgress()
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {

          this.serviceManager.stopProgress()
          this.myOrders = res.orders
          !this.myOrders || this.myOrders.length === 0 ? this.noOrderAvailable = true : this.noOrderAvailable = false
          if (!this.myOrders || this.myOrders.length === 0) return
          let abc = this.serviceManager.getFromLocalStorage(this.serviceManager.IS_MY_ORDER_REQUESTED_ALREADY_INITIATED)
          if (!abc) {

            this.myOrders.forEach(order => {

              let obj: SHIPPING_ADDRESS_DATA = {
              }
              obj.o_address = order.o_address
              obj.o_city = order.o_city
              obj.o_province = order.o_province
              obj.o_contact_person = order.o_contact_person
              obj.o_phone = order.o_phone
              obj.o_completeAddress = order.o_address + ',' + order.o_city + ',' + order.o_province
              this.ShppingAddress.push(obj)
            });

            this.shippingAddressModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.shippingAddressModal.createShippingAddressTable().then(tblCreated => {
                  this.shippingAddressModal.InsertInToShippingAddressTable(this.ShppingAddress).then(sa => {
                    this.serviceManager.setInLocalStorage(this.serviceManager.IS_MY_ORDER_REQUESTED_ALREADY_INITIATED, this.serviceManager.IS_MY_ORDER_REQUESTED_ALREADY_INITIATED)
                  })
                })
              }
            })
          }
        },
        (error) => {
          this.loading.dismissAll();
          //  this.serviceManager.stopProgress()
          console.log(error);
          
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        },
        () => {
          //   this.serviceManager.stopProgress()
        }
      )


  }
  btnReOrderTapped(order, event) {
    event.stopPropagation();
    let reOrderItems: PRODUCT[] = order.order_items
    if (!reOrderItems) { this.serviceManager.makeToastOnFailure(AppMessages.msgErrorInOrderDetails); return }
    let productIDs = ''
    reOrderItems.forEach(product => {
      productIDs += product.p_id + ','
      product.isChecked = true;
      product.p_quantity = Number(product.p_quantity)

      return product
    });

    productIDs = productIDs.slice(0, productIDs.length - 1)
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(ViewCartPage, {
      productIDs: productIDs
    })
  }
  btnCancelTapped(order: MY_ORDERS, event) {

    event.stopPropagation();
    let alert = this.alertCtrl.create({
      title: 'Canceling Order',
      message: 'Are you sure you want to cancel your order?',
      buttons: [
        {
          text: 'NO',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'YES',
          handler: () => {
            this.loading = this.loadingController.create({ content: "Please wait..." });
            this.loading.present();
            const params = {
              service: btoa('update_order_status'),
              cust_id: btoa(this.myCustomer.cust_id),
              o_id: btoa(order.o_id),
              o_status: btoa('Cancelled'),
            }
            this.serviceManager.showProgress()
            this.serviceManager.getData(params)
              .retryWhen((err) => {
                return err.scan((retryCount) => {
                  retryCount += 1;
                  if (retryCount < 3) {
                    return retryCount;
                  }
                  else {
                    throw (err);
                  }
                }, 0).delay(1000)
              })
              .subscribe(
                (res) => {
                  this.loading.dismissAll();
                  if (res.status === "1" || res.status === 1) {
                    let _index = this.myOrders.indexOf(order)
                    order.o_status = 'Cancelled'
                    this.myOrders[_index] = order
                    // this.myOrders.splice(this.myOrders.indexOf(order), 1);
                    this.serviceManager.makeToastOnSuccess(AppMessages.msgCancelOrder)
                    console.log(this.myOrders.length);
                    (this.myOrders)
                    !this.myOrders || this.myOrders.length === 0 ? this.noOrderAvailable = true : this.noOrderAvailable = false
                    return this.myOrders
                  }
                },
                (error) => {
                  this.loading.dismissAll();
                  //  this.serviceManager.stopProgress()
                  console.log(error);
                  this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

                  console.log('something went wrong', error);
                },
                () => {
                  //   this.serviceManager.stopProgress()
                }
              )
          }
        }
      ]
    });
    alert.present();




  }
  orderItemClick(event, orderItem, index) {
    event.stopPropagation();
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(OrderDetailPage, {
      order_detail: orderItem
    })
  }
  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };


    if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

    if (this.navCtrl.canGoBack()) {

      this.navCtrl.pop();
    } else {

      this.navCtrl.setRoot(MainHomePage)
    }

  }
  getCurrencyDetail() {
    this.currencySymbol = this.gsp.currencySymbol

    if (!this.currencySymbol || this.currencySymbol.length === 0) {
      let _appConfig = this.serviceManager.getFromLocalStorage('isAppConfigs')

      if (_appConfig && _appConfig.currency) {
        this.gsp.currencySymbol = _appConfig.currency
        this.gsp.distance_unit = _appConfig.distance_unit
        this.currencySymbol = _appConfig.currency
      }

    }
  }
  getFormattedDate(o_datetime) {
    return this.serviceManager.getFormattedTimeForMyorder(o_datetime)
  }
  sumOfOrder(orderAmount: any, o_delivery_charges: number) {
    if (!o_delivery_charges) o_delivery_charges = 0
    return (Number(orderAmount)+Number(o_delivery_charges))
  }
  roundAmound(amount) {
    return Math.round(amount)
  }
}
