import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController,  } from 'ionic-angular';
// import * as Bounce from 'bounce.js';
import {  ElementRef } from '@angular/core';
import { MainHomePage } from '../main-home/main-home';
import { timer } from 'rxjs/observable/timer';

import { ParticlesProvider } from '../../providers/particles/particles';

/**
 * Generated class for the CongratsreditsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-congratsredits',
  templateUrl: 'congratsredits.html',

})
export class CongratsreditsPage {
  @ViewChild('canvasObj') canvasElement : ElementRef;
  private _CANVAS 			: any;
  private _CONTEXT 		: any;
  public particleQuantity  : number 	= 1;
  private _NUM_PARTICLES 	: number 				= 	this.particleQuantity;
  private _ANIMATION;
  public isPlaying  				: boolean;

  public totalCredits:any=1000
  public credditsPercentage:any=20
  constructor(public navCtrl: NavController,
     public navParams: NavParams,
    private viewCtrl: ViewController,
    private toastCtrl: ToastController,
    public searchElementRef: ElementRef,
    private _PARTICLE 	: ParticlesProvider
    ) {
      let promo_code = this.navParams.get('promo_code');
       this.totalCredits=promo_code.pc_max_value;
       this.credditsPercentage=promo_code.pc_percent;
     
    }
  nextPage(){
    this.navCtrl.push(MainHomePage).then(() => {
        this.navCtrl.remove(0);
      });
  }
  prepareCanvas() : void
  {
     this._CANVAS 			= this.canvasElement.nativeElement;
     this._CANVAS.width  	= 500;
     this._CANVAS.height 	= 500;
     this.initialiseCanvas();
  }
  initialiseCanvas() : void
  {
     if(this._CANVAS.getContext)
     {
        this.setupCanvas();
        this.renderToCanvas();
     }
  }
  setupCanvas() : void
  {
     this._CONTEXT 			= this._CANVAS.getContext('2d');
     this._CONTEXT.fillStyle 	= "#fff";
     this._CONTEXT.fillRect(0, 0, 500, 500);
  }
  clearCanvas() : void
  {
     this._CONTEXT.clearRect(0, 0, this._CANVAS.width, this._CANVAS.height);
     this.setupCanvas();
  }
  renderToCanvas() : void
   {
      this.createParticleAnimation();
      this.isPlaying 		= 	true;
  }
   createParticleAnimation() : void
   {
      // Generate a new particle via loop iteration
      for(var i = 0;
              i < this._NUM_PARTICLES;
              i++)
      {
         this._PARTICLE.renderParticle(this._CONTEXT,
         							   this._CANVAS.width,
         							   this._CANVAS.height);
      }
      // Use the requestAnimationFrame method to generate new particles a minimum
      // of 60x a second (or whatever the browser refresh rate is) BEFORE the next
      // browser repaint
      this._ANIMATION = requestAnimationFrame(() =>
      {
         this.createParticleAnimation();
      });
   }
   refreshCanvas(val : any) : void
   {
      this._NUM_PARTICLES 		= val.value;
      this.clearCanvas();
      this.renderToCanvas();
   }
   stopAnimation() : void
   {
      // Cancel the current animation
      cancelAnimationFrame(this._ANIMATION);
      this.isPlaying   = false;
   }
   replayAnimation() : void
   {
      this.clearCanvas();
      this.renderToCanvas();
   }
  ionViewDidLoad() {
    timer(3000).subscribe(() => {
      this.stopAnimation();
    })
    this.prepareCanvas();
    console.log('ionViewDidLoad CongratsreditsPage');
  }
}