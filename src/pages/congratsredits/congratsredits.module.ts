import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CongratsreditsPage } from './congratsredits';

@NgModule({
  declarations: [
    CongratsreditsPage,
  ],
  imports: [
    IonicPageModule.forChild(CongratsreditsPage),
  ],
})
export class CongratsreditsPageModule {}
