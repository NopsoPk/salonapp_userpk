import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { GlobalProvider } from './../../providers/global/global';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform, ViewController } from 'ionic-angular';
import { MainHomePage } from '../main-home/main-home';
import { ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { CosntantsProvider } from "../../providers/cosntants/cosntants";
import { Events } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';



import { Http, HttpModule, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { Customer } from '../../providers/SalonAppUser-Interface';
import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { AppMessages } from '../../providers/AppMessages/AppMessages';


@Component({
  selector: 'page-sing-up',
  templateUrl: 'sing-up.html',
})
export class SingUpPage {

  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };


  firstname = '';
  email = '';
  PhoneNumber: '';
  responseJson: any[];
  response: string;
  location = '';
  deviceType: string;
  errorMessageName = '';
  errorMessageEmail = '';
  errorMessageLocation = '';
  isValidName = true;
  isValidEmail = true;
  isValidLocation = true;
  isValidLatLan=false;
  isGenderMale = true;
  customerGender: string
  postCode: string;
  longitude: any;
  latitude: any;
  emailExpression = '^[\w\.]+@([\w]+\.)+[A-Z]{2,7}$';
  constructor(
    public navCtrl: NavController, public navParams: NavParams,
    
    private loadingController: LoadingController,
    public storage: Storage,
    private toastCtrl: ToastController,
    private constProvider: CosntantsProvider,
    public events: Events,
    private geolocation: Geolocation,
    public platform: Platform,
    public http: Http,
    public serviceManager: GlobalProvider,
    private viewCtrl: ViewController,
    public customerModal: CustomerModal,
    private nativePageTransitions: NativePageTransitions,
  ) {
    this.customerGender = 'male';
    this.postCode = "";
    this.PhoneNumber = navParams.get('PhoneNumber');
    //  this.getAndSetCusrrentLocation();
    console.log("PhoneSignUpGetting", this.PhoneNumber)
    this.isValidName = false;
    if (platform.is('ios')) {
      this.deviceType = "1";
    }
    if (platform.is('android')) {
      this.deviceType = "2";
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SingUpPage');
    this.useCurrentLocation()
    this.selectMenGender()
  }

  OnValueEnterName(name) {
    this.isValidNameInput();
  }
  OnValueEnterEmail(email) {
    this.isValidEmailInput();
  }
  useCurrentLocation() {

    this.geolocation.getCurrentPosition().then(location => {
      this.latitude = location.coords.latitude
      this.longitude = location.coords.longitude
      this.isValidLatLan=true;
      
    }).catch(e=>{
      this.isValidLatLan=false;
      console.log('error: while getting location');
      
    })
  }
  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'top',
      cssClass: "ToastClass",
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }
  OnValueEnterLocation(location) {
    this.isValidLocationInput();
  }
  OnCompleteActivation() {
    this.checkValidation();
    if (this.isValidEmail && this.isValidName) {

      if(this.isValidLatLan){
        this.resgisterUser();
      }else{
        this.serviceManager.makeToastOnFailure(AppMessages.msgEnableLocation)

      }
      
    } else {
      //this.serviceManager.makeToastOnFailure("Can't get your location, please check your internet connection!"+this.latitude,0);

      console.log("Activation can't completes with errors");
    }
  }
  checkValidation() {

    this.isValidNameInput();
    this.isValidEmailInput();
     //  this.isValidLocationInput();
   // this.checkLocationFormat();
    this.checkEmailExpression();
  }
  isValidNameInput() {
    if (this.firstname.length == 0) {
      this.isValidName = false;
      this.errorMessageName = 'Please provide your name'
    } else {
      this.isValidName = true;
      this.errorMessageName = ''
    }
  }
  isValidEmailInput() {



    if (this.email.length == 0) {
      this.isValidEmail = false;
      this.errorMessageEmail = 'Please provide your email'
     
    } else {
      this.isValidEmail = true;
      this.errorMessageEmail = ''
      //check email pattern
    }
  }
  checkEmailExpression() {
    if (this.email.length > 0) {
      if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.email)) {
        this.isValidEmail = true;
        this.errorMessageEmail = ''
      } else {
        this.isValidEmail = false;
        this.errorMessageEmail = 'Please provide your valid email address'
      }
    }
  }
  checkLocationFormat() {
    if ((this.location.length < 6)) {

      this.isValidLocation = false;
      this.errorMessageLocation = 'Please enter a valid postcode including a space.'

    } else if (!this.checkSpaces()) {
      this.isValidLocation = false;
      this.errorMessageLocation = 'Invalid postcode'
    }

  }
  checkSpaces() {
    let spaceFound = false;
    let spaceCOunter = 0;
    let text = this.location;
    for (let i = 0; i < text.length; i++) {
      let valueATIndex = text.charAt(i);
      let value = valueATIndex.toString();
      console.log('value', value);
      if (value == ' ') {
        console.log('spaceFound', value);
        spaceFound = true;
      } else {
        spaceFound = false;
      }
      if (spaceFound) {
        spaceCOunter = (spaceCOunter) + 1;
      }
    }
    console.log('spaceCOunter', spaceCOunter);
    if (spaceCOunter < 1 || spaceCOunter > 1) {
      return false;
    }
    return true;
  }
  isValidLocationInput() {
    if (this.location.length == 0) {
      this.isValidLocation = false;
      this.errorMessageLocation = 'Please provide your post code'
    } else {
      this.isValidLocation = true;
      this.errorMessageLocation = ''
      //check location pattren 
    }

  }
  resgisterUser() {
    let notificationController = this.loadingController.create({
      content: "Please wait..."
    });

    notificationController.present();
    let token = this.serviceManager.getFromLocalStorage(this.serviceManager.GCM_TOKEN)

    let cust_Gender = ''
    if (this.isGenderMale) {
      cust_Gender = this.serviceManager.CUSTOMER_GENDER_MALE
    } else {
      cust_Gender = this.serviceManager.CUSTOMER_GENDER_FEMALE
    }
    
    
    
    var params = {
    
      service: btoa('register'),
      cust_name: btoa(this.firstname),
      // cust_zip: btoa(this.location),
      cust_lat:btoa(this.latitude),
      cust_lan:btoa(this.longitude),
      
      cust_email: btoa(this.email),
      cust_gender: btoa(cust_Gender),
      cust_phone: btoa(this.getPhoneFromPref()),
      cust_device_id: btoa(token),// for now its 2 but its will cahnge 
      device_datetime: btoa(this.serviceManager.getCurrentDateTime()),
      cust_device_type: btoa(this.deviceType),
    }
    console.log(params);
    
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe((response) => {
        this.response = response.status;
        this.responseJson = response;
        notificationController.dismiss();
        console.log('AllResponse', this.response);
        // this.checkResgisterStatus(response);
        if (response.status === '1') {
          this.storage.set('isLogin', true);
          this.saveCustomerIntoDB(response.customer)
        }
      },
        error => {
          notificationController.dismiss();
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        }
      );
    // 
  }
  getPhoneFromPref() {
    return this.PhoneNumber;
  }
  OnValueEnter(vale) {
  }
  MaleGenderTapped() {
    this.isGenderMale = !this.isGenderMale
  }
  genderMaleTapped() {
    
    this.isGenderMale = true

    // this.selectMenGender()
  }
  selectMenGender() {
    // var elem = document.getElementById('maleGender').children;
    // elem[0].setAttribute("style", "background:green !important");

    // var elem = document.getElementById('femaleGender').children;
    // elem[0].setAttribute("style", "background:orange !important");
  }
  genderFemaleTapped() {
    this.isGenderMale = false

    // var elem = document.getElementById('maleGender').children;
    // elem[0].setAttribute("style", "background:orange !important");

    // var elem = document.getElementById('femaleGender').children;
    // elem[0].setAttribute("style", "background:green !important");

    // document.getElementById("maleGender").style.color = "red";
    // document.getElementById("femaleGender").style.color = "blue";
  }
  saveCustomerIntoDB(customer:Customer) {
    
    if (customer) {
      this.customerModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.customerModal.createCustomerTable().then(isTableCreated => {
            if (isTableCreated === true) {
              this.customerModal.InsertInToCustomerTable(customer).then(isCustomerInserted => {
                if (isCustomerInserted === true) {
                  console.log('success: InsertInToCustomerTable ');


                  this.serviceManager.setInLocalStorage(this.serviceManager.IS_USER_LOGIN, true)
                  this.storage.set('isLogin', true);
                  this.events.publish('customer:changed', customer);

                  this.nativePageTransitions.slide(this.options)
                  this.navCtrl.setRoot(MainHomePage, { myCustomer: customer })
                    

                } else {
                  console.log('error: InsertInToCustomerTable ', isCustomerInserted);

                }


              }, error => {
                console.log('error: ', error.message);

              })
            }

          })
        }
      })
    }

  }

}