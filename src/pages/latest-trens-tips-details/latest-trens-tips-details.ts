import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { GlobalProvider } from './../../providers/global/global';
import { Component } from '@angular/core';
import { App, IonicPage, LoadingController, NavController, NavParams, Platform } from 'ionic-angular';
import { LatestTrendSubCatPage } from '../latest-trend-sub-cat/latest-trend-sub-cat'
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalServiceProvider } from '../../providers/global-service/global-service'

import { AddAppointmentPage } from '../add-appointment/add-appointment'
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { AppMessages } from '../../providers/AppMessages/AppMessages';

/**
 * Generated class for the LatestTrensTipsDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-latest-trens-tips-details',
  templateUrl: 'latest-trens-tips-details.html',
})
export class LatestTrensTipsDetailsPage {

  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };

  public data = ["Lips", "Eye Lining", "Massage", "Facial", "Lips", "Eye Lining", "Massage", "Facial"];
  public images = ["./assets/imgs/lips.png", "./assets/imgs/massage.png", "./assets/imgs/lips.png", "./assets/imgs/massage.png", "./assets/imgs/lips.png", "./assets/imgs/massage.png", "./assets/imgs/lips.png", "./assets/imgs/massage.png"];
  public postsArray = Array();
  public salonName = "";
  public selectedPost;

  constructor(public http: Http,
    public globalSearch: GlobalServiceProvider,
    
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public serviceManager: GlobalProvider,
    private nativePageTransitions: NativePageTransitions,
  ) {
  }

  ionViewDidLoad() {
    // alert('testing:2')
    console.log('ionViewDidLoad LatestTrensTipsDetailsPage');
    this.selectedPost = this.navParams.get('selectedPost');
    this.getPostsByHandle();
  }

  getPostsByHandle() {
    let cat = this.navParams.get('selectedPost');
    this.salonName = cat.user_screen_name;
    console.log('sub cat')
    console.log(cat)
    let tagURL = 'http://www.laravel.nopso.qwpcorp.com/InstaApi/public/api/getPostsByHandle/' + cat.user_screen_name;

    let loading = this.loadingController.create({ content: "Please Wait..." });
    loading.present();
    console.log('__url ', tagURL);

    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');

    let options = new RequestOptions({ headers: headers });

    this.http.get(tagURL, options)
      .map(Response => Response.json())
      .retryWhen((err) => {
        return err.scan((retryCount) => {
            retryCount += 1;
            if (retryCount < 3) {
                return retryCount;
            }
            else {
                throw (err);
            }
        }, 0).delay(1000)
    })
  
      .subscribe(res => {
        loading.dismiss()
        if (res.data.postsByHandle != undefined) {
          if (res.data.postsByHandle.length > 0) {
            this.postsArray = res.data.postsByHandle;
            console.log('Posts...');
            console.log(res.data.postsByHandle);
          } else {
            console.log('No Latest Trend...');
          }
        }

      }, error2 => {
        loading.dismiss()
        this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

      }
      )
  }

  goBack() {
    this.navCtrl.pop();
  }

  goToAppointments(index) {
    this.getDataFromServer()
  }

  getDataFromServer() {
    let loading = this.loadingController.create({ content: "Fetching Details, Please Wait..." });
    loading.present();

    let tempSalon = this.navParams.get("selectedSalon");
    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
      service: btoa('get_salon_details'),
      cust_id: btoa('777'),
      sal_id: btoa(this.selectedPost.sal_id),//
    }

    this.serviceManager.getData(params)
    .retryWhen((err) => {
      return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
              return retryCount;
          }
          else {
              throw (err);
          }
      }, 0).delay(1000)
  })
  
      .subscribe((response) => {
        console.log(response["salons"][0]);
        
        this.nativePageTransitions.slide(this.options)
        this.navCtrl.push(AddAppointmentPage, {
          isRescheduleAppointment: false,
          selectedSalon: response["salons"][0]
        })

        loading.dismissAll();
      },
        error => {
          loading.dismiss();
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        }
      );
  }

}
