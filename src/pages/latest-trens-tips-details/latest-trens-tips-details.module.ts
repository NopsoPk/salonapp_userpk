import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LatestTrensTipsDetailsPage } from './latest-trens-tips-details';

@NgModule({
  declarations: [
    LatestTrensTipsDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(LatestTrensTipsDetailsPage),
  ],
})
export class LatestTrensTipsDetailsPageModule {}
