import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-subcategories-pop-over',
  templateUrl: 'subcategories-pop-over.html',
})
export class SubcategoriesPopOverPage {
  public subStyles: any[]
  public scrollHeight: number
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.subStyles = this.navParams.get('subCategories')
    this.scrollHeight = this.navParams.get('scrollHeight')

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubcategoriesPopOverPage');
  }

}
