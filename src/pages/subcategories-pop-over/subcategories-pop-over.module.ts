import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubcategoriesPopOverPage } from './subcategories-pop-over';

@NgModule({
  declarations: [
    SubcategoriesPopOverPage,
  ],
  imports: [
    IonicPageModule.forChild(SubcategoriesPopOverPage),
  ],
})
export class SubcategoriesPopOverPageModule {}
