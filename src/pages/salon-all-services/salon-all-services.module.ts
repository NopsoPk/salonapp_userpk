import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalonAllServicesPage } from './salon-all-services';

@NgModule({
  declarations: [
    SalonAllServicesPage,
  ],
  imports: [
    IonicPageModule.forChild(SalonAllServicesPage),
  ],
})
export class SalonAllServicesPageModule {}
