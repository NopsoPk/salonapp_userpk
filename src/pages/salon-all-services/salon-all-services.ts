import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform, ViewController } from 'ionic-angular';
import { DatePipe } from '@angular/common'

import { Storage } from '@ionic/storage';
import { Pipe, PipeTransform } from '@angular/core';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { GlobalServiceProvider } from '../../providers/global-service/global-service';
import { GlobalProvider } from '../../providers/global/global';
import { AppMessages } from '../../providers/AppMessages/AppMessages';

@IonicPage()
@Component({
  selector: 'page-salon-all-services',
  templateUrl: 'salon-all-services.html',
})
@Pipe({ name: 'order-by' })
export class SalonAllServicesPage {
  allNotifications: any[];
  allServices: any[];
  Categories: any[];
  servicesSortedByCat: any[];
  salId: string;
  currentDate: string = "";
  public currencySymbol = ''

  constructor(
    
    private loadingController: LoadingController,
    public navCtrl: NavController,
    public datepipe: DatePipe,
    public storage: Storage,
    private viewCtrl: ViewController,
    public navParams: NavParams,
    public platform: Platform,
    public gsp: GlobalServiceProvider,
    public serviceManager: GlobalProvider,
    private nativePageTransitions: NativePageTransitions,

  ) {
    this.allNotifications = [{
      message: "4403",
    },
    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SalonAllServicesPage');
    this.currencySymbol =  this.gsp.currencySymbol 
  }
  ionViewWillEnter() {


    this.salId = this.navParams.get("salId");
    let AllServices = this.navParams.get('sal_services')
    this.allServices = this.navParams.get('sal_services')
    this.Categories = this.removeDuplicates(this.allServices, 'ssc_name')
    
    this.sortServices();
    // this.currentDate = this.serviceManager.getCurrentDateTime();
    // this.getSalonDetails();
  }

  getSalonDetails() {
    let loading_controller = this.loadingController.create({
      content: "Getting Reviews"
    });
    loading_controller.present();
    var params = {
      device_datetime: btoa(this.currentDate),
      service: btoa('get_salon_details'),
      sal_id: btoa(this.salId),
    }
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe((response) => {
        console.log('Service', response.sal_services);
        this.allServices = response.sal_services;
        this.Categories = this.removeDuplicates(response.sal_services, 'ssc_name')

        console.log('unique cat', this.Categories);
        this.sortServices();
        loading_controller.dismiss();
      },
        error => {
          loading_controller.dismiss();
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        }
      );
  }
  isServicescatChange(newService, previousService) {
    if (newService != previousService) {
      return true
    } else {
      return false;
    }
  }


  sortServices() {
    this.servicesSortedByCat = []
    this.Categories.forEach(element => {
      let catFromCategory: string = element['ssc_name'];
      this.allServices.forEach(element => {
        let catFromService: string = element['ssc_name'];
        if (catFromCategory.trim() == catFromService.trim()) {
          let elementNew = element;
          console.log('serivce element pushed', elementNew);
          this.servicesSortedByCat.push(elementNew)
        }
      });
    });
  }

  goBack() {
    this.navCtrl.pop();
  }
  removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }

  close() {
    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 400,
      slowdownfactor: 3,
      slidePixels: 0,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0, // not to include this top section in animation 
      fixedPixelsBottom: 0 // not to include this Bottom section in animation 
    };

    if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
    this.viewCtrl.dismiss();
  }

}
