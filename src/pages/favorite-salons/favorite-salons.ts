import { MainHomePage } from './../main-home/main-home';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { config } from './../../providers/config/config';
import { Salon, PINTEREST_OBJECT, Customer, SALON_CARD_OPTIONS } from './../../providers/SalonAppUser-Interface';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ModalController, Platform } from 'ionic-angular';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db';
import { MenuController} from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
import { SalonDetailsPage } from '../salon-details/salon-details';
import { PinActionSheetPage } from '../pin-action-sheet/pin-action-sheet';
import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { ActivateSalonPage } from '../activate-salon/activate-salon';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { FavoriteModal } from '../../providers/FavoriteModal/FavoriteModal';

@IonicPage()
@Component({
  selector: 'page-favorite-salons',
  templateUrl: 'favorite-salons.html',
})
export class FavoriteSalonsPage {
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  public GenderBasedTrends: string
  public isSalonSelected = false
  public isFavoriteSelected = false
  public favoriteSalons: Salon[] = []
  public favoritePosts: PINTEREST_OBJECT[] = []
  public salonImagesURL = config.salonImgUrl;
  public myCustomer: Customer
  public infiniteScroll: any
  public noPostsFound = true
  public noSalonFound = true
  public isAppLaunchedFirstTime = 'isAppLaunchedFirstTime'
  PaginationLinks: any
  public selectedPostObject: PINTEREST_OBJECT
  public salonCardOption: SALON_CARD_OPTIONS
  public unregisterBackButtonAction: any;
  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public http: Http,
    public events: Events,
    public navParams: NavParams,
    public salonModal: SqliteDbProvider,
    public favPostModal: FavoriteModal,
    public customerModal: CustomerModal,
    public modalCtrl: ModalController,
    public SM: GlobalProvider,
    private menu: MenuController,
    public nativePageTransitions: NativePageTransitions,
  ) {
    

    this.GenderBasedTrends = 'posts'
    this.getFavoriteSalonsFromDB()

  }
  ionViewWillEnter() {
   
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(customer => {
          if (customer) {
            this.myCustomer = customer
          }
        }, error => {
          console.log('error: while getting customer');
        })
      }
    })
  }
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
    // this.salonTapped()
    let firstLaunch = this.SM.getFromLocalStorage(this.isAppLaunchedFirstTime) 
    if (firstLaunch === undefined || firstLaunch === null) {
      !this.myCustomer ? this.getCustomer('getFavoritePostsFromServer') : this.getFavoritePostsFromServer()

    } else {
      this.getPostsFromDB()
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoriteSalonsPage');
  }


  getPostsFromDB() {
    this.favPostModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.favPostModal.getAllFavoritePosts().then(res => {
          this.favoritePosts = res
          console.log(JSON.stringify(this.favoritePosts));
          !this.favoritePosts || this.favoritePosts.length === 0 ? this.noPostsFound = true : this.noPostsFound = false
          if (!this.favoritePosts || (this.favoritePosts && this.favoritePosts.length === 0)) {
            this.getFavoritePostsFromServer()
          }
        })
      }
    })
  }
  getFavoriteSalonsFromDB(){
    this.salonModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonModal.getFavoriteSalon().then(favoriteSalons => {
          this.favoriteSalons = favoriteSalons
          this.salonCardOption = {
            CardHeight: '100vh',
            calledFrom: config.FavoritePage,
            ShouldShowSalonImage: true,
            shouldScroll: true,
            salon: this.favoriteSalons,
         
          }
            if(!this.favoriteSalons || this.favoriteSalons.length === 0){
              this.noSalonFound = true
            }else{
              this.isSalonSelected=true
              this.noSalonFound = false
            }
          
         // !this.favoriteSalons || this.favoriteSalons.length === 0 ? this.noSalonFound = true : this.noSalonFound = false
  
        })
      }
    })
  }
  
  salonTapped() {
    this.isSalonSelected = true
    this.isFavoriteSelected = false
    !this.favoriteSalons || this.favoriteSalons.length === 0 ? this.noSalonFound = true : this.noSalonFound = false
  }
  FavoriteTapped() {
    this.isFavoriteSelected = true
    this.isSalonSelected = false
    !this.favoritePosts || this.favoritePosts.length === 0 ? this.noPostsFound = true : this.noPostsFound = false
  }
  getSalonNameFirstCharacte(salonName: string): string {
    if (salonName.trim().length === 0) {
      return salonName
    }
    var shortSalonName = ''
    let charCount = 0
    let SplitSalonNames = salonName.split(' ')
    SplitSalonNames.forEach(element => {
      let salName: string = element

      if (charCount < 4 && salName.trim().length > 0) {
        if (charCount === 3 && salName.length === 1 && SplitSalonNames.length > 3) {
        } else {
          shortSalonName += salName.slice(0, 1)
          charCount += 1
        }

      }

    });

    return shortSalonName.toLocaleUpperCase();
  }

  //custom back button

ionViewWillLeave() {
  this.unregisterBackButtonAction && this.unregisterBackButtonAction();
}
public initializeBackButtonCustomHandler(): void {
  this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
    this.customHandleBackButton();
  }, 10);
}
private customHandleBackButton(): void {
  // if (this.navCtrl.canGoBack()) {
  //   this.navCtrl.pop();
  // } else {
  //   this.navCtrl.setRoot(MainHomePage)
  // }
  if(this.menu.isOpen()){
    this.menu.close();
  }else {
     if (this.navCtrl.canGoBack()) {
    this.navCtrl.pop();
  } else {
    this.navCtrl.setRoot(MainHomePage)
  }
  } 
  // this.navCtrl.push(MainHomePage, { isHideTab: "1" }, { animate: false })
}
//end custom back button 

  btnBackTapped() {
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }

    // let options: NativeTransitionOptions = {
    //   direction: 'right',
    //   duration: 500,
    //   slowdownfactor: 3,
    //   slidePixels: 20,
    //   iosdelay: 100,
    //   androiddelay: 150,
    //   fixedPixelsTop: 0,
    //   fixedPixelsBottom: 0
    // };
    // if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
    // if (this.navCtrl.canGoBack()) {
    //   this.navCtrl.pop({
    //     animate: false,
    //     animation: 'ios-transition',
    //     direction: 'back',
    //     duration: 500,
    //   })
    // } else {
    //   this.navCtrl.setRoot(MainHomePage)
    // }

  }


  getFavoritePostsFromServer() {
    if (!this.myCustomer) {

      return
    }
    let favoritePostsURL = config.getFavoritePostsFromServer + this.myCustomer.cust_id
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    let options = new RequestOptions({ headers: headers });
    console.log('fetching fresh posts');
    console.log(favoritePostsURL);
    // this.SM.showProgress()
    this.http.get(favoritePostsURL, options)
      .map(Response => Response.json())
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(res => {
        // this.SM.stopProgress()

        if (res.data.posts.length > 0) {
          this.favoritePosts = res.data.posts

          this.SM.setInLocalStorage(this.isAppLaunchedFirstTime, this.isAppLaunchedFirstTime)
          !this.favoritePosts || this.favoritePosts.length === 0 ? this.noPostsFound = true : this.noPostsFound = false

          this.favPostModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.favPostModal.InsertBulkPostsInTable(this.favoritePosts).then(res => {
                if (res === true) {
                  console.log('success: Bulk posts Saved');

                } else {
                  this.SM.makeToastOnFailure(res)
                }
              })
            }
          })
        } else {
          this.SM.setInLocalStorage(this.isAppLaunchedFirstTime, this.isAppLaunchedFirstTime)
        }
      }, error2 => {
        // this.SM.stopProgress()

        this.SM.makeToastOnFailure(AppMessages.msgNoInternetAVailable)


      }
      )
  }
  postImageTapped(objPin, $event) {
    let ssc_id = objPin.ssc_id
    let pinObject: PINTEREST_OBJECT = objPin
    var PinRelatedSalon: Salon[] = []
    let requiredSalonIDs = ''
    // let options: NativeTransitionOptions = {
    //   direction: 'up',
    //   duration: 500,
    //   slowdownfactor: 3,
    //   slidePixels: 0,
    //   iosdelay: 100,
    //   androiddelay: 150,
    //   fixedPixelsTop: 0, // not to include this top section in animation 
    //   fixedPixelsBottom: 0 // not to include this Bottom section in animation 
    // };
    this.salonModal.getDatabaseState().subscribe(ready => {
      if (ready) {

        this.salonModal.getSalSerSubCatFromSqliteDB(ssc_id).then(allSalonIDs => {
          allSalonIDs.forEach(element => {
            requiredSalonIDs += element.sal_id + ',';
          });
          requiredSalonIDs = requiredSalonIDs.slice(0, requiredSalonIDs.length - 1)
          if (requiredSalonIDs.length > 0) {
            // this.databse.getDatabaseState().subscribe(ready => {
            //   if (ready) {
            this.salonModal.getSalonsListingFromSqliteDB(requiredSalonIDs).then(res => {
              if (res && res.length > 0) {
                PinRelatedSalon = res
                PinRelatedSalon = PinRelatedSalon.slice(0, 5)

                this.nativePageTransitions.slide(this.options)
                this.navCtrl.push(PinActionSheetPage, {
                  PinRelatedSalon: PinRelatedSalon,
                  cateogry: 'test Category',
                  objPin: pinObject,

                })

                // const modal = this.modalCtrl.create(PinActionSheetPage, {
                //   PinRelatedSalon: PinRelatedSalon,
                //   cateogry: 'test Category',
                //   objPin: pinObject,
                // }, { cssClass: 'PopOverClass' });
                // modal.present()
              } else {
                this.SM.makeToastOnFailure(AppMessages.msgStyleNotAssociatedWithSalon)
              }
            })
          }
        })
      }
    })
  }
  makeSalonFavoriteTapped(salon) {
    console.log(JSON.stringify(salon));

    let mySalon: Salon = salon
    let makeSalonFavorite: number
    if (mySalon.favsal === 1) {
      makeSalonFavorite = 0
    } else {
      makeSalonFavorite = 1
    }
    console.log(makeSalonFavorite);
    this.favoriteSalons.splice(this.favoriteSalons.indexOf(salon), 1);
    !this.favoriteSalons || this.favoriteSalons.length === 0 ?  this.noSalonFound = true : this.noSalonFound = false
    let params = {
      service: btoa('favorite'),
      sal_id: btoa(mySalon.sal_id),
      cust_id: btoa(this.myCustomer.cust_id),
      favorite: btoa(makeSalonFavorite.toString()),

    }
    this.SM.getData(params).subscribe(res => {
      if (res['status'] === '1') {
        // this.favoriteSalons.forEach(salon => {
        //   if (salon.sal_id === mySalon.sal_id) {
        //     return salon.favsal = makeSalonFavorite
        //   }
        // });
        
        // if (!this.favoriteSalons || this.favoriteSalons.length === 0) this.noSalonFound = true
        this.salonModal.getDatabaseState().subscribe(ready => {
          if (ready) {
            this.salonModal.makeSalonFavorite(mySalon.sal_id, makeSalonFavorite).then(isSalonMarkedFavorite => {
              if (isSalonMarkedFavorite) {

                // this.salonModal.getSalonDetailsFrom().then()
              }
            })
          }
        })
      } else {

      }

    })
  }

  pinKeyWordTapped(objSubCat) {
    console.log(objSubCat);
    let keyword: string = objSubCat.p_keywords
    if (keyword.trim().length === 0) {
      return
    }
    let obj = this.SM.getFromLocalStorage(this.SM.OBJ_SUB_STYLE)
    if (obj) {
      let FilterPinsBaseURL = config.GetFilteredPinsURL
      FilterPinsBaseURL += keyword + '/' + obj.ssc_id + '/' + this.myCustomer.cust_id


      // paste code here

      var headers = new Headers();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json');
      headers.append('Access-Control-Allow-Origin', '*');

      let options = new RequestOptions({ headers: headers });
      console.log('going to call api pinKeyWordTapped');
      // Getting Posts from server 
      console.log(FilterPinsBaseURL);
      // this.SM.showProgress()
      this.http.get(FilterPinsBaseURL, options)
        .map(Response => Response.json())
        .retryWhen((err) => {
          return err.scan((retryCount) => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            }
            else {
              throw (err);
            }
          }, 0).delay(1000)
        })

        .subscribe(res => {
          console.log('response is back');

          // this.SM.stopProgress()
          if (res.data.posts != undefined && res.data.posts.length > 0) {
            if (this.infiniteScroll) {
              this.infiniteScroll.enable(true);
            }
            // if () {
            this.favoritePosts = res.data.posts;
            this.PaginationLinks = res.links
            !this.favoritePosts || this.favoritePosts.length === 0 ? this.noPostsFound = true : this.noPostsFound = false


            console.log('Posts...');
            console.log(res.data.posts);
            console.log(res.links);
            this.noPostsFound = false
            // }

          } else {
            console.log('No Latest Trend...');

            if (this.infiniteScroll) {
              this.infiniteScroll.enable(false);
            }
          }

        }, error => {
          // this.SM.stopProgress()
          console.log(JSON.stringify(error));

          this.SM.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

        })
    }
  }


  makePostFavoriteTapped(post: PINTEREST_OBJECT) {
    this.selectedPostObject = post
    if (!this.myCustomer) {
      
      this.getCustomer('makePostFavoriteTapped')
    }
    console.log(post);

    let myPost: PINTEREST_OBJECT = post
    let favoPostURL = config.makePostFavoriteURL
    // postiD/CustID/favUnFav


    let makePostFavorite: number
    if (myPost.favpost === 1) {
      makePostFavorite = 0
    } else {
      makePostFavorite = 1
    }
    console.log(makePostFavorite);

    this.favoritePosts.splice(this.favoritePosts.indexOf(post), 1);
    if (!this.favoritePosts || this.favoritePosts.length === 0) this.noPostsFound = true
      else this.noPostsFound = false
    favoPostURL += myPost.ip_id + '/' + this.myCustomer.cust_id + '/' + makePostFavorite
    console.log(favoPostURL);


    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');

    let options = new RequestOptions({ headers: headers });
    console.log('going to call api call');
    // Getting Posts from server 
    this.http.get(favoPostURL, options)
      .map(Response => Response.json())
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(res => {
        if (res.status === 1 || res.status === '1') {
          // this.favoritePosts.forEach(post => {
          //   if (post.ip_id === myPost.ip_id) {
          //     return myPost.favpost = makePostFavorite
          //   }
          // });
          
          if (makePostFavorite === 1) {
            this.favPostModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.favPostModal.InsertInTopostsTable(post).then(isSalonMarkedFavorite => {
                  if (isSalonMarkedFavorite) {
                    console.log('jobs Done');
                  }
                })
              }
            })
          } else if (makePostFavorite === 0) {
            this.favPostModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.favPostModal.deletePost(post.ip_id).then(isSalonMarkedFavorite => {
                  if (isSalonMarkedFavorite) {
                    console.log('jobs Done');
                  }
                })
              }
            })
          }
        }
      })
  }



  openDetailsPage(salon) {
   
    this.navCtrl.push(SalonDetailsPage, {
      selectedSalon: salon,
      sal_id: salon.sal_id
    });
  }
  getCustomer(andExecureThisMethod: string) {

    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(customer => {
          if (customer) {
            this.myCustomer = customer
            if (andExecureThisMethod === 'getFavoritePostsFromServer') {
              this.getFavoritePostsFromServer()
            } else if (andExecureThisMethod === 'makePostFavoriteTapped') {
              this.makePostFavoriteTapped(this.selectedPostObject)
            }

          } else {
            // this.navCtrl.setRoot(ActivateSalonPage)
          }
        }, error => {
          console.log('error: while getting customer');
        })
      }
    })
  }
}
