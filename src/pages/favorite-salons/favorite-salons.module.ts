import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavoriteSalonsPage } from './favorite-salons';

@NgModule({
  declarations: [
    FavoriteSalonsPage,
  ],
  imports: [
    IonicPageModule.forChild(FavoriteSalonsPage),
  ],
})
export class FavoriteSalonsPageModule {}
