import { GlobalProvider } from './../../providers/global/global';
import { Component } from '@angular/core';
import { App, IonicPage, LoadingController, NavController, NavParams, Platform } from 'ionic-angular';
import { LatestTrensTipsCategoryPage } from '../latest-trens-tips-category/latest-trens-tips-category'
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalServiceProvider } from '../../providers/global-service/global-service'

import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';

import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { config } from '../../providers/config/config';
import { AppMessages } from '../../providers/AppMessages/AppMessages';

/**
 * Generated class for the LatestTrendSubCatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-latest-trend-sub-cat',
  templateUrl: 'latest-trend-sub-cat.html',
})
export class LatestTrendSubCatPage {
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };


  public imageUrl = config.CategoryImageURL 
  public categories = Array();
  public cateogry = "";

  constructor(public http: Http,
    public globalSearch: GlobalServiceProvider,
    
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public serviceManager: GlobalProvider,
    private nativePageTransitions: NativePageTransitions,
    public platform: Platform) {
    console.log('constructor');

  }

  ionViewDidLoad() {
   
    console.log('ionViewDidLoad LatestTrendSubCatPage');
    this.getLatestesTrendsFromServer();
  }

  getLatestesTrendsFromServer() {
    let cat = this.navParams.get('selectedCat');
    this.cateogry = cat.sc_name;
    let tagURL = config.GetAllSubStylesURL+ cat.sc_id;
      
    let loading = this.loadingController.create({ content: "Please Wait..." });
    loading.present();
    console.log('config.GetAllSubStylesURL');
    
    console.log(tagURL);

    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');

    let options = new RequestOptions({ headers: headers });

    this.http.get(tagURL, options)
      .map(Response => Response.json())
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe(res => {
        loading.dismiss()
        console.log('res');
        console.log(res);
        if (res.data.Sub_Categories != undefined) {
          if (res.data.Sub_Categories.length > 0) {
            this.categories = res.data.Sub_Categories;
            console.log(res.data.Sub_Categories);
          } else {
            console.log('No Posts...');
          }
        }

      }, error2 => {
        loading.dismiss()
        this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
      }
      )
  }

  goBack() {

    this.navCtrl.pop();
  }
  categoryClicked(index) {
    console.log('Cats...zz');
    console.log(this.categories[index]);
    let selectedSubStyles = this.categories[index]

    if (selectedSubStyles) {
      this.serviceManager.setInLocalStorage(this.serviceManager.OBJ_SUB_STYLE,selectedSubStyles)
    }
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(LatestTrensTipsCategoryPage, {
      subCat: this.categories[index]
    });
  }

  btnBackTapped() {

    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };


    if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

    this.navCtrl.pop({
      animate: false,
      animation: 'ios-transition',
      direction: 'back',
      duration: 500,
    })
    // console.log(this.navCtrl.getPrevious().name);

    // this.navCtrl.pop({ animate: true, direction: 'back' })

  }


}
