import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LatestTrendSubCatPage } from './latest-trend-sub-cat';

@NgModule({
  declarations: [
    LatestTrendSubCatPage,
  ],
  imports: [
    IonicPageModule.forChild(LatestTrendSubCatPage),
  ],
})
export class LatestTrendSubCatPageModule {}
