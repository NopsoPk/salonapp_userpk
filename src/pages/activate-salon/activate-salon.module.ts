import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActivateSalonPage } from './activate-salon';


@NgModule({
  declarations: [
    ActivateSalonPage,
  ],
  imports: [
    IonicPageModule.forChild(ActivateSalonPage),
  ],
})
export class ActivateSalonPageModule {}
