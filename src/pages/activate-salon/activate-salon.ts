import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions';
import { GlobalProvider } from './../../providers/global/global';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ViewController, LoadingController, App, Content } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { PhoneNumberAuthenticationPage } from '../phone-number-authentication/phone-number-authentication'
import { Storage } from '@ionic/storage';

import { DatePipe } from '@angular/common'
import { AppVersion } from '@ionic-native/app-version';
import { StatusBar } from '@ionic-native/status-bar';
import { Platform } from 'ionic-angular';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { Geolocation } from '@ionic-native/geolocation';
import { FCM } from '@ionic-native/fcm';


import { Diagnostic } from '@ionic-native/diagnostic';
import { AUTH_CODE_VALIDATION } from '../../providers/SalonAppUser-Interface';
import { timer } from 'rxjs/observable/timer';
import { config } from '../../providers/config/config';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { Network } from '@ionic-native/network';
import { Keyboard } from '@ionic-native/keyboard';


//https://stackoverflow.com/questions/38652827/disable-swipe-to-view-sidemenu-ionic-2
@IonicPage()
@Component({
  selector: 'page-activate-salon',
  templateUrl: 'activate-salon.html',
})
export class ActivateSalonPage {
  nputPhoneNumberForActivation
  @ViewChild(Content) content: Content;
  @ViewChild('inputPhoneNumberForActivation') inputPhoneNumberForActivation: any;
  //public customerPhoneNumber = '03000000001';
   public customerPhoneNumber = '';
  response = '';
  blockAttempts = 0;
  isBlock = false;
  blockTime = '';
  isLocationFetched = false;
  counter = 0;
  versionName: any;
  versionCode: any;
  AppName: any;
  packageName: any;
  api_key: '';
  blockTimeRemeaning = 0;
  isValid = true;
  submitted = false;
  isLogin = true;
  errorMessage = '';
  isenabledNextbutton: boolean = false;
  public objAuthCodeValidtion: AUTH_CODE_VALIDATION
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  public latitude: any
  public longitude: any

  constructor(
    public appCtrl: App,
    private diagnostic: Diagnostic,
    private geolocation: Geolocation,
    public global: GlobalProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private menu: MenuController,
    private fcm: FCM,

    private loadingController: LoadingController, private viewCtrl: ViewController,
    public datepipe: DatePipe,
    private appVersion: AppVersion,
    public storage: Storage,
    public statusBar: StatusBar,
    public serviceManager: GlobalProvider,
    private nativePageTransitions: NativePageTransitions,
    public platForm: Platform,
    private androidPermissions: AndroidPermissions,
    public network: Network,
    public keyboard: Keyboard,
  ) {
    if (this.platForm.is('android')) {
      this.statusBar.styleLightContent();
    } else {
      this.statusBar.styleDefault();
    }
    // this.platForm.ready().then((readySource) => {
    //   console.log('Platform ready from', readySource);
    //  // this.checkLocation();
    //   // Platform now ready, execute any required native code
    // });
    // this.appVersion.getAppName().then((s) => {
    //   this.AppName = s;
    // })
    // this.appVersion.getVersionCode().then((s) => {
    //   this.versionCode = s;
    // })
    // this.appVersion.getVersionNumber().then((s) => {
    //   this.versionName = s;
    //   this.appVersion.getPackageName().then((s) => {
    //     this.packageName = s;
    //     this.getAppVersion();
    //   })
    // })
    let gcmDeviceID = this.global.getFromLocalStorage(this.global.GCM_TOKEN)
    if (!gcmDeviceID) {

    }
    this.checkConnection();
  }




  getAppVersion() {

    var params = {
      service: btoa('get_version_api_key'),
      app_name: btoa(this.packageName),
      version_no: btoa(this.versionName),
      release_date: btoa('2018-09-28'),
    }

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((response) => {
        this.objAuthCodeValidtion = this.serviceManager.getFromLocalStorage('AUTH_CODE_VALIDATION')
        if (this.objAuthCodeValidtion) {
          this.objAuthCodeValidtion.currentServerTime = response.response_datetime
          this.serviceManager.setInLocalStorage('AUTH_CODE_VALIDATION', this.objAuthCodeValidtion)
        }

        this.isenabledNextbutton = true;
        this.response = response.api_key;
        let status = response.status;
        if (status === '1') {
          this.api_key = response.api_key;
          this.storeApiKey(response.api_ke);
        }
      },
        error => {
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong while getting api key', error);
        }
      );
    // 

  }

  storeApiKey(key) {
    this.storage.set('api_key', key);
  }
  isValidLastIndex(value) {
    if (value == '0' || value == '1' || value == '2' || value == '3' || value == '4' || value == '5' || value == '6' || value == '7' || value == '8' || value == '9') {
      return true
    } else {
      return false;
    }
  }
  OnValueEnter(value) {

    var lastChar = value[value.length - 1];

    let elementChecker: string;
    elementChecker = value;
    if (this.isValidLastIndex(lastChar)) {
      this.isValid = true;

      if (this.customerPhoneNumber.length == 1) {
        if (this.customerPhoneNumber != '0') {
          this.customerPhoneNumber = '';
          this.isValid = false;
          this.errorMessage = AppMessages.msgMobileNumberStartingDigits
        }
      } else if (this.customerPhoneNumber.length == 2) {
        if (this.customerPhoneNumber != '03') {
          this.customerPhoneNumber = '';
          this.isValid = false;
          this.errorMessage = AppMessages.msgMobileNumberStartingDigits
        }
      } else if (this.customerPhoneNumber.length == 0) {
        this.errorMessage = AppMessages.msgMobileNumberStartingDigits
      }

    } else {
      this.customerPhoneNumber = elementChecker.slice(0, -1);
    }
    // this.customerPhoneNumber='';
  }

  sendActivationClickEvent() {
    if (this.customerPhoneNumber.length == 0) {
      this.isValid = false;
      this.serviceManager.makeToastOnFailure(AppMessages.msgPhoneNumberRequiredForRegistration)
      return
    }
    if (this.customerPhoneNumber.length < 11) {
      this.isValid = false;
      this.serviceManager.makeToastOnFailure(AppMessages.msgCustomerMobileNumber)
      return
    }
    if (this.objAuthCodeValidtion) {

      let timeDifference = (Date.parse(this.objAuthCodeValidtion.currentServerTime.toLowerCase().replace(/-|'/g, '/')) - Date.parse(this.objAuthCodeValidtion.serverTimeWhenUserWasBlocked.toLowerCase().replace(/-|'/g, '/'))) / 60000
     
      if (this.objAuthCodeValidtion.isUserBlocked && (timeDifference >= config.blockTimeOnInCorrectCode)) {
        this.objAuthCodeValidtion.isUserBlocked = false
        this.objAuthCodeValidtion.serverTimeWhenUserWasBlocked = ''
        this.serviceManager.setInLocalStorage('AUTH_CODE_VALIDATION', this.objAuthCodeValidtion)
      }
      if (this.objAuthCodeValidtion && this.objAuthCodeValidtion.isUserBlocked) {
        this.serviceManager.makeToastOnFailure(AppMessages.msgOTPMaxAttempts)
        return
      }
    }
    // calling api to send activation code
    let notificationController = this.loadingController.create({
      content: "Please wait.."
    });
    notificationController.present();
    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
      service: btoa('send_sms'),
      phone_number: btoa(this.customerPhoneNumber),
      api_key: btoa(this.api_key),
    }
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe((res) => {
        notificationController.dismiss();
        if (Number(res.status) === 1) {
          this.nativePageTransitions.slide(this.options)
          this.navCtrl.setRoot(PhoneNumberAuthenticationPage,
            {
              PhoneNumber: this.customerPhoneNumber
            });
        } else {
          this.serviceManager.makeToastOnFailure(res.message)
        }


        this.checkRegisterStatus();
      },
        error => {
          notificationController.dismiss();
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        }
      );


  }

  millisToMinutesAndSeconds(millis) {
    this.blockTimeRemeaning = Math.floor(millis / 60000);


  }


  getBlockTime() {
    let date = new Date();
    let latest_date = this.datepipe.transform(date, 'yyyy-MM-dd HH:mm:ss');
    let dt1 = new Date(latest_date);
    let dt2 = new Date(this.blockTime);

    let diffinmil = new Date(latest_date).getTime() - new Date(this.blockTime).getTime();

    this.millisToMinutesAndSeconds(diffinmil);


    let diff = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));

  }

  resgisterUser() {
    // if (this.api_key.trim().length === 0) { this.getAppVersion();return}
    let notificationController = this.loadingController.create({
      content: "Please wait.."
    });
    notificationController.present();
    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
      service: btoa('send_sms'),
      phone_number: btoa(this.customerPhoneNumber),
      api_key: btoa(this.api_key),
    }
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe((response) => {
        this.response = response.status;
        notificationController.dismiss();
        console.log('AllResponse', this.response);
        this.checkRegisterStatus();
      },
        error => {
          notificationController.dismiss();
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        }
      );
    // 
  }
  checkRegisterStatus() {
    if (this.response == '1') {

      if (!this.isBlock) {
        this.setAttempts();
      }

      console.log('AllAttemts', ":" + this.blockAttempts);
      this.nativePageTransitions.slide(this.options)
      this.navCtrl.setRoot(PhoneNumberAuthenticationPage,
        {
          PhoneNumber: this.customerPhoneNumber
        });
    }
  }

  // UpdateComponent() {
  //   let isFirstTime = this.serviceManager.getFromLocalStorage('isFirstTime')
  //   if (!isFirstTime) {
  //     this.serviceManager.setInLocalStorage('isFirstTime', '1')
  //     // this.appCtrl.getRootNav().setRoot(ActivateSalonPage);
  //     location.reload();
  //   } else {
  //   }
 
  // }


  onLogin(form: NgForm) {
    this.submitted = true;
    if (form.valid) {
    }
  }
  ionViewDidLoad() {
    setTimeout(() => {
      this.keyboard.show()
      this.inputPhoneNumberForActivation.setFocus();
    }, 1000);
  }

  ionViewDidEnter() {

    if (this.platForm.is('android')) this.requestSMSPermission()


  }
  permissionRequestCount = 0
  requestSMSPermission() {



    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(
      success => console.log('Permission granted'),
      err => this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]).then(() => {

      }, er => {

      })
    )
    if (this.permissionRequestCount < 5) {
      setTimeout(() => {
        this.requestSMSPermission()
        console.log('calling permision request again.....zahid');
        this.permissionRequestCount += 1
      }, 2000);

    }

    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS])

    if (this.platForm.is('cordova')) {

      //Subscribe on pause
      this.platForm.pause.subscribe(() => {
       

      });


    }

  }

  ionViewDidLeave() {
    // Don't forget to return the swipe to normal, otherwise 
    // the rest of the pages won't be able to swipe to open menu
    //  this.menu.swipeEnable(true);
    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(true, 'menu1');
  }

  enableGPSLocationSetting() {

  }


  ionViewWillEnter() {
    this.objAuthCodeValidtion = this.serviceManager.getFromLocalStorage('AUTH_CODE_VALIDATION')
    this.getAppVersion();

  }

  setAttempts() {
    this.storage.get('blockAttempts').then((value) => {
      if (value != undefined && value != null) {
        value = (value + 1);
      } else {
        value = 1;
      }

      if (value >= 4) {
        let blockTime1 = this.serviceManager.getCurrentDateTime();
        this.storage.set('blockTime', blockTime1);
        this.storage.set('isBlock', true);
      } else {
        this.storage.set('isBlock', false);
      }

      this.storage.set('blockAttempts', value);
    });
  }
  getAttempts() {
    this.storage.get('blockAttempts').then((value) => {
      this.blockAttempts = value;
    });

    this.storage.get('blockTime').then((value) => {
      this.blockTime = value;
    });



    this.storage.get('isBlock').then((value) => {
      this.isBlock = value;
    });
  }
  checkBlockStatus() {
    if (this.blockAttempts != null && this.blockAttempts != undefined) {
      if (this.blockAttempts >= 4) {
        this.serviceManager.makeToastOnFailure(AppMessages.msgWrongCodeEntrance)
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }
  checkConnection() {
    let disconnectSubscription = this.network.onDisconnect().subscribe((data) => {
      console.log('network was disconnected', data);
      this.serviceManager.makeToastOnFailure('Internet is disconnected.');
    });

    // watch network for a connection
    let connectSubscription = this.network.onConnect().subscribe((data) => {
      console.log('network connected!', data);
      this.global.makeToastOnSuccess('You are connected to Internet');
      this.getAppVersion()

    });
  }
  endEndEditing(value) {
    this.keyboard.close()
    this.content.scrollTo(0, 0).then(res => {
      this.content.resize()
      this.content.scrollTo(0, 0, 900).then(res => {
      })
    })
  }
}
//zahid updated code....
// import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions';
// import { GlobalProvider } from './../../providers/global/global';
// import { Component, ViewChild } from '@angular/core';
// import { IonicPage, NavController, NavParams, MenuController, ViewController, LoadingController, App, Content } from 'ionic-angular';
// import { NgForm } from '@angular/forms';
// import { PhoneNumberAuthenticationPage } from '../phone-number-authentication/phone-number-authentication'
// import { Storage } from '@ionic/storage';

// import { DatePipe } from '@angular/common'
// import { AppVersion } from '@ionic-native/app-version';
// import { StatusBar } from '@ionic-native/status-bar';
// import { Platform } from 'ionic-angular';
// import 'rxjs/add/operator/retrywhen';
// import 'rxjs/add/operator/delay';
// import 'rxjs/add/operator/scan';
// import { Geolocation } from '@ionic-native/geolocation';



// import { Diagnostic } from '@ionic-native/diagnostic';
// import { AUTH_CODE_VALIDATION, Customer } from '../../providers/SalonAppUser-Interface';
// import { timer } from 'rxjs/observable/timer';
// import { config } from '../../providers/config/config';
// import { AndroidPermissions } from '@ionic-native/android-permissions';
// import { AppMessages } from '../../providers/AppMessages/AppMessages';
// import { Network } from '@ionic-native/network';
// import { Keyboard } from '@ionic-native/keyboard';
// import { SplashScreen } from '@ionic-native/splash-screen';
// import { MainHomePage } from '../main-home/main-home';
// import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
// import { CongratsreditsPageModule } from '../congratsredits/congratsredits.module';


// //https://stackoverflow.com/questions/38652827/disable-swipe-to-view-sidemenu-ionic-2
// @IonicPage()
// @Component({
//   selector: 'page-activate-salon',
//   templateUrl: 'activate-salon.html',
// })
// export class ActivateSalonPage {
//   nputPhoneNumberForActivation
//   @ViewChild(Content) content: Content;
//   @ViewChild('inputPhoneNumberForActivation') inputPhoneNumberForActivation: any;


//   // public customerPhoneNumber = '03000000001';
//   public customerPhoneNumber = '';

//   // public customerPhoneNumber = '';
//   response = '';
//   blockAttempts = 0;
//   isBlock = false;
//   blockTime = '';
//   isLocationFetched = false;
//   counter = 0;
//   versionName: any;
//   versionCode: any;
//   AppName: any;
//   packageName: any;
//   api_key: '';
//   blockTimeRemeaning = 0;
//   isValid = true;
//   submitted = false;
//   isLogin = true;
//   errorMessage = '';
//   isenabledNextbutton: boolean = false;
//   public objAuthCodeValidtion: AUTH_CODE_VALIDATION
//   options: NativeTransitionOptions = {
//     direction: 'left',
//     duration: 500,
//     slowdownfactor: 3,
//     slidePixels: 0,
//     iosdelay: 100,
//     androiddelay: 150,
//     fixedPixelsTop: 0, // not to include this top section in animation 
//     fixedPixelsBottom: 0 // not to include this Bottom section in animation 
//   };
//   public latitude: any
//   public longitude: any

//   constructor(
//     public appCtrl: App,
//     private diagnostic: Diagnostic,
//     private geolocation: Geolocation,
//     public global: GlobalProvider,
//     public navCtrl: NavController,
//     public navParams: NavParams,
//     private menu: MenuController,
//     public customerModal: CustomerModal,

//     private loadingController: LoadingController, private viewCtrl: ViewController,
//     public datepipe: DatePipe,
//     private appVersion: AppVersion,
//     public storage: Storage,
//     public statusBar: StatusBar,
//     public serviceManager: GlobalProvider,
//     private nativePageTransitions: NativePageTransitions,
//     public platForm: Platform,
//     private androidPermissions: AndroidPermissions,
//     public network: Network,
//     public keyboard: Keyboard,
//     public splashScreen: SplashScreen,
//   ) {
//     if (this.platForm.is('android')) {
//       this.statusBar.styleLightContent();
//     } else {
//       this.statusBar.styleDefault();
//     }
//     // this.platForm.ready().then((readySource) => {
//     //   console.log('Platform ready from', readySource);
//     //  // this.checkLocation();
//     //   // Platform now ready, execute any required native code
//     // });
//     // this.appVersion.getAppName().then((s) => {
//     //   this.AppName = s;
//     // })
//     // this.appVersion.getVersionCode().then((s) => {
//     //   this.versionCode = s;
//     // })
//     // this.appVersion.getVersionNumber().then((s) => {
//     //   this.versionName = s;
//     //   this.appVersion.getPackageName().then((s) => {
//     //     this.packageName = s;
//     //     this.getAppVersion();
//     //   })
//     // })
//     let gcmDeviceID = this.global.getFromLocalStorage(this.global.GCM_TOKEN)
//     // if (!gcmDeviceID) {

//     // }
//     this.checkConnection();
//   }




//   getAppVersion() {

//     var params = {
//       service: btoa('get_version_api_key'),
//       app_name: btoa(this.packageName),
//       version_no: btoa(this.versionName),
//       release_date: btoa('2018-09-28'),
//     }

//     this.serviceManager.getData(params)
//       .retryWhen((err) => {
//         return err.scan((retryCount) => {
//           retryCount += 1;
//           if (retryCount < 3) {
//             return retryCount;
//           }
//           else {
//             throw (err);
//           }
//         }, 0).delay(1000)
//       })
//       .subscribe((response) => {
//         this.objAuthCodeValidtion = this.serviceManager.getFromLocalStorage('AUTH_CODE_VALIDATION')
//         if (this.objAuthCodeValidtion) {
//           this.objAuthCodeValidtion.currentServerTime = response.response_datetime
//           this.serviceManager.setInLocalStorage('AUTH_CODE_VALIDATION', this.objAuthCodeValidtion)
//         }

//         this.isenabledNextbutton = true;
//         this.response = response.api_key;
//         let status = response.status;
//         if (status === '1') {
//           this.api_key = response.api_key;
//           this.storeApiKey(response.api_ke);
//         }
//       },
//         error => {
//           this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
//           console.log('something went wrong while getting api key', error);
//         }
//       );
//     // 

//   }

//   storeApiKey(key) {
//     this.storage.set('api_key', key);
//   }
//   isValidLastIndex(value) {
//     if (value == '0' || value == '1' || value == '2' || value == '3' || value == '4' || value == '5' || value == '6' || value == '7' || value == '8' || value == '9') {
//       return true
//     } else {
//       return false;
//     }
//   }
//   OnValueEnter(value) {

//     var lastChar = value[value.length - 1];

//     let elementChecker: string;
//     elementChecker = value;
//     if (this.isValidLastIndex(lastChar)) {
//       this.isValid = true;

//       if (this.customerPhoneNumber.length == 1) {
//         if (this.customerPhoneNumber != '0') {
//           this.customerPhoneNumber = '';
//           this.isValid = false;
//           this.errorMessage = AppMessages.msgMobileNumberStartingDigits
//         }
//       } else if (this.customerPhoneNumber.length == 2) {
//         if (this.customerPhoneNumber != '03') {
//           this.customerPhoneNumber = '';
//           this.isValid = false;
//           this.errorMessage = AppMessages.msgMobileNumberStartingDigits
//         }
//       } else if (this.customerPhoneNumber.length == 0) {
//         this.errorMessage = AppMessages.msgMobileNumberStartingDigits
//       }

//     } else {
//       this.customerPhoneNumber = elementChecker.slice(0, -1);
//     }
//     // this.customerPhoneNumber='';
//   }

//   jugarForFareezApp() {


//     const params = {
//       service: btoa('cust_without_sms'),
//       phone_number: btoa(this.customerPhoneNumber),
//     }
//     this.serviceManager.showProgress()
//     this.serviceManager.getData(params)
//       .retryWhen((err) => {
//         return err.scan((retryCount) => {
//           retryCount += 1;
//           if (retryCount < 3) {
//             return retryCount;
//           }
//           else {
//             throw (err);
//           }
//         }, 0).delay(1000)
//       })
//       .subscribe(
//         (res) => {
//           this.serviceManager.stopProgress()
//           if (res.customer) {
//             this.saveCustomerIntoDB(res)
//           }

//         },
//         (error) => {
//           this.serviceManager.stopProgress()

//           this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
//         },
//         () => {
//           this.serviceManager.stopProgress()
//         }
//       )






//   }

//   sendActivationClickEvent() {


//     // this.content.scrollTo(0, 0).then(res => {
//     //   this.content.resize()
//     //   this.content.scrollTo(0, 0, 900).then(res => {
//     //   })
//     // })

//     if (this.customerPhoneNumber.length == 0) {
//       this.isValid = false;
//       this.serviceManager.makeToastOnFailure(AppMessages.msgPhoneNumberRequiredForRegistration)
//       return
//     }
//     if (this.customerPhoneNumber.length < 11) {
//       this.isValid = false;
//       this.serviceManager.makeToastOnFailure(AppMessages.msgCustomerMobileNumber)
//       return
//     }
//     this.jugarForFareezApp()
//     /*
//     if (this.objAuthCodeValidtion) {
 
//       let timeDifference = (Date.parse(this.objAuthCodeValidtion.currentServerTime.toLowerCase().replace(/-|'/g, '/')) - Date.parse(this.objAuthCodeValidtion.serverTimeWhenUserWasBlocked.toLowerCase().replace(/-|'/g, '/'))) / 60000
//       if (this.objAuthCodeValidtion.isUserBlocked && (timeDifference >= config.blockTimeOnInCorrectCode)) {
//         this.objAuthCodeValidtion.isUserBlocked = false
//         this.objAuthCodeValidtion.serverTimeWhenUserWasBlocked = ''
//         this.serviceManager.setInLocalStorage('AUTH_CODE_VALIDATION', this.objAuthCodeValidtion)
//       }
//       if (this.objAuthCodeValidtion && this.objAuthCodeValidtion.isUserBlocked) {
//         this.serviceManager.makeToastOnFailure(AppMessages.msgOTPMaxAttempts)
//         return
//       }
//     }
//     // calling api to send activation code
//     let notificationController = this.loadingController.create({
//       content: "Please wait.."
//     });
//     notificationController.present();
//     var params = {
//       device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
//       service: btoa('send_sms'),
//       phone_number: btoa(this.customerPhoneNumber),
//       api_key: btoa(this.api_key),
//     }
//     this.serviceManager.getData(params)
//       .retryWhen((err) => {
//         return err.scan((retryCount) => {
//           retryCount += 1;
//           if (retryCount < 3) {
//             return retryCount;
//           }
//           else {
//             throw (err);
//           }
//         }, 0).delay(1000)
//       })
 
//       .subscribe((res) => {
//         notificationController.dismiss();
//         if (Number(res.status) === 1) {
//           this.nativePageTransitions.slide(this.options)
//           this.navCtrl.setRoot(MainHomePage,
//             {
//               PhoneNumber: this.customerPhoneNumber
//             });
//         } else {
//           this.serviceManager.makeToastOnFailure(res.message)
//         }
 
 
//         this.checkRegisterStatus();
//       },
//         error => {
//           notificationController.dismiss();
//           this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
//           console.log('something went wrong', error);
//         }
//       );
 
// */
//   }
//   saveCustomerIntoDB(res) {
//     if (res.customer) {
//       this.customerModal.getDatabaseState().subscribe(ready => {
//         if (ready) {
//           this.customerModal.createCustomerTable().then(isTableCreated => {
//             if (isTableCreated === true) {
//               this.customerModal.InsertInToCustomerTable(res.customer).then(isCustomerInserted => {
//                 if (isCustomerInserted === true) {

//                   this.serviceManager.setInLocalStorage(this.serviceManager.IS_USER_LOGIN, true)
//                   this.storage.set('isLogin', true);

//                   let promo_code = res.promo_code;
//                   if (promo_code && promo_code.pc_id != undefined) {

//                     this.nativePageTransitions.slide(this.options)

//                     this.navCtrl.setRoot(CongratsreditsPageModule, { promo_code: promo_code })
//                       .then(() => {
//                         // this.navCtrl.remove(1);
//                         // this.navCtrl.remove(0);
//                       });
//                   } else {
//                     this.nativePageTransitions.slide(this.options)
//                     this.navCtrl.setRoot(MainHomePage, { myCustomer: res.customer })
//                       .then(() => {
//                         // this.navCtrl.remove(1);
//                         // this.navCtrl.remove(0);
//                       });
//                   }
//                   // this.nativePageTransitions.slide(this.options)
//                   // this.navCtrl.setRoot(MainHomePage, { myCustomer: customer })
//                   //   .then(() => {
//                   //     this.navCtrl.remove(1);
//                   //     this.navCtrl.remove(0);
//                   //   });

//                 } else {
//                   console.log('error: InsertInToCustomerTable ', isCustomerInserted);

//                 }


//               }, error => {
//                 console.log('error: ', error.message);

//               })
//             }

//           })
//         }
//       })
//     }

//   }

//   millisToMinutesAndSeconds(millis) {
//     this.blockTimeRemeaning = Math.floor(millis / 60000);


//   }


//   getBlockTime() {
//     let date = new Date();
//     let latest_date = this.datepipe.transform(date, 'yyyy-MM-dd HH:mm:ss');
//     let dt1 = new Date(latest_date);
//     let dt2 = new Date(this.blockTime);

//     let diffinmil = new Date(latest_date).getTime() - new Date(this.blockTime).getTime();

//     this.millisToMinutesAndSeconds(diffinmil);


//     let diff = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));

//   }

//   resgisterUser() {
//     // if (this.api_key.trim().length === 0) { this.getAppVersion();return}
//     let notificationController = this.loadingController.create({
//       content: "Please wait.."
//     });
//     notificationController.present();
//     var params = {
//       device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
//       service: btoa('send_sms'),
//       phone_number: btoa(this.customerPhoneNumber),
//       api_key: btoa(this.api_key),
//     }
//     this.serviceManager.getData(params)
//       .retryWhen((err) => {
//         return err.scan((retryCount) => {
//           retryCount += 1;
//           if (retryCount < 3) {
//             return retryCount;
//           }
//           else {
//             throw (err);
//           }
//         }, 0).delay(1000)
//       })

//       .subscribe((response) => {
//         this.response = response.status;
//         notificationController.dismiss();
//         console.log('AllResponse', this.response);
//         this.checkRegisterStatus();
//       },
//         error => {
//           notificationController.dismiss();
//           this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
//           console.log('something went wrong', error);
//         }
//       );
//     // 
//   }
//   checkRegisterStatus() {
//     if (this.response == '1') {

//       if (!this.isBlock) {
//         this.setAttempts();
//       }

//       console.log('AllAttemts', ":" + this.blockAttempts);
//       this.nativePageTransitions.slide(this.options)
//       this.navCtrl.setRoot(MainHomePage,
//         {
//           PhoneNumber: this.customerPhoneNumber
//         });
//     }
//   }





//   UpdateComponent() {
//     if (! this.platForm.is('android')) {//only for ios
//       let isFirstTime = this.serviceManager.getFromLocalStorage('isFirstTimeForUpdateComponent')
//       if (!isFirstTime) {
//         this.splashScreen.show()
  
//         this.serviceManager.setInLocalStorage('isFirstTimeForUpdateComponent', '1')
//         this.appCtrl.getRootNav().setRoot(ActivateSalonPage);
//         location.reload();
//       } else {
//       }
//     }

//   }


//   onLogin(form: NgForm) {
//     this.submitted = true;
//     if (form.valid) {
//     }
//   }
//   ionViewDidLoad() {
//     setTimeout(() => {
//       this.keyboard.show()
//       this.inputPhoneNumberForActivation.setFocus();
//     }, 1000);
//   }

//   ionViewDidEnter() {

//     if (this.platForm.is('android')) this.requestSMSPermission()


//   }
//   permissionRequestCount = 0
//   requestSMSPermission() {



//     this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(
//       success => console.log('Permission granted'),
//       err => this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]).then(() => {

//       }, er => {

//       })
//     )
//     if (this.permissionRequestCount < 5) {
//       setTimeout(() => {
//         this.requestSMSPermission()
//         console.log('calling permision request again.....zahid');
//         this.permissionRequestCount += 1
//       }, 2000);

//     }

//     this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS])

//     if (this.platForm.is('cordova')) {

//       //Subscribe on pause
//       this.platForm.pause.subscribe(() => {


//       });


//     }

//   }

//   ionViewDidLeave() {
//     // Don't forget to return the swipe to normal, otherwise 
//     // the rest of the pages won't be able to swipe to open menu
//     //  this.menu.swipeEnable(true);
//     // If you have more than one side menu, use the id like below
//     // this.menu.swipeEnable(true, 'menu1');
//   }

//   enableGPSLocationSetting() {

//   }


//   ionViewWillEnter() {
//     // timer(500).subscribe(() => {
//     this.UpdateComponent();
//     // })

//     this.objAuthCodeValidtion = this.serviceManager.getFromLocalStorage('AUTH_CODE_VALIDATION')
//     this.getAppVersion();

//   }

//   setAttempts() {
//     this.storage.get('blockAttempts').then((value) => {
//       if (value != undefined && value != null) {
//         value = (value + 1);
//       } else {
//         value = 1;
//       }

//       if (value >= 4) {
//         let blockTime1 = this.serviceManager.getCurrentDateTime();
//         this.storage.set('blockTime', blockTime1);
//         this.storage.set('isBlock', true);
//       } else {
//         this.storage.set('isBlock', false);
//       }

//       this.storage.set('blockAttempts', value);
//     });
//   }
//   getAttempts() {
//     this.storage.get('blockAttempts').then((value) => {
//       this.blockAttempts = value;
//     });

//     this.storage.get('blockTime').then((value) => {
//       this.blockTime = value;
//     });



//     this.storage.get('isBlock').then((value) => {
//       this.isBlock = value;
//     });
//   }
//   checkBlockStatus() {
//     if (this.blockAttempts != null && this.blockAttempts != undefined) {
//       if (this.blockAttempts >= 4) {
//         this.serviceManager.makeToastOnFailure(AppMessages.msgWrongCodeEntrance)
//         return false;
//       } else {
//         return true;
//       }
//     } else {
//       return true;
//     }
//   }
//   checkConnection() {
//     let disconnectSubscription = this.network.onDisconnect().subscribe((data) => {
//       console.log('network was disconnected', data);
//       this.serviceManager.makeToastOnFailure('Internet is disconnected.');
//     });

//     // watch network for a connection
//     let connectSubscription = this.network.onConnect().subscribe((data) => {
//       console.log('network connected!', data);
//       this.global.makeToastOnSuccess('You are connected to Internet');
//       this.getAppVersion()

//     });
//   }
//   endEndEditing(value) {

//     // this.keyboard.close()
//     this.content.scrollTo(0, 0).then(res => {
//       this.content.resize()
//       this.content.scrollTo(0, 0, 900).then(res => {
//       })
//     })

//   }
// }
