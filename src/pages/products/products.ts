import { Component, NgZone, ViewChild } from '@angular/core';
import { IonicPage, NavParams, Events, Keyboard, Content, ModalController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { MainHomePage } from '../main-home/main-home';
import { GlobalProvider } from '../../providers/global/global';
import { PRODUCT, NOPSO_Header_OPTIONS, PRODUCT_CATEGORY, Customer, PRODUCT_BRANDS } from '../../providers/SalonAppUser-Interface';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { MenuController } from 'ionic-angular';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db';
import { SalonServicesModal } from '../../providers/salon-services-modal/salon-services-modal';

import { config } from '../../providers/config/config';
import { ProductModal, } from '../../providers/ProductModal/ProductModal';
import { Slides } from 'ionic-angular';
import { NavController } from 'ionic-angular';
import { ProductColorModal } from '../../providers/ProductColorModal/ProductColorModal';
import { ProductImageModal } from '../../providers/ProductImageModal/ProductImageModal';
import { ViewCartPage } from '../view-cart/view-cart';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { ProductdetailPage } from '../productdetail/productdetail';
import { ProductCategoryModal } from '../../providers/ProductCategoryModal/ProductCategoryModal';
import { ProductCategoriesPage } from '../product-categories/product-categories';
import * as $ from "jquery";
import { timer } from 'rxjs/observable/timer';
import { SocialSharing } from '@ionic-native/social-sharing';
import { CartModal } from '../../providers/cartModal/cartModal';
import { GlobalServiceProvider } from '../../providers/global-service/global-service';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { ProductBrandModal } from '../../providers/ProductBrandModal/ProductBrandModal';
import { ProductSortingPage } from '../product-sorting/product-sorting';
import { Storage } from '@ionic/storage';
@IonicPage()
@Component({
  selector: 'page-products',
  templateUrl: 'products.html',
})
export class ProductsPage {
  @ViewChild(Slides) slides: Slides;
  @ViewChild(Content) content: Content;

  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  public productsImageURL = config.prouductImages
  public selectedProductCategory: PRODUCT_CATEGORY
  public selectedProductBrand: PRODUCT_BRANDS
  public selectedProduct: PRODUCT
  public selectedProductBrandID = ''
  public selectedProductCategoryID = ''
  public selectedProductIndex: -1
  public screenWidth = 0
  public numberOfChar = 0
  public products: PRODUCT[] = []
  public searhedProducts: PRODUCT[] = []
  public CartItems: PRODUCT[] = []

  public GenderBasedTrends: string
  public shouldShowPostsInEnglish = true
  public noProductAvailable = false
  public shouldShowMiniHeader = false
  public slideOutNow = false;
  public shouldShowMiniHeaderPrevious = false
  public nopsoHeaderOptions: NOPSO_Header_OPTIONS
  public myCustomer: Customer
  public txtSearchProduct = ''
  public productRating
  public zz = ['Facial Cleanser', 'Facial Cleanser Mask & Packs', 'Face', 'Face Mask & Packs', 'Facial Cleanser', 'Face', 'Face Mask & Packs']
  public socials = ['Share', 'Share on Twitter', 'Share on Twitter', 'Share', 'Share', 'Share on Twitter', 'Share on Twitter', 'Share']

  public isInShowingMoreProductMode = false
  public infiniteScroll: any

  public lastProductID = 0
  public productOffsetCount = 0
  public myScrollToTop = 0
  public isSearching = false
  public isAddedToCart = false;
  public productIDsRemovedFromCart = ''
  public currencySymbol = ''
  public unregisterBackButtonAction: any;

  public loadMoreCase = {
    productListing: 1,
    productForCategory: 2,
    productsForSearch: 3,
    productsForBrand: 4,
    productsForCategoryFilter: 5,
  }
  public selectedProductListingCase = 1

  constructor(
    private menu: MenuController,
    private zone: NgZone,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public serviceManager: GlobalProvider,
    public productModal: ProductModal,
    public cartModal: CartModal,
    public productColorModal: ProductColorModal,
    public productImageModal: ProductImageModal,
    public salonServiceModal: SalonServicesModal,
    public database: SqliteDbProvider,
    private nativePageTransitions: NativePageTransitions,
    public events: Events,
    public ga: GoogleAnalytics,
    public customerModal: CustomerModal,
    public ProductModal: ProductModal,
    public keyboard: Keyboard,
    private socialSharing: SocialSharing,
    public productCatModal: ProductCategoryModal,
    public prdouctBrandModal: ProductBrandModal,
    public gsp: GlobalServiceProvider,
    public modalCtrl: ModalController,
    public storage: Storage,
  ) {
    this.serviceManager.removeFromStorageForTheKey('productSorting')
    this.productRating = 4
    this.nopsoHeaderOptions = {
      miniHeaderHeight: '50pt',
      calledFrom: config.BeautyTipsDetailPage,
    }



    this.selectedProductCategory = navParams.get('selectedProductCategory')
    this.selectedProductBrand = navParams.get('selectedProductBrand')

    platform.ready().then((readySource) => {

      this.screenWidth = platform.width()
      this.numberOfChar = (platform.width() / 7.8) // one character takes

    });

  }
  //custom back button for android
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
    this.serviceManager.removeFromStorageForTheKey('productSorting')
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    // this.navCtrl.pop();
    if (this.menu.isOpen()) {
      this.menu.close();
    } else {
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop();
      } else {
        this.navCtrl.setRoot(MainHomePage)
      }
    }
  }
  //end custom back button for android

  doAnimation(event) {
    if (event.tapCount === 2) {
      $(".heart").on('click touchstart', function () {
        $(this).toggleClass('is_animating');
      });

      /*when the animation is over, remove the class*/
      $(".heart").on('animationend', function () {
        $(this).toggleClass('is_animating');
      });
    }
  }

  ionViewDidLoad() {
  }

  isSortingSelected = false;
  ionViewWillEnter() {

    this.getCurrencyDetail()
    this.productIDsRemovedFromCart = ''
    this.isSortingSelected = this.navParams.get('isSortingSelected')

    if (this.isSortingSelected) {
      this.lastProductID = 0
      this.products = []
      this.productOffsetCount = 0
    }
    let productIDsRemovedFromCart = this.navParams.get('productIDsRemovedFromCart')
    if (productIDsRemovedFromCart) this.productIDsRemovedFromCart = productIDsRemovedFromCart
    let isAddedToCart = this.navParams.get('isAddedToCart')

    if (isAddedToCart) {
      this.zone.run(() => {
        this.isAddedToCart = isAddedToCart

      });
    }

    let myOrderByArray = [' p.p_price asc', ' p.p_price desc', ' trim(upper(p.p_name)) asc', ' trim(upper(p.p_name)) desc', ' p.p_modified_date asc ', ' p.`is_featured` desc, p.p_modified_date desc']
    let sortingMode = Number(this.serviceManager.getFromLocalStorage('productSorting'))
    sortingMode ? this.orderBy = myOrderByArray[sortingMode - 1] : this.orderBy = myOrderByArray[5]

    if (this.lastProductID === 0) this.makeProductsReadyToRender()

    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(customer => {
          this.myCustomer = customer;
        })
      }
    })
  }

  orderBy = ''
  ionViewDidEnter() {

    this.initializeBackButtonCustomHandler();
    this.isAddedToCart = this.navParams.get('isAddedToCart')
    if (this.isAddedToCart) {

      let elem = document.getElementById('product-id.' + this.selectedProduct.p_id.toString())

      elem.textContent = 'Added To Cart'
    }
    this.shouldShowMiniHeader = this.shouldShowMiniHeaderPrevious

    this.events.subscribe('canShowMiniHeader', (isHidden: boolean) => {
      this.zone.run(() => {
        this.shouldShowMiniHeader = isHidden

      });
    });

    this.ProductModal.getDatabaseState().subscribe(ready => {
      if (ready) {

        this.cartModal.getCartItems().then(cartItems => {
          this.CartItems = cartItems
        })
      }
    })
    if (this.productIDsRemovedFromCart && this.productIDsRemovedFromCart.length > 0) {

      timer(1000).subscribe(() => {
        this.products.forEach(prod => {
          if (this.productIDsRemovedFromCart.includes(prod.p_id.toString())) return prod.isAddedtoCart = false;
        });
      })
    }
  }
  makeProductsReadyToRender() {

    let isBTFetched = this.serviceManager.getFromLocalStorage(this.serviceManager.PRODUCTS_LAST_FETCHED_DATE)
    if (isBTFetched) {
      this.productModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          if (this.selectedProductCategory) { // get products for when we come from product cateogry
            this.lastProductID = 0
            this.productModal.getProductsForCategory(this.selectedProductCategory.pc_id, this.lastProductID, this.productOffsetCount, this.orderBy).then(myProducts => {
              this.products = []
              this.products = myProducts

              this.productOffsetCount = this.products.length
              if (this.products && this.products.length > 0) this.lastProductID = this.products[this.products.length - 1].p_id
              !this.products || this.products.length === 0 ? this.noProductAvailable = true : this.noProductAvailable = false
              this.isInShowingMoreProductMode = false
              this.selectedProductListingCase = this.loadMoreCase.productForCategory
              this.setUpProductCategoryFilter(this.selectedProductCategory)

            })
          } else if (this.selectedProductBrand) {

            this.selectedProductBrandID = this.selectedProductBrand.pb_id.toString()
            this.lastProductID = 0
            this.productModal.getProductsForBrand(this.selectedProductBrandID, this.lastProductID, this.productOffsetCount, this.orderBy).then(myProducts => {
              this.products = []
              this.products = myProducts

              this.productOffsetCount = this.products.length
              if (this.products && this.products.length > 0) this.lastProductID = this.products[this.products.length - 1].p_id
              !this.products || this.products.length === 0 ? this.noProductAvailable = true : this.noProductAvailable = false
              this.isInShowingMoreProductMode = false
              this.selectedProductListingCase = this.loadMoreCase.productsForBrand
              this.setUpBrandFilter(this.selectedProductBrand)
            })

          } else {

            this.productModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.productModal.getAllProducts(this.lastProductID, this.productOffsetCount, this.orderBy).then(myProducts => {
                  myProducts.forEach(product => {
                    return product.isAddedtoCart = Boolean(product.isAddedtoCart)
                  })
                  this.products = []
                  this.products = myProducts
                  this.productOffsetCount = this.products.length
                  this.selectedProductListingCase = this.loadMoreCase.productListing
                  if (this.products && this.products.length > 0) {
                    this.lastProductID = this.products[this.products.length - 1].p_id

                  }
                  !this.products || this.products.length === 0 ? this.noProductAvailable = true : this.noProductAvailable = false
                  this.isInShowingMoreProductMode = false
                  this.getProductsFromServer()

                })

              }
            })
          }
        }
      })
    } else {
      this.getProductsFromServer()
    }
  }
  btnViewMoreTapped() {
  }
  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 700,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };
    if (this.platform.is(`ios`)) {
      this.nativePageTransitions.slide(options)
    }

    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
      // this.navCtrl.pop({
      //   animate: false,
      //   // animation: 'ios-transition',
      //   direction: 'back',
      //   duration: 500,
      // })
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
  }

  // getTipsDetailFromServer() {
  //   const params = {
  //     service: btoa('get_beauty_tips'),
  //   }

  //   // this.serviceManager.showProgress()
  //   this.serviceManager.getData(params)
  //     .retryWhen((err) => {
  //       return err.scan((retryCount) => {
  //         retryCount += 1;
  //         if (retryCount < 3) {
  //           return retryCount;
  //         }
  //         else {
  //           throw (err);
  //         }
  //       }, 0).delay(1000)
  //     })
  //     .subscribe(
  //       (res) => {

  //         this.serviceManager.stopProgress()
  //         this.products = res.TipCategories
  //         if (!this.products || this.products.length === 0) return
  //         this.productModal.getDatabaseState().subscribe(ready => {
  //           if (ready) {
  //             this.productModal.InsertInToProductTable(this.products).then(done => {
  //               this.serviceManager.setInLocalStorage(this.serviceManager.PRODUCTS_LAST_FETCHED_DATE, true)
  //             })
  //           }
  //         })
  //       },
  //       (error) => {
  //         this.serviceManager.stopProgress()
  // this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

  //         console.log('something went wrong', error);
  //       })
  // }




  btnAddToCartTapped(product: PRODUCT, index: string | number, event) {
    this.selectedProduct = product

    let elem = document.getElementById("isAddedtoCart_" + product.p_id)


    this.selectedProduct.p_quantity = 1
    let _tempGrandTotal = this.calculatePrice()
    _tempGrandTotal += this.selectedProduct.p_quantity * this.selectedProduct.p_price

    if (_tempGrandTotal > config.maxCartPrice) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgOrderLimit + config.maxCartPrice)
      return
    }

    // let _tempGrandTotal = this.calculatePrice()
    // this.selectedProduct.p_quantity = 1
    // _tempGrandTotal = this.selectedProduct.p_quantity * this.selectedProduct.p_price


    // if (_tempGrandTotal > config.maxCartPrice) {

    //   this.serviceManager.makeToastOnFailure('You cannot order more than Rs. ' + config.maxCartPrice + '.', 'top')
    //   return
    // }


    event.stopPropagation();
    // if (elem.textContent == "true") { this.serviceManager.makeToastOnSuccess(' items is already in cart.', 'top'); return }
    // elem.textContent == "false" && (product.isAddedtoCart = !product.isAddedtoCart)
    if (this.selectedProduct && this.selectedProduct.isAddedtoCart) { this.serviceManager.makeToastOnSuccess(AppMessages.msgProductAlreadyInCart); return }
    this.selectedProduct && (product.isAddedtoCart = !product.isAddedtoCart)

    product.p_quantity += 1
    this.productModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.cartModal.InsertInToCartTable(product).then(added => {
          this.productModal.updateProductTable(product)
          this.cartModal.getCartItems().then(cartItems => {
            this.CartItems = cartItems
            this.serviceManager.makeToastOnSuccess(AppMessages.msgAddedToCart)
          })
        })
      }
    })

  }

  btnViewCartTapped() {
    if (!this.CartItems || this.CartItems.length === 0) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgNoProductInCart)
      return
    }
    this.myScrollToTop = this.content.scrollTop
    this.nativePageTransitions.slide(this.options)
    this.shouldShowMiniHeaderPrevious = this.shouldShowMiniHeader
    this.navCtrl.push(ViewCartPage, {
      CartItems: this.CartItems,
      selectedProduct: this.selectedProductCategory
    })
  }

  googleAnalytics() {
    this.ga.startTrackerWithId(config.GoogleAnalyticsAppID)
      .then(() => {
        console.log('Google analytics is ready now');
        this.ga.trackView(this.myCustomer.cust_name + 'visited Prdocut screen for product_cateogry ' + this.selectedProductCategory.pc_name);
        this.ga.trackEvent('cat: ionViewWill Enter', 'act: loads everytime', 'lab: general label', 200, true)
        this.ga.trackTiming('cat: tracking timing', 600000, 'variable: not sure what will go here', 'label: and the same')
        this.ga.debugMode();
        this.ga.setAllowIDFACollection(true);
        this.ga.setUserId(this.myCustomer.cust_id)

      })
      .catch(e => {
        console.log('Error starting GoogleAnalytics', e)

      });

  }
  btnShowProductDetailTapped(product, index) {
    console.log(product);

    this.nativePageTransitions.slide(this.options)
    this.shouldShowMiniHeaderPrevious = this.shouldShowMiniHeader
    this.navCtrl.push(ProductdetailPage, {
      product: product
    })
  }
  shouldBeginEditing(value) {
    if (this.txtSearchProduct.trim().length > 0) {
      this.isSearching = true;
      this.shouldChangeCharacter()
    }
  }
  endEndEditing(value) {
    if (this.txtSearchProduct && this.txtSearchProduct.trim().length === 0) {

    }

  }

  shouldChangeCharacter() {
    if (this.txtSearchProduct.trim().length === 0) {
      this.isSearching = false
      this.searhedProducts = []



    } else {

      this.isSearching = true
      this.ShouldLoadDataFromStart = false;
      this.productModal.getDatabaseState().subscribe(ready => {
        if (ready) {

          this.productModal.searchProduct(this.txtSearchProduct).then(prdoucts => {
            prdoucts ? this.searhedProducts = prdoucts : []

            !this.searhedProducts || this.searhedProducts.length === 0 ? (this.isInShowingMoreProductMode = false, this.noProductAvailable = true) : ''
          })
        }
      })
    }

  }
  handleReturnkey() {
    this.btnSearchedProductTapped({ p_type: 1, p_id: 0 })
    this.keyboard.close()
  }

  isScrollUp = false
  didHideMainHeader = false;

  scrollHandler(event) {
    // return 

    if (this.products && this.products.length <= 2) return

    let elem: HTMLElement = this.content._scrollContent.nativeElement

    if (event.directionY === 'down') {
      console.log('if .....');
      this.isScrollUp = true
      elem.setAttribute("style", "margin-top: 0px !important;");
      this.didHideMainHeader = true;
      if (event.scrollTop > 150 && !this.shouldShowMiniHeader) {
        this.zone.run(() => {
          // elem.animate(slideOutHeader)
          this.slideOutNow = true;
          timer(400).subscribe(() => {
            this.shouldShowMiniHeader = true;
          })

        });
      }
    } else {
      this.zone.run(() => {
        console.log('else .....');

        elem.setAttribute("style", "margin-top: 130px !important;");
        this.isScrollUp = false
        this.slideOutNow = false;
        this.didHideMainHeader = false
        this.shouldShowMiniHeader = false;

      });

    }

    this.keyboard.close()
    if (event.scrollTop <= 0) {
      elem.setAttribute("style", "margin-top: 130px !important;");

    }
  }
  browseCategories() {
    this.shouldShowMiniHeaderPrevious = this.shouldShowMiniHeader
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(ProductCategoriesPage, {
      isForProductCategory: true
    })
  }
  browseBrands() {

    this.shouldShowMiniHeaderPrevious = this.shouldShowMiniHeader
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(ProductCategoriesPage, {
      isForProductCategory: false
    })
  }


  expandCollapseSeeMoreOptions(product: PRODUCT, index) {
    this.products[index].isShowingSeeMoreOptions = !this.products[index].isShowingSeeMoreOptions
  }
  toggleClass(element, className) {
    if (!element || !className) {
      return;
    }

    var classString = element.className, nameIndex = classString.indexOf(className);
    if (nameIndex == -1) {
      classString += ' ' + className;
    }
    else {
      classString = classString.substr(0, nameIndex) + classString.substr(nameIndex + className.length);
    }
    element.className = classString;

  }

  productCall = 1
  productOffset = 0
  productLimit = 5
  isProductFetchingProcessCompleted = false;
  getProductsFromServer() {

    // if(true)return 
    let lastFetchDate = ''
    let isFrstLanunch = this.serviceManager.getFromLocalStorage(this.serviceManager.IS_PRODUCTS_FIRST_LAUNCH)
    if (!isFrstLanunch) {
      this.makeFinalRequestToServer('');
    } else {
      let LastFetchOnFirstCycleComleted = this.serviceManager.getFromLocalStorage('isProductFetchingProcessCompleted')

      if (!LastFetchOnFirstCycleComleted) {
        this.productModal.getDatabaseState().subscribe(ready => {
          if (ready) {
            this.productModal.getMaxProductID().then(_productOffset => {
              this.productOffset = _productOffset
              let responseTime = lastFetchDate ? lastFetchDate : ''
              // let responseTime = ''
              this.makeFinalRequestToServer(responseTime);

            })
          }
        })
      } else {
        lastFetchDate = this.serviceManager.getFromLocalStorage(this.serviceManager.PRODUCTS_LAST_FETCHED_DATE)
        let responseTime = lastFetchDate ? lastFetchDate : ''
        // let responseTime = ''
        this.makeFinalRequestToServer(responseTime);
      }
    }
  }

  private makeFinalRequestToServer(responseTime: string) {
    const params = {
      service: btoa('get_products'),
      last_fetched: btoa(responseTime)
    };
    if (!responseTime || (responseTime && responseTime.trim().length === 0)) {
      params['limit'] = btoa(this.productLimit.toString());
      params['offset'] = btoa(this.productOffset.toString());
    }
    else {
      this.productCall = -1;
    }
    if (this.productCall === 1)
      this.serviceManager.showProgress();
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000);
      })
      .subscribe((res) => {
        if (this.productCall === 1)
          this.serviceManager.stopProgress();

        // !lastFetchDate || lastFetchDate.length === 0 ? this.serviceManager.showProgress() : ''
        // this.serviceManager.stopProgress()
        if (res.inactive_products && res.inactive_products.length > 0) {
          this.serviceManager.setInLocalStorage(this.serviceManager.PRODUCTS_LAST_FETCHED_DATE, res.response_datetime);
          this.productModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.productModal.delteDuplicateProduct(res.inactive_products).then(() => {
                this.cartModal.deleteDuplicateCartItems(res.inactive_products).then(res => {
                  if (res && Number(res.rowsAffected) > 0)
                    alert('Your cart has been updated.');
                });
              });
            }
          });
        }
        if (!res.products || res.products.length === 0) {
          this.isProductFetchingProcessCompleted = true;
          this.serviceManager.setInLocalStorage(this.serviceManager.PRODUCTS_LAST_FETCHED_DATE, res.response_datetime);

          return;
        }


        this.serviceManager.setInLocalStorage(this.serviceManager.IS_PRODUCTS_FIRST_LAUNCH, true);
        // this.serviceManager.setInLocalStorage(this.serviceManager.PRODUCTS_LAST_FETCHED_DATE, res.response_datetime);

        if (this.productCall <= 2)
          this.products = this.products.concat(res.products);
        this.ProductModal.getDatabaseState().subscribe(ready => {
          if (ready) {
            this.ProductModal.InsertInToProductTable(res.products).then(done => {
              switch (this.productCall) {
                case 1: //2nd call to fetch more 5 products
                  this.productCall += 1;
                  this.productOffset = 5;
                  this.serviceManager.setInLocalStorage('isProductFetchingProcessCompleted', res.response_datetime);
                  this.getProductsFromServer();
                  break;
                case 2:
                  this.productCall += 1;
                  this.productLimit = 500;
                  this.productOffset = 10;
                  this.lastProductID = this.products[this.products.length - 1].p_id;
                  this.getProductsFromServer();
                  break;
                case 3: //4th call which will be repeated untill all 
                  this.productLimit = 500;
                  this.productOffset += 500;
                  if (res.products && res.products.length >= 500) {
                    this.getProductsFromServer();
                  }
                  else {
                    this.isProductFetchingProcessCompleted = true;
                    let LastFetchOnFirstCycle = this.serviceManager.getFromLocalStorage('isProductFetchingProcessCompleted');
                    this.serviceManager.setInLocalStorage(this.serviceManager.PRODUCTS_LAST_FETCHED_DATE, LastFetchOnFirstCycle);
                    this.getProdCategoriesFromServer();
                    this.getProdBrandsFromServer();
                  }
                  break;
                default:
                  this.serviceManager.setInLocalStorage(this.serviceManager.PRODUCTS_LAST_FETCHED_DATE, res.response_datetime);
                  break;
              }
            });
          }
        });
      }, (error) => {
        this.serviceManager.stopProgress();
        this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable);
        console.log('something went wrong', error);
      });
  }

  getProdCategoriesFromServer() {
    // let lastFetchDate = this.serviceManager.getFromLocalStorage(this.serviceManager.LAST_FETCH_DATE_FOR_BEAUTY_TIPS)
    let lastFetchDate = this.serviceManager.getFromLocalStorage(this.serviceManager.PRODUCT_CATEGORIES_LAST_FETCHED_DATE)

    let responseTime = lastFetchDate ? lastFetchDate : ''

    const params = {
      service: btoa('get_product_categories'),
      last_fetched: btoa(responseTime)
    }

    // !lastFetchDate || lastFetchDate.length === 0 ? this.serviceManager.showProgress() : ''

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          //added by rizwan 

          if (res.inactive_categories && res.inactive_categories.length > 0) {
            this.productCatModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.productCatModal.delteDuplicateCategories(res.inactive_categories).then(() => {

                });
              }
            });
          }
          //end added by rizwan

          // !lastFetchDate || lastFetchDate.length === 0 ? this.serviceManager.stopProgress() : ''
          if (!res.ProductCategories || res.ProductCategories.length === 0) {
            this.serviceManager.setInLocalStorage(this.serviceManager.PRODUCT_CATEGORIES_LAST_FETCHED_DATE, res.response_datetime)
            return
          }
          if (!res.ProductCategories || res.ProductCategories.length === 0) return
          // this.ProductCategories = this.ProductCategories.concat(this.ProductCategories)

          this.productCatModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.productCatModal.InsertInToProductCategoryTable(res.ProductCategories).then(done => {
                this.serviceManager.setInLocalStorage(this.serviceManager.PRODUCT_CATEGORIES_LAST_FETCHED_DATE, res.response_datetime)
              }).then(jobsDone => {

              })
            }
          })
        },
        (error) => {
          this.serviceManager.stopProgress()
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        })
  }
  getProdBrandsFromServer() {

    let lastFetchDate = this.serviceManager.getFromLocalStorage(this.serviceManager.PRODUCT_BRANDS_LAST_FETCHED_DATE)

    let responseTime = lastFetchDate ? lastFetchDate : ''

    const params = {
      service: btoa('get_product_brands'),
      last_fetched: btoa(responseTime)
    }

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {

          console.log(res);

          if (res.inactive_brands && res.inactive_brands.length > 0) {
            this.prdouctBrandModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.prdouctBrandModal.delteDuplicateProductBrands(res.inactive_brands).then(() => {

                });
              }
            });
          }

          if (!res.ProductBrands) {
            this.serviceManager.setInLocalStorage(this.serviceManager.PRODUCT_BRANDS_LAST_FETCHED_DATE, res.response_datetime)
            return
          }
          if (!res.ProductBrands || res.ProductBrands.length === 0) return

          this.prdouctBrandModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.prdouctBrandModal.InsertInToProductBrandsTable(res.ProductBrands).then(done => {
                this.serviceManager.setInLocalStorage(this.serviceManager.PRODUCT_BRANDS_LAST_FETCHED_DATE, res.response_datetime)
              })
            }
          })
        },
        (error) => {
          this.serviceManager.stopProgress()
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        })
  }
  alreadyWentForShare = false;
  btnShareWithMenuTapped(product: PRODUCT) {
    if (this.alreadyWentForShare) return
    this.alreadyWentForShare = true
    let url = this.productsImageURL + product.p_image
    let productSshareURL = config.shareProductURL + product.p_id + '/' + product.p_name.replace(/ /g, '-')
    this.socialSharing.share('This is ' + this.myCustomer.cust_name + '.  I am using BeautyApp.pk on my phone and seen something interesting for you.  Please check the attached.\n' + productSshareURL, 'image', url).then(() => {
      this.alreadyWentForShare = false
    }).catch(er => {
      this.alreadyWentForShare = false
    })
  }
  wentToLoadMore = false
  loadMoreProducts(infiniteScroll) {
    if (this.wentToLoadMore || this.lastProductID === -1) {
      if (infiniteScroll) {
        try {
          infiniteScroll.complete();
        } catch (ex) {
        }
      }
      return
    }
    this.infiniteScroll = infiniteScroll
    // getAllProducts
    this.productModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        switch (this.selectedProductListingCase) {
          case this.loadMoreCase.productListing:
            this.productModal.getAllProducts(this.lastProductID, this.productOffsetCount, this.orderBy).then(products => {
              this.doRestOfThings(products)
            })
            break;
          case this.loadMoreCase.productForCategory:
            this.productModal.getProductsForCategory(this.selectedProductCategory.pc_id, this.lastProductID, this.productOffsetCount, this.orderBy).then(myProducts => {
              this.doRestOfThings(myProducts)
            })
            break;
          case this.loadMoreCase.productsForSearch:
            this.productModal.searchProductForLazyLoad(this.txtSearchProduct, this.lastProductID, this.productOffsetCount, this.orderBy).then(myProducts => {
              this.doRestOfThings(myProducts)
            })
            break;
          case this.loadMoreCase.productsForBrand:
            this.productModal.getProductsForBrand(this.selectedProductBrandID, this.lastProductID, this.productOffsetCount, this.orderBy).then(myProducts => {
              this.doRestOfThings(myProducts)
            })
            break;
          case this.loadMoreCase.productsForCategoryFilter:
            this.productModal.getProductsByCategory(this.selectedProductCategoryID, this.lastProductID, this.productOffsetCount, this.orderBy).then(myProducts => {
              this.doRestOfThings(myProducts)
            })
            break;
          default:
            break;
        }
        /*
        if (this.selectedProductCategory) {
          // need to do lazy loading as well
          this.productModal.getProductsForCategory(this.selectedProductCategory.pc_id, this.lastProductID).then(myProducts => {
            // this.products = this.products.concat(myProducts)
            this.doRestOfThings(myProducts)
            // infiniteScroll.complete()
            // if (this.products && this.products.length > 0) {
            //   this.lastProductID = this.products[this.products.length - 1].p_id
            //   this.noProductAvailable = false
            // } else {
            //   this.noProductAvailable = true
            // }
            // if (myProducts && myProducts.length < 8) {
            //   infiniteScroll.enable(false)
            // }
            // timer(4000).subscribe(() => {
            //   this.wentToLoadMore = false
            // })
          })
        } else {
          this.productModal.getAllProducts(this.lastProductID).then(products => {
            // this.products = this.products.concat(products)
            this.doRestOfThings(products)
            // infiniteScroll.complete()
            // if (this.products && this.products.length > 0) {
            //   this.lastProductID = this.products[this.products.length - 1].p_id
            // }
            // timer(4000).subscribe(() => {
            //   this.wentToLoadMore = false
            // })
          })
        }
        */
      }
    })
  }
  noMoreLazyLoading = false
  doRestOfThings(myProducts: PRODUCT[]) {
    this.products = this.products.concat(myProducts)
    this.productOffsetCount = this.products.length
    this.infiniteScroll.complete()
    if (this.products && this.products.length > 0) {
      this.lastProductID = this.products[this.products.length - 1].p_id
      this.noProductAvailable = false
    } else {
      this.noProductAvailable = true
    }
    if (myProducts && myProducts.length < 8) {
      this.infiniteScroll.enable(false)
      console.log('should not go for load more');
      this.noMoreLazyLoading = true
    } else {
      this.noMoreLazyLoading = false
    }
    timer(4000).subscribe(() => {
      this.wentToLoadMore = false


    })
  }
  productImageTapped(event, product: PRODUCT) {
    console.log(product);
    this.selectedProduct = product
    let selectedProductIndex = this.products.indexOf(product)
    console.log(event.tapCount);
    this.myScrollToTop = this.content.scrollTop
    if (event.tapCount === 1) {
      this.nativePageTransitions.slide(this.options)
      this.navCtrl.push(ProductdetailPage, {
        product: product,
        isFromProductPage: true
      })
    } else if (event.tapCount === 2) {
      this.btnShareWithMenuTapped(product)
    }
  }
  public highlight(SubStyleName: string) {
    // console.log(SubStyleName);
    if (!this.txtSearchProduct) {
      return SubStyleName;
    }
    return SubStyleName.replace(new RegExp(this.txtSearchProduct, "gi"), match => {
      return '<span class="highlightText">' + match + '</span>';
    });
  }
  public ShouldLoadDataFromStart = false;
  btnSearchedProductTapped(selectedProduct: { p_type: number; p_id: string | number; }) {
    this.productOffsetCount = 0
    this.ShouldLoadDataFromStart = true;

    if (selectedProduct.p_type === 1) {
      this.productModal.getDatabaseState().subscribe(ready => {
        if (ready) {

          if (selectedProduct.p_id > 0) {
            // get products detail
            this.productModal.getProductDetail(selectedProduct.p_id).then(product => {
              // this.btnShowProductDetailTapped(product, 0)
              this.nativePageTransitions.slide(this.options)
              this.navCtrl.push(ProductdetailPage, {
                product: product
              })
            })
          } else {
            // show all products with given keyword
            this.productModal.searchProductForLazyLoad(this.txtSearchProduct, '', this.productOffsetCount, this.orderBy).then(myProducts => {

              this.products = []
              this.products = myProducts
              this.productOffsetCount = this.products.length
              if (this.products && this.products.length > 0) this.lastProductID = this.products[this.products.length - 1].p_id
              !this.products || this.products.length === 0 ? this.noProductAvailable = true : this.noProductAvailable = false
              this.isInShowingMoreProductMode = false
              this.selectedProductListingCase = this.loadMoreCase.productsForSearch
            })
          }
        }
      })

    } else {
      // get products for the cateogry

      this.productModal.getDatabaseState().subscribe(ready => {
        this.productModal.getProductsForCategory(selectedProduct.p_id, 0, this.productOffsetCount, this.orderBy).then(myProducts => {
          this.products = []
          this.products = myProducts
          this.productOffsetCount = this.products.length
          if (this.products && this.products.length > 0) this.lastProductID = this.products[this.products.length - 1].p_id
          !this.products || this.products.length === 0 ? this.noProductAvailable = true : this.noProductAvailable = false
          this.isInShowingMoreProductMode = false
        })

      })
    }

    this.isSearching = false



  }
  btnClearSearchTapped() {

    this.txtSearchProduct = ''
    this.shouldChangeCharacter()

  }
  getCurrencyDetail() {
    this.currencySymbol = this.gsp.currencySymbol

    if (!this.currencySymbol || this.currencySymbol.length === 0) {
      let _appConfig = this.serviceManager.getFromLocalStorage('isAppConfigs')
      if (_appConfig) {
        if (_appConfig && _appConfig.currency) {
          this.gsp.currencySymbol = _appConfig.currency
          this.gsp.distance_unit = _appConfig.distance_unit

        }
      }
    }
  }
  setUpBrandFilter(product: PRODUCT_BRANDS | PRODUCT) {
    this.selectFilterName = product.pb_name
    this.filterToggle = true
    this.isProductsByCategory = false

    this.selectedProductBrandID = product.pb_id.toString()
  }

  filterProductsByBrand(product: PRODUCT) {
    this.setUpBrandFilter(product)
    this.productModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.productModal.getProductsForBrand(this.selectedProductBrandID, 0, this.productOffsetCount, this.orderBy).then(myProducts => {
          this.products = []

          this.products = myProducts
          this.productOffsetCount = this.products.length
          this.content.scrollTo(0, 0).then(res => {
            this.content.resize()
            this.content.scrollTo(0, 0, 900).then(res => {

            })
          })
          if (this.products && this.products.length > 0) this.lastProductID = this.products[this.products.length - 1].p_id
          !this.products || this.products.length === 0 ? this.noProductAvailable = true : this.noProductAvailable = false
          this.isInShowingMoreProductMode = false
          this.selectedProductListingCase = this.loadMoreCase.productsForBrand

          // this.doRestOfThings(myProducts) // zahid brand work
        })
      }
    })
  }
  isProductsByCategory = false;
  setUpProductCategoryFilter(product: PRODUCT_CATEGORY | PRODUCT) {
    this.selectFilterName = product.pc_name
    this.filterToggle = true;
    this.isProductsByCategory = true;
    this.selectedProductCategoryID = product.pc_id.toString()
  }
  filterProductsByCategory(product: PRODUCT) {

    this.setUpProductCategoryFilter(product)
    this.productModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.productModal.getProductsByCategory(this.selectedProductCategoryID, 0, this.productOffsetCount, this.orderBy).then(myProducts => {

          this.products = []
          this.products = myProducts
          this.productOffsetCount = this.products.length
          this.content.scrollTo(0, 0).then(res => {
            this.content.resize()
            this.content.scrollTo(0, 0, 900).then(res => {

            })
          })
          if (this.products && this.products.length > 0) this.lastProductID = this.products[this.products.length - 1].p_id
          !this.products || this.products.length === 0 ? this.noProductAvailable = true : this.noProductAvailable = false
          this.isInShowingMoreProductMode = false
          this.selectedProductListingCase = this.loadMoreCase.productsForCategoryFilter

          // this.doRestOfThings(myProducts) // zahid brand work
        })
      }
    })
  }
  calculatePrice() {
    !this.CartItems || this.CartItems.length === 0 ? this.noProductAvailable = true : this.noProductAvailable = false


    let numberOfItemAddedCart = 0
    let totalOrderPrice = 0.0
    this.CartItems.forEach(item => {
      if (item.isChecked) {
        totalOrderPrice += Number(item.p_price * item.p_quantity)
      }

      if (item.isAddedtoCart) numberOfItemAddedCart += 1
      return item.isAddedtoCart = true
    });

    return totalOrderPrice
  }
  filterToggle = false;
  selectFilterName = ''
  toggle() {
    this.filterToggle = !this.filterToggle
  }

  btnShowProductSortingPageTapped() {
    this.navCtrl.push(ProductSortingPage)

  }

  // End of Main Class
}


/**
 * some Old Code
 */
/*


            _beautyTipsDetail.forEach(beautyTipPost => {
              if (beautyTipPost.bt_description && beautyTipPost.bt_description.trim().length > 0) {
                this.beautyTipsDetailEnglish.push(beautyTipPost)
              }
              if (beautyTipPost.bt_description_urdu && beautyTipPost.bt_description_urdu.trim().length > 0) {
                this.beautyTipsDetailUrdu.push(beautyTipPost)

              }
            });

            if (this.serviceManager.getFromLocalStorage(this.serviceManager.SHOULD_SHOW_ENGLISH_POSTS) === true) {
              this.shouldShowPostsInEnglish = true
              this.GenderBasedTrends = 'English'
              this.beautyTipsDetail = this.beautyTipsDetailEnglish
              !this.beautyTipsDetail || this.products.length === 0 ? this.noProductAvailable = true : this.noProductAvailable = false
            } else if (this.serviceManager.getFromLocalStorage(this.serviceManager.SHOULD_SHOW_ENGLISH_POSTS) === false) {
              this.shouldShowPostsInEnglish = false
              this.GenderBasedTrends = 'Urdu'
              this.beautyTipsDetail = this.beautyTipsDetailUrdu
              !this.beautyTipsDetail || this.products.length === 0 ? this.noProductAvailable = true : this.noProductAvailable = false
              this.serviceManager.setInLocalStorage(this.serviceManager.SHOULD_SHOW_ENGLISH_POSTS, false)
            } else {
              this.shouldShowPostsInEnglish = true
              this.GenderBasedTrends = 'English'
              this.beautyTipsDetail = this.beautyTipsDetailEnglish
              !this.beautyTipsDetail || this.products.length === 0 ? this.noProductAvailable = true : this.noProductAvailable = false
              this.serviceManager.setInLocalStorage(this.serviceManager.SHOULD_SHOW_ENGLISH_POSTS, true)
            }
            //  SEction 2

            postsInEnglishTapepd() {
    if (this.shouldShowPostsInEnglish) {
      return
    }
    this.shouldShowPostsInEnglish = true
    this.products = this.beautyTipsDetailEnglish
    !this.products || this.products.length === 0 ? this.noProductAvailable = true : this.noProductAvailable = false
    this.serviceManager.setInLocalStorage(this.serviceManager.SHOULD_SHOW_ENGLISH_POSTS, true)

  }
  postsInUrduTapepd() {
    if (!this.shouldShowPostsInEnglish) {
      return
    }
    this.shouldShowPostsInEnglish = false
    this.products = this.beautyTipsDetailUrdu
    !this.products || this.products.length === 0 ? this.noProductAvailable = true : this.noProductAvailable = false
    this.serviceManager.setInLocalStorage(this.serviceManager.SHOULD_SHOW_ENGLISH_POSTS, false)
  }
  // checkThis() {

  //   var elem = document.querySelector('#some-element');
  //   elem.setAttribute("style", "background:blue !important")

  // }



  // SEction 3

  // postImageTapped(beautyTip: PRODUCT, $event) {

  //   this.selectedTipDetail = beautyTip
  //   if (!this.selectedTipDetail || this.selectedTipDetail.p_id.toString().trim().length === 0) {
  //     // this.serviceManager.makeToastOnFailure('shary beta iski ssc_ID ni aa rahi', 'top')
  //     return
  //   }

  //   var PinRelatedSalon: Salon[] = []
  //   let requiredSalonIDs = ''


  //   this.database.getDatabaseState().subscribe(ready => {
  //     if (ready) {

  //       this.database.getSalSerSubCatFromSqliteDBForBeautyTips(this.selectedTipDetail.p_id).then(allSalonIDs => {

  //         allSalonIDs.forEach(element => {
  //           requiredSalonIDs += element.sal_id + ',';
  //         });
  //         requiredSalonIDs = requiredSalonIDs.slice(0, requiredSalonIDs.length - 1)
  //         if (requiredSalonIDs.length > 0) {

  //           this.database.getSalonsListingFromSqliteDB(requiredSalonIDs).then(res => {
  //             if (res && res.length > 0) {
  //               PinRelatedSalon = res
  //               this.salonServiceModal.getDatabaseState().subscribe(ready => {
  //                 if (ready) {


  //                   PinRelatedSalon.forEach(salon => {
  //                     this.salonServiceModal.getSalonServicesForSubCategory(salon.sal_id, this.selectedTipDetail.p_id).then(pinRelatedSers => {
  //                       return salon['pinRelatedServices'] = pinRelatedSers
  //                     })
  //                   });
  //                   this.nativePageTransitions.slide(this.options)
  //                   this.navCtrl.push(PinActionSheetPage, {
  //                     PinRelatedSalon: PinRelatedSalon,
  //                     cateogry: this.selectedProduct,
  //                     selectedTipDetail: this.selectedTipDetail,
  //                     isFromBeautyTip: true,
  //                     shouldShowPostsInEnglish: this.shouldShowPostsInEnglish,

  //                   })
  //                 }
  //               })

  //             }
  //             // else {
  //             //   this.SM.makeToastOnFailure('No salon is associated with this style', 'top')
  //             // }
  //           })
  //         }
  //       })
  //     }
  //   })


  // }

  // slideChanged() {
  //   if (this.slides.isEnd()) {
  //     return
  //   }
  //   let currentIndex = this.slides.getActiveIndex();
  //   console.log('Current index is', currentIndex);
  // }









   /**
    this.products.map((prod) => {

      if (product == prod) {
        prod.isAddedtoCart = !prod.isAddedtoCart;

      } else {
        prod.isAddedtoCart = false;
      }

      return prod;

    });
    if (this.CartItems.length === 0) {
      this.CartItems.push(product)
      this.productModal.updateProductTable(1,product.p_id)
    } else {


      this.CartItems.forEach((_product, index) => {
        if (_product.p_id === product.p_id) {
          this.productModal.updateProductTable(0,product.p_id)
          this.CartItems.splice(index, 1)

        } else {
          this.CartItems.push(product)
          this.productModal.updateProductTable(1,product.p_id)
        }
      });
    }

 */