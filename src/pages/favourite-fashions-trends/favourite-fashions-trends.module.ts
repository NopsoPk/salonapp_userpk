import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavouriteFashionsTrendsPage } from './favourite-fashions-trends';

@NgModule({
  declarations: [
    FavouriteFashionsTrendsPage,
  ],
  imports: [
    IonicPageModule.forChild(FavouriteFashionsTrendsPage),
  ],
})
export class FavouriteFashionsTrendsPageModule {}
