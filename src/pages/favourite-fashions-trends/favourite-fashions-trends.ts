import { SqliteDbProvider } from './../../providers/sqlite-db/sqlite-db';
import { SALON_SERVICES_SUB_CATEGORIES, Salon, PINTEREST_OBJECT, Posts, Customer, SalonServices, CATEGORY_SUB_STYLE } from './../../providers/SalonAppUser-Interface';
import { PinActionSheetPage } from './../pin-action-sheet/pin-action-sheet';
import { Component } from '@angular/core';
import { App, IonicPage, LoadingController, NavController, NavParams, Platform, Alert, Events } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalServiceProvider } from '../../providers/global-service/global-service'
import { timer } from 'rxjs/observable/timer';
import { LatestTrensTipsDetailsPage } from '../latest-trens-tips-details/latest-trens-tips-details'
import { HomePage } from '../home/home'
import { SalonDetailsPage } from '../salon-details/salon-details';
import { Storage } from '@ionic/storage';
import { GlobalProvider } from '../../providers/global/global'
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { config } from "../../providers/config/config";
import { PopoverController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { FavoriteModal } from '../../providers/FavoriteModal/FavoriteModal';
import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { MainHomePage } from '../main-home/main-home';
import { GoogleAnalytics } from '../../../node_modules/@ionic-native/google-analytics';
import { SalonServicesModal } from '../../providers/salon-services-modal/salon-services-modal';
import { LatestFashionCollectionDetailPage } from '../latest-fashion-collection-detail/latest-fashion-collection-detail';
import { FashionBrandModal } from '../../providers/FashionBrandModal/FashionBrandModal';
import { Content } from 'ionic-angular';
import {  ViewChild, NgZone } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing';
import {  BEAUTY_TIPS, TIPS_DETAIL, NOPSO_Header_OPTIONS , FB_IMAGES} from './../../providers/SalonAppUser-Interface';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { MenuController} from 'ionic-angular';
import { LoginSignUpPage } from '../login-sign-up/login-sign-up';

@IonicPage()
@Component({
  selector: 'page-favourite-fashions-trends',
  templateUrl: 'favourite-fashions-trends.html',
})
export class FavouriteFashionsTrendsPage {

  public selectedCategory
  twoBeautyTips: any = [];
 
  public collectionName="";
  public slider: HTMLElement
  last_fbci_id:any
  fbc_modify_datetime:any
  public nopsoHeaderOptions: NOPSO_Header_OPTIONS
  @ViewChild(Content) content: Content;
  ionScrollTop = 0
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  public unregisterBackButtonAction: any;
  public shouldShowMiniHeader = false
  public fbc_images: FB_IMAGES[]
  public getPostsForKeyWordURL
  public postsArray = Array()
  public noPostsFound = false;
  public clearPostsAfterIntervval = 30 //minutes
  public infiniteScroll: any
  public myCustomer: Customer
  public isLoginSignUp=false
  public PaginationLinks: any
  public AllRecordsFetched=true
  public imageUrl = config.FashionImageURL
  page = 0;
  maximumPages = 20
  public selectedSubCategory
  public selectedPost: PINTEREST_OBJECT
  public imageUrlKeyWord:any //for showing keyword tap search 
  trendCall = 1
  trendOffset = 0
  trendLimit = 5
  gender:any
  // fb_id:any
  public lastFetchDate:any
  constructor(
    public serviceManager: GlobalProvider,
    public http: Http,
    private socialSharing: SocialSharing,
    public globalSearch: GlobalServiceProvider,
    private menu: MenuController,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public platform: Platform,
    public SM: GlobalProvider,
    public popoverCtrl: PopoverController,
    public modalCtrl: ModalController,
    public database: SqliteDbProvider,
    public favModal: FavoriteModal,
    public customerModal: CustomerModal,
    public fashionBrandModal: FashionBrandModal,
    public events: Events,
    private zone: NgZone,
    private ga: GoogleAnalytics,
    private nativePageTransitions: NativePageTransitions,
  ){
      this.nopsoHeaderOptions = {
        miniHeaderHeight: '50pt',
        calledFrom: config.BeautyTipsViewPage,
      }
      this.selectedCategory = this.navParams.get('fashionTrends');
      
      this.collectionName=this.navParams.get('brandName');
      // this.fb_id=this.selectedCategory.fb_id
      this.gender=this.navParams.get('gender');
     
     
} 
  //custom back button for android
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    if(this.menu.isOpen()){
      this.menu.close();
    }else {
       if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
    }

    // this.navCtrl.pop();
  }
//end custom back button for android

ionViewDidEnter() {
  this.initializeBackButtonCustomHandler();
  let abc = false
  this.events.subscribe('canShowMiniHeader', (isHidden) => {
    this.zone.run(() => {
      this.shouldShowMiniHeader = isHidden
    });
  });  
}

  ionViewDidLoad() {
    this.isLoginSignUp = this.serviceManager.getFromLocalStorage(this.serviceManager.IS_USER_LOGIN);
    this.requestToDbAndServer();
  }


  requestToDbAndServer(){
    this.fbc_modify_datetime=0
    this.last_fbci_id=0
    this.content.ionScroll.subscribe(ev =>
      requestAnimationFrame(() => this.ionScrollChange(ev.scrollTop)));
      this.customerModal.getDatabaseState().subscribe(ready => {
        if (ready) {
       
          this.customerModal.getCustomer().then(customer => {
            this.myCustomer = customer
            
           
          })
        }
      })
            this.fashionBrandModal.getDatabaseState().subscribe(ready => {
              if (ready) {
              
                this.fbc_images=[]
              
                  this.fashionBrandModal.getFBC_Imag_Favourite('2').then(res => {  
                  
                  // console.log('ALLData',JSON.stringify(res))      
                  if (res && res.length > 0) {
                    this.fbc_images = res    
                    //show post from local databse
                    if (this.infiniteScroll) {
                      this.infiniteScroll.enable(true);
                    }
                    let categorySubStyleServer: FB_IMAGES[] = []
                    this.fbc_images = res  
                    this.postsArray = res;
                    this.fbc_modify_datetime = this.postsArray[this.postsArray.length - 1].fbc_modify_datetime
                    this.last_fbci_id = this.postsArray[this.postsArray.length - 1].fbci_id

                    
                    this.noPostsFound = false
                 
                  
                  // this.getbrandDetails();

                
                  } else {
                    
                    this.getFavImgFromServer();
                  
                  }   
                }, error => {
                })
              }
            })
  }
 

  makeFavoriteTapped(collection, index:number) {
    
    if(!this.isLoginSignUp){
      this.navCtrl.push(LoginSignUpPage, { animate: false })
      return
    }

    // if (!this.myCustomer) {
    //   this.navCtrl.push(LoginSignUpPage, { animate: false })
    //   return
    // }


    let itemBackUp=collection;
        this.setFavToDbAndServer(itemBackUp)
         
    let mycollection: FB_IMAGES = collection
    let makeSalonFavorite: number
    if (mycollection.fav_id !== 0  ) {
      this.postsArray[index].fav_id = 0
    } else {
      this.postsArray[index].fav_id = collection.fbci_id
    }
    
    
    
  
   // return  this.fbc_images[index].fav_id = collection.fbci_id
    // if (this.salonCardOptions.calledFrom === config.FavoritePage) {
    //   this.allSalons.splice(this.allSalons.indexOf(salon), 1);
    // }
    // let params = {
    //   service: btoa('favorite'),
    //   sal_id: btoa(mySalon.sal_id),
    //   cust_id: btoa(this.myCustomer.cust_id),
    //   favorite: btoa(makeSalonFavorite.toString()),

    // }
    // this.serviceManager.getData(params).subscribe(res => {
    //   if (res['status'] === '1') {

    //     this.salonModal.getDatabaseState().subscribe(ready => {
    //       if (ready) {
    //         this.salonModal.makeSalonFavorite(mySalon.sal_id, makeSalonFavorite).then(isSalonMarkedFavorite => {
    //           if (isSalonMarkedFavorite) {

    //             // this.salonModal.getSalonDetailsFrom().then()
    //           }
    //         })
    //       }
    //     })
    //   } else {
    //     this.allSalons.forEach(salon => {
    //       if (salon.sal_id === mySalon.sal_id) {
    //         return salon.favsal = !makeSalonFavorite
    //       }
    //     });
    //   }

    // })
  }
  setFavToServer(item, status){
  
    const params = {
      service: btoa('favorites'),
      fav_id: btoa(item.fav_id),
      cft_id: btoa('2'),
      cust_id: btoa(this.myCustomer.cust_id),
      favorite:btoa(status),
    }
    console.log('params'+JSON.stringify(params))
   // if (this.trendCall === 1) this.SM.showProgress()
    this.SM.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          console.log('Response'+JSON.stringify(res))
        },
        (error) => {
          this.SM.stopProgress()
          this.SM.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

      
        })
  }
  setFavToDbAndServer(collection){
       console.log('Items',JSON.stringify(collection))
      let item={
        fav_id:collection.fbci_id,
        cft_id:2,
        fbci_id:collection.fbci_id,
        fbci_name:collection.fbci_name,
        fbc_id:collection.fbc_id,
        fbc_name:collection.fbc_name,
        fb_name:collection.fb_name
      }

      if(collection.fav_id == 0){
       
        this.setFavToDb(item)
        this.setFavToServer(item, 1)
      }else{
        
       this.removeFavFromDb(item)
       this.setFavToServer(item, 0)
      }
  }
  removeFavFromDb(item){
    this.favModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.favModal.deleteFavorite(item.fav_id).then(isSalonMarkedFavorite => {
          if (isSalonMarkedFavorite) {
            this.removeFromFavFashion(item);
          }
        })
      }
    })

  }
  setFavToDb(item){
    
    this.favModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.favModal.InsertInToFavoriteTable(item).then(isSalonMarkedFavorite => {
          if (isSalonMarkedFavorite) {
            this.setFavFashions(item)
            // this.salonModal.getSalonDetailsFrom().then()
          }
        })
      }
    })
  }

  removeFromFavFashion(item){
    this.fashionBrandModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        
        this.fashionBrandModal.deleteFavFashion(item.fav_id).then(isSalonMarkedFavorite => {
          if (isSalonMarkedFavorite) {
            // this.salonModal.getSalonDetailsFrom().then()
          }
        })
      }
    })
  }
  setFavFashions(item){
    this.fashionBrandModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.fashionBrandModal.InsertInToFavFashionTable(item).then(isSalonMarkedFavorite => {
          if (isSalonMarkedFavorite) {  //  alert('saved-fashion')
            // this.salonModal.getSalonDetailsFrom().then()
          }
        })
      }
    })
  } 
  

  goToCollection(item) {
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(LatestFashionCollectionDetailPage, {
      fashionTrends: item,
      gender:this.gender,
      collectionName:item.fbc_name,
    });
  }
  getFavImgFromServer() {
    if (!this.myCustomer) {
     
      return 
    }
    this.SM.showProgress()
    const params = {
      service: btoa('get_favorites'),
      cft_id: btoa('2'),
      cust_id: btoa(this.myCustomer.cust_id)
    }
    console.log('Params',JSON.stringify(params))
    this.SM.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          console.log('responseFav'+JSON.stringify(res))
        
         this.SM.stopProgress()
    

      // if(  res.data.fashionBrandDetails != undefined && res.data.fashionBrandDetails.length > 0)
      // {
      //   this.fashionBrandModal.getDatabaseState().subscribe(ready => {
      //     if (ready) {
      //        let fbc_images: FB_IMAGES[] = []
      //        fbc_images = res.data.fashionBrandDetails
      //        this.fashionBrandModal.saveIntoFB_IMAGES(fbc_images).then(categoryInsertedSuccessfully => {
      //          this.getPostFromDbOnServerResponce();
      //        })
      //     }
      //   })
      // }

        },
        (error) => {
          this.SM.stopProgress()
          this.SM.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

      
        })
  }

  deleteInactiveBrand(inactiveCollection){
    if(inactiveCollection && inactiveCollection.length>0){
      this.fashionBrandModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.fashionBrandModal.deleteMultipleFashionBrands(inactiveCollection).then(categoryInsertedSuccessfully => {
          })
        }
      })
    }
  }

  saveDateToDb(date){
    // this.fashionBrandModal.getDatabaseState().subscribe(ready => {
    //   if (ready) {
    //     let dateObject={
    //       fash_dates_id:this.fb_id,
    //       fbc_genders:this.gender,
    //       fbc_date:date,
    //       fbc_type:'brand'
    //     }
    
    //     this.fashionBrandModal.saveIntoFashDatesBrandTable(dateObject,date).then(categoryInsertedSuccessfully => {
          
    //     })

    //   }
    // })
  }

//share contesnt on differnt platforms
btnShareTapped(latestTrends , keyword){

  let loading = this.loadingController.create({ content: "Please Wait..." });
  loading.present();
  timer(1500).subscribe(() => {
    loading.dismiss()
  })
  // console.log(JSON.stringify(latestTrends));

  
  
    let productSshareURL = config.shareLatestTrendsURL + latestTrends.fb_id + '/' + keyword.replace(' ', '-')
    this.socialSharing.share('This is ' + this.myCustomer.cust_name + '.  I am using BeautyApp.pk on my phone and seen something interesting for you.  Please check the attached.\n' + productSshareURL, 'image',this.imageUrl + latestTrends.fbci_name).then(() => {
    }).catch(er => {

    })
  // this.socialSharing.share(keyword, 'image', url).then(() => {

  // }).catch(er => {

  // })
 }
 wentToLoadMore=false;
  loadMoreProducts(infiniteScroll) {


    if (this.wentToLoadMore || this.fbc_modify_datetime === -1) {
      if (infiniteScroll) {
        try {
          infiniteScroll.complete();
        } catch (ex) {
        }
      }
      return
    }
    this.infiniteScroll = infiniteScroll
    this.getPostFromDb();



   
  }
  getPostFromDb(){
    this.fashionBrandModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.fbc_images=[]
        let serviceCat = this.navParams.get('fashionTrends');
        this.fashionBrandModal.getFBC_Imag_Favourite('1').then(res => {
          this.updatePageData(res)
        }, error => {
        })
      }
    })
  }
  updatePageData(res) {
    this.postsArray =  this.postsArray.concat(res)
    this.infiniteScroll.complete()
    if (res && res.length > 0) {
     
      this.noPostsFound = false
      this.fbc_modify_datetime = this.postsArray[this.postsArray.length - 1].fbc_modify_datetime
      this.last_fbci_id = this.postsArray[this.postsArray.length - 1].fbci_id
    } else {
      this.noPostsFound = true
    } 
  
    if (res && res.length < 8) {
      this.infiniteScroll.enable(false)
    }
    timer(4000).subscribe(() => {
      this.wentToLoadMore = false
  
    })
  
  
  }
  
  getPostFromDbOnServerResponce(){
    this.fashionBrandModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.fbc_images=[]
        let serviceCat = this.navParams.get('fashionTrends');
        this.fashionBrandModal.getFBC_Imag_Favourite('1').then(res => {
          this.updatePageDataOnResponce(res)
        }, error => {
        })
      }
    }) 
  }

  updatePageDataOnResponce(res) {
     ///
     this.postsArray =  this.postsArray.concat(res)
     if (res && res.length > 0) {
      
       this.noPostsFound = false
       this.fbc_modify_datetime = this.postsArray[this.postsArray.length - 1].fbc_modify_datetime
       this.last_fbci_id = this.postsArray[this.postsArray.length - 1].fbci_id
     } else {
       this.noPostsFound = true
     } 
  }



 btnFavTapped(latestTrends , keyword){
  
 }
btnShareFacebookTapped() {
  this.socialSharing.shareViaFacebook('message', 'image', 'url').then(() => {

  }).catch(er => {

  })
}
btnShareWhatsAppTapped() {
  this.socialSharing.shareViaWhatsApp('message', 'image', 'url').then(() => {

  }).catch(er => {

  })
}
btnShareTwitterTapped() {
  this.socialSharing.shareViaTwitter('message', 'image', 'url').then(() => {

  }).catch(er => {
    
  })
}
  //below all is static code
  btnBackTapped() {
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
  }

   MainCtrl($scope, $ionicScrollDelegate) {
    
    $scope.scrollMainToTop = function () {
      $ionicScrollDelegate.$getByHandle('mainScroll').scrollTop();
    };
    $scope.scrollSmallToTop = function () {
      $ionicScrollDelegate.$getByHandle('small').scrollTop();
    };
  }
  ionScrollChange(scrollTop) { 
    this.zone.run(() => {
      this.ionScrollTop = scrollTop
    });
 }
  //no need gender base selection for now
  menTrendsTapepd() {

  }
  womenTrendsTapepd() {
    this.slider = document.getElementById('slider_ld');
    this.slider.style.cssFloat = 'right';
  }

}
