import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InActiveSalonPage } from './in-active-salon';

@NgModule({
  declarations: [
    InActiveSalonPage,
  ],
  imports: [
    IonicPageModule.forChild(InActiveSalonPage),
  ],
})
export class InActiveSalonPageModule {}
