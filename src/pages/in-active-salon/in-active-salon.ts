import { Component } from '@angular/core';
import { DatePipe } from '@angular/common';
import { App, IonicPage,LoadingController, NavController, NavParams, Platform } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import {GlobalServiceProvider}  from '../../providers/global-service/global-service'
import { GlobalProvider } from '../../providers/global/global';


@IonicPage()
@Component({
  selector: 'page-in-active-salon',
  templateUrl: 'in-active-salon.html',
})
export class InActiveSalonPage {


  public showPicker = false;
  today = new Date();
  timeCols = Array();
  selectedDate = new Date();
  servicessss = "";
  selectedTime = ""
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http,
    public  globalSearch:   GlobalServiceProvider,
    public serviceManager: GlobalProvider,
    public loadingController: LoadingController,
  ) {
    this.servicessss = "";
    this.timeCols = [
      {
        name: 'col1',
        options: [
          { text: 'Morning', value: '1' },
          { text: 'Afternoon', value: '2' },
          { text: 'Evening', value: '3' }
        ]
      }
    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InActiveSalonPage');
  }

  setDate(event){
    console.log(event.constructor.name);
    this.selectedDate = event;
  }

  sendFeedback(){
    if (this.servicessss == undefined || this.servicessss.length == 0){
      this.serviceManager.makeToastOnFailure('Please enter desired services.')
      
      return;
    }

    if (this.selectedDate == undefined) {
      this.serviceManager.makeToastOnFailure('Please enter valid date.')
      
      return;
    }

    if (this.selectedTime == undefined || this.selectedTime.length == 0){
      this.serviceManager.makeToastOnFailure('Please enter valid date.')
      return;
    }

    ///userId/services/date/time/salId
    let tagURL = 'https://www.laravel.nopso.qwpcorp.com/InstaApi/public/api/InactiveSalon/'+123+"/"+this.servicessss+"/"+this.selectedDate+"/"+this.timeCols[this.selectedTime]+"/1";
    
    let loading = this.loadingController.create({ content: "Please Wait..." });
    loading.present();

    
    this.serviceManager.getDataFromLaravelService(tagURL)
    .subscribe((response) => 
    { 
      loading.dismissAll();
      this.navCtrl.pop()
      this.servicessss = ""
      this.selectedTime = ""
      this.selectedDate = new Date()
      this.serviceManager.makeToastOnFailure('Data sent successfully.');
    },
    error => {
     loading.dismiss();
      this.serviceManager.makeToastOnFailure(error)
     console.log('something went wrong', error);
    }
   );
  }

  goBack(){
    this.navCtrl.pop();
  }

}
