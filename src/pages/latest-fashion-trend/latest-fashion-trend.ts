import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { SalonServicesModal } from '../../providers/salon-services-modal/salon-services-modal';
import { FASHION_BRANDS, FASHION_BRANDS_COLLECTION, FASHION_COLLECTION } from './../../providers/SalonAppUser-Interface';
import { MenuController } from 'ionic-angular';
import { FashionBrandModal } from '../../providers/FashionBrandModal/FashionBrandModal';
import { FavoriteModal } from '../../providers/FavoriteModal/FavoriteModal';
import { GlobalProvider } from '../../providers/global/global';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalServiceProvider } from '../../providers/global-service/global-service'

import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db'
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { IonicPage, NavController, NavParams, Navbar, Content, Events, LoadingController, PopoverController, Platform } from 'ionic-angular';
import { Component, ViewChild, NgZone } from '@angular/core';
import { BEAUTY_TIPS, TIPS_DETAIL, NOPSO_Header_OPTIONS, PRODUCT } from '../../providers/SalonAppUser-Interface';
import { config } from '../../providers/config/config';
import { MainHomePage } from '../main-home/main-home';
import { LatestFashionBrandDetailsPage } from '../latest-fashion-brand-details/latest-fashion-brand-details'
import { LatestTrendsDetailPage } from '../latest-trends-detail/latest-trends-detail';

import { LatestFashionCollectionDetailPage } from '../latest-fashion-collection-detail/latest-fashion-collection-detail';

import { FavouriteFashionsTrendsPage } from '../favourite-fashions-trends/favourite-fashions-trends';



import { LatestTrendsArchiveCollectionPage } from '../latest-trends-archive-collection/latest-trends-archive-collection';

import { SALON_SERVICES_SUB_CATEGORIES, LATEST_TRENDS_STYLES, SalonServices, Salon, SALON_SERVICES_SEARCH, ServiceSubCATEGORIES, Customer, Homepage, CATEGORY_SUB_STYLE, Category } from '../../providers/SalonAppUser-Interface';
import { CartModal } from '../../providers/cartModal/cartModal';
import { ViewCartPage } from '../view-cart/view-cart';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
/**
 * Generated class for the BeautyTipsFashionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-latest-fashion-trend',
  templateUrl: 'latest-fashion-trend.html',
})
export class LatestFashionTrendPage {
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  
  public loadingData=false
  public imageUrl = config.FashionImageURL
  public FashionStyFeatureItem = null;
  public FashionStyFeatureItemMen = null;
  public FashionStyFeatureItemWomen = null;
  public unregisterBackButtonAction: any;
  public LatestTrendsStyles = Array();
  public LatestTrendsStylesMale = Array();
  public LatestTrendsStylesFemale = Array();

  public LatestCollectionMale = Array();
  public LatesCollectionFemale = Array();

  public LatestTrendsSubStyles = Array();
  public LatestFashionCollection= Array();
  public StylesBackup = Array();
  public myCustomer: Customer
  public cateogry = ""
  public defaultImage = 'default.png'
  public shouldShowMenTrends: boolean
  public shouldShowWomenTrends: boolean
  public GenderBasedTrends: string
  public parentPage: Homepage
  public parentPageDescription
  public SALON_SERVICE_SEARCH: SALON_SERVICES_SEARCH[]
  public ServiceSubCATEGORIE: ServiceSubCATEGORIES[]
  public selectedStyle: Category
  public noStyleFound = false
  public HeaderHeightIsZero = false
  public isAppLaunchedFirstTime = 'isAppLaunchedFirstTime'
  public archiveMenFashionNames = ""
  public archiveWoemFashionNames = ""
  public archiveFashionName = null
  public isFirstTime = false
  public shouldShowMiniHeader = false
  public isFavourites=false
  public twoBeautyTips: any = [];
  public slider: HTMLElement
  public loading: any
  public nopsoHeaderOptions: NOPSO_Header_OPTIONS
  public CartItems: PRODUCT[] = []
  isBrands = true;
  customerGender: string='male'
  @ViewChild(Content) content: Content;
  @ViewChild(Navbar) navBar: Navbar;
  ionScrollTop = 0
  constructor(
    private menu: MenuController,
    public globalService: GlobalServiceProvider,
    public favModel: FavoriteModal,
    private zone: NgZone,
    public http: Http,
    public sqlProvider: SqliteDbProvider,
    public salonServicesModal: SalonServicesModal,
    public cartModal: CartModal,
    public fashionBrandModal: FashionBrandModal,
    public globalSearch: GlobalServiceProvider,
    
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public serviceManager: GlobalProvider,
    private nativePageTransitions: NativePageTransitions,
    public customerModal: CustomerModal,
    public ga: GoogleAnalytics,
    public popoverCtrl: PopoverController,
    public platform: Platform,
    public events: Events, ) {
    this.nopsoHeaderOptions = {
      miniHeaderHeight: '50pt',
      calledFrom: config.BeautyTipsViewPage,
    }
   
   
    this.LatestTrendsStyles = []
    this.isFirstTime = true;

  }
  //S1 app life cycle events.
  ionViewWillEnter() {
    this.checkIfFavouriteExist();
       // this.scrollToTop();
    this.getCustomer()
    this.initializeBackButtonCustomHandler();
    //this.getFeatureItemFromDB()
    this.cartModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.cartModal.getCartItems().then(cartItems => {
          this.CartItems = cartItems
        })
      }
    })
  }
  GenderTapped() {
    
    this.isBrands = !this.isBrands
  }
  brandsTapped() {
    let options: NativeTransitionOptions = {
      direction: 'top',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 150,
      androiddelay: 0,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
      };
       this.nativePageTransitions.fade(options);
   // this.myCustomer['cust_gender'] = '1'
    this.isBrands = true
  }
  collectionsTapped() {
    let options: NativeTransitionOptions = {
      direction: 'top',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 150,
      androiddelay: 0,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
      };
       this.nativePageTransitions.fade(options);
    this.isBrands = false
    //this.myCustomer['cust_gender'] = '0'
  }
  //custom back button for android
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    if(this.menu.isOpen()){
      this.menu.close();
    }else {
       if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
    }
    // this.navCtrl.pop();
  }
  //end custom back button for android
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
    let abc = false
    this.events.subscribe('canShowMiniHeader', (isHidden) => {
      this.zone.run(() => {
        this.shouldShowMiniHeader = isHidden
      });
    });
    //page refresh on comming back from detail page
    if (!this.isFirstTime) {
      //this.scrollToTop();
     
      let options: NativeTransitionOptions = {
        direction: 'top',
        duration: 500,
        slowdownfactor: 3,
        slidePixels: 20,
        iosdelay: 150,
        androiddelay: 0,
        fixedPixelsTop: 0,
        fixedPixelsBottom: 0
      };
      this.nativePageTransitions.fade(options);
     // this.navCtrl.setRoot(LatestFashionTrendPage, {}, { animate: false });

      //this.navCtrl.setRoot(this.navCtrl.getActive().component);
    } else {
      
      this.isFirstTime = false
    }
    //End page refresh on comming back from detail page


  }
  ionViewDidLoad() {
    // this.refreshPage();
    this.content.ionScroll.subscribe(ev =>
      requestAnimationFrame(() => this.ionScrollChange(ev.scrollTop)));
  }
  //S2 server side requests.
  //S3 local db requests.
  getCustomer() {
    if (this.myCustomer && this.LatestTrendsStyles && this.LatestTrendsStyles.length > 1) {
      return
    }
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(customer => {

          if (customer) {
            this.myCustomer = customer
            this.googleAnalytics(this.myCustomer.cust_id, this.myCustomer.cust_name)
          }else{
            let publicUserId = this.serviceManager.getFromLocalStorage(this.serviceManager.PUBLIC_USER_ID)
            if(publicUserId){
              this.googleAnalytics(publicUserId, 'guest user')
            }
          } 
          if (!this.LatestTrendsStyles || this.LatestTrendsStyles.length === 0) {
            this.getFashionBrandsFromDB();
          }else{
            console.log('NotGoingToFetchBrands'+ this.LatestTrendsStyles.length)
          }
          if (!this.LatestFashionCollection || this.LatestFashionCollection.length === 0) {
            this.getFashionCollectionFromDB();
          }else{
            console.log('NotGoingToFetchCollection'+this.LatestFashionCollection.length )
          }
          
        }, error => {
          
        })
      }
    })
  }


  getFashionCollectionFromDB() {
    this.fashionBrandModal.getDatabaseState().subscribe(ready => {
      if (ready) {
          this.fashionBrandModal.getFashionCollection().then(fasionCollections => {  
            // alert('fasionCollections'+fasionCollections.length)
            if(!fasionCollections || fasionCollections.length < 1){
              if(!this.loadingData){
                this.reRequestToFetchFashionTrends();
              }
             
            }


            fasionCollections.forEach(category => {
              if (category.fc_gender == 2 || category.fc_gender == 3) { //gender base sorting
                
                  this.LatesCollectionFemale.push(category)
                 
                
              }
              if (category.fc_gender == 1 || category.fc_gender == 3)  {//for male rest is same 
              
                  
                  this.LatestCollectionMale.push(category)
                 
                
              }
            });
            let gender=null
            if(this.myCustomer){
              gender = Number(this.myCustomer.cust_gender)
            }
             
            if (gender && gender === 1) {
              this.GenderBasedTrends = 'man'
              this.LatestFashionCollection = []
              this.LatestFashionCollection = this.LatestCollectionMale
              this.menTrendsTapepd()
            } else {
              this.GenderBasedTrends = 'woman'
              this.LatestFashionCollection = []
              this.LatestFashionCollection = this.LatesCollectionFemale
              this.womenTrendsTapepd()
            }

           })
         }
      })
   }
  getFashionBrandsFromDB() {
    this.fashionBrandModal.getDatabaseState().subscribe(ready => {
      if (ready) {
          this.fashionBrandModal.getFashionBrands().then(serviceCategories => {
            if(!serviceCategories || serviceCategories.length< 1){
              if(!this.loadingData){
                this.reRequestToFetchFashionTrends();
              }
             
            }
          
            serviceCategories.forEach(category => {
              if (category.fb_gender == 2 || category.fb_gender == 3) { //gender base sorting
                if (!this.FashionStyFeatureItemWomen) { //first item should be feature
                  this.FashionStyFeatureItemWomen = category
                } else {
                  this.LatestTrendsStylesFemale.push(category)
                  // if (category.fb_gender == 2) {
                  //   this.LatestTrendsStylesFemale.push(category)
                  // } else {
                  //   this.LatestTrendsStylesFemale.push(category)
                  // }
                }
              }
              if (category.fb_gender == 1 || category.fb_gender == 3)  {//for male rest is same 
                if (!this.FashionStyFeatureItemMen) {
                  this.FashionStyFeatureItemMen = category
                } else {
                  //get archive categories too here.
                  this.LatestTrendsStylesMale.push(category)
                  // if (category.fb_gender == 2) {
                  //   this.LatestTrendsStylesMale.push(category)
                  // } else {
                  //   this.LatestTrendsStylesMale.push(category)
                  // }
                }
              }
            });
            let gender=null
            if(this.myCustomer){
              gender = Number(this.myCustomer.cust_gender)
            }
            //let gender = Number(this.myCustomer.cust_gender)
            if (gender && gender === 1) {
              this.GenderBasedTrends = 'man'
              this.LatestTrendsStyles = []
              this.LatestTrendsStyles = this.LatestTrendsStylesMale
              this.menTrendsTapepd()
            } else {
              this.GenderBasedTrends = 'woman'
              this.LatestTrendsStyles = []
              this.LatestTrendsStyles = this.LatestTrendsStylesFemale
              this.womenTrendsTapepd()
            }
          })
      }
    })
  }
  //S4 click events.

  fashionClicked() {
    let genderStatus=this.shouldShowMenTrends
    let gender=0
    if(genderStatus){
      gender=1
    }else{
      gender=2
    }

    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(LatestFashionBrandDetailsPage, {
      fashionTrends: this.FashionStyFeatureItem,
      gender:gender,
      brandName:this.FashionStyFeatureItem.fb_name,     
    });
  }
  favCategoryTapped(item, index) {
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(FavouriteFashionsTrendsPage);
  }

  categoryClicked(item, index) {
    let genderStatus=this.shouldShowMenTrends
    let gender=0
    if(genderStatus){
      gender=1
    }else{
      gender=2
    }

    LatestFashionCollectionDetailPage
    if(this.isBrands){
      
      
      this.nativePageTransitions.slide(this.options)
      this.navCtrl.push(LatestFashionBrandDetailsPage, {
        fashionTrends: item,
        gender:gender,
        brandName:item.fb_name,
      });
    }else{
     
     
      this.nativePageTransitions.slide(this.options)
      this.navCtrl.push(LatestFashionCollectionDetailPage, {
        fashionTrends: item,
        gender:gender,
        collectionName:item.fc_title
      });
    }
    
    //if (selectedSubStyles) {
    //this.serviceManager.setInLocalStorage(this.serviceManager.OBJ_SUB_STYLE, selectedSubStyles)
    // }
  }

  archiveCategoryClicked() {
    let selectedGender;
    if (this.shouldShowMenTrends) {
      selectedGender = 1
    } else {
      selectedGender = 2
    }


    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(

      LatestTrendsArchiveCollectionPage, {
        selectedGender: selectedGender
      });


  }
  menTrendsTapepd() {
    this.slider = document.getElementById('slider_lt');
    this.slider.style.cssFloat = 'left';
    this.LatestTrendsStyles = []
    this.LatestTrendsStyles = this.LatestTrendsStylesMale;
    this.FashionStyFeatureItem = null
    this.FashionStyFeatureItem = this.FashionStyFeatureItemMen
    //for collection
    this.LatestFashionCollection = []
    this.LatestFashionCollection = this.LatestCollectionMale
    //clear archive
    this.archiveFashionName = null
    this.archiveFashionName = this.archiveMenFashionNames
    !this.LatestTrendsStyles || this.LatestTrendsStyles.length === 0 ? this.noStyleFound = true : this.noStyleFound = false
    this.shouldShowMenTrends = true
    this.shouldShowWomenTrends = false
  }
  womenTrendsTapepd() {
    this.slider = document.getElementById('slider_lt');
    this.slider.style.cssFloat = 'right';
    this.LatestTrendsStyles = []
    this.LatestTrendsStyles = this.LatestTrendsStylesFemale;
    this.FashionStyFeatureItem = null
    this.FashionStyFeatureItem = this.FashionStyFeatureItemWomen
    
    //for collection
    this.LatestFashionCollection = []
    this.LatestFashionCollection = this.LatesCollectionFemale
    //clear archive
    this.archiveFashionName = null
    this.archiveFashionName = this.archiveWoemFashionNames

    !this.LatestTrendsStyles || this.LatestTrendsStyles.length === 0 ? this.noStyleFound = true : this.noStyleFound = false
    this.shouldShowMenTrends = false
    this.shouldShowWomenTrends = true
    
  }
  btnBackTapped() {
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
  }
  

  //get fashion-trends if not found
  reRequestToFetchFashionTrends() {
    this.loading = this.loadingController.create({ content: "Fetching fashion trends, Please Wait..." });
    this.loading.present();
    this.loadingData=true;
   let lds='5';//for fetching fashion trends
   let LAST_UPDATE_DATE_TIME=''//its emty to fetch all data
    var params = {
      service: btoa('local_data_single'),
      device_datetime: btoa(this.serviceManager.getCurrentDateTime()),
      lds_id: btoa(lds),
      sal_last_fetched: btoa(LAST_UPDATE_DATE_TIME),
    
    }
    console.log('Param',JSON.stringify(params))
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((response) => {
        // this.loadingData=false
        this.loading.dismissAll();
         console.log('Results',JSON.stringify(response))
        // this.Response_Salons_HomePage_SalServiceSabCat(response, response.lds_ids);
        if(response){
          this.handle_FashionCollection_Response(response.FashCollections)
          this.handle_FashionBrands_Response(response.data.FashionBrands, response.data.inactive_fash_brand_collections, response.data.inactive_fash_brands)
        }
      },
        error => {
          // this.loadingData=false
          this.loading.dismissAll();
          //this.loading.dismiss();
          
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
        }
      );
  }

  handle_FashionCollection_Response(FashCollections) {
    if(FashCollections && FashCollections!=undefined && FashCollections.length > 0){
    let FASHIONBRANDSCOLLECTIONARRAY: FASHION_COLLECTION[] = []
    FASHIONBRANDSCOLLECTIONARRAY = FashCollections
    console.log('COllection', JSON.stringify(FashCollections))
    this.fashionBrandModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.fashionBrandModal.saveIntoFashCollectionTable(FASHIONBRANDSCOLLECTIONARRAY).then(categoryInsertedSuccessfully => {
          
          if (!this.LatestFashionCollection || this.LatestFashionCollection.length === 0) {
            this.getFashionCollectionFromDB();
          }else{
            console.log('NotGoingToFetchCollection'+this.LatestFashionCollection.length )
          }
        })
      }
    })
  }
  }

  getFavImgFromServer() {
    if (!this.myCustomer) {
      // // alert('customer ni mila')
      return 
    }
    const params = {
      service: btoa('get_favorites'),
      cft_id: btoa('2'),
      cust_id: btoa(this.myCustomer.cust_id)
    }
    console.log('Params',JSON.stringify(params))
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          console.log('responseFav'+JSON.stringify(res))
          if(res.favorites && res.favorites.length > 0){
            this.setFavFashions(res.favorites)
          }
          
        },
        (error) => {
        })
  }

  setFavFashions(item){
    this.fashionBrandModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.fashionBrandModal.InsertInMultToFavFashionTable(item).then(isSalonMarkedFavorite => {
          if (isSalonMarkedFavorite) {
           
          
          }
        })
      }
    })
  }

  chekIfFavImgExist(){
    this.fashionBrandModal.getDatabaseState().subscribe(ready => {
      if (ready) {
          this.fashionBrandModal.checkIfFavFashImg().then(res => {  
            if(!res){
              this.getFavImgFromServer(); 
           }
        }, error => {
        })
      }
    })
  }
  checkIfFavouriteExist(){
  
    this.favModel.getDatabaseState().subscribe(ready => {
      if (ready) {
          this.favModel.checkIfFavFashion('2').then(res => { 
          
            if(res){
              this.chekIfFavImgExist();//for temprarily    
              this.isFavourites=true
           }else{
            this.chekIfFavImgExist();
            this.isFavourites=false
           } 
        }, error => {
        })
      }
    })
  }
  handle_FashionBrands_Response(FashionBrands, inactive_fash_brand_collections, inactive_fash_brands) {
    if (FashionBrands && FashionBrands != undefined  && FashionBrands.length > 0) {
      if (FashionBrands.length > 0) {
        let fashionBrands: FASHION_BRANDS[] = []
        fashionBrands = FashionBrands
        let FASHIONBRANDSCOLLECTIONARRAY: FASHION_BRANDS_COLLECTION[] = []
        fashionBrands.forEach(fashionBrands => {
          FASHIONBRANDSCOLLECTIONARRAY = FASHIONBRANDSCOLLECTIONARRAY.concat(fashionBrands.FashionBrandCollections)
        });
        this.sqlProvider.getDatabaseState().subscribe(ready => {
          if (ready) {
            if (inactive_fash_brand_collections !== undefined && inactive_fash_brand_collections !== null) {
              this.sqlProvider.deleteSearchKeyWordsBySerCatId(inactive_fash_brand_collections, 'key_word').then(isDataInserted => {
              })
            }
          }
        })
        this.fashionBrandModal.getDatabaseState().subscribe(ready => {
          if (ready) {
            if (inactive_fash_brand_collections !== undefined && inactive_fash_brand_collections !== null) {
              this.fashionBrandModal.deleteMultipleFashionBrandsCollection(inactive_fash_brand_collections).then(isDeleted => {
              })
            }
            if (inactive_fash_brands !== undefined && inactive_fash_brands !== null) {
              this.fashionBrandModal.deleteMultipleFashionBrands(inactive_fash_brands).then(isDataInserted => {
              })
            }

            this.fashionBrandModal.saveIntoFashBrandsTable(FashionBrands).then(categoryInsertedSuccessfully => {
              this.fashionBrandModal.saveIntoFashBranCollectionsTable(FASHIONBRANDSCOLLECTIONARRAY).then(isSubCategoriesSsaved => {
                if (!this.LatestTrendsStyles || this.LatestTrendsStyles.length === 0) {
                  this.getFashionBrandsFromDB();
                }else{
                  console.log('NotGoingToFetchBrands'+ this.LatestTrendsStyles.length)
                }
              })
            })
          }
        })
      } else {
      }
    }
  }

 

  refreshPage(){
     //to resolve page ios app
     if(this.globalService.isFirstlaunch){
      this.globalService.isFirstlaunch=false
     this.navCtrl.setRoot(LatestFashionTrendPage)
     return
   }else {
     this.globalService.isFirstlaunch=true
   }
  }

  //S5 static code scroll etc.
  scrollToTop() {
    this.content.scrollToTop();
    let options: NativeTransitionOptions = {
      direction: 'top',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 150,
      androiddelay: 0,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };
    this.nativePageTransitions.fade(options);
  }
  btnViewCartTapped() {
    if (!this.CartItems || this.CartItems.length === 0) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgNoProductInCart)
      return
    }
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(ViewCartPage, {
      CartItems: this.CartItems,
      // selectedProduct: this.selectedProductCategory
    })
  }
  googleAnalytics(cust_id, cust_name) {
    this.ga.startTrackerWithId(config.GoogleAnalyticsAppID)
      .then(() => {
        this.ga.trackView(cust_name + ' on latest-trends screen');
        this.ga.trackEvent('cat: ionViewWill Enter', 'act: loads everytime', 'lab: general label', 200, true)
        this.ga.trackTiming('cat: tracking timing', 600000, 'variable: not sure what will go here', 'label: and the same')
        this.ga.debugMode();
        this.ga.setAllowIDFACollection(true);
        this.ga.setUserId(cust_id)
        // Tracker is ready
        // You can now track pages or set additional information such as AppVersion or UserId
      })
      .catch(e => {
       
      });
  }
  MainCtrl($scope, $ionicScrollDelegate) {


    $scope.scrollMainToTop = function () {
      $ionicScrollDelegate.$getByHandle('mainScroll').scrollTop();
    };
    $scope.scrollSmallToTop = function () {
      $ionicScrollDelegate.$getByHandle('small').scrollTop();
    };
  }
  ionScrollChange(scrollTop) {
    this.zone.run(() => {
      this.ionScrollTop = scrollTop
    });
  }
  scrollEvent() {
  }
}

