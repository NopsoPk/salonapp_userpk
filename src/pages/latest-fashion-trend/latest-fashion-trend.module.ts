import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LatestFashionTrendPage } from './latest-fashion-trend';

@NgModule({
  declarations: [
    LatestFashionTrendPage,
  ],
  imports: [
    IonicPageModule.forChild(LatestFashionTrendPage),
  ],
})
export class LatestFashionTrendPageModule {}
