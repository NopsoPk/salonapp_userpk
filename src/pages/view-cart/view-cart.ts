import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, ModalController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { MainHomePage } from '../main-home/main-home';
import { LoginSignUpPage } from '../login-sign-up/login-sign-up';
import { GlobalProvider } from '../../providers/global/global';
import { PRODUCT, SHIPPING_ADDRESS_DATA, Customer, PROMOTIONS, APP_CONFIG } from '../../providers/SalonAppUser-Interface';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { MenuController } from 'ionic-angular';
import { config } from '../../providers/config/config';
import { ProductModal } from '../../providers/ProductModal/ProductModal';
import { Slides } from 'ionic-angular';

import { ReviewOrderPage } from '../review-order/review-order';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { CartModal } from '../../providers/cartModal/cartModal';
import { ProductdetailPage } from '../productdetail/productdetail';
import { GlobalServiceProvider } from '../../providers/global-service/global-service';
import { ProductsPage } from '../products/products';
import { ShippingAddressModal } from '../../providers/ShippingAddressModal/ShippingAddressModal';
import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { PromotionModal } from '../../providers/PromotionModal/PromotionModal';
import { ConfigModal } from '../../providers/ConfigModal/ConfigModal';
import { PromotionDetailPage } from '../promotion-detail/promotion-detail';





@IonicPage()
@Component({
  selector: 'page-view-cart',
  templateUrl: 'view-cart.html',
})
export class ViewCartPage {
  @ViewChild(Content) content: Content;
  selectOptions = {
    title: 'Pizza Toppings',
    subTitle: 'Select your toppings',
    mode: 'md'
  };
  @ViewChild(Slides) slides: Slides;

  dropDownDataSource = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', "Delete"]
  pQuantity: any
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  public techSlots = [1, 2, 3, 4, 5]
  public productsImageURL = config.prouductImages
  // public objDelieveryAddress: SHIPPING_ADDRESS_DATA
  public CartItems: PRODUCT[] = []

  public productQuantity = 1
  public isAddedToCart = true
  public totalOrderPrice = 0.0
  public DelieverCharges = 50
  public GrandTotal = 0.0
  public numberOfItemAddedCart = 0.0;
  public reOrderProducts: PRODUCT[]
  public productIDsRemovedFromCart = ''
  public noProductAvailable = false
  public currencySymbol = ''
  public isFromReOrder = false
  public myCustomer: Customer

  ngModelUsePromotion: boolean = false
  ngModelUseCredit: boolean = false
  public promotions: PROMOTIONS[] = []
  public selectedPromotions: PROMOTIONS
  public selctedStoredCredit = 0

  public promotionResponse: any

  constructor(
    private menu: MenuController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public productModal: ProductModal,
    public cartModal: CartModal,
    private nativePageTransitions: NativePageTransitions,
    public serviceManager: GlobalProvider,
    public alertCtrl: AlertController,
    public gsp: GlobalServiceProvider,
    public addressBookModal: ShippingAddressModal,
    public customerModal: CustomerModal,
    public platform: Platform,
    public promotionModal: PromotionModal,
    public configModal: ConfigModal,
    public modalctrl: ModalController,
  ) {

  }

  public unregisterBackButtonAction: any;

  //custom back button for android
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler() {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);



    // this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
    //   this.navCtrl.canGoBack ? this.navCtrl.pop() : this.navCtrl.setRoot(MainHomePage)
    // }, 10);
  }
  customHandleBackButton() {
    if (this.menu.isOpen()) {
      this.menu.close();
    } else {

      if (this.navCtrl.canGoBack()) {

        this.navCtrl.pop();
      } else {

        this.navCtrl.setRoot(MainHomePage)
      }
    }
  }
  //end custom back button for android
  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewCartPage');
  }

  ionViewWillEnter() {
    this.getCurrencyDetail()
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(customer => {
          this.myCustomer = customer;
          if (this.myCustomer) {
            
            this.selctedStoredCredit = this.myCustomer.cust_store_credit
            this.fetchPromotionsData()
            this.getUserStatus()
            let abc = this.serviceManager.getFromLocalStorage(this.serviceManager.IS_MY_ORDER_REQUESTED_ALREADY_INITIATED)
            if (!abc) this.getMyOrdersForDelieveryAddress()
          }
        })
      }
    })

    let productIDs = this.navParams.get('productIDs')
    if (productIDs) {
      this.productModal.getProductsForReorder(productIDs).then(myProducts => {
        this.isFromReOrder = true
        this.CartItems = myProducts
        this.CartItems.forEach(product => {
          product.p_quantity = 1
          product.isChecked = true

        });


        this.GrandTotal = this.calculatePrice()
      })
      // this.reOrderProducts = productIDs
      // !this.CartItems || this.CartItems.length === 0 ? this.noProductAvailable = true : this.noProductAvailable = false
    } else {
      this.getCartItems()
    }



  }
  ionViewDidEnter() {

    this.getAppConfigFromServer()
    // this.promotionModal.getDatabaseState().subscribe(ready => {
    //   if (ready) {
    //     this.promotionModal.getPromotions().then(myPromotions => {
    //       this.promotions = myPromotions
    //       if (this.promotions && this.promotions.length > 0) this.selectedPromotions = this.promotions[0]
    //     })
    //   }
    // })
    this.initializeBackButtonCustomHandler();
  }
  slideChanged() {
    if (this.slides.isEnd()) {
      return
    }
    let currentIndex = this.slides.getActiveIndex();
    console.log('Current index is', currentIndex);
  }
  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };

    if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

    if (this.navCtrl.canGoBack()) {
      this.navCtrl.getPrevious().data.productIDsRemovedFromCart = this.productIDsRemovedFromCart

      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
  }


  getQuantity(quantity: any, index: number) {


    let selectedProduct = this.CartItems[index]
    if (quantity === 'Delete') {
      this.btnRemoveFromCartTapped(selectedProduct, index)
    } else {


      this.CartItems[index].p_quantity = Number(quantity)

      let _tempGrandTotal = this.calculatePrice()
      if (_tempGrandTotal > config.maxCartPrice) {
        this.CartItems[index].p_quantity = 1

        this.GrandTotal = this.calculatePrice()
        this.CartItems[index].shouldShowDropDown = false
        this.serviceManager.makeToastOnFailure(AppMessages.msgOrderLimit + config.maxCartPrice)
        return
      }

      if (this.reOrderProducts) {
        this.GrandTotal = this.calculatePrice()
      } else {
        this.GrandTotal = this.calculatePrice()
        this.cartModal.getDatabaseState().subscribe(ready => {
          if (ready) {
            this.cartModal.updateCartTable(selectedProduct)
          }
        })
      }
      this.CartItems[index].shouldShowDropDown = false
      return this.CartItems
    }
  }
  incrementQty(product: PRODUCT, index: number) {
    // console.log(((product.p_quantity+1)*product.p_price)+this.GrandTotal);

    // if(((product.p_quantity+1)*product.p_price)+this.GrandTotal > config.maxCartPrice) return 
    this.CartItems[index].p_quantity += 1
    this.CartItems[index].CalculatedPrice = Number(this.CartItems[index].p_quantity * this.CartItems[index].p_price)
    let _tempGrandTotal = this.calculatePrice()
    if (_tempGrandTotal > config.maxCartPrice) {
      this.CartItems[index].p_quantity -= 1
      this.CartItems[index].CalculatedPrice = Number(this.CartItems[index].p_quantity * this.CartItems[index].p_price)
      this.GrandTotal = this.calculatePrice()
      this.serviceManager.makeToastOnFailure(AppMessages.msgOrderLimit + config.maxCartPrice)
      return
    }

    if (this.reOrderProducts) {
      this.GrandTotal = this.calculatePrice()
    } else {

      this.GrandTotal = 0.0
      this.totalOrderPrice = 0.0
      this.numberOfItemAddedCart = 0
      this.CartItems.forEach(item => {
        if (item.isAddedtoCart) this.numberOfItemAddedCart += 1
        if (item.isAddedtoCart) this.totalOrderPrice += Number(item.p_price * item.p_quantity)

      });
      this.cartModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.cartModal.updateCartTable(product)
        }
      })
      this.GrandTotal = this.totalOrderPrice + this.DelieverCharges
    }

    return this.CartItems

  }
  DecrementQty(product: PRODUCT, index: number) {

    this.CartItems[index].p_quantity -= 1
    if (this.CartItems[index].p_quantity < 1) this.CartItems[index].p_quantity = 1
    if (this.reOrderProducts) {
      this.GrandTotal = this.calculatePrice()
    } else {

      this.GrandTotal = 0.0
      this.totalOrderPrice = 0.0
      this.numberOfItemAddedCart = 0
      this.CartItems.forEach(item => {
        if (item.isAddedtoCart) this.numberOfItemAddedCart += 1
        if (item.isAddedtoCart) this.totalOrderPrice += Number(item.p_price * item.p_quantity)

      });

      this.cartModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.cartModal.updateCartTable(product)
        }
      })
      this.GrandTotal = this.totalOrderPrice + this.DelieverCharges
    }

    return this.CartItems

  }

  btnCheckoutTapped() {
    let isLoginSignUp = this.serviceManager.getFromLocalStorage(this.serviceManager.IS_USER_LOGIN);
    if (!this.customerStatusOk) {
      if(!isLoginSignUp){//if customer not exist
        this.nativePageTransitions.slide(this.options)
        this.navCtrl.push(LoginSignUpPage, {//if customer exist but blocked
          comingFrom: 'ViewCartPage',
        });  
        return
      }else{
      this.serviceManager.makeToastOnFailure(AppMessages.customeriSBlocked)
      return
      } 
      
    }
    // this.serviceManager.makeToastOnSuccess('Checkout is coming soon.', 'top')
    let noItemToBuy = false
    this.CartItems.forEach(cartItem => {
      if (cartItem.isChecked) { noItemToBuy = true }
    });
    if (this.CartItems && this.CartItems.length > 0) {
      if (!noItemToBuy) {
        // return this.serviceManager.makeToastOnFailure('you have a product but is not marked to buy. Please mark it.', 'top')
      }
    }
    if (!this.CartItems || this.CartItems.length === 0 || !noItemToBuy) return this.serviceManager.makeToastOnFailure(AppMessages.msgNoProductInCart)
    this.nativePageTransitions.slide(this.options)


    // if (this.ngModelUsePromotion && this.ngModelUseCredit) {

    this.navCtrl.push(ReviewOrderPage, {
      CartItems: this.CartItems,
      orderSubTotal: this.totalOrderPrice,
      ngModelUsePromotion: this.ngModelUsePromotion,
      ngModelUseCredit: this.ngModelUseCredit,
      promotionResponse: this.promotionResponse,
      selectedPromotions: this.selectedPromotions,

    }).then(() => {
      this.CartItems.forEach(item => {
        this.productModal.updateProductTable(item)
      });

    })
    // }
    // else if (this.ngModelUsePromotion) {
    //   this.navCtrl.push(ReviewOrderPage, {
    //     CartItems: this.CartItems,
    //     orderSubTotal: this.totalOrderPrice,
    //     selectedPromotion: this.selectedPromotions,

    //   }).then(() => {
    //     this.CartItems.forEach(item => {
    //       this.productModal.updateProductTable(item)
    //     });

    //   })
    // } else if (this.ngModelUseCredit) {
    //   this.navCtrl.push(ReviewOrderPage, {
    //     CartItems: this.CartItems,
    //     orderSubTotal: this.totalOrderPrice,
    //     selectedCustomerStoreCredit: this.myCustomer.cust_store_credit



    //   }).then(() => {
    //     this.CartItems.forEach(item => {
    //       this.productModal.updateProductTable(item)
    //     });

    //   })
    // }


  }
  removeFromCart(product: PRODUCT, i: number) {
    this.CartItems[i].isAddedtoCart = !this.CartItems[i].isAddedtoCart

    this.GrandTotal = 0.0
    this.totalOrderPrice = 0.0
    this.numberOfItemAddedCart = 0
    this.CartItems.forEach(item => {
      if (item.isAddedtoCart) this.numberOfItemAddedCart += 1
      if (item.isAddedtoCart) this.totalOrderPrice += Number(item.p_price * item.p_quantity)

    });
    //if (this.numberOfItemAddedCart > 0)
    this.GrandTotal = this.totalOrderPrice + this.DelieverCharges
    /* if (this.CartItems.length === 0) {
       this.CartItems.push(product)
       this.productModal.updateProductTable(1,product.p_id)
     } else {
 
 
       this.CartItems.forEach((_product, index) => {
         if (_product.p_id === product.p_id) {
           this.productModal.updateProductTable(0,product.p_id)
           this.CartItems.splice(index, 1)
 
         } else {
           this.CartItems.push(product)
           this.productModal.updateProductTable(1,product.p_id)
         }
       });
     }
     */
    return this.CartItems
  }
  getCheked(cartItem: PRODUCT, i: any) {
    if (this.reOrderProducts) {
      this.CartItems[i].isChecked = !cartItem.isChecked
      this.GrandTotal = this.calculatePrice()
      return
    } else {
      cartItem.isChecked = !cartItem.isChecked
      this.cartModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.cartModal.isThisProductTobeAddedInOrder(cartItem).then(done => {
            this.getCartItems()
          })
        }
      })
    }

  }
  getCartItems() {
    this.cartModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.cartModal.getCartItems().then(cartItems => {
          cartItems.forEach(item => {
            return item.shouldShowDropDown = false
          });
          this.CartItems = cartItems

          this.GrandTotal = this.calculatePrice()

          // !this.CartItems || this.CartItems.length === 0 ? this.noProductAvailable = true : this.noProductAvailable = false

          // this.numberOfItemAddedCart = 0
          // this.GrandTotal = 0.0
          // this.totalOrderPrice = 0.0
          // this.CartItems.forEach(item => {
          //   if (item.isChecked) {
          //     this.totalOrderPrice += Number(item.p_price * item.p_quantity)
          //   }

          //   if (item.isAddedtoCart) this.numberOfItemAddedCart += 1
          //   return item.isAddedtoCart = true
          // });

          // this.GrandTotal = this.totalOrderPrice + this.DelieverCharges

        })
      }
    })
  }
  btnRemoveFromCartTapped(cartItem: PRODUCT, i: any) {

    let alert = this.alertCtrl.create({
      title: 'Removing Item From Cart',
      message: 'Are you sure you want to remove this item from cart?',
      buttons: [
        {
          text: 'NO',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'YES',
          handler: () => {
            if (this.reOrderProducts) {
              this.CartItems.splice(this.CartItems.indexOf(cartItem), 1);
              this.GrandTotal = this.calculatePrice()
            } else {
              cartItem.isAddedtoCart = false
              this.cartModal.getDatabaseState().subscribe(ready => {
                if (ready) {
                  this.cartModal.removeProductFromCart(cartItem).then(done => {
                    this.productIDsRemovedFromCart = cartItem.p_id.toString() + ','
                    // let elem = document.getElementById("isAddedtoCart_" + cartItem.p_id)
                    // elem.textContent = 'false';
                    // let elem2 = document.getElementById("product-id." + cartItem.p_id)
                    // elem.textContent = 'Add to cart';
                    this.getCartItems()
                    cartItem.isAddedtoCart = false
                    this.productModal.updateProductTable(cartItem).then(() => {

                    })
                    // this.CartItems.splice(this.CartItems.indexOf(cartItem), 1);

                  })
                }
              })
            }
          }
        }
      ]
    });
    alert.present();
  }
  calculatePrice() {
    !this.CartItems || this.CartItems.length === 0 ? this.noProductAvailable = true : this.noProductAvailable = false

    this.numberOfItemAddedCart = 0
    this.GrandTotal = 0.0
    this.totalOrderPrice = 0.0
    this.CartItems.forEach(item => {
      if (item.isChecked) {
        this.totalOrderPrice += Number(item.p_price * item.p_quantity)
      }

      if (item.isAddedtoCart) this.numberOfItemAddedCart += 1
      return item.isAddedtoCart = true
    });

    return this.totalOrderPrice + this.DelieverCharges
  }

  showProductDetailTapped(cartItem: PRODUCT) {
    this.productModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.productModal.getProductDetail(cartItem.p_id).then(product => {

          this.nativePageTransitions.slide(this.options)
          this.navCtrl.push(ProductdetailPage, {
            product: cartItem
          })
        })
      }
    })
  }
  getCurrencyDetail() {
    this.currencySymbol = this.gsp.currencySymbol

    if (!this.currencySymbol || this.currencySymbol.length === 0) {
      let _appConfig = this.serviceManager.getFromLocalStorage('isAppConfigs')
      if (_appConfig) {
        if (_appConfig && _appConfig.currency) {
          this.gsp.currencySymbol = _appConfig.currency
          this.gsp.distance_unit = _appConfig.distance_unit

        }
      }
    }
  }
  onBtnContinueShoppingTapped() {
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.setRoot(ProductsPage)
  }

  getMyOrdersForDelieveryAddress() {

    const params = {
      service: btoa('get_orders'),
      cust_id: btoa(this.myCustomer.cust_id),
      last_fetched: ''
    }

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          if (res.orders && res.orders.length > 0) {
            let ShppingAddress = []
            res.orders.forEach(order => {

              let obj: SHIPPING_ADDRESS_DATA = {
              }
              obj.o_address = order.o_address
              obj.o_city = order.o_city
              obj.o_province = order.o_province
              obj.o_contact_person = order.o_contact_person
              obj.o_phone = order.o_phone
              obj.o_completeAddress = order.o_address + ',' + order.o_city + ',' + order.o_province
              ShppingAddress.push(obj)
            });

            this.addressBookModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.addressBookModal.createShippingAddressTable().then(tblCreated => {
                  if (tblCreated) {

                    this.addressBookModal.InsertInToShippingAddressTable(ShppingAddress).then(sa => {
                      this.serviceManager.setInLocalStorage(this.serviceManager.IS_MY_ORDER_REQUESTED_ALREADY_INITIATED, this.serviceManager.IS_MY_ORDER_REQUESTED_ALREADY_INITIATED)
                    })
                  }
                })
              }
            })
          } else {

            // no order placed for now
            if (this.myCustomer && this.myCustomer.cust_address.trim().length > 0 && this.myCustomer.cust_city.trim().length > 0 && this.myCustomer.cust_province.trim().length > 0) {
              this.addressBookModal.getDatabaseState().subscribe(ready => {
                if (ready) {
                  let obj: SHIPPING_ADDRESS_DATA = {
                  }
                  obj.o_address = this.myCustomer.cust_address
                  obj.o_city = this.myCustomer.cust_city
                  obj.o_province = this.myCustomer.cust_province
                  obj.o_contact_person = this.myCustomer.cust_phone
                  obj.o_phone = this.myCustomer.cust_phone
                  obj.o_completeAddress = this.myCustomer.cust_address + ',' + this.myCustomer.cust_city + ',' + this.myCustomer.cust_province
                  let ShppingAddress = []
                  ShppingAddress.push(obj)
                  this.addressBookModal.createShippingAddressTable().then(tblCreated => {
                    if (tblCreated) {
                      this.addressBookModal.InsertInToShippingAddressTable(ShppingAddress).then(sa => {
                        this.serviceManager.setInLocalStorage(this.serviceManager.IS_MY_ORDER_REQUESTED_ALREADY_INITIATED, this.serviceManager.IS_MY_ORDER_REQUESTED_ALREADY_INITIATED)
                      })
                    }
                  })
                }
              })

            } else {
              this.serviceManager.setInLocalStorage(this.serviceManager.IS_MY_ORDER_REQUESTED_ALREADY_INITIATED, this.serviceManager.IS_MY_ORDER_REQUESTED_ALREADY_INITIATED)
              // lets wait for the new address to add
            }

          }
        },
        (error) => {
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        },
        () => {
          //   this.serviceManager.stopProgress()
        }
      )


  }
  showDropDown(selectedCartItem: PRODUCT, index) {

    if (index >= 1) {
      this.content.scrollTo(0, this.scrollToTop + 200, 900).then(res => {
        this.content.resize()
      })
    }
    this.CartItems.map((item) => {

      if (selectedCartItem == item) {
        item.shouldShowDropDown = !item.shouldShowDropDown;
      } else {
        item.shouldShowDropDown = false;
      }

      return item;

    });
  }
  scrollToTop = 0
  scrollHandler(event) {
    this.scrollToTop = event.scrollTop
  }
  showPromotionDetail() {

  }
  showStoredCreditDetail() {

  }

  fetchPromotionsData() {

    const params = {
      service: btoa('get_customer_credit'),
      cust_id: btoa(this.myCustomer.cust_id),
    }

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          console.log(res);
          this.promotionResponse = res
          let promotions: PROMOTIONS[] = res.cust_promo_credit

          if (res.cust_store_credit && Number(res.cust_store_credit) > 0) {
            this.myCustomer.cust_credits = res.cust_store_credit
            this.selctedStoredCredit = res.cust_store_credit
            this.customerModal.updateCustomerStoreCredit(this.selctedStoredCredit)
          } else {
            this.selctedStoredCredit = 0
            this.customerModal.updateCustomerStoreCredit(0)

          }
          if (!promotions || promotions.length === 0) {
            this.promotions = []
            this.selectedPromotions = null
            return
          }
          this.promotions = promotions
          if (this.promotions && this.promotions.length > 0) this.selectedPromotions = this.promotions[0]
          else this.selectedPromotions = null
          this.promotionModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.promotionModal.InsertInToPromotionTable(promotions)
            }
          })
        },
        (error) => {
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        })
  }
  getAppConfigFromServer() {

    const params = {
      service: btoa('get_config'),
    }

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          let apConfigData: APP_CONFIG = res.config

          this.serviceManager.setInLocalStorage('isAppConfigs', apConfigData)

          if (apConfigData) {
            this.gsp.currencySymbol = apConfigData.currency
            this.gsp.distance_unit = apConfigData.distance_unit
          }
          this.configModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.configModal.InsertInAppConfigTable(apConfigData)
            }
          })
        },
        (error) => {
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        })
  }
  customerStatusOk = false;
  getUserStatus() {

    // alert('getting-cust-status')
  // alert('cust_id'+this.myCustomer.cust_id)
    const params = {
      service: btoa('get_cust_status'),
      cust_id: btoa(this.myCustomer.cust_id),
    }

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(res => {
        console.log(res);
        if (res && Number(res.status) === 1) {
          this.customerStatusOk = true;
        } else {
          this.customerStatusOk = false;
        }
      },
        (error) => {
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        })
  }
  // shouldShowPromotionDetail = false
  shouldShowPromotionDetailTapped() {
    let searchMOda = this.modalctrl.create(PromotionDetailPage, {
      promotions: this.promotions
    })
    searchMOda.present()
    searchMOda.onDidDismiss(selectedPromotions => {
      if (selectedPromotions) this.selectedPromotions = selectedPromotions
    });

    // this.shouldShowPromotionDetail = !this.shouldShowPromotionDetail
  }
  getFormattedDate(o_datetime) {
    return this.serviceManager.getFormattedTimeForMyorder(o_datetime)
  }
  promotionTapped(selectedPromotions: PROMOTIONS) {
    this.selectedPromotions = selectedPromotions
    // this.shouldShowPromotionDetail = !this.shouldShowPromotionDetail
  }

}
