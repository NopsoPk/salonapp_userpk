import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { GlobalProvider } from './../../providers/global/global';
import { last } from 'rxjs/operators';
import { LatestTrendsPage } from './../latest-trends/latest-trends';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { App, IonicPage, LoadingController, NavController, NavParams, Platform, Navbar, ViewController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalServiceProvider } from '../../providers/global-service/global-service'
import { MenuController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs'
import { MainHomePage } from '../main-home/main-home'
import { Keyboard } from '@ionic-native/keyboard'
import { SalonDetailsPage } from '../salon-details/salon-details';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';
import { Pipe, PipeTransform } from '@angular/core';
import { isUndefined } from 'util';
import { config } from '../../providers/config/config';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db'
import { SALON_SERVICES_SUB_CATEGORIES, SalonServices, Salon, SALON_SERVICES_SEARCH, Customer, PINTEREST_OBJECT } from './../../providers/SalonAppUser-Interface';

import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { ActivateSalonPage } from '../activate-salon/activate-salon';
import { FavoriteModal } from '../../providers/FavoriteModal/FavoriteModal';
import { SalonServicesModal } from '../../providers/salon-services-modal/salon-services-modal';
import { LatestTrensTipsCategoryPage } from '../latest-trens-tips-category/latest-trens-tips-category';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { PinActionSheetPage } from '../pin-action-sheet/pin-action-sheet';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
@Pipe({ name: 'highlightify' })



@IonicPage()
@Component({
  selector: 'page-looking-for',
  templateUrl: 'looking-for.html',
})
export class LookingForPage {
  @ViewChild(Navbar) navBar: Navbar;
  transitionOptions: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  SQL_TABLE_SEARCh = 'search'
  public SALON_SERVICE_SEARCH: SALON_SERVICES_SEARCH[]
  public searchArray = []
  public searchArrayTemp: SALON_SERVICES_SEARCH[]
  public myCustomer: Customer
  public selectedPost: PINTEREST_OBJECT

  tabBarElement: any;
  @ViewChild('txtSearchTag') txtSearchTag;
  public unregisterBackButtonAction: any;

  public salonPicsURL = config.salonImgUrl 



  public serSabCatPicsURL = config.StylesImagesURL;


  // zahid's defined Globall variables


  showHeader = false;
  showSpinner = false
  showCloseButton = false
  PaginationLinks: any
  isgetTagsFromServerCalled = false
  page = 0
  maximumPages = 20
  previousSearchText = ''
  noPostsForTag = ''
  isBtnCloseTaped = false
  previousSearchTag = ''
  UniqueTagsObjects = [] //previously found search from server
  noMoreSubStyles = false
  shouldShowRecentSearches = false
  shouldShowLblPinsWith = false
  infiniteScroll: any

  lastSearcheArray = []
  objSelectedSubStyle: any
  // topSearchTrends = ["Top", "People", "Women Hair Style", "Hair Stylist", "Hair color"]
  // zahid's defined Globall variables

  txtSearchStyles: string = "";
  heightForHeader = 0
  tagsObject = []
  tagsObjectBackUp = []
  selectedTagIndex = -1
  selectedPinID = ''
  tagsArray = []
  filteredTags = []
  localSearchArr = []
  AggregatedData = []
  AggregatedDataBackUp = []
  pinsArray = [];
  options: RequestOptions
  headers: Headers
  salonViewPushed = false
  public isSearching = false;
  public showTagsPopOver = false;
  search_salon: string = "";
  public pushed = false;




  constructor(public keyBoard: Keyboard,
    public customerModal: CustomerModal,
    public appContr: App,
    public http: Http,
    public sqlProvider: SqliteDbProvider,
    
    public salonServicesModal: SalonServicesModal,
    public globalSearch: GlobalServiceProvider,
    
    public loadingController: LoadingController,
    public storage: Storage,
    private sqlite: SQLite,
    public navCtrl: NavController,
    public viewCtrl: ViewController,

    public navParams: NavParams,
    public serviceManager: GlobalProvider,
    public favPostModal: FavoriteModal,
    private menu: MenuController,
    private nativePageTransitions: NativePageTransitions,
    private ga: GoogleAnalytics,
    public platform: Platform) {
    this.lastSearcheArray = []
    this.tagsObject = []
    this.setUpHeaderAndOptions()
    this.fetch_search_table();
  }
  fetch_search_table(): any {
    this.SALON_SERVICE_SEARCH = []

    this.sqlite.create({
      name: 'ionicdb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

      db.executeSql('SELECT * FROM ' + this.SQL_TABLE_SEARCh + ' ', [])
        .then(res => {
          for (var i = 0; i < res.rows.length; i++) {
            let item = { keyword: res.rows.item(i).keyword, type: res.rows.item(i).type, refer_id: res.rows.item(i).refer_id, image_url: res.rows.item(i).image_url }
            this.SALON_SERVICE_SEARCH.push(item)
          }


          // return 0;
          // if(res.rows.length>0) {
          //   this.totalIncome = parseInt(res.rows.item(0).totalIncome);
          //   this.balance = this.totalIncome-this.totalExpense;
          // }
        })
        .catch(e => {

          return 1;
        }
        );

    }).catch(e => {

      return 2;
    });
    return this.SALON_SERVICE_SEARCH;
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter() {

    this.getCustomer();

    // let HideTabe = this.navParams.get('isHideTab');
    // if (HideTabe == '1') {
    //   this.navCtrl.remove(this.navCtrl.getPrevious().index);

    //   this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    //   this.tabBarElement.style.display = 'none';
    // }

    if (this.txtSearchStyles && this.txtSearchStyles.length > 0) {
      this.shouldChangeCharacter(this.txtSearchStyles)
    }
    
  }

  getCustomer() {
    console.log('came in get customer');

    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(customer => {
          if (customer) {
            this.myCustomer = customer

          } else {

            // this.serviceManager.makeToastOnFailure(AppMessages.msgCustomerDataMissing)
            // this.navCtrl.setRoot(ActivateSalonPage)
          }
        }, error => {
          console.log('error: while getting customer');
        })
      }
    })
  }



  popPage() {

    this.nativePageTransitions.slide(this.transitionOptions)
    this.navCtrl.push(MainHomePage, { isHideTab: "1" }, { animate: false })
  }

  setUpHeaderAndOptions() {
    this.headers = new Headers();
    this.options = new RequestOptions({ headers: this.headers });
  }

  // searchByTag() {
  //   if (this.txtSearchStyles != undefined) {
  //     if (this.txtSearchStyles.trim().length > 2) {
  //       this.keyBoard.close();
  //       this.getSubStylesFromServer(this.txtSearchStyles);
  //     } else {
  //       this.toastProvider.makeToastOnFailure('Please enter valid search keyword.', 0);
  //     }
  //   }
  // }

  searchFromLocalStorage(searchText): any {
    searchText = searchText.toLowerCase();
    this.searchArray = []
    //hair1, hair2, hair3, hair4
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        let search_keyword2 = "hair";
        this.sqlProvider.localSearchLookingFor(search_keyword2).then(res => {

          this.searchArray = res

          var tempArray = this.filteredTags.concat(this.searchArray);
          this.localSearchArr = this.searchArray;
          this.localSearchArr = this.removeDuplicates(this.localSearchArr, 'p_keywords')

          this.localSearchArr = this.removeDuplicates(this.localSearchArr, 'p_keywords')
        })

        //here add old code after getting result from search

      }
    })


    // searchText=searchText.toLowerCase();
    // let searchArray=[]
    // this.SALON_SERVICE_SEARCH.forEach(element => {
    //   let keyWord: string = element.keyword
    //   console.log("keyWord",keyWord+' Id '+element.refer_id+' refer_id '+element.type)
    //   if (keyWord && keyWord.toLowerCase().startsWith(searchText)) {
    //     let searchedSalon={
    //       p_keywords:element.keyword,
    //       type:element.type,
    //       search_type:element.type
    //     }
    //     searchArray.push(searchedSalon)
    //   }

    // });
    // console.log('SearchArray',this.searchArray)
    // console.log('SearchArrayLocal',this.searchArray)

    // var tempArray = this.filteredTags.concat(this.searchArray);
    // this.localSearchArr=this.searchArray;
    // this.localSearchArr = this.removeDuplicates(this.localSearchArr , 'p_keywords')
    // console.log('Temp',  this.filteredTags);


    //    this.localSearchArr = this.removeDuplicates(this.localSearchArr , 'p_keywords')
  }




  getSubStylesFromServer(searchStr: string) {


    searchStr = searchStr.trim();
    let tagURL = 'https://www.laravel.nopso.qwpcorp.com/InstaApi/public/api/searchTag';
    if (searchStr.trim().length > 0) {
      tagURL = 'https://www.laravel.nopso.qwpcorp.com/InstaApi/public/api/searchTag/' + searchStr;
    }

    let loading = this.loadingController.create({ content: "Please Wait..." });


    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');

    let options = new RequestOptions({ headers: headers });
    this.showSpinner = true
    this.showCloseButton = false
    this.isgetTagsFromServerCalled = true
    // this.showTagsPopOver = false

    // 

    this.http.get(tagURL, options)
      .map(Response => Response.json())
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe(res => {
        // loading.dismiss()
        this.showSpinner = false
        this.showCloseButton = true
        this.isgetTagsFromServerCalled = false

        if (res.data != undefined) {



          if (res.data.length > 0 || this.localSearchArr.length > 0) {


            if (res.data.length < 20) {
              this.noPostsForTag = searchStr
              this.noMoreSubStyles = true
            } else {
              this.noPostsForTag = ''
              this.noMoreSubStyles = false
            }
            this.shouldShowLblPinsWith = false

            this.tagsArray = this.tagsArray.concat(res.data)


            this.UniqueTagsObjects = this.removeDuplicates(this.tagsArray, 'p_keywords')


            if (this.txtSearchStyles.length > 0) {
              this.showTagsPopOver = true
            } else {
              this.showTagsPopOver = false
            }
            // 
            if (this.UniqueTagsObjects.length > 0 || this.filteredTags.length > 0) {
              this.filterSearchedTagFromResponse(this.txtSearchStyles)
            }
          } else {

            this.filteredTags = [];
            this.tagsArray = [];
            this.showTagsPopOver = false
            this.noPostsForTag = searchStr
            let sSLen = this.txtSearchStyles.length
            if (this.previousSearchText.trim().length > sSLen && (this.noPostsForTag.length - 1 === sSLen || this.noPostsForTag.length - 1 > sSLen)) {
              this.isgetTagsFromServerCalled = false
              this.noPostsForTag = ''
            }

          }
        }

      }, error2 => {
        this.showSpinner = false
        this.isgetTagsFromServerCalled = false
        loading.dismiss()
        this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

      }
      )

  }

  checkFocuses() {
    this.heightForHeader = 250
  }

  btnStyleTapped(objSubStyle, index) {



    if (!objSubStyle) {
      return
    }

    if (this.lastSearcheArray && this.lastSearcheArray.length >= 5) {
      this.lastSearcheArray.splice(0, 1);
    }

    this.lastSearcheArray.push(objSubStyle)
    this.lastSearcheArray = this.removeDuplicates(this.lastSearcheArray, 'p_keywords')

    if (index === this.selectedTagIndex) {
      return
    }


    if (objSubStyle.p_keywords.length == 0) {
      this.serviceManager.makeToastOnFailure('Tag Name is missing')
      return
    }
    this.selectedTagIndex = index
    this.txtSearchStyles = objSubStyle.p_keywords


    //new code for populating salons and services
    //if type= service populate all servics belongs to that particular service
    //if type= salon go to salon page and show salon details 
    
    let type = objSubStyle.type
    if (type == 'service') {

      this.showAllSalons(objSubStyle.refer_id, objSubStyle.type);
    } else if (type == 'salon') {

      this.gotToSalonDetailPage(objSubStyle.refer_id);
    } else if (type == "ser_sub_categories") {
      this.salonServicesModal
      this.showPostsForStyles(objSubStyle.refer_id)
      // this.showAllSalonsForSubStyles(objSubStyle.refer_id, objSubStyle.type);

    } else {

      this.showTagsPopOver = false

      this.getPinsAgainstSubStyle(objSubStyle.p_keywords)//first call
    }
    //end new code

  }
  gotToSalonDetailPage(refer_id) {
    this.sqlProvider.getSalonDetailsFrom(refer_id).then(salon => {
      if (salon) {
        this.navCtrl.push(SalonDetailsPage, {
          selectedSalon: salon,
          sal_id: salon.sal_id
        });
      }
    })
  }

  showAllSalons(ser_id, type) {
    var sal_ids = ""
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServicesModal.getSalonServicesBySerId(ser_id).then(res => {
          let salonsServices = res
          salonsServices.forEach(salonService => {
            sal_ids += salonService.sal_id + ','
          });
          sal_ids = sal_ids.slice(0, sal_ids.length - 1)

          this.sqlProvider.getSalonsListingFromSqliteDB(sal_ids).then(salons => {

            this.navCtrl.push(HomePage, {
              allSalons: salons
            });
          })
        })
      }
    })
  }

  showPostsForStyles(ssc_id) {
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServicesModal.getSubCategory(ssc_id).then(subStyle => {
          console.log(JSON.stringify(subStyle));

          this.nativePageTransitions.slide(this.transitionOptions)
          this.navCtrl.push(LatestTrensTipsCategoryPage, {
            subCat: subStyle
          });

        })
      }
    })

    // let subStyle;
    // if(subStyle1.keyword){
    //   subStyle={
    //    ssc_name: subStyle1.keyword,
    //    ssc_id: subStyle1.refer_id,
    //    ssc_image:subStyle1.image_url
    //   }
    // }else{
    //   subStyle=subStyle1;
    // } 

  }


  getPinsAgainstSubStyle(subStyleID: string) {

    let custId = this.myCustomer.cust_id

    if (subStyleID === this.previousSearchTag) {

      return;
    }
    this.previousSearchTag = subStyleID

    let tagURL = 'http://www.laravel.nopso.qwpcorp.com/InstaApi/public/api/getPinsForSubStyle/'

    console.log('URL' + tagURL + subStyleID + '/' + custId)

    this.selectedPinID = subStyleID
    let loading = this.loadingController.create({ content: "Please Wait..." });

    this.showSpinner = true
    this.showCloseButton = false
    this.http.get(tagURL + subStyleID + '/' + custId, this.options)
      .map(Response => Response.json())
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe(res => {
        // loading.dismiss()
        this.showSpinner = false
        this.showCloseButton = true

        this.pinsArray = [];
        if (res.data.posts.length > 0) {
          this.pinsArray = res.data.posts;
          this.PaginationLinks = res.links
          if (this.infiniteScroll) {
            this.infiniteScroll.enable(true);
          }

        }
      }, error2 => {
        // loading.dismiss()

        this.showSpinner = false
        this.showCloseButton = true
        this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
        if (this.infiniteScroll) {
          this.infiniteScroll.enable(false);
        }

      }
      )
  }

  btnCloseTaped() {


    this.isBtnCloseTaped = true
    this.heightForHeader = 0
    this.isSearching = false;
    this.txtSearchStyles = "";
    this.shouldShowLblPinsWith = false

    this.filteredTags = this.tagsArray;
    this.selectedTagIndex = -1;


    this.previousSearchTag = ''
    this.isgetTagsFromServerCalled = false
    this.showCloseButton = true
    this.showTagsPopOver = false
    this.noPostsForTag = ''
    if (this.infiniteScroll) {
      this.infiniteScroll.enable(false)
    }
    this.pinsArray = []
    // this.getSubStylesFromServer('');
  }
  btnClearRecentTapped() {
    this.lastSearcheArray = []
    this.shouldShowRecentSearches = false
  }
  checkFocus() {

    this.isSearching = !this.isSearching;
  }
  searchFromLocalSalon(keyWord) {
    //make a temp(searchArray) array and search from local salon
    //if an item found from loacal salon add it into searchArray array
    //again concat searchArray array with filteredTags
    //make a new array and assign it to filteredTags
    let searchArray = []
    let customer = this.serviceManager.getFromLocalStorage(this.serviceManager.SALONS_NEAR_CUSTOMER)
    customer.forEach(element => {
      let post: string = element.sal_name
      if (post && post.toLowerCase().startsWith(keyWord)) {
        let searchedSalon = {
          p_keywords: element.sal_name,
          sal_id: element.sal_id,
          search_type: 'local_salon'
        }
        searchArray.push(searchedSalon)
      }


    });
    var tempArray = this.filteredTags.concat(searchArray);
    this.filteredTags = tempArray;

  }
  shouldChangeCharacter(searchText: string) {

    let newSearchTextLength = this.txtSearchStyles.length

    if (this.previousSearchText.length > newSearchTextLength && ((this.noPostsForTag.length - 1 === newSearchTextLength || this.noPostsForTag.length - 1 > newSearchTextLength))) {

      this.isgetTagsFromServerCalled = false
      this.noPostsForTag = ''
      this.noMoreSubStyles = false
    }

    // if (this.txtSearchStyles.length < this.noPostsForTag.length) {

    //   this.noMoreSubStyles = false
    //   this.noPostsForTag = ''
    // }

    // var lastChar = target.value[target.value.length - 1];
    // if (lastChar === ' ' || lastChar === '') {
    //   this.txtSearchStyles = target.value.trim()
    //   return
    // }
    // 

    this.isBtnCloseTaped = false


    if (searchText.trim().length === 0) {

      this.txtSearchStyles = ''
      this.showTagsPopOver = false
      this.showCloseButton = false
      this.shouldShowLblPinsWith = false
    } else {

      this.filteredTags = []
      //search from local storage

      searchText = searchText.toLowerCase();
      this.searchArray = []

      this.sqlProvider.getDatabaseState().subscribe(ready => {
        if (ready) {

          this.sqlProvider.localSearchLookingFor(searchText).then(res => {
            this.searchArrayTemp = []
            this.searchArrayTemp = res
            this.searchArray = []
            this.searchArrayTemp.forEach(element => {
              let searchedSalon = {
                p_keywords: element.keyword,
                type: element.type,
                refer_id: element.refer_id,
                image_url: element.image_url,
              }
              this.searchArray.push(searchedSalon)
            });
            this.filteredTags = []
            this.filteredTags = this.searchArray

            this.filteredTags = this.removeDuplicates(this.filteredTags, 'p_keywords')
            // console.log(JSON.stringify(this.filteredTags))



            //  var tempArray = this.filteredTags.concat(this.searchArray);
            //  this.localSearchArr=this.searchArray;
            //  this.localSearchArr = this.removeDuplicates(this.localSearchArr , 'p_keywords')
            //  this.filteredTags = this.localSearchArr

            //here add old code after getting result from search
            //new code
            //search from salon locally saved.

            // this.searchFromLocalStorage(searchText);

            this.showTagsPopOver = true
            //serach from salon
            //  

            //end new code 
            if (!this.showSpinner) {
              this.showCloseButton = true
            }
            this.showTagsPopOver = true;
            this.shouldShowLblPinsWith = true
            this.selectedTagIndex = -1
            let targetValueForSearch = searchText.toLowerCase()
            //  let tempFilterTags = []
            //  if (this.UniqueTagsObjects && this.UniqueTagsObjects.length > 0) {
            //    this.UniqueTagsObjects.forEach(element => {
            //      let post: string = element.p_keywords
            //      if (post && post.toLowerCase().startsWith(targetValueForSearch)) {
            //        tempFilterTags.push(element)
            //      }
            //    });
            //  } 
            //if (tempFilterTags.length < 5 && !this.noMoreSubStyles && this.isgetTagsFromServerCalled === false) { 
            //      if (tempFilterTags.length < 5 && !this.noMoreSubStyles && this.isgetTagsFromServerCalled === false) {


            //       //new code to integrate local search with old search

            //        var tempArray = this.localSearchArr
            //        //var tempArray = this.filteredTags.concat(searchArray);
            //         //old code
            //       // this.filteredTags = tempFilterTags
            //        //old code 
            //        let localAndOldSearch = tempArray.concat(tempFilterTags);
            //        this.filteredTags=localAndOldSearch
            //         //new code to integrate local search with old search

            //         //old code 
            //        // this.filteredTags = tempFilterTags

            //        if (this.txtSearchStyles.trim().length > 0) {
            //         //commented for a while
            //        //  this.getSubStylesFromServer(target.value);  
            //        }

            //      } else {
            // //new code to integrate local search with old search
            //        var tempArray = this.localSearchArr
            //        let localAndOldSearch =tempArray.concat(tempFilterTags)  //tempFilterTags.concat(tempArray);
            //        this.filteredTags=localAndOldSearch
            //         // end new code to integrate local search with old search

            //         //old code 
            //        //  this.filteredTags = tempFilterTags
            //       //old code
            //      }


            //add below block here 
            if (this.txtSearchStyles.length > 0) {

              this.shouldShowRecentSearches = false
              if (this.filteredTags && this.filteredTags.length <= 0 && !this.shouldShowRecentSearches) {
                this.shouldShowLblPinsWith = true
              } else {
                this.shouldShowLblPinsWith = false
              }

            }

          })


        }
      })



      //       //else block01 old code comented here
      //       //new code
      //       //search from salon locally saved.

      //        this.searchFromLocalStorage(searchText);

      //         this.showTagsPopOver = true
      //       //serach from salon
      //     //  

      //       //end new code 
      //       if (!this.showSpinner) {
      //         this.showCloseButton = true
      //       }
      //       this.showTagsPopOver = true;
      //       this.shouldShowLblPinsWith = true
      //       this.selectedTagIndex = -1
      //       let targetValueForSearch = target.value.toLowerCase()
      //       let tempFilterTags = []
      //       if (this.UniqueTagsObjects && this.UniqueTagsObjects.length > 0) {


      //         this.UniqueTagsObjects.forEach(element => {
      //           let post: string = element.p_keywords
      //           if (post && post.toLowerCase().startsWith(targetValueForSearch)) {
      //             tempFilterTags.push(element)
      //           }
      //         });
      //       } else {

      //       }

      //       //if (tempFilterTags.length < 5 && !this.noMoreSubStyles && this.isgetTagsFromServerCalled === false) { 
      //       if (tempFilterTags.length < 5 && !this.noMoreSubStyles && this.isgetTagsFromServerCalled === false) {




      //        //new code to integrate local search with old search

      //         var tempArray = this.localSearchArr

      //        //var tempArray = this.filteredTags.concat(searchArray);
      //          //old code
      //        // this.filteredTags = tempFilterTags
      //         //old code 
      //         let localAndOldSearch = tempArray.concat(tempFilterTags);
      //         this.filteredTags=localAndOldSearch

      //          //new code to integrate local search with old search

      //          //old code 
      //         // this.filteredTags = tempFilterTags

      //         if (this.txtSearchStyles.trim().length > 0) {
      //          //commented for a while
      //         //  this.getSubStylesFromServer(target.value);  
      //         }

      //       } else {
      //  //new code to integrate local search with old search
      //         var tempArray = this.localSearchArr
      //         let localAndOldSearch =tempArray.concat(tempFilterTags)  //tempFilterTags.concat(tempArray);
      //         this.filteredTags=localAndOldSearch

      //          // end new code to integrate local search with old search

      //          //old code 

      //       //  this.filteredTags = tempFilterTags
      //        //old code
      //       }

      //        //end else block01 old code comented here
    }
    if (this.txtSearchStyles.length > 0) {

      this.shouldShowRecentSearches = false
      if (this.filteredTags && this.filteredTags.length <= 0 && !this.shouldShowRecentSearches) {
        this.shouldShowLblPinsWith = true
      } else {
        this.shouldShowLblPinsWith = false
      }

    } else {

    }



    this.previousSearchText = this.txtSearchStyles
  }
  filterSearchedTagFromResponse(tagName: string) {

    //new code for add local search
    var tempArray = this.localSearchArr
    //end new code


    let sSLen = this.txtSearchStyles.length
    if (this.previousSearchText.length > sSLen && (this.noPostsForTag.length - 1 === sSLen || this.noPostsForTag.length - 1 > sSLen)) {
      this.isgetTagsFromServerCalled = false
      this.noPostsForTag = ''
    }

    this.selectedTagIndex = -1

    let targetValueForSearch = tagName.toLowerCase()
    let tempFilterTags = []

    if (this.UniqueTagsObjects && this.UniqueTagsObjects.length > 0) {
      this.UniqueTagsObjects.forEach(element => {

        let post: string = element.p_keywords
        if (post && post.toLowerCase().startsWith(targetValueForSearch)) {
          tempFilterTags.push(element)
        }
      });
    } else {
    }

    //added

    if (tempFilterTags.length < 5 && !this.noMoreSubStyles && this.isgetTagsFromServerCalled === false) {
      // //new code 

      let localAndOldSearch = tempArray.concat(tempFilterTags) // tempFilterTags.concat(tempArray);
      this.filteredTags = localAndOldSearch
      //end new code 
      this.filteredTags = localAndOldSearch

      //old code 
      this.isgetTagsFromServerCalled = false
      // this.filteredTags = tempFilterTags
      this.getSubStylesFromServer(tagName);
      //end old code
    } else {

      //new code 
      let localAndOldSearch = tempArray.concat(tempFilterTags) //tempFilterTags.concat(tempArray);
      this.filteredTags = localAndOldSearch
      //end new code 

      //old code
      //this.filteredTags = tempFilterTags
    }

    if (tagName.length !== 0) {
      this.txtSearchStyles = tagName
    } else {
      this.txtSearchStyles = 'Men Hair style'
    }


  }

  endEndEditing(event) {
    // this.shouldShowRecentSearches = false 

  }
  shouldBeginEditing(event) {
    

    if (this.lastSearcheArray && this.lastSearcheArray.length > 0 && this.txtSearchStyles.trim().length === 0) {
      this.showCloseButton = false
      this.shouldShowRecentSearches = true
    }

  }
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {

    if(this.menu.isOpen()){
      this.menu.close();
    }else {
       if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
    }
    
    // this.nativePageTransitions.slide(this.transitionOptions)
    // this.navCtrl.push(MainHomePage, { isHideTab: "1" }, { animate: false })
  }

  // postImageTapped(objPin) {
  //   //objPin = {ip_id: 155839, ssc_id: 8, imageUrl: 
  //   //"https://i.pinimg.com/736x/15/14/b3/sdds.jpg",
  //   // likes: "", user_screen_name: "", …}

  //   //
  //   console.log(JSON.stringify(objPin))
  //   if (!objPin || !objPin.ip_id) {
  //     return
  //   }
  //   this.selectedPinID = objPin.ip_id
  //   this.serviceManager.setInLocalStorage(this.serviceManager.OBJ_PIN, objPin)
  //   this.nativePageTransitions.slide(this.transitionOptions)
  //   if (objPin.sal_id != 0) {
  //     let salon = {
  //       sal_id: objPin.sal_id
  //     }

  //     this.navCtrl.push(SalonDetailsPage, { selectedSalon: salon });
  //   } else {

  //     // let sourceName = data.source_name.replace('#', '');

  //     this.serviceManager.removeFromStorageForTheKey(this.serviceManager.OBJ_SUB_STYLE)
  //     this.navCtrl.push(HomePage, { url: config.GetSalonFromPinURL + this.selectedPinID + "/" + objPin.ssc_id });
  //   }
  // }


  postImageTapped(post, $event) {

    let ssc_id = post.ssc_id
    let postObject: PINTEREST_OBJECT = post
    this.selectedPost = postObject
    
    var PinRelatedSalon: Salon[] = []
    let requiredSalonIDs = ''
    let SSCID = post.ssc_id
    
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {

        this.sqlProvider.getSalSerSubCatFromSqliteDB(ssc_id).then(allSalonIDs => {

          allSalonIDs.forEach(element => {
            requiredSalonIDs += element.sal_id + ',';
          });
          requiredSalonIDs = requiredSalonIDs.slice(0, requiredSalonIDs.length - 1)
          if (requiredSalonIDs.length > 0) {
            // this.databse.getDatabaseState().subscribe(ready => {
            //   if (ready) {
            this.sqlProvider.getSalonsListingFromSqliteDB(requiredSalonIDs).then(res => {
              if (res && res.length > 0) {
                PinRelatedSalon = res
                // PinRelatedSalon = PinRelatedSalon.slice(0, 5)
                this.salonServicesModal.getDatabaseState().subscribe(ready => {
                  if (ready) {


                    PinRelatedSalon.forEach(salon => {
                      this.salonServicesModal.getSalonServicesForSubCategory(salon.sal_id, SSCID).then(pinRelatedSers => {
                        return salon['pinRelatedServices'] = pinRelatedSers
                      })
                    });
                    this.nativePageTransitions.slide(this.transitionOptions)
                    this.navCtrl.push(PinActionSheetPage, {
                      PinRelatedSalon: PinRelatedSalon,
                      // cateogry: this.cateogry,
                      objPin: postObject,

                    })
                  }
                })
               
              }
            })
          }
        })
      }
    })

    
  }

  loadMore(infiniteScroll) {

    if (this.txtSearchStyles.length > 0) {
      this.infiniteScroll = infiniteScroll
      this.page++;

      if (this.page === this.maximumPages) {

        infiniteScroll.enable(false);
      } else {
        this.loadMorePosts(infiniteScroll)
      }


      // if (this.PaginationLinks && !this.PaginationLinks.next) {
      //   console.log('looks like next page found nill');
      //   return
      // } 
    } else {
      infiniteScroll.enable(false);
    }
  }

  loadMorePosts(infiniteScroll) {
    if (!this.PaginationLinks && this.infiniteScroll) {

      this.infiniteScroll.enable(false)

      return
    }

    this.http.get(this.PaginationLinks.next, this.options)
      .map(Response => Response.json())
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe(res => {
        // this.pinsArray = [];
        if (res.data.posts.length > 0 && this.isBtnCloseTaped === false) {
          this.pinsArray = this.pinsArray.concat(res.data.posts)
          this.PaginationLinks = res.links

          if (infiniteScroll) {
            try {
              infiniteScroll.complete();
            } catch (ex) {

            }
          }
        }
      }, error2 => {
        // loading.dismiss()

        // this.serviceManager.makeToastOnFailure('No more posts available against this tag', 1)
        infiniteScroll.enable(false);
      }
      )
  }

  removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }
  lblRecentSearchTapped(objSubStyle) {


    //here

    if (!objSubStyle) {
      return
    }

    if (this.lastSearcheArray && this.lastSearcheArray.length >= 5) {
      this.lastSearcheArray.splice(0, 1);
    }

    this.lastSearcheArray.push(objSubStyle)
    this.lastSearcheArray = this.removeDuplicates(this.lastSearcheArray, 'p_keywords')


    this.showTagsPopOver = false

    if (objSubStyle.p_keywords.length == 0) {
      this.serviceManager.makeToastOnFailure('Tag Name is missing')
      return
    }

    this.txtSearchStyles = objSubStyle.p_keywords


    //new code for populating salons and services
    //if type= service populate all servics belongs to that particular service
    //if type= salon go to salon page and show salon details 
    
    let type = objSubStyle.type
    if (type == 'service') {

      this.showAllSalons(objSubStyle.refer_id, objSubStyle.type);
    } else if (type == 'salon') {

      this.gotToSalonDetailPage(objSubStyle.refer_id);
    } else if (type == "ser_sub_categories") {

      this.showAllSalons(objSubStyle.refer_id, objSubStyle.type);

    } else {

      this.getPinsAgainstSubStyle(objSubStyle.p_keywords)//first call
      this.shouldShowRecentSearches = false
      this.txtSearchStyles = objSubStyle.p_keywords
    }
    //end new code



    //old code
    // if (objPin) {
    //   let substyleName: string = objPin.p_keywords;
    //   this.getPinsAgainstSubStyle(objPin.p_keywords)//2nd call
    //   this.shouldShowRecentSearches = false
    //   this.txtSearchStyles = objPin.p_keywords
    // }
    //end old code

  }


  public isService(value) {

    if (value === 'service') {
      return true;
    } else {
      return false;
    }
  }

  public isSerSubCat(value, url) {

    if (value === 'ser_sub_categories') {

      return true;
    } else {
      return false;
    }
  }


  public isSalon(value, url) {


    if (value === 'salon') {

      return true;
    } else {
      return false;
    }

  }

  public isKeyWord(value) {

    if (value === 'key_word') {
      return true;
    } else {
      return false;
    }
  }

  public highlight(SubStyleName: string) {


    if (!this.txtSearchStyles) {
      return SubStyleName;
    }
    return SubStyleName.replace(new RegExp(this.txtSearchStyles, "gi"), match => {
      return '<span class="highlightText">' + match + '</span>';
    });
  }

  makePostFavoriteTapped(post: PINTEREST_OBJECT) {
    console.log(post);

    let myPost: PINTEREST_OBJECT = post
    let favoPostURL = config.makePostFavoriteURL
    // postiD/CustID/favUnFav


    let makePostFavorite: number
    if (myPost.favpost === 1) {
      makePostFavorite = 0
    } else {
      makePostFavorite = 1
    }
    console.log(makePostFavorite);
    favoPostURL += myPost.ip_id + '/' + this.myCustomer.cust_id + '/' + makePostFavorite
    console.log(favoPostURL);


    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');

    let options = new RequestOptions({ headers: headers });
    console.log('going to call api call');
    // Getting Posts from server 
    this.http.get(favoPostURL, options)
      .map(Response => Response.json())
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(res => {
        if (res.status === 1 || res.status === '1') {
          this.pinsArray.forEach(post => {
            if (post.ip_id === myPost.ip_id) {
              return myPost.favpost = makePostFavorite
            }
          });
          if (makePostFavorite === 1) {
            this.favPostModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.favPostModal.InsertInTopostsTable(post).then(isSalonMarkedFavorite => {
                  if (isSalonMarkedFavorite) {
                    console.log('jobs Done');
                  }
                })
              }
            })
          } else if (makePostFavorite === 0) {
            this.favPostModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.favPostModal.deletePost(post.ip_id).then(isSalonMarkedFavorite => {
                  if (isSalonMarkedFavorite) {
                    console.log('jobs Done');
                  }
                })
              }
            })
          }
        }
      })
  }
  // googleAnalytics() {

  //   // this.ga.trackView(this.myCustomer.cust_name + ' visited Salon Detail Page');
  //   let eventAction = this.myCustomer.cust_name + ' Tapped on post id ' + this.selectedPost.ip_id + ' in ' + this.selectedPost.ssc_id + '-' + this.cateogry
  //   this.ga.trackEvent('Tap', eventAction, 'lab: general label', 200, true)
  //   this.ga.trackTiming('cat: tracking timing', 600000, 'variable: not sure what will go here', 'label: and the same')
  //   this.ga.debugMode()
  //   this.ga.setAllowIDFACollection(true)
  //   this.ga.setUserId(this.myCustomer.cust_id)
 
  // }

  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };


    if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

    if (this.navCtrl.canGoBack()) {

      this.navCtrl.pop();
    } else {

      this.navCtrl.setRoot(MainHomePage)
    }

  }
  handleReturnkey() {
    this.keyBoard.close()
  }
  scrollHandler(event) {
    this.keyBoard.close()
  }
}