import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PRODUCT } from '../../providers/SalonAppUser-Interface';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { MainHomePage } from '../main-home/main-home';
import { ProductModal } from '../../providers/ProductModal/ProductModal';
import { GlobalProvider } from '../../providers/global/global';
import { ViewCartPage } from '../view-cart/view-cart';
import { config } from '../../providers/config/config';
import { Platform } from 'ionic-angular';
import { CartModal } from '../../providers/cartModal/cartModal';
import { GlobalServiceProvider } from '../../providers/global-service/global-service';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { MenuController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-productdetail',
  templateUrl: 'productdetail.html',
})
export class ProductdetailPage {
  public selectedProduct: PRODUCT
  public productChecked = true
  public CartItems: PRODUCT[] = []
  public productsImageURL = config.prouductImages
  public productRating
  public isFromReviewOrder = false
  public isFromProductPage = false
  public isAddedToCart = false
  public currencySymbol = ''
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };

  constructor(
    private menu: MenuController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public productModal: ProductModal,
    public cartModal: CartModal,
    public serviceManager: GlobalProvider,
    public nativePageTransitions: NativePageTransitions,
    public gsp: GlobalServiceProvider,
    public platform: Platform,

  ) {
    this.productRating = 4
    this.selectedProduct = navParams.get('product')
    this.isFromReviewOrder = navParams.get('isFromReviewOrder')
    this.isFromProductPage = navParams.get('isFromProductPage')

    if (this.selectedProduct.p_quantity === 0) this.selectedProduct.p_quantity += 1
  }

  public unregisterBackButtonAction: any;

  //custom back button for android
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    // this.navCtrl.pop();
    if(this.menu.isOpen()){
      this.menu.close();
    }else {
       if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
    }
  }
  //end custom back button for android

  ionViewDidLoad() {
    this.initializeBackButtonCustomHandler();
    console.log('ionViewDidLoad ProductdetailPage');
  }
  ionViewWillEnter() {
    this.getCurrencyDetail()
  }
  ionViewDidEnter() {


    this.productModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.cartModal.getCartItems().then(cartItems => {
          this.CartItems = cartItems

        })
      }
    })
  }
  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };


    this.navCtrl.getPrevious().data.isAddedToCart = this.isAddedToCart
    if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop().then(() => {

      })
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
  }
  btnAddToCartTapped() {



    this.selectedProduct.p_quantity = 1
    if (this.selectedProduct.isAddedtoCart) { this.serviceManager.makeToastOnSuccess(AppMessages.msgProductAlreadyInCart); return }
    console.log(this.selectedProduct);

    let _tempGrandTotal = this.calculatePrice()
    _tempGrandTotal += this.selectedProduct.p_quantity * this.selectedProduct.p_price

    if (_tempGrandTotal > config.maxCartPrice) {
      this.selectedProduct.p_quantity -= 1
      this.serviceManager.makeToastOnFailure(AppMessages.msgOrderLimit+config.maxCartPrice)
      return
    }

    !this.selectedProduct.isAddedtoCart && (this.selectedProduct.isAddedtoCart = !this.selectedProduct.isAddedtoCart)

    this.cartModal.InsertInToCartTable(this.selectedProduct).then(added => {
      this.productModal.updateProductTable(this.selectedProduct).then(() => {
        this.isAddedToCart = true
        this.nativePageTransitions.slide(this.options)
        this.navCtrl.push(ViewCartPage)
      })

      // this.cartModal.getCartItems().then(cartItems => {
      //   this.CartItems = cartItems
      // })
    })

  }

  btnAddToCartTapped_old(product: PRODUCT) {
    // isAddedToCart
    if (product.isAddedtoCart) { this.serviceManager.makeToastOnSuccess(AppMessages.msgProductAlreadyInCart); return }
    !product.isAddedtoCart && (product.isAddedtoCart = !product.isAddedtoCart)
    this.productModal.updateProductTable(this.selectedProduct).then(isUpdated => {
      this.cartModal.getCartItems().then(cartItems => {
        this.CartItems = cartItems
        this.serviceManager.makeToastOnSuccess(AppMessages.msgProductAlreadyInCart)
      })
    })
    /**
    this.products.map((prod) => {

      if (product == prod) {
        prod.isAddedtoCart = !prod.isAddedtoCart;

      } else {
        prod.isAddedtoCart = false;
      }

      return prod;

    });
    if (this.CartItems.length === 0) {
      this.CartItems.push(product)
      this.productModal.updateProductTable(1,product.p_id)
    } else {


      this.CartItems.forEach((_product, index) => {
        if (_product.p_id === product.p_id) {
          this.productModal.updateProductTable(0,product.p_id)
          this.CartItems.splice(index, 1)

        } else {
          this.CartItems.push(product)
          this.productModal.updateProductTable(1,product.p_id)
        }
      });
    }
 */
  }
  btnViewCartTapped() {
    if (!this.CartItems || this.CartItems.length === 0) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgNoProductInCart)
      return
    }
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(ViewCartPage, {
      CartItems: this.CartItems,
      selectedProduct: null
    })
  }
  slideChanged() {

  }

  expandCollapseSeeMoreOptions() {


    // elm.style.animation = 'animation: cascadeInSimple 1s 0s ease-in-out forwards;'


    this.selectedProduct.isShowingSeeMoreOptions = !this.selectedProduct.isShowingSeeMoreOptions

    let elm = document.getElementById('productCard-seeMoreOptions')
    //   if (!elm) return
    //   elm.className = 'productCard-seeMoreOptions3';
    //   timer(1000).subscribe(() => {
    //     this.selectedProduct.isShowingSeeMoreOptions = !this.selectedProduct.isShowingSeeMoreOptions
    //     elm.className = 'productCard-seeMoreOptions';
    //   })
    // } else {
    //   // var animationName = "cascadeInSimple";
    //   // eval("$('productCard-seeMoreOptions')." + animationName + "(200, function(){})");

    //   let elm = document.getElementById('productCard-seeMoreOptions')
    //   if (!elm) return
    //   elm.className = 'productCard-seeMoreOptions2';
    //   // this.products[index].isShowingSeeMoreOptions = !this.products[index].isShowingSeeMoreOptions
    // }



  }

  incrementQty() {

    this.selectedProduct.p_quantity += 1
    this.cartModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.cartModal.updateProductQuantity(this.selectedProduct)
      }
    })
    return this.selectedProduct

  }
  DecrementQty() {

    this.selectedProduct.p_quantity -= 1
    if (this.selectedProduct.p_quantity < 1) this.selectedProduct.p_quantity = 1
    this.cartModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.cartModal.updateProductQuantity(this.selectedProduct)
      }
    })
    return this.selectedProduct

  }
  // incrementQty(product: PRODUCT, index: number) {


  //   this.CartItems[index].p_quantity += 1
  //   this.CartItems[index].CalculatedPrice = Number(this.CartItems[index].p_quantity * this.CartItems[index].p_price)
  //   // if (this.reOrderProducts) {
  //   //   this.calculatePrice()
  //   // } else {

  //   //   this.GrandTotal = 0.0
  //   //   this.totalOrderPrice = 0.0
  //   //   this.numberOfItemAddedCart = 0
  //   //   this.CartItems.forEach(item => {
  //   //     if (item.isAddedtoCart) this.numberOfItemAddedCart += 1
  //   //     if (item.isAddedtoCart) this.totalOrderPrice += Number(item.p_price * item.p_quantity)

  //   //   });
  //   //   this.cartModal.getDatabaseState().subscribe(ready => {
  //   //     if (ready) {
  //   //       this.cartModal.updateCartTable(product)
  //   //     }
  //   //   })
  //   //   this.GrandTotal = this.totalOrderPrice + this.DelieverCharges
  //   // }
  //       this.cartModal.getDatabaseState().subscribe(ready => {
  //         if (ready) {
  //           this.cartModal.updateCartTable(product)
  //         }
  //       })

  //       if (this.selectedProduct.p_quantity < 1) this.selectedProduct.p_quantity = 1

  //       return this.selectedProduct
  //   return this.CartItems

  // }
  // DecrementQty(product: PRODUCT, index: number) {

  //   this.CartItems[index].p_quantity -= 1
  //   if (this.CartItems[index].p_quantity < 1) this.CartItems[index].p_quantity = 1
  //   if (this.reOrderProducts) {
  //     this.calculatePrice()
  //   } else {

  //     this.GrandTotal = 0.0
  //     this.totalOrderPrice = 0.0
  //     this.numberOfItemAddedCart = 0
  //     this.CartItems.forEach(item => {
  //       if (item.isAddedtoCart) this.numberOfItemAddedCart += 1
  //       if (item.isAddedtoCart) this.totalOrderPrice += Number(item.p_price * item.p_quantity)

  //     });

  //     this.cartModal.getDatabaseState().subscribe(ready => {
  //       if (ready) {
  //         this.cartModal.updateCartTable(product)
  //       }
  //     })
  //     this.GrandTotal = this.totalOrderPrice + this.DelieverCharges
  //   }

  //   return this.CartItems

  // }
  getCurrencyDetail() {
    this.currencySymbol = this.gsp.currencySymbol

    if (!this.currencySymbol || this.currencySymbol.length === 0) {
      let _appConfig = this.serviceManager.getFromLocalStorage('isAppConfigs')
      if (_appConfig) {
        if (_appConfig && _appConfig.currency) {
          this.gsp.currencySymbol = _appConfig.currency
          this.gsp.distance_unit = _appConfig.distance_unit

        }
      }
    }
  }
  calculatePrice() {

    let numberOfItemAddedCart = 0
    let totalOrderPrice = 0.0
    this.CartItems.forEach(item => {
      if (item.isChecked) {
        totalOrderPrice += Number(item.p_price * item.p_quantity)
      }

      if (item.isAddedtoCart) numberOfItemAddedCart += 1
      return item.isAddedtoCart = true
    });

    return totalOrderPrice
  }
}
