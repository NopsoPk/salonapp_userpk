import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalonBeautyTrendsPage } from './salon-beauty-trends';

@NgModule({
  declarations: [
    SalonBeautyTrendsPage,
  ],
  imports: [
    IonicPageModule.forChild(SalonBeautyTrendsPage),
  ],
})
export class SalonBeautyTrendsPageModule {}
