import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';

import { config } from './../../providers/config/config';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { Salon, Services, SALON_SERVICES_SEARCH, Customer, OFFERS } from './../../providers/SalonAppUser-Interface';
/**
 * Generated class for the AllOffersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-all-offers',
  templateUrl: 'all-offers.html',
})
export class AllOffersPage {
  public offersForListing:OFFERS[];
  PinRelatedSalons: any
  public salonImagesURL = config.salonImgUrl;
  servicesSortedByCat:any
  public sal_name=""
  public sal_pic=""
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public nativePageTransitions: NativePageTransitions,
    public platform: Platform,
  ) {
    this.PinRelatedSalons=[]
    this.servicesSortedByCat=[2,3,5,6,7,8,8,79,70,70,70,2,3,5,6,7,8,8,79,70,70,90]
    
    // let item={item: 1}

    // this.servicesSortedByCat.push(item)
    // this.servicesSortedByCat.push(item)
    // this.servicesSortedByCat.push(item)
    // this.servicesSortedByCat.push(item)


  }

  ionViewDidLoad() {
    this.offersForListing = this.navParams.get('offersForListing')
    this.sal_pic=this.navParams.get('sal_pic')
    this.sal_name=this.navParams.get('sal_name')
    this.offersForListing.forEach(element => {
      console.log('element',JSON.stringify(element))
  });
  
  }

  close() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };


    if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(AllOffersPage)
    }
  }

}
