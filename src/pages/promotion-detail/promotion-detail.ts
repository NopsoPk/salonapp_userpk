import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { PROMOTIONS } from '../../providers/SalonAppUser-Interface';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the PromotionDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-promotion-detail',
  templateUrl: 'promotion-detail.html',
})
export class PromotionDetailPage {
  public promotions: PROMOTIONS[]
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public serviceManager: GlobalProvider,
    public viewCtrl: ViewController
  ) {
    this.promotions = this.navParams.get('promotions')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PromotionDetailPage');
  }
  getFormattedDate(o_datetime) {
    return this.serviceManager.getFormattedTimeForMyorder(o_datetime)
  }
  closemodal() {
    this.viewCtrl.dismiss();

  }

  promotionTapped(selectedPromotions: PROMOTIONS) {
    this.viewCtrl.dismiss(selectedPromotions)
  }
}
