import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LatestTrensTipsCategoryPage } from './latest-trens-tips-category';

@NgModule({
  declarations: [
    LatestTrensTipsCategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(LatestTrensTipsCategoryPage),
  ],
})
export class LatestTrensTipsCategoryPageModule {}
