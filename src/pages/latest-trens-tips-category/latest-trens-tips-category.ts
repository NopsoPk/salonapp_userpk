import { SqliteDbProvider } from './../../providers/sqlite-db/sqlite-db';
import { Salon, PINTEREST_OBJECT, Customer, CATEGORY_SUB_STYLE } from './../../providers/SalonAppUser-Interface';
import { PinActionSheetPage } from './../pin-action-sheet/pin-action-sheet';
import { Component } from '@angular/core';
import { IonicPage, LoadingController, NavController, NavParams, Platform, Alert, Events } from 'ionic-angular';

import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalServiceProvider } from '../../providers/global-service/global-service'

import { LatestTrensTipsDetailsPage } from '../latest-trens-tips-details/latest-trens-tips-details'
import { HomePage } from '../home/home'

import { Storage } from '@ionic/storage';
import { GlobalProvider } from '../../providers/global/global'
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { config } from "../../providers/config/config";
import { PopoverController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { FavoriteModal } from '../../providers/FavoriteModal/FavoriteModal';
import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { MainHomePage } from '../main-home/main-home';
import { GoogleAnalytics } from '../../../node_modules/@ionic-native/google-analytics';
import { SalonServicesModal } from '../../providers/salon-services-modal/salon-services-modal';
import { Content } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { AppMessages } from '../../providers/AppMessages/AppMessages';

@IonicPage()
@Component({
  selector: 'page-latest-trens-tips-category',
  templateUrl: 'latest-trens-tips-category.html',
})
export class LatestTrensTipsCategoryPage {
  @ViewChild(Content) content: Content;
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  public categorySubStyle: CATEGORY_SUB_STYLE[]
  public getPostsForKeyWordURL
  public postsArray = Array();
  public cateogry = "";
  public noPostsFound = false;
  public clearPostsAfterIntervval = 30 //minutes
  public infiniteScroll: any
  public myCustomer: Customer
  public PaginationLinks: any
  public imageUrl = config.CategoryImageURL
  page = 0;
  maximumPages = 20
  public selectedSubCategory
  public selectedPost: PINTEREST_OBJECT
  public imageUrlKeyWord: any //for showing keyword tap search 
  constructor(public http: Http,
    public globalSearch: GlobalServiceProvider,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public platform: Platform,
    public SM: GlobalProvider,
    public popoverCtrl: PopoverController,
    public modalCtrl: ModalController,
    public database: SqliteDbProvider,
    public favPostModal: FavoriteModal,
    public customerModal: CustomerModal,
    public salonServiceModal: SalonServicesModal,
    public events: Events,
    private ga: GoogleAnalytics,
    private nativePageTransitions: NativePageTransitions,
    public serviceManager: GlobalProvider,
  ) {
    // alert('page-2nd');
    this.selectedSubCategory = this.navParams.get('subCat');
    this.imageUrlKeyWord = this.navParams.get('imageUrlKeyWord');
    this.events.subscribe('sendCustomer', customer => {
      if (customer) {
        this.myCustomer = customer
      }
    })
    this.events.publish('getCustomer')
  }
  ionViewDidLoad() {
    // this.postsArray = this.SM.getFromLocalStorage('postsArray')
    // this.cateogry = "test"
    // this.SM.setInLocalStorage('postsArray',this.postsArray)

    this.getPostsForKeyWordURL = this.navParams.get('getPinsForSubStyleURL')
    console.log("LinKs", this.getPostsForKeyWordURL)
    if (this.getPostsForKeyWordURL) {
      this.cateogry = this.navParams.get('cateogry')
      this.getPinsForSubStyle(this.getPostsForKeyWordURL)

    } else {

      //serach by ssc_id
      //fetch from local databse if exist
      //of yes then in back ground call the post from server and save to local database if there are new post added after last fetch date
      //go to server and save if no posts exit in loacl side
      this.customerModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.customerModal.getCustomer().then(customer => {
            this.myCustomer = customer
            //fetch from local side first then go to server
            let cat = this.navParams.get('subCat');

            this.salonServiceModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                //get last fetch date




                this.categorySubStyle = []
                //old search
                // this.salonServicesModal.getKeyWordsOnlyForLatestTrendsSearch(searchValue).then(res => {
                this.salonServiceModal.getCategorySubStyle(cat.ssc_id, 0,1).then(res => {
                  if (res && res.length > 0) {
                    this.categorySubStyle = res
                    //show post from local databse
                    if (this.infiniteScroll) {
                      this.infiniteScroll.enable(true);
                    }
                    let categorySubStyleServer: CATEGORY_SUB_STYLE[] = []
                    this.categorySubStyle = res
                    this.postsArray = res;
                    this.noPostsFound = false
                    this.salonServiceModal.getCategorySubStyleMaxDate().then(dateMax => {
                      this.getFreshPostsFromServerAndSaveInBackground(dateMax);

                    })
                    // this.PaginationLinks = res.links
                    //end show post from local database
                  } else {
                    this.getFreshPostsFromServer();
                    //got to server
                  }
                }, error => {
                })
              }
            })
            //late add in to else above
            // this.getFreshPostsFromServer()
          })
        }
      })
    }
  }
  ionViewWillLeave() {
    this.content.scrollToTop();
  }
  getPinsForSubStyle(getPinsForSubStyleURL: string) {

    let loading = this.loadingController.create({ content: "Please Wait..." });
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');



    let options = new RequestOptions({ headers: headers });
    loading.present();
    this.http.get(getPinsForSubStyleURL, options)
      .map(Response => Response.json())
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe(res => {
        loading.dismiss()

        if (res.data.posts.length > 0) {

          this.postsArray = res.data.posts;
          this.noPostsFound = false
          this.SM.setInLocalStorage('postsArray', this.postsArray)
          this.PaginationLinks = res.links
          if (this.infiniteScroll) {
            this.infiniteScroll.enable(true);
          }

        } else {
          this.noPostsFound = true
        }
      }, error2 => {
        this.noPostsFound = true
        loading.dismiss()
        this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
        if (this.infiniteScroll) {
          this.infiniteScroll.enable(false);
          this.PaginationLinks = null
        }

      }
      )
  }
  getFreshPostsFromServerAndSaveInBackground(dateTime) {

    let cat = this.navParams.get('subCat');
    this.cateogry = cat.ssc_name;
    let tagURL = config.getPostForSubCategory + cat.ssc_id + '/' + this.myCustomer.cust_id + '/' + dateTime;
    console.log('URL', tagURL)
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    let options = new RequestOptions({ headers: headers });

    // Getting Posts from server 
    this.http.get(tagURL, options)
      .map(Response => Response.json())
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(res => {

        if (res.data.posts != undefined && res.data.posts.length > 0) {

          let categorySubStyleServer: CATEGORY_SUB_STYLE[] = []
          categorySubStyleServer = res.data.posts
          //save to local database
          this.salonServiceModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.salonServiceModal.saveIntoCategorySubStyle(categorySubStyleServer).then(categoryInsertedSuccessfully => {

              })
            }
          })
          //end save to local database

        } else {

        }
      }, error2 => {

        this.SM.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
      }
      )
  }
  getFreshPostsFromServer() {
    let cat = this.navParams.get('subCat');
    this.cateogry = cat.ssc_name;

    let tagURL = config.getPostForSubCategory + cat.ssc_id + '/' + this.myCustomer.cust_id;
    let loading = this.loadingController.create({ content: "Please Wait..." });
    loading.present();
    console.log('URL:', tagURL)

    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    let options = new RequestOptions({ headers: headers });

    // Getting Posts from server 
    this.http.get(tagURL, options)
      .map(Response => Response.json())
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(res => {

        loading.dismiss()
        if (res.data.posts != undefined && res.data.posts.length > 0) {
          if (this.infiniteScroll) {
            this.infiniteScroll.enable(true);
          }

          let categorySubStyleServer: CATEGORY_SUB_STYLE[] = []
          categorySubStyleServer = res.data.posts
          //save to local database
          this.salonServiceModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.salonServiceModal.saveIntoCategorySubStyle(categorySubStyleServer).then(categoryInsertedSuccessfully => {

              })
            }
          })
          //end save to local database

          this.postsArray = res.data.posts;
          this.noPostsFound = false
          this.PaginationLinks = res.links
        } else {

          this.noPostsFound = true
          if (this.infiniteScroll) {
            this.infiniteScroll.enable(false);
            this.PaginationLinks = null
          }
        }
      }, error2 => {
        loading.dismiss()
        // this.SM.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          this.SM.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
      }
      )
  }
  getPostsFromServer() {
    let cat = this.navParams.get('subCat');
    this.cateogry = cat.ssc_name;

    //checking if first 20 chunk of data is in local storage then show that to user
    let postsKey = 'postsFor/' + cat.ssc_id
    let paginateKey = 'paginationFor/' + cat.ssc_id
    let startTimeKey = 'postsSavedTimeFor/' + cat.ssc_id

    let _StartTime = this.SM.getFromLocalStorage(startTimeKey);

    if (_StartTime && (new Date().getTime() / (1000 * 60) - _StartTime) < this.clearPostsAfterIntervval) { // start < 20 minutes

      let tempPosts: any = this.SM.getFromLocalStorage(postsKey);
      if (tempPosts && tempPosts !== this.SM.NO_POST_FOR_SUBSTYLE_FOUND) {


        let paginationss: any = this.SM.getFromLocalStorage(paginateKey);
        this.postsArray = tempPosts
        this.PaginationLinks = paginationss
        this.SM.setInLocalStorage(postsKey, this.postsArray)
        this.SM.setInLocalStorage(paginateKey, this.PaginationLinks)
        !this.postsArray || this.postsArray.length === 0 ? this.noPostsFound = true : this.noPostsFound = false

      } else {

        this.postsArray = []
        this.noPostsFound = true
        if (this.infiniteScroll) {
          this.infiniteScroll.enable(false)
        }

      }


    } else {
      let tagURL = config.getPostForSubCategory + cat.ssc_id + '/' + this.myCustomer.cust_id;

      let loading = this.loadingController.create({ content: "Please Wait..." });
      loading.present();

      var headers = new Headers();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json');
      headers.append('Access-Control-Allow-Origin', '*');

      let options = new RequestOptions({ headers: headers });

      // Getting Posts from server 
      this.http.get(tagURL, options)
        .map(Response => Response.json())
        .retryWhen((err) => {
          return err.scan((retryCount) => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            }
            else {
              throw (err);
            }
          }, 0).delay(1000)
        })

        .subscribe(res => {

          loading.dismiss()
          if (res.data.posts != undefined && res.data.posts.length > 0) {
            if (this.infiniteScroll) {
              this.infiniteScroll.enable(true);
            }
            // if () {
            this.postsArray = res.data.posts;
            this.SM.setInLocalStorage('postsArray', this.postsArray)
            this.PaginationLinks = res.links
            this.SM.setInLocalStorage(postsKey, this.postsArray)
            this.SM.setInLocalStorage(paginateKey, this.PaginationLinks)
            this.SM.setInLocalStorage(startTimeKey, new Date().getTime() / (1000 * 60))

            !this.postsArray || this.postsArray.length === 0 ? this.noPostsFound = true : this.noPostsFound = false
            // }

          } else {
            !this.postsArray || this.postsArray.length === 0 ? this.noPostsFound = true : this.noPostsFound = false
            this.SM.setInLocalStorage(postsKey, this.SM.NO_POST_FOR_SUBSTYLE_FOUND)
            this.SM.setInLocalStorage(paginateKey, this.SM.NO_POST_FOR_SUBSTYLE_FOUND)
            this.SM.setInLocalStorage(startTimeKey, new Date().getTime() / (1000 * 60))

            if (this.infiniteScroll) {
              this.infiniteScroll.enable(false);
              this.PaginationLinks = null
            }
          }

        }, error2 => {
          loading.dismiss()
          !this.postsArray || this.postsArray.length === 0 ? this.noPostsFound = true : this.noPostsFound = false
          this.SM.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

        }
        )
    }
  }
  viewSalonsWork(index) {
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(LatestTrensTipsDetailsPage, {
      selectedPost: this.postsArray[index]
    });
  }
  viewSalons(index) {

    let ip = this.postsArray[index];

    this.storage.get(this.SM.CUSTOMER_DATA).then((value) => {
      this.nativePageTransitions.slide(this.options)

      this.navCtrl.push(HomePage, {

        url: config.getSalonsByPCodeURL + ip['ip_id'] + '/' + value.cust_lat + '/' + value.cust_lng


      })
    }).catch((Error) => {
    });
  }

  onImageLoad(ev) {
  }
  goBack() {
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop({ animate: true, direction: "back" });
    } else {
      //set root page
      this.navCtrl.setRoot(MainHomePage)
    }

  }
  postImageTapped(post, $event) {
    // alert('post-tapped')

    let ssc_id = post.ssc_id
    let postObject: PINTEREST_OBJECT = post
    this.selectedPost = postObject
    this.googleAnalytics()
    var PinRelatedSalon: Salon[] = []
    let requiredSalonIDs = ''
    let SSCID = -1
    if (this.selectedSubCategory) {
      SSCID = this.selectedSubCategory.ssc_id
    } else {
      SSCID = postObject.ssc_id
    }
    this.database.getDatabaseState().subscribe(ready => {
      if (ready) {

        this.database.getSalSerSubCatFromSqliteDB(ssc_id).then(allSalonIDs => {

          allSalonIDs.forEach(element => {
            requiredSalonIDs += element.sal_id + ',';
          });
          requiredSalonIDs = requiredSalonIDs.slice(0, requiredSalonIDs.length - 1)
          if (requiredSalonIDs.length > 0) {
            // this.databse.getDatabaseState().subscribe(ready => {
            //   if (ready) {
            this.database.getSalonsListingFromSqliteDB(requiredSalonIDs).then(res => {
              if (res && res.length > 0) {
                PinRelatedSalon = res
                // PinRelatedSalon = PinRelatedSalon.slice(0, 5)
                this.salonServiceModal.getDatabaseState().subscribe(ready => {
                  if (ready) {


                    PinRelatedSalon.forEach(salon => {
                      this.salonServiceModal.getSalonServicesForSubCategory(salon.sal_id, SSCID).then(pinRelatedSers => {
                        return salon['pinRelatedServices'] = pinRelatedSers
                      })
                    });
                    this.nativePageTransitions.slide(this.options)
                    this.navCtrl.push(PinActionSheetPage, {
                      PinRelatedSalon: PinRelatedSalon,
                      cateogry: this.cateogry,
                      objPin: postObject,

                    })
                  }
                })
                // if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

                // const modal = this.modalCtrl.create(PinActionSheetPage, {
                //   PinRelatedSalon: PinRelatedSalon,
                //   cateogry: this.cateogry,
                //   objPin: postObject,
                // }, { cssClass: 'PopOverClass' });
                // modal.present()
              }
              // else {
              //   this.SM.makeToastOnFailure('No salon is associated with this style', 'top')
              // }
            })
          }
        })
      }
    })

    /** old code till friday july 2018 
    // Fetching salnos from local Storage 
    let allSalons: Salon[] = this.SM.getFromLocalStorage(this.SM.SALONS_NEAR_CUSTOMER)
    let AssociatedSalon: SALON_SERVICES_SUB_CATEGORIES[] = this.SM.getFromLocalStorage(this.SM.NEAREST_SALON_ASSOCIATED_WITH_PIN)
    if (allSalons && AssociatedSalon && allSalons.length > 0 && AssociatedSalon.length > 0) {
      //  Filtering salon on the basis of styles ID from All Salons 
      let salonFilteredOnSylesID = AssociatedSalon.filter(serv => serv.ssc_id === postObject.ssc_id)
      salonFilteredOnSylesID.forEach(_filterSalon => {
        let filterSalon: SALON_SERVICES_SUB_CATEGORIES = _filterSalon
        let requiredSalon = allSalons.filter(allSalon => Number(allSalon.sal_id) === Number(filterSalon.sal_id))
        if (requiredSalon && requiredSalon.length > 0) {
          PinRelatedSalon.push(requiredSalon[0])
        }
      });
    }
    if (PinRelatedSalon && PinRelatedSalon.length > 0) {
      // let popover = this.popoverCtrl.create(PinActionSheetPage, {
      //   PinRelatedSalon: PinRelatedSalon.concat(PinRelatedSalon),
      //   cateogry: this.cateogry,
      //   objPin: postObject,
      // }, { cssClass: 'PopOverClass' });
      // popover.present({
      //   ev: objPin,
      // });
    } else {
      // let salon = this.sqlProvider.fetch_salon_table()
      // let _mySalon: Salon[] = this.SM.getFromLocalStorage(this.SM.SALONS_NEAR_CUSTOMER)
      // let myAllSalons: Salon[]
      // if (_mySalon) {
      //   myAllSalons = _mySalon
      // }
      // this.navCtrl.push(HomePage, {
      //   allSalon: myAllSalons,
      //   url: config.GetSalonFromPinURL + objPin['ip_id'] + "/" + objPin['ssc_id'],
      // });
    }
    // return
    // if (objPin) {
    //   this.SM.setInLocalStorage(this.SM.OBJ_PIN, objPin)
    // }
    // // let source = data.source_name.replace('#', '');
    // this.nativePageTransitions.slide(this.options)
    // if (objPin.sal_id != 0) {
    //   let salon = {
    //     sal_id: objPin.sal_id
    //   }
    //   this.navCtrl.push(SalonDetailsPage, { selectedSalon: salon });
    // } else {
    //   this.navCtrl.push(HomePage, {
    //     url: config.GetSalonFromPinURL + objPin['ip_id'] + "/" + objPin['ssc_id']
    //   });
    // }
    */
  }
  pinKeyWordTapped(objSubCat) {

    let baseURLTogetPinsForSubStyle = config.getPinsForSubStyleURL + objSubCat.p_keywords + '/' + this.myCustomer.cust_id

    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(LatestTrensTipsCategoryPage, {
      getPinsForSubStyleURL: baseURLTogetPinsForSubStyle,
      cateogry: objSubCat.p_keywords,
      imageUrlKeyWord: objSubCat.imageUrl,
    });


    // let keyword: string = objSubCat.p_keywords
    // if (keyword.trim().length === 0) {
    //   return
    // }
    // let obj = this.SM.getFromLocalStorage(this.SM.OBJ_SUB_STYLE)
    // if (obj) {
    //   let FilterPinsBaseURL = config.GetFilteredPinsURL
    //   FilterPinsBaseURL += keyword + '/' + obj.ssc_id + '/' + this.myCustomer.cust_id
    //   let loading = this.loadingController.create({ content: "Please Wait..." });
    //   loading.present();
    //   var headers = new Headers();
    //   headers.append("Accept", 'application/json');
    //   headers.append('Content-Type', 'application/json');
    //   headers.append('Access-Control-Allow-Origin', '*');
    //   let options = new RequestOptions({ headers: headers });
    //   this.http.get(FilterPinsBaseURL, options)
    //     .map(Response => Response.json())
    //     .retryWhen((err) => {
    //       return err.scan((retryCount) => {
    //         retryCount += 1;
    //         if (retryCount < 3) {
    //           return retryCount;
    //         }
    //         else {
    //           throw (err);
    //         }
    //       }, 0).delay(1000)
    //     })
    //     .subscribe(res => {
    //       loading.dismiss()
    //       if (res.data.posts != undefined && res.data.posts.length > 0) {
    //         if (this.infiniteScroll) {
    //           this.infiniteScroll.enable(true);
    //         }
    //         this.postsArray = res.data.posts;
    //         this.PaginationLinks = res.links
    //         this.noPostsFound = false
    //       } else {
    //         if (this.infiniteScroll) {
    //           this.infiniteScroll.enable(false);
    //           this.PaginationLinks = null
    //         }
    //       }
    //     }, error => {
    //       loading.dismiss()
    // this.SM.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
    //     })
    // }
  }
  loadMore(infiniteScroll) {
    this.infiniteScroll = infiniteScroll
    this.page++;

    if (this.PaginationLinks === undefined || this.PaginationLinks.next === undefined || this.PaginationLinks.next === null) {
      infiniteScroll.enable(false);
      return
    }

    this.loadMorePosts(infiniteScroll)

    if (this.page === this.maximumPages) {
      infiniteScroll.enable(false);
    }
  }
  loadMorePosts(infiniteScroll) {

    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');

    let options = new RequestOptions({ headers: headers });
    this.http.get(this.PaginationLinks.next, options)
      .map(Response => Response.json())
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe(res => {

        if (res.data.posts.length > 0) {
          this.postsArray = this.postsArray.concat(res.data.posts)
          this.PaginationLinks = res.links

          if (infiniteScroll) {
            try {
              infiniteScroll.complete();
            } catch (ex) {

            }
          }
        }
      }, error => {

        
        infiniteScroll.enable(false);
      }
      )

  }
  btnBackTapped() {

    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };

    if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop({
        animate: false,
        animation: 'ios-transition',
        direction: 'back',
        duration: 500
      })
      // this.navCtrl.pop({ animate: true, direction: "back" });  
    } else {
      //set root page
      this.navCtrl.setRoot(MainHomePage)
    }
  }
  updateFavLocally(postStatus, ssc_id) {

    if (postStatus == "0" || postStatus === 0) {
      postStatus = "1";
    } else {
      postStatus = "0";
    }

    this.favPostModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServiceModal.updateFavPost(postStatus, ssc_id).then(isSalonMarkedFavorite => {
          if (isSalonMarkedFavorite) {

          }
        })
      }
    })

  }
  makePostFavoriteTapped(post: PINTEREST_OBJECT) {
    let myPost: PINTEREST_OBJECT = post
    let favoPostURL = config.makePostFavoriteURL
    // postiD/CustID/favUnFav
    this.updateFavLocally(myPost.favpost, myPost.ssc_id);
    let makePostFavorite: number
    myPost.favpost === 1 ? makePostFavorite = 0 : makePostFavorite = 1
    this.postsArray.forEach(post => {
      if (post.ip_id === myPost.ip_id) {
        return myPost.favpost = makePostFavorite
      }
    });
    favoPostURL += myPost.ip_id + '/' + this.myCustomer.cust_id + '/' + makePostFavorite
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');

    let options = new RequestOptions({ headers: headers });

    this.http.get(favoPostURL, options)
      .map(Response => Response.json())
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(res => {
        if (res.status === 1 || res.status === '1') {
          if (makePostFavorite === 1) {
            this.favPostModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.favPostModal.InsertInTopostsTable(post).then(isSalonMarkedFavorite => {
                  if (isSalonMarkedFavorite) {

                  }
                })
              }
            })
          } else if (makePostFavorite === 0) {
            this.favPostModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.favPostModal.deletePost(post.ip_id).then(isSalonMarkedFavorite => {
                  if (isSalonMarkedFavorite) {

                  }
                })
              }
            })
          }
        }
      })
  }
  isFav(fav) {

    if (fav === 1) {
      return true;
    } else if (fav == "1") {
      return true;
    } else {
      return false;
    }
  }
  googleAnalytics() {

    // this.ga.trackView(this.myCustomer.cust_name + ' visited Salon Detail Page');
    let eventAction = this.myCustomer.cust_name + ' Tapped on post id ' + this.selectedPost.ip_id + ' in ' + this.selectedPost.ssc_id + '-' + this.cateogry
    this.ga.trackEvent('Tap', eventAction, 'lab: general label', 200, true)
    this.ga.trackTiming('cat: tracking timing', 600000, 'variable: not sure what will go here', 'label: and the same')
    this.ga.debugMode()
    this.ga.setAllowIDFACollection(true)
    this.ga.setUserId(this.myCustomer.cust_id)

  }
}
