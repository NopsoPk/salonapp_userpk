import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { MainHomePage } from '../main-home/main-home';
import { ProductsPage } from '../products/products';
import { GlobalProvider } from '../../providers/global/global';
import { Platform } from 'ionic-angular/platform/platform';



@IonicPage()
@Component({
  selector: 'page-thank-you',
  templateUrl: 'thank-you.html',
})
export class ThankYouPage {
   public response:any
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  public orderId = ''
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private nativePageTransitions: NativePageTransitions,
    public serviceManager: GlobalProvider,
    public platform: Platform,
  ) {
    let res = this.navParams.get('res')
    this.response = res
    if (res && res.o_id) {
      this.orderId = res.o_id
      console.log(this.response);
    }
  }

  ionViewWillEnter() {

    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ThankYouPage');
  }
  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };


    if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

    if (this.navCtrl.canGoBack()) {

      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
  }

  onBtnContinueShoppingTapped() {
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.setRoot(ProductsPage)
  }
  getFormattedDate(o_datetime) {
    return this.serviceManager.getFormattedTimeForMyorder(o_datetime)
  }
}
