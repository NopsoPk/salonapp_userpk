import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LatestFashionCollectionDetailPage } from './latest-fashion-collection-detail';

@NgModule({
  declarations: [
    LatestFashionCollectionDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(LatestFashionCollectionDetailPage),
  ],
})
export class LatestFashionCollectionDetailPageModule {}
