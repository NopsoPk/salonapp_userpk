import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions';
import { Salon, Services, Customer, PINTEREST_OBJECT } from './../../providers/SalonAppUser-Interface';
import { GlobalProvider } from './../../providers/global/global';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AppMessages } from '../../providers/AppMessages/AppMessages';


@IonicPage()
@Component({
  selector: 'page-request-appointment',
  templateUrl: 'request-appointment.html',
})
export class RequestAppointmentPage {

  Backoptions: NativeTransitionOptions = {
    direction: 'right',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 0
  };

  public app_details: string
  public service: any
  public SalonServices: Services[]
  public selectedServices: string;
  public selectedSalon: Salon
  public customer: Customer
  public objPin: PINTEREST_OBJECT

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public serviceManager: GlobalProvider,
    
    public nativePageTransitions: NativePageTransitions

  ) {
    this.SalonServices = []
    this.selectedServices = ''
    // this.gaming = 'sser_name'
  }

  ionViewDidLoad() {

    let salons: Salon[] = this.serviceManager.getFromLocalStorage(this.serviceManager.SALONS_NEAR_CUSTOMER)
    this.selectedSalon = this.navParams.get('selectedSalon')
    
    let pin = this.navParams.get('objPin')
    if (pin) {
      this.objPin = pin
    }
    this.customer = this.serviceManager.getFromLocalStorage(this.serviceManager.CUSTOMER_DATA)
    if (!this.customer) {
      // this.navCtrl.setPages = ActivateSalonPage
    }


    this.getSalonDetail()

    this.SalonServices = this.serviceManager.getFromLocalStorage('sservies')

  }
  getSalonDetail() {
    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
      service: btoa('get_salon_details'),
      sal_id: btoa(this.selectedSalon.sal_id),
    }
    this.serviceManager.showProgress()
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe((response) => {
        this.serviceManager.stopProgress()
        this.SalonServices = response.sal_services
        console.log(this.SalonServices);
        this.serviceManager.setInLocalStorage('sservies', this.SalonServices)

      });
  }
  serviceTapped(service) {
    
    console.log(this.service);

  }
  btnOkTapped() {

    this.selectedServices = ''
    let selectedServices: Services[] = this.service
    selectedServices.forEach(_service => {
      let service: Services = _service
      this.selectedServices += service.sser_name + ','
    });
    this.selectedServices = this.selectedServices.slice(0, this.selectedServices.length - 1)
  }
  RequestAppointment() {
    if (this.selectedServices.trim().length === 0) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgSelectSalonServicesOneOrMore)
      return
    }
    let pinID = ''
    if (this.objPin) {
      pinID = this.objPin.ip_id.toString()
    }
    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
      service: btoa('request_appointment'),
      sal_id: btoa(this.selectedSalon.sal_id),
      cust_id: btoa(this.customer.cust_id),
      pin_id: btoa(pinID),
      app_details: btoa(this.app_details),
      app_services: btoa(this.selectedServices),
      app_user_info: btoa('app_name:Salon App User Ionic,version: 1.1.5, Build 38'), // version info build,api,version etc
    }
    this.serviceManager.showProgress()
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((res) => {
        if (res) {
          this.serviceManager.stopProgress()
          if (res['status'] === '1') {
            this.serviceManager.makeToastOnSuccess(AppMessages.msgAppointmentRequest)
            this.nativePageTransitions.slide(this.Backoptions)
            this.navCtrl.popToRoot()
          }
        }
      },
        error => {
          this.serviceManager.stopProgress()
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

        }
      );

  }


  btnBackTapped() {



    this.nativePageTransitions.slide(this.Backoptions)

    this.navCtrl.pop({
      animate: false,
      animation: 'ios-transition',
      direction: 'back',
      duration: 500,
    })
    // console.log(this.navCtrl.getPrevious().name);

    // this.navCtrl.pop({ animate: true, direction: 'back' })

  }
}
