import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { config } from '../../providers/config/config';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { MyOrdersPage } from '../my-orders/my-orders';
import { GlobalServiceProvider } from '../../providers/global-service/global-service';
import { GlobalProvider } from '../../providers/global/global';
import { PRODUCT, Customer } from '../../providers/SalonAppUser-Interface';
import { ProductModal } from '../../providers/ProductModal/ProductModal';
import { ProductdetailPage } from '../productdetail/productdetail';
import { MainHomePage } from '../main-home/main-home';
import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { MenuController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-order-detail',
  templateUrl: 'order-detail.html',
})
export class OrderDetailPage {
  public myCustomer: Customer
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  orderStatus = {
    Pending: 'Pending',
    Placed: 'Placed',
    Processing: 'Processing',
    Cancelled: 'Cancelled',
    Dispatched: 'Dispatched',
    Delivered: 'Delivered',
    Failed: 'Failed'
  }


  public productsImageURL = config.prouductImages
  public OrderId = '';
  public OrderDetail = []
  public myOrdersTemp = []
  public currencySymbol = ''
  public selectedOrder: any
  CartItems = []
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private nativePageTransitions: NativePageTransitions,
    public gsp: GlobalServiceProvider,
    public serviceManager: GlobalProvider,
    public productModal: ProductModal,
    public platform: Platform,
    public customerModal: CustomerModal,
    private menu: MenuController,
  ) {
    let _OrderDetail = this.navParams.get('order_detail')
    if (_OrderDetail) {
      this.selectedOrder = _OrderDetail
      console.log(this.selectedOrder);

      this.OrderDetail = _OrderDetail.order_items

    }
  }

  ionViewWillEnter() {
    this.getCustomer()
    this.getCurrencyDetail()


    // alert(this.OrderDetail.length)
    // this.OrderId = this.OrderDetail[0].od_id;
    // console.log(JSON.stringify(this.OrderDetail))

  }

  getCheked(cartItem, index) {

  }

  btnBackTapped() {

    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };
    if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MyOrdersPage)
    }

    // let options: NativeTransitionOptions = {
    //   direction: 'right',
    //   duration: 500,
    //   slowdownfactor: 3,
    //   slidePixels: 20,
    //   iosdelay: 100,
    //   androiddelay: 150,
    //   fixedPixelsTop: 0,
    //   fixedPixelsBottom: 0
    // };
    // if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
    // this.appCtrl.getRootNav().setRoot(MainHomePage)



  }
  getCurrencyDetail() {
    this.currencySymbol = this.gsp.currencySymbol

    if (!this.currencySymbol || this.currencySymbol.length === 0) {
      let _appConfig = this.serviceManager.getFromLocalStorage('isAppConfigs')

      if (_appConfig && _appConfig.currency) {
        this.gsp.currencySymbol = _appConfig.currency
        this.gsp.distance_unit = _appConfig.distance_unit
        this.currencySymbol = _appConfig.currency

      } else {
        // alert('got an issue')
      }

    }
  }
  showProductDetailTapped(cartItem: PRODUCT, index) {

    this.productModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.productModal.getProductDetail(cartItem.p_id).then(product => {

          this.nativePageTransitions.slide(this.options)
          this.navCtrl.push(ProductdetailPage, {
            product: product,
            isFromReviewOrder: true
          })
        })
      }
    })
  }
  
  public unregisterBackButtonAction: any;
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    // this.navCtrl.pop();
    if(this.menu.isOpen()){
      this.menu.close();
    }else {
       if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
    }
  }
  //end custom back button for android

  ionViewDidLoad() {
    this.initializeBackButtonCustomHandler();
    console.log('ionViewDidLoad OrderDetailPage');
  }
  getCustomer() {

    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(customer => {
          if (customer) {

            this.myCustomer = customer

          }
        })
      }
    })
  }
  roundAmound(amount: number) {
    return Math.round(amount)
  }
}
