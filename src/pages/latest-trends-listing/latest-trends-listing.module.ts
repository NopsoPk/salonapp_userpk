import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LatestTrendsListingPage } from './latest-trends-listing';

@NgModule({
  declarations: [
    LatestTrendsListingPage,
  ],
  imports: [
    IonicPageModule.forChild(LatestTrendsListingPage),
  ],
})
export class LatestTrendsListingPageModule {}
