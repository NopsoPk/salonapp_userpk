import { CustomerModal } from './../../providers/CustomerModal/CustomerModal';
import { SalonServicesModal } from './../../providers/salon-services-modal/salon-services-modal';
import { GlobalProvider } from './../../providers/global/global';
import { Http } from '@angular/http';
import { GlobalServiceProvider } from '../../providers/global-service/global-service'
import { FavouriteBeautyTrendsPage } from './../favourite-beauty-trends/favourite-beauty-trends';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db'
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { GoogleAnalytics } from '../../../node_modules/@ionic-native/google-analytics';
import { IonicPage, NavController, NavParams, Navbar, Content, Events, LoadingController, PopoverController, Platform } from 'ionic-angular';
import { Component, ViewChild, NgZone } from '@angular/core';
import { BEAUTY_TIPS, TIPS_DETAIL, NOPSO_Header_OPTIONS, PRODUCT } from './../../providers/SalonAppUser-Interface';
import { config } from '../../providers/config/config';
import { MainHomePage } from './../main-home/main-home';
import { MenuController } from 'ionic-angular';
import { LatestTrendsDetailPage } from './../latest-trends-detail/latest-trends-detail';
import { timer } from 'rxjs/observable/timer';
import { LatestTrendsArchiveCollectionPage } from './../latest-trends-archive-collection/latest-trends-archive-collection';
import { FavoriteModal } from '../../providers/FavoriteModal/FavoriteModal';
import { SALON_SERVICES_SUB_CATEGORIES, LATEST_TRENDS_STYLES, SalonServices, Salon, SALON_SERVICES_SEARCH, ServiceSubCATEGORIES, Customer, Homepage, CATEGORY_SUB_STYLE, Category } from './../../providers/SalonAppUser-Interface';
import { CartModal } from '../../providers/cartModal/cartModal';
import { ViewCartPage } from '../view-cart/view-cart';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
@IonicPage()
@Component({
  selector: 'page-latest-trends-listing',
  templateUrl: 'latest-trends-listing.html',
})
export class LatestTrendsListingPage {
  //https://www.npmjs.com/package/ionic-img-viewer

  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };

  public isFavourites=false
  public reRequestCounter=0
  public imageUrl = config.CategoryImageURL
  public FashionStyFeatureItem = null;
  public FashionStyFeatureItemMen = null;
  public FashionStyFeatureItemWomen = null;
  public unregisterBackButtonAction: any;
  public LatestTrendsStyles = Array();
  public LatestTrendsStylesMale = Array();
  public LatestTrendsStylesFemale = Array();
  public LatestTrendsSubStyles = Array();
  public StylesBackup = Array();
  public myCustomer: Customer
  public cateogry = ""
  public defaultImage = 'default.png'
  public shouldShowMenTrends: boolean
  public shouldShowWomenTrends: boolean
  public GenderBasedTrends: string
  public parentPage: Homepage
  public parentPageDescription
  public SALON_SERVICE_SEARCH: SALON_SERVICES_SEARCH[]
  public ServiceSubCATEGORIE: ServiceSubCATEGORIES[]
  public selectedStyle: Category
  public noStyleFound = false
  public HeaderHeightIsZero = false
  public isAppLaunchedFirstTime = 'isAppLaunchedFirstTime'
  public archiveMenFashionNames = ""
  public archiveWoemFashionNames = ""
  public archiveFashionName = null
  public isFirstTime = false
  public shouldShowMiniHeader = false
  public twoBeautyTips: any = [];
  public slider: HTMLElement
  public nopsoHeaderOptions: NOPSO_Header_OPTIONS
  public CartItems: PRODUCT[] = []

  @ViewChild(Content) content: Content;
  @ViewChild(Navbar) navBar: Navbar;
  ionScrollTop = 0
  constructor(
    public salonServiceModal: SalonServicesModal,
    private zone: NgZone,
    public http: Http,
    public sqlProvider: SqliteDbProvider,
    public salonServicesModal: SalonServicesModal,
    public cartModal: CartModal,
    public favModel: FavoriteModal,
    public globalSearch: GlobalServiceProvider,
    private menu: MenuController,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public serviceManager: GlobalProvider,
    private nativePageTransitions: NativePageTransitions,
    public customerModal: CustomerModal,
    public ga: GoogleAnalytics,
    public popoverCtrl: PopoverController,
    public platform: Platform,
    public events: Events, ) {
    this.nopsoHeaderOptions = {
      miniHeaderHeight: '50pt',
      calledFrom: config.BeautyTipsViewPage,
    }
    this.LatestTrendsStyles = []
    this.isFirstTime = true;

  }
  //S1 app life cycle events.
  ionViewWillEnter() {
    // this.loading = this.loadingController.create({ content: "Fetching Beauty Trends, Please Wait..." });
    // this.loading.present();
    this.serviceManager.showProgress()
    // alert('progress-called')
    // this.scrollToTop();
    this.checkIfFavPosts();
    this.getCustomer();
    this.initializeBackButtonCustomHandler();
    //this.getFeatureItemFromDB()
    this.cartModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.cartModal.getCartItems().then(cartItems => {
          this.CartItems = cartItems
        })
      }
    })
  }

 


  checkIfFavPosts(){
    this.favModel.getDatabaseState().subscribe(ready => {
      if (ready) {
          this.favModel.checkIfFavFashion('3').then(res => {  
            if(res){
              this.chekIfFavImgExist();
              this.isFavourites=true
           }else{
            this.isFavourites=false
           } 
        }, error => {
        })
      }
    })
  }
  //custom back button for android
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    // this.navCtrl.pop();
    if(this.menu.isOpen()){
      this.menu.close();
    }else {
       if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
    }
  }
  //end custom back button for android
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
    let abc = false
    this.events.subscribe('canShowMiniHeader', (isHidden) => {
      this.zone.run(() => {
        this.shouldShowMiniHeader = isHidden
      });
    });
    //page refresh on comming back from detail page
    if (!this.isFirstTime) {
     

      let options: NativeTransitionOptions = {
        direction: 'top',
        duration: 500,
        slowdownfactor: 3,
        slidePixels: 20,
        iosdelay: 150,
        androiddelay: 0,
        fixedPixelsTop: 0,
        fixedPixelsBottom: 0
      };
      // this.nativePageTransitions.fade(options);
      // this.navCtrl.setRoot(LatestTrendsListingPage, {}, { animate: false });

      //this.navCtrl.setRoot(this.navCtrl.getActive().component);
    } else {
      
      this.isFirstTime = false
    }
    //End page refresh on comming back from detail page


  }
  ionViewDidLoad() {
    // this.refreshPage()
    this.content.ionScroll.subscribe(ev =>
      requestAnimationFrame(() => this.ionScrollChange(ev.scrollTop)));
  }
  //S2 server side requests.
  //S3 local db requests.
  getCustomer() {
    if (this.myCustomer && this.LatestTrendsStyles && this.LatestTrendsStyles.length > 1) {
      this.serviceManager.stopProgress()
      return
    }
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(customer => {
          if (customer) {
            // alert('customer'+JSON.stringify(customer))
            this.myCustomer = customer
            this.googleAnalytics(this.myCustomer.cust_id, this.myCustomer.cust_name)
          }else{
            let publicUserId = this.serviceManager.getFromLocalStorage(this.serviceManager.PUBLIC_USER_ID)
            if(publicUserId){
              this.googleAnalytics(publicUserId, 'public user')
            }
          } 
          if (!this.LatestTrendsStyles || this.LatestTrendsStyles.length === 0) {
            this.serviceManager.stopProgress()
            this.getLatestTrendsSTylesFromDB();
          }else{
            this.serviceManager.stopProgress()
          }

        }, error => {
        })
      }
    })
  }
  favoriteClicked(item, index){
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(FavouriteBeautyTrendsPage)
  }
  getLatestTrendsSTylesFromDB() {
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServicesModal.createServiceCategoryTable().then(res => {
          this.salonServicesModal.getServiceCategories().then(serviceCategories => {
           // alert('its here')
             if(serviceCategories.length < 1 ){
             
              timer(1500).subscribe(() => {
                if(this.reRequestCounter === 15){
                  this.serviceManager.stopProgress()
                  this.reRequestCounter += 1
                  this.serviceManager.setInLocalStorage(this.serviceManager.FETCH_DATA_AGAIN, 'fetch_local_data')
                }else{
                  this.reRequestCounter += 1
                }
                console.log('Getting-order-status-first-time')
                if(this.reRequestCounter < 16){
                  this.getLatestTrendsSTylesFromDB();
                }
               
              }) 
             }else{
              this.serviceManager.stopProgress()
             }
            // serviceCategories.forEach(category => {
            //   return category.expanded = false
            // });
            serviceCategories.forEach(category => {

              if (category.sc_gender == 2) {//gender base sorting
                if (!this.FashionStyFeatureItemWomen) {//first item should be feature
                  this.FashionStyFeatureItemWomen = category
                } else {
                  //get archive and ltest trends listing
                  //push to trend list women
                  //get archive categories too here.
                  if (category.sc_type == 2) {//it's arhive fashion
                    if (this.archiveWoemFashionNames) {
                      this.archiveWoemFashionNames = this.archiveWoemFashionNames + ',' + category.sc_name
                    } else {
                      this.archiveWoemFashionNames = category.sc_name
                    }
                  } else {//category.sc_type==1 it goes to trend list
                    this.LatestTrendsStylesFemale.push(category)
                  }



                }
                // this.FashionStyFeatureItemMen.push(category)
              } else {//for male rest is same 
                if (!this.FashionStyFeatureItemMen) {
                  this.FashionStyFeatureItemMen = category
                } else {
                  //get archive categories too here.
                  if (category.sc_type == 2) {

                    if (this.archiveMenFashionNames) {
                      this.archiveMenFashionNames = this.archiveMenFashionNames + ',' + category.sc_name
                    } else {
                      this.archiveMenFashionNames = category.sc_name
                    }
                  } else {
                    //push to trend list
                    this.LatestTrendsStylesMale.push(category)
                  }
                  //push trends list men
                }
              }

              //category.sc_gender == 2 ? this.LatestTrendsStylesFemale.push(category) : this.LatestTrendsStylesMale.push(category)
            });
            let gender = null
            if(this.myCustomer){
              gender = Number(this.myCustomer.cust_gender)
            }
            if (gender && gender === 1) {
              this.GenderBasedTrends = 'man'
              this.LatestTrendsStyles = []
              this.LatestTrendsStyles = this.LatestTrendsStylesMale
              // this.ShowMenTrends()
              this.menTrendsTapepd()

            } else {
              this.GenderBasedTrends = 'woman'
              this.LatestTrendsStyles = []
              this.LatestTrendsStyles = this.LatestTrendsStylesFemale
              this.womenTrendsTapepd()

            }
          })
        })
      }
    })
  }
  //S4 click events.
  fashionClicked() {
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(LatestTrendsDetailPage, {
      serviceCat: this.FashionStyFeatureItem
    });
  }
  categoryClicked(item, index) {
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(LatestTrendsDetailPage, {
      serviceCat: item
    });
    //if (selectedSubStyles) {
    //this.serviceManager.setInLocalStorage(this.serviceManager.OBJ_SUB_STYLE, selectedSubStyles)
    // }
  }
  archiveCategoryClicked() {
    let selectedGender;
    if (this.shouldShowMenTrends) {
      selectedGender = 1
    } else {
      selectedGender = 2
    }


    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(

      LatestTrendsArchiveCollectionPage, {
        selectedGender: selectedGender
      });


  }
  menTrendsTapepd() {
    this.slider = document.getElementById('slider_listing');
    this.slider.style.cssFloat = 'left';
    this.LatestTrendsStyles = []
    this.LatestTrendsStyles = this.LatestTrendsStylesMale;
    this.FashionStyFeatureItem = null
    this.FashionStyFeatureItem = this.FashionStyFeatureItemMen
    //clear archive
    this.archiveFashionName = null
    this.archiveFashionName = this.archiveMenFashionNames
    !this.LatestTrendsStyles || this.LatestTrendsStyles.length === 0 ? this.noStyleFound = true : this.noStyleFound = false
    this.shouldShowMenTrends = true
    this.shouldShowWomenTrends = false
  }
  womenTrendsTapepd() {
    this.slider = document.getElementById('slider_listing');
    this.slider.style.cssFloat = 'right';
    this.LatestTrendsStyles = []
    this.LatestTrendsStyles = this.LatestTrendsStylesFemale;
    this.FashionStyFeatureItem = null
    this.FashionStyFeatureItem = this.FashionStyFeatureItemWomen
    //clear archive
    this.archiveFashionName = null
    this.archiveFashionName = this.archiveWoemFashionNames


    !this.LatestTrendsStyles || this.LatestTrendsStyles.length === 0 ? this.noStyleFound = true : this.noStyleFound = false
    this.shouldShowMenTrends = false
    this.shouldShowWomenTrends = true


  }
 
  btnBackTapped() {
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
  }
  
  //S5 static code scroll etc.
  scrollToTop() {
    this.content.scrollToTop();
    let options: NativeTransitionOptions = {
      direction: 'top',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 150,
      androiddelay: 0,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };
    this.nativePageTransitions.fade(options);
  }
  btnViewCartTapped() {
    if (!this.CartItems || this.CartItems.length === 0) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgNoProductInCart)
      return
    }
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(ViewCartPage, {
      CartItems: this.CartItems,
      // selectedProduct: this.selectedProductCategory
    })
  }
  googleAnalytics(cust_id, custName) {
    
    this.ga.startTrackerWithId(config.GoogleAnalyticsAppID)
      .then(() => {
    
        this.ga.trackView(custName + ' on latest-trends screen');
        this.ga.trackEvent('cat: ionViewWill Enter', 'act: loads everytime', 'lab: general label', 200, true)
        this.ga.trackTiming('cat: tracking timing', 600000, 'variable: not sure what will go here', 'label: and the same')
        this.ga.debugMode();
        this.ga.setAllowIDFACollection(true);
        this.ga.setUserId(cust_id)
        // Tracker is ready
        // You can now track pages or set additional information such as AppVersion or UserId
      })
      .catch(e => {
        
      });
  }
  chekIfFavImgExist(){
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
          this.salonServicesModal.getFavouriteBeautyTrends("3").then(res => { 
            if(res.length<1){
              this.getFavImgFromServer(); 
           }else{
            // this.getFavImgFromServer(); 
           }
        }, error => {
        })
      }
    })
  }

 
  //fetch favourite from server
  getFavImgFromServer() {
    if (!this.myCustomer) {
      // // alert('customer ni mila')
      return 
    }
    const params = {
      service: btoa('get_favorites'),
      cft_id: btoa('3'),
      cust_id: btoa(this.myCustomer.cust_id)
    }
    console.log('params'+JSON.stringify(params))
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          console.log('responseFav'+JSON.stringify(res))
          if(res.favorites){
            this.saveToSubCat(res.favorites)
          }
          
        },
        (error) => {
        })
  }


  refreshPage(){
    //to resolve page ios app
    if(this.globalSearch.isFirstlaunch){
     this.globalSearch.isFirstlaunch=false
    this.navCtrl.setRoot(LatestTrendsListingPage)
    return
  }else {
    this.globalSearch.isFirstlaunch=true
  }
 }

  saveToSubCat(categorySubStyleServer){
    // alert('wait...'+JSON.stringify(categorySubStyleServer))
    this.salonServiceModal.getDatabaseState().subscribe(ready => {
       
      if (ready) {
            this.salonServiceModal.saveIntoCategorySubStyle(categorySubStyleServer).then(categoryInsertedSuccessfully => {
        })
      }
    })
  }

  MainCtrl($scope, $ionicScrollDelegate) {
    $scope.scrollMainToTop = function () {
      $ionicScrollDelegate.$getByHandle('mainScroll').scrollTop();
    };
    $scope.scrollSmallToTop = function () {
      $ionicScrollDelegate.$getByHandle('small').scrollTop();
    };
  }
  ionScrollChange(scrollTop) {
    this.zone.run(() => {
      this.ionScrollTop = scrollTop
    });
  }
  scrollEvent() {
  }



}
