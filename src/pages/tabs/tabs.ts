import { NavController, Events, MenuController, NavParams, LoadingController, ViewController, Platform } from 'ionic-angular';
import { Component } from '@angular/core';
import { Keyboard } from '@ionic-native/keyboard';
import { HomePage } from '../home/home';
import { AppointmentsPage } from '../appointments/appointments';
import { NotificationsPage } from '../notifications/notifications';
import { ProfilePage } from '../profile/profile';
import { Homepage, SWITCH_TO_THE_TAB } from '../../providers/SalonAppUser-Interface';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root = HomePage;
  tab2Root = AppointmentsPage;
  tab3Root = NotificationsPage;
  tab4Root = ProfilePage;
  public parentPage: Homepage
  // public paramsToTabChild: SWITCH_TO_THE_TAB[] = []
  public parentSectionName = ''
  // public shouldSwitchToAppointmentsTabs = false;
  // public shouldSwitchToNotificationsTabs = false
  public paramsToTabChild: SWITCH_TO_THE_TAB

  valueforngif = true;
  setTabIndex: number;

  constructor(
    public keyboard: Keyboard,
    public navParms: NavParams, public events: Events, public menu: MenuController) {
    this.paramsToTabChild = {

    }
    this.setTabIndex = navParms.get('opentab');
    this.parentPage = navParms.get('parentPage')

    if (this.parentPage && this.parentPage.sectionName.toLowerCase().includes('book ')) {
      let mytitleName = this.parentPage.sectionName.slice(4, this.parentPage.sectionName.length)
      this.paramsToTabChild.sectionName = mytitleName
    }

    // this.paramsToTabChild.shouldSwitchToAppointmentsTabs = navParms.get('shouldSwitchToAppointmentsTabs')

    // this.paramsToTabChild.shouldSwitchToNotificationsTabs = navParms.get('shouldSwitchToNotificationsTabs')
    // console.log(JSON.stringify(this.paramsToTabChild))
  }


  ionViewWillEnter() {
    this.menu.swipeEnable(true);
    this.events.publish('menuSide', 1);
  }

}