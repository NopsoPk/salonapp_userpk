import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { PRODUCT, SHIPPING_ADDRESS_DATA, Customer } from '../../providers/SalonAppUser-Interface';
import { GlobalProvider } from '../../providers/global/global';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { MainHomePage } from '../main-home/main-home';
import { ReviewOrderPage } from '../review-order/review-order';
import { PLACE_ORDER_PARAM } from '../../providers/params-interface/params-interface';
import { ShippingAddressModal } from '../../providers/ShippingAddressModal/ShippingAddressModal';
import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
// import { DatePicker } from '@ionic-native/date-picker';
import { WheelSelector } from '@ionic-native/wheel-selector';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { Platform } from 'ionic-angular/platform/platform';
import { MenuController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-shipping-info',
  templateUrl: 'shipping-info.html',
})
export class ShippingInfoPage {
  public unregisterBackButtonAction: any;
  pakistanCities = {
    cities: [
      { description: "Ahmadpur East" },
      { description: " Ahmed Nager Chatha" },
      { description: " Ali Khan Abad" },
      { description: " Alipur" },
      { description: " Arifwala" },
      { description: " Attock" },
      { description: " Bhera" },
      { description: " Bhalwal" },
      { description: " Bahawalnagar" },
      { description: " Bahawalpur" },
      { description: " Bhakkar" },
      { description: " Burewala" },
      { description: " Chillianwala" },
      { description: " Choa Saidanshah" },
      { description: " Chakwal" },
      { description: " Chak Jhumra" },
      { description: " Chichawatni" },
      { description: " Chiniot" },
      { description: " Chishtian" },
      { description: " Chunian" },
      { description: " Dajkot" },
      { description: " Daska" },
      { description: " Davispur" },
      { description: " Darya Khan" },
      { description: " Dera Ghazi Khan" },
      { description: " Dhaular" },
      { description: " Dina" },
      { description: " Dinga" },
      { description: " Dhudial Chakwal" },
      { description: " Dipalpur" },
      { description: " Faisalabad" },
      { description: " Fateh Jang" },
      { description: " Ghakhar Mandi" },
      { description: " Gojra" },
      { description: " Gujranwala" },
      { description: " Gujrat" },
      { description: " Gujar Khan" },
      { description: " Harappa" },
      { description: " Hafizabad" },
      { description: " Haroonabad" },
      { description: " Hasilpur" },
      { description: " Haveli Lakha" },
      { description: " Jalalpur Jattan" },
      { description: " Jampur" },
      { description: " Jaranwala" },
      { description: " Jhang" },
      { description: " Jhelum" },
      { description: " Kallar Syedan" },
      { description: " Kalabagh" },
      { description: " Karor Lal Esan" },
      { description: " Kasur" },
      { description: " Kamalia" },
      { description: " Kāmoke" },
      { description: " Khanewal" },
      { description: " Khanpur" },
      { description: " Khanqah Sharif" },
      { description: " Kharian" },
      { description: " Khushab" },
      { description: " Kot Adu" },
      { description: " Jauharabad" },
      { description: " Lahore" },
      { description: " Islamabad" },
      { description: " Lalamusa" },
      { description: " Layyah" },
      { description: " Lawa Chakwal" },
      { description: " Liaquat Pur" },
      { description: " Lodhran" },
      { description: " Malakwal" },
      { description: " Mamoori" },
      { description: " Mailsi" },
      { description: " Mandi Bahauddin" },
      { description: " Mian Channu" },
      { description: " Mianwali" },
      { description: " Miani" },
      { description: " Multan" },
      { description: " Murree" },
      { description: " Muridke" },
      { description: " Mianwali Bangla" },
      { description: " Muzaffargarh" },
      { description: " Narowal" },
      { description: " Nankana Sahib" },
      { description: " Okara" },
      { description: " Renala Khurd" },
      { description: " Pakpattan" },
      { description: " Pattoki" },
      { description: " Pindi Bhattian" },
      { description: " Pind Dadan Khan" },
      { description: " Pir Mahal" },
      { description: " Qaimpur" },
      { description: " Qila Didar Singh" },
      { description: " Rabwah" },
      { description: " Raiwind" },
      { description: " Rajanpur" },
      { description: " Rahim Yar Khan" },
      { description: " Rawalpindi" },
      { description: " Sadiqabad" },
      { description: " Sagri" },
      { description: " Sahiwal" },
      { description: " Sambrial" },
      { description: " Samundri" },
      { description: " Sangla Hill" },
      { description: " Sarai Alamgir" },
      { description: " Sargodha" },
      { description: " Shakargarh" },
      { description: " Sheikhupura" },
      { description: " Shujaabad" },
      { description: " Sialkot" },
      { description: " Sohawa" },
      { description: " Soianwala" },
      { description: " Siranwali" },
      { description: " Tandlianwala" },
      { description: " Talagang" },
      { description: " Taxila" },
      { description: " Toba Tek Singh" },
      { description: " Vehari" },
      { description: " Wah Cantonment" },
      { description: " Wazirabad" },
      { description: " Yazman" },
      { description: " Zafarwal" }
    ]
  }
  jsonData = {

    fruits: [
      { description: "lahore" },
      { description: "Banana" },
      { description: "Tangerine" }
    ]
  }

  public ngModelGender = ''
  public toppings = ''
  public params: PLACE_ORDER_PARAM
  public provinces = ['Punjab', 'KPK', 'Balochistan', 'Sindh', 'FATA']
  public ShippingAddressArray: SHIPPING_ADDRESS_DATA[] = []

  public selectedIndex = 0;
  public isProvinceTapped = false
  public ShipAddress: string
  public IsNewShippingAddress = false
  public markthisAsDefaultAddress = false
  public myCustomer: Customer
  public isValid = true
  public caseAddress = 0
  public shouldShowMoreOption = true;
  public selectMoreOption = ''

  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };

  public CartItems: PRODUCT[] = []

  public customerName = ''
  public customerPhone = ''
  public shippingAddress = ''
  public city = ''

  public province = this.provinces[0]
  public isCredentialsCorrect = true
  public selectedAdress: SHIPPING_ADDRESS_DATA
  public selectedAdressToEdit: SHIPPING_ADDRESS_DATA
  public selectedPreviousAddress: SHIPPING_ADDRESS_DATA
  public isDatabaseReady = false


  constructor(
    private menu: MenuController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public shippingAddressModal: ShippingAddressModal,
    public serviceManager: GlobalProvider,
    private nativePageTransitions: NativePageTransitions,
    public customerModal: CustomerModal,
    public alertCtrl: AlertController,
    public platform: Platform,
    // private datePicker: DatePicker,
    private selector: WheelSelector,
  ) {

    this.selectedAdressToEdit = this.navParams.get('isRequestforNewOrEditAddress')
    console.log(this.selectedAdressToEdit);

    this.IsNewShippingAddress = true
    if (this.selectedAdressToEdit) {
      this.customerName = this.selectedAdressToEdit.o_contact_person
      this.customerPhone = this.selectedAdressToEdit.o_phone
      this.selectedAdress = this.selectedAdressToEdit
      this.ngModelGender = this.selectedAdress.o_province
      if (!this.selectedAdress.o_province || this.selectedAdress.o_province.trim().length === 0) this.ngModelGender = 'Punjab'


      this.city = this.selectedAdress.o_city
      this.shippingAddress = this.selectedAdress.o_completeAddress
      this.markthisAsDefaultAddress = this.selectedAdress.isdefaultaddress

      this.caseAddress = 1
    } else {
      this.caseAddress = 2
      this.ngModelGender = 'Punjab'
      // alert('should be new address')
    }
    // this.isRequestforEditAddress? this.selectedAdress = this.isRequestforEditAddress : this.selectedAdress = this.isRequestforNewAddress
    // this.shippingAddress = this.selectedAdress.o_completeAddress

    // this.ngModelGender = 'Punjab'

    this.shippingAddressModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.shippingAddressModal.getShippingAddress().then(sa => {
          if (sa) this.ShippingAddressArray = sa
          // alert(this.ShippingAddressArray.length)

          // if (this.ShippingAddressArray && this.ShippingAddressArray.length > 0) this.selectedAdress = this.ShippingAddressArray[0]
        })
      }
    })

    // this.ShipAddress = 'ShipAddres'
    this.CartItems = this.navParams.get('CartItems')
    this.params = {

    }

  }
  selectANumber() {
    this.selector.show({
      title: "Select city",
      positiveButtonText: "Ok",
      negativeButtonText: "Cancel",
      items: [
        this.pakistanCities.cities
      ],
    }).then(
      result => {
        console.log(result[0].description + ' at index: ' + result[0].index);
      },
      err => console.log('Error: ', err)
    );
  }

  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
    //  this.gsp.tabIndex = "1";
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    if (this.menu.isOpen()) {
      this.menu.close();
    } else {

      if (this.navCtrl.canGoBack()) {

        this.navCtrl.pop();
      } else {

        this.navCtrl.push(MainHomePage, { isHideTab: "1" }, { animate: false })
      }
    }
  }


  ionViewWillEnter() {
    // this.selectANumber()
    this.getCustomer()
    // this.shippingAddressModal.getMaxID().then(res => {
    //   alert(res.maxID)
    // })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ShippingInfoPage');
  }
  reviewOrder() {
    if (this.customerName.trim().length === 0) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgMissingCustomerName)
      return
    }
    if (this.shippingAddress.trim().length === 0) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgMissingShippingAddress)
      return
    }
    if (this.city.trim().length === 0) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgMissingCityName)
      return
    }
    if (this.province.trim().length === 0) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgMissingProvinceName)
      return
    }

    this.params.service = btoa('place_order'),
      this.params.o_address = btoa(this.shippingAddress),
      this.params.o_city = btoa(this.city),
      this.params.o_province = btoa(this.province),

      this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(ReviewOrderPage, {
      CartItems: this.CartItems,
      params: this.params
    })
  }

  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };


    if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

    if (this.navCtrl.canGoBack()) {

      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
  }
  shouldChangeCharacter(value) {
    this.isProvinceTapped = !this.isProvinceTapped
    return
  }
  provinceSelected(i) {

    this.province = this.provinces[i]
    this.selectedIndex = i
    this.isProvinceTapped = !this.isProvinceTapped
  }

  getSelectedProvince() {
    this.provinces.forEach(province => {
      if (province === this.province) console.log('');

    });
    return this.provinces[this.selectedIndex]
  }
  shippingAddressSelected(shipAddres: SHIPPING_ADDRESS_DATA, index) {
    this.caseAddress = 0
    this.selectedPreviousAddress = shipAddres
    this.btnSaveAddressapped()
    /** 
     if (this.selectedPreviousAddress && this.selectedPreviousAddress.o_id === shipAddres.o_id) return
    this.selectedPreviousAddress = shipAddres

    this.ShippingAddressArray.map((sa) => {

      if (shipAddres == sa) {
        sa.checked = !sa.checked
        sa.isdefaultaddress = !sa.isdefaultaddress;
      } else {
        sa.isdefaultaddress = false;
        sa.checked = false
      }
      return sa;

    });

    */

  }

  btnAddNewAddressTapped() {
    this.IsNewShippingAddress = true
    this.selectedPreviousAddress = null
    this.selectedAdress = null
    this.selectedAdressToEdit = null
    this.caseAddress = 2
    this.IsNewShippingAddress = true;

    this.customerName = ''
    this.customerPhone = ''
    this.city = ''
    this.ngModelGender = 'Punjab'
    this.shippingAddress = ''
  }

  btnAddPreviousAddressTapped() {

    this.IsNewShippingAddress = false
    this.caseAddress = 0

  }
  makeAddressDefault() {
    this.markthisAsDefaultAddress = !this.markthisAsDefaultAddress
  }
  OnValueEnter(value) {
    var lastChar = value[value.length - 1];
    let elementChecker: string;
    elementChecker = value;

    var reg = new RegExp('^[0-9]+$');
    if (reg.test(lastChar)) {
      if (this.customerPhone.length == 1) {
        if (this.customerPhone != '0') {
          this.customerPhone = '';
          this.serviceManager.makeToastOnFailure(AppMessages.msgMobileNumberStartingDigits)
        }
      } else if (this.customerPhone.length == 2) {
        if (this.customerPhone != '03') {
          this.customerPhone = '';

          this.serviceManager.makeToastOnFailure(AppMessages.msgMobileNumberStartingDigits)
        }
      } else if (this.customerPhone.length == 0) {
        this.serviceManager.makeToastOnFailure(AppMessages.msgElevenDigitRequired)

      }

    } else {
      this.customerPhone = elementChecker.slice(0, -1);
    }
    // this.customerPhone='';
  }

  btnSaveAddressapped() {

    switch (this.caseAddress) {
      case 0:// for using previous address without any change
        this.selectedAdress = this.selectedPreviousAddress
        this.goBackToReviewOrder()
        break;
      case 1: // for updating shipping address

        if (this.validateAddressInformation()) {
          this.selectedAdress.isdefaultaddress = this.markthisAsDefaultAddress
          this.selectedAdress.o_contact_person = this.customerName
          this.selectedAdress.o_phone = this.customerPhone

          this.selectedAdress.o_city = this.city
          this.selectedAdress.o_province = this.ngModelGender
          this.selectedAdress.o_address = this.shippingAddress

          this.selectedAdress.o_completeAddress = this.shippingAddress
          this.shippingAddressModal.updateShippingAddress(this.selectedAdress).then(() => {
            this.goBackToReviewOrder()
          })

        }
        break;
      case 2: // simple insert a new address
        if (this.validateAddressInformation()) {

          let newAddressArray: SHIPPING_ADDRESS_DATA[] = []
          let newAddress: SHIPPING_ADDRESS_DATA = {

          }
          newAddress.o_contact_person = this.customerName
          newAddress.o_phone = this.customerPhone
          newAddress.isdefaultaddress = this.markthisAsDefaultAddress
          newAddress.o_city = this.city
          newAddress.o_province = this.province
          newAddress.o_address = this.shippingAddress
          newAddress.o_completeAddress = this.shippingAddress // this.customerName + ',\n' + this.shippingAddress + this.city + ', ' + this.province + ',\n #' + this.customerPhone

          this.selectedAdress = {

          }
          this.selectedAdress = newAddress
          newAddressArray.push(newAddress)
          this.shippingAddressModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.shippingAddressModal.InsertInToShippingAddressTable(newAddressArray).then(() => {
                this.goBackToReviewOrder()
              })
            }
          })
        }
        break;

      default:
        break;
    }


  }
  goBackToReviewOrder() {
    if (this.selectedAdress && this.selectedAdress.isdefaultaddress) {
      this.customerModal.getDatabaseState().subscribe(isReady => {
        if (isReady) {
          this.customerModal.TruncCateCustomerTable().then(res => {
            this.saveCustomerIntoDB()
          })
        }
      })
    }
    this.markthisAsDefaultAddress && this.makeAddressAsDefault()
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };
    this.navCtrl.getPrevious().data.updatedAddress = this.selectedAdress

    if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
    this.navCtrl.pop({
      animate: false,
      animation: 'ios-transition',
      direction: 'back',
      duration: 500,
    }).then(() => {

    })
  }
  saveCustomerIntoDB() {

    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.createCustomerTable().then(isTableCreated => {
          if (isTableCreated === true) {
            this.customerModal.InsertInToCustomerTable(this.myCustomer).then(isCustomerInserted => {

            }, error => {
              // this.serviceManager.makeToastOnFailure('error: ' + error.message, 'top')
              console.log('error: ', error.message);
            })
          } else {
            // this.serviceManager.makeToastOnFailure('Error occoured while creating Customer Table', 'top')
          }

        }, error => {
          // this.serviceManager.makeToastOnFailure('error: ' + error.message, 'top')
          console.log('error: ', error.message);
        })
      }
    })


  }

  validateAddressInformation() {
    var reg = new RegExp('^[0-9]+$');
    if (this.customerName.trim().length === 0) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgMissingCustomerName)
      return false
    }
    if (this.customerPhone.trim().length === 0) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgMobileNumberStartingDigits)
      return false
    }
    if (this.customerPhone.trim().length < 11) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgMobileNumberStartingDigits)
      return false
    }
    if (!reg.test(this.customerPhone.trim())) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgVerifyCustomerMobileNumber)
      return false
    }
    if (this.city.trim().length === 0) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgMissingCityName)
      return false
    }
    if (this.shippingAddress.trim().length === 0) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgMissingShippingAddress)
      return false
    }
    return true
  }
  makeAddressAsDefault() {

    if (this.isDatabaseReady && this.selectedAdress && this.selectedAdress.o_id) {
      this.shippingAddressModal.makeAddressAsDefault(this.selectedAdress)
    }
  }

  btnMoreOptionTapped(event, shipAddres: SHIPPING_ADDRESS_DATA) {
    if (event === 'Delete') {
      let alert = this.alertCtrl.create({
        title: 'Warning',
        message: 'Surely want to remove this address?',
        buttons: [
          {
            text: 'NO',
            role: 'cancel',
            handler: () => {
            }
          },
          {
            text: 'YES',
            handler: () => {
              this.shippingAddressModal.getDatabaseState().subscribe(ready => {
                if (ready) {
                  this.shippingAddressModal.deleteAddress(shipAddres.o_id).then(() => {
                    this.shippingAddressModal.getShippingAddress().then(sa => {
                      if (sa) this.ShippingAddressArray = sa

                    })
                  })

                }
              })
            }
          }
        ]
      });
      alert.present();


    } else if (event === 'Edit') {
      this.caseAddress = 1
      this.IsNewShippingAddress = true;
      this.selectedAdress = shipAddres
      this.customerName = shipAddres.o_contact_person
      this.customerPhone = shipAddres.o_phone
      this.city = shipAddres.o_city
      this.ngModelGender = shipAddres.o_province
      this.shippingAddress = shipAddres.o_address
      // alert('updated address')
    }


  }
  getCustomer() {

    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(customer => {
          if (customer) {
            this.myCustomer = customer
            if (!this.selectedAdress) {
              this.customerName = this.myCustomer.cust_name
              this.customerPhone = this.myCustomer.cust_phone
            }
          }
        })
      }
    })
  }
}



    // if (!this.selectedAdress || !this.selectedAdress.o_id) {

    //   let newAddressArray: SHIPPING_ADDRESS_DATA[] = []
    //   let newAddress: SHIPPING_ADDRESS_DATA = {

    //   }

    //   newAddress.isdefaultaddress = this.markthisAsDefaultAddress
    //   newAddress.o_city = this.city
    //   newAddress.o_province = this.province
    //   newAddress.o_address = this.shippingAddress
    //   newAddress.o_completeAddress = this.shippingAddress // this.customerName + ',\n' + this.shippingAddress + this.city + ', ' + this.province + ',\n #' + this.customerPhone

    //   this.selectedAdress = {

    //   }
    //   this.selectedAdress = newAddress
    //   newAddressArray.push(newAddress)
    //   this.shippingAddressModal.getDatabaseState().subscribe(ready => {
    //     if (ready) {
    //       this.shippingAddressModal.InsertInToShippingAddressTable(newAddressArray)
    //     }
    //   })
    // } else
    // {

/*
    this.selectedAdress.isdefaultaddress = this.markthisAsDefaultAddress
    this.selectedAdress.o_city = this.city
    this.selectedAdress.o_province = this.ngModelGender
    this.selectedAdress.o_address = this.shippingAddress
    this.selectedAdress.o_completeAddress = this.shippingAddress //this.customerName + ',\n' + this.shippingAddress + ',\n #' + this.customerPhone
    */

    // this.shippingAddressModal.updateShippingAddress(this.selectedAdress)


    // this.selectedAdress.o_completeAddress =
    // this.shippingAddressModal.getDatabaseState().subscribe(ready => {
    //   if (ready) {
    //     this.shippingAddressModal.makeAddressAsDefault(this.selectedAdress).then(() => {
    //       alert('done')
    //     })
    //   }
    // })

