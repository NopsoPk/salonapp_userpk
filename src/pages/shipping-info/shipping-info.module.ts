import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShippingInfoPage } from './shipping-info';

@NgModule({
  declarations: [
    ShippingInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(ShippingInfoPage),
  ],
})
export class ShippingInfoPageModule {}
